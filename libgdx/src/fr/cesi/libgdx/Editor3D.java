package fr.cesi.libgdx;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.LocalFileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import fr.cesi.libgdx.entities.models.Carpentry;
import fr.cesi.libgdx.entities.models.Divider;
import fr.cesi.libgdx.entities.models.MaderaModel;
import fr.cesi.libgdx.entities.models.Slab;
import fr.cesi.libgdx.entities.models.TouchBox;
import fr.cesi.libgdx.entities.models.Wall;
import fr.cesi.libgdx.entities.models.WallTBox;
import fr.cesi.libgdx.misc.exceptions.IncoherentCollisionDetectionException;
import fr.cesi.libgdx.misc.exceptions.SelectedModelException;

/**
 * Gestion de l'affichage des objet 3D dans l'éditeur
 */
public class Editor3D implements ApplicationListener {

    /** Taille d'une dalle de sol (largeur == longueur) */
    public final static float FLOOR_SIZE = 1f;
    /** Epaisseur d'une dalle de sol */
    public final static float FLOOR_THIN = 0.01f;
    /** Taille des murs */
    public final static float WALL_HEIGHT = 2.5f;
    /** Diamètre des bouton de selection d'un slot de mur */
    public final static float WALL_TBOX_WIDTH = 0.5f;
    /** profondeur des boutons de selection d'un slot de mur */
    public final static float WALL_TBOX_HEIGHT = 0.05f;
    /** Nombre de division du cylindre constituant le bouton de selection d'un slot de mur*/
    public final static int WALL_TBOX_DIVISION_NB = 30;
    /** Epaisseur du mur */
    public final static float WALL_THIN = 0.01f;
    /** Hauteur des selecteur d'emplacement de cloison */
    public final static float FLOOR_TBOX_HEIGHT = 1.80f;
    /** Marge en mêtre entre les carpentry lors du placement sur le mur */
    public final float CARPENTRY_MARGIN = 0.2f;
    /** Type du composant utilisé pour filtrer la liste des modèles */
    public static final int FLOOR_TOUCHBOX = 0;
    public static final int WALL = 1;
    public static final int WALL_TOUCHBOX = 3;
    /** Mode visualisation */
    public static boolean VISU_MODE = false;
    /** Camera de la vue 3D*/
    private PerspectiveCamera camera;
    /** Controller de la caméra permettant à l'utilisateur de la faire pivoter */
    public CameraInputController camController;
    /** Chargeur de modèle 3D*/
    private ModelBatch modelBatch;
    /** Chargeur des */
    public AssetManager assets;
    /** Classe contenant les méthodes de manipulation des modèles 3D*/
    public fr.cesi.libgdx.utils.Editor3DService e3DService;
    /** Liste des dalles constituantes du sol*/
    public Array<Slab> floorParts = new Array<Slab>();
    /** Liste des murs extérieurs */
    public Array<Wall> walls = new Array<fr.cesi.libgdx.entities.models.Wall>();
    /** Liste des éléments constituant les cloisons*/
    public Array<Divider> dividers = new Array<fr.cesi.libgdx.entities.models.Divider>();
    /** Liste des éléments de menuiseries*/
    public Array<Carpentry> carpentries = new Array<fr.cesi.libgdx.entities.models.Carpentry>();
    /** Liste des slots de selection (murs et cloisons) */
    public Array<TouchBox> floorTouchBoxes = new Array<fr.cesi.libgdx.entities.models.TouchBox>();
    /** Liste des slot sur les murs*/
    public Array<WallTBox> wallTouchBoxes = new Array<fr.cesi.libgdx.entities.models.WallTBox>();
    /** Environnement 3D pour l'affichage des modèles */
    private Environment environment;
    /** variable permettant d'activer ou de bloquer le rendu des objets 3D */
    private boolean loading;
    /** Ecouteur d'évènement au clic sur la fenêtre */
    private InputAdapter androidInputAdapter;
    private GestureDetector.GestureListener androidListener;

    public Material wall_in_mat;
    public Material wall_out_mat;
    public Material floor_mat;

    public Editor3D() {
        assets = new AssetManager(new LocalFileHandleResolver());
        e3DService = new fr.cesi.libgdx.utils.Editor3DService(this,assets);
    }

    @Override
    public void create() {
        modelBatch = new ModelBatch();
        initEnvironment();
        initCamera();
        InputMultiplexer multiplexer = new InputMultiplexer(new GestureDetector(androidListener), camController);
        multiplexer.addProcessor(androidInputAdapter);
        Gdx.input.setInputProcessor(multiplexer);
        loading = false;
    }

    @Override
    public void render () {
        if (assets.update()){
            camController.update();
            Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
            modelBatch.begin(camera);
            if(!VISU_MODE){
                modelBatch.render(wallTouchBoxes, environment);
                modelBatch.render(floorTouchBoxes, environment);
            }
            modelBatch.render(floorParts, environment);
            modelBatch.render(walls, environment);
            modelBatch.render(dividers, environment);
            modelBatch.render(carpentries, environment);
            modelBatch.end();
        }
    }

    @Override
    public void dispose () {
        modelBatch.dispose();
        floorParts.clear();
        walls.clear();
        carpentries.clear();
        dividers.clear();
        floorTouchBoxes.clear();
        wallTouchBoxes.clear();
    }

    /**
     * Initialisation de la caméra pour la vue 3D
     */
    private void initCamera(){
        camera = new PerspectiveCamera(30, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(7f, 16f, 7f);
        camera.lookAt(0,0,0);
        camera.near = 1f;
        camera.far = 300f;
        camera.update();
        camController = new CameraInputController(camera);
    }

    /**
     * Initialisation de l'environnement 3D
     */
    private void initEnvironment(){
        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
    }

    /**
     * Dessine uen dalle de sol de 1 x 1 dont le center est citué aux coordonnées passées en argument
     * @param posX
     * @param posY
     * @param posZ
     * @param floorCoveringPath
     */
    public void drawFloorPart(float posX, float posY, float posZ, String floorCoveringPath) {
        if(floor_mat == null && floorCoveringPath != null ) {
            floor_mat = loadCovering(floorCoveringPath);
        }
        floorParts.add(e3DService.createFloorPart(posX,posY,posZ));
    }

    /**
     * Déssines les murs extérieurs entourant la maison
     */
    public Array<Wall> drawWallParts(){
        walls = e3DService.createAndDispatchWall(floorParts);
        return walls;
    }

    /**
     * Déssine un élément consituant les cloison en fonction du modèle choisit par l'utilisateur
     * et passé en argument. Le centre de l'élément déssiné coincidera avec le center du selectedModel
     * @param modelpath
     * @return
     */
    public Vector3 drawDivider(String modelpath) {
        Divider divider = e3DService.createDivider(modelpath);
        dividers.add(divider);
        return divider.getCenter();
    }

    /**
     * Déssine un carpentry
     * @param modelPath
     * @param heightPlacement
     * @return le centre sur lequel est placé le carpentry
     */
    public Vector3 drawCarpentry(String modelPath, float heightPlacement) {
       Carpentry carpentry = e3DService.createCarpentry(modelPath,heightPlacement);
        try {
            // Si le modèle sélectionné n'est pas un carpentry et qu'il y a la place
            if(!e3DService.carpentryCollideAnother(carpentry)) {
                carpentries.add(carpentry);
                removeModel(e3DService.selectedModel);
                return carpentry.getCenter();
            }
        } catch (IncoherentCollisionDetectionException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Déssine les bouton de sélection d'un slot sur un mur en fonction de son placement relatif
     * @param placement placement relatif du slot sur le mur
     * (% de la largeur du mur en partant de la gauche quand on regarde le mur de face depuis l'extérieur de la maison.
     * @return le center du bouton et donc du slot
     */
    public Vector3 drawTouchBoxOnWall(Float placement) {
        Vector3 center = e3DService.calculPosFromPlacement(placement);
        WallTBox wTBox = e3DService.drawWallTouchBox(center);
        wTBox.setRotated(getSelectedModel().isRotated());
        wallTouchBoxes.add(wTBox);
        return center;
    }

    public void drawTextureOnModels(String coveringPath, boolean in, boolean out) throws SelectedModelException {
        if(getSelectedModel() == null){
            throw new SelectedModelException("Aucun modèle sélectionné, impossible d'appliquer la texture");
        }
        e3DService.drawTextureOnModels(coveringPath,in,out);
    }

    /**
     * Supprime tous les slots et/ou les carpentries dessinés sur le mur
     */
    public void deleteModelsOnSelectedWall() throws SelectedModelException {
        if(e3DService.selectedModel == null){
            throw new SelectedModelException("Impossible de supprimer tous les slot et les carpentries sur le mur" +
                    "car aucun mur n'est selectionné");
        }
        e3DService.deleteModelsOnSelectedWall();
    }

    /**
     * Supprime le divider et créé un FloorTBox à l'emplacement  divider supprimé
     * @param modeli
     */
    public void removeDivider(MaderaModel modeli) {
        dividers.removeValue((Divider) modeli,true);
        Vector3 center = modeli.getCenter();
        center.y = FLOOR_TBOX_HEIGHT /2 + 0.05f;
        floorTouchBoxes.add(e3DService.placeFloorTBoxOnWorld(center,modeli.isRotated()));
    }

    /**
     * Supprime le carpentry et créé un WallTBox à l'emplacement center du carpentry
     * @param modeli
     */
    public void removeCarpentry(MaderaModel modeli) {
        carpentries.removeValue((Carpentry) modeli,true);
        Vector3 center = modeli.getCenter();
        center.y = WALL_HEIGHT /2;
        wallTouchBoxes.add(e3DService.placeWallTBoxOnWorld(center,modeli.isRotated()));
    }

    /**
     * Charge le modèle d'un composant à partir du chemin d'accès au fichier de définition du modèle
     * @param modelPath chemin vers le fichier de définition du modèle
     */
    public void loadComponentModel(String modelPath) {
        Gdx.app.log("madera","load models ! " + modelPath);

        assets.load(modelPath,Model.class);
    }


    public Material loadCovering(String coveringPath){
        if (coveringPath != null) {
            FileHandle handle = new FileHandle("/data/data/fr.cesi.madera/files" +"/" +coveringPath);
            Texture texture = new Texture(handle);
            TextureAttribute textAttr = new TextureAttribute(TextureAttribute.Diffuse, texture);
            Material material = new Material();
            material.set(textAttr);
            return material;
        }
        return null;
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    /**
     *
     * @return La taille du modèle sélectionné
     */
    public float getLenghtOfSelectedModel() throws SelectedModelException {
        if(e3DService.selectedModel instanceof Wall) {
            return e3DService.getLenghtOfModel(e3DService.selectedModel);
        }else{
            throw new SelectedModelException("Le calcul de la longueur du mur ne peut pas être éffectué car le modèle sélectionné n'est pas un mur !");
        }
    }

    public Class getClassOfSelectedModel() {
        if(getSelectedModel() == null){
           return null;
        }
        return getSelectedModel().getClass();
    }

    public Array getAdjoiningSlotsOrConponents() {
        try {
            return e3DService.getAdjoiningWallTBoxOrCarpentries(getSelectedModel());
        } catch (IncoherentCollisionDetectionException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Sélectionne le modèle situé à la position centerOfModel
     * @param centerOfModel
     */
    public void selectModelAtCoord(Vector3 centerOfModel, int type) throws SelectedModelException {
        int nbMatch = 0;
        Array<MaderaModel> models = new Array<MaderaModel>();
        switch (type){
            case FLOOR_TOUCHBOX:
                models.addAll(floorTouchBoxes);
                break;
            case WALL:
                models.addAll(walls);
                break;
            case WALL_TOUCHBOX:
                models.addAll(wallTouchBoxes);
                break;
        }
        for (MaderaModel model: models) {
            if(model.getBox().contains(centerOfModel)){
                selectModel(model);
                nbMatch++;
            }
        }
        if(nbMatch != 1){
            throw new SelectedModelException("Un ou plusieurs modèle correspondent à cet emplacement nbMatch : " + nbMatch + " type : " + type + " center : " + centerOfModel);
        }
    }

    /**
     * Supprime tous les éléments sur le plan 3D
     */
    public void clearEditor() {
        floorParts = new Array<Slab>();
        walls = new Array<Wall>();
        carpentries = new Array<Carpentry>();
        floorTouchBoxes = new Array<TouchBox>();
        wallTouchBoxes = new Array<WallTBox>();
        dividers = new Array<Divider>();
    }

    /**
     * Renvoi la liste de tous les modèles 3D affichés à l'écran
     * @return La liste des modèles
     */
    public Array<MaderaModel> getAllModels(){
        Array<MaderaModel> allModels = new Array<fr.cesi.libgdx.entities.models.MaderaModel>();
        allModels.addAll(floorParts);
        allModels.addAll(walls);
        allModels.addAll(carpentries);
        allModels.addAll(floorTouchBoxes);
        allModels.addAll(wallTouchBoxes);
        allModels.addAll(dividers);
        return allModels;
    }

    /**
     * Supprime un modèle de la vue 3D quel qu'il soit de la liste des modèles
     * qui le contient en fonction de son type
     * @param selectedModel Modèle à qupprimer de la vue 3D
     */
    public void removeModel(fr.cesi.libgdx.entities.models.MaderaModel selectedModel) {
        if(selectedModel instanceof Slab) {
            floorParts.removeValue((Slab)selectedModel,false);
        }else if(selectedModel instanceof fr.cesi.libgdx.entities.models.ComplexModel) {
            walls.removeValue((fr.cesi.libgdx.entities.models.Wall)selectedModel,false);
        }else if(selectedModel instanceof fr.cesi.libgdx.entities.models.Divider) {
            dividers.removeValue((fr.cesi.libgdx.entities.models.Divider)selectedModel,false);
        }else if(selectedModel instanceof fr.cesi.libgdx.entities.models.Carpentry) {
            carpentries.removeValue((fr.cesi.libgdx.entities.models.Carpentry)selectedModel,false);
        }else if(selectedModel instanceof fr.cesi.libgdx.entities.models.WallTBox) {
            wallTouchBoxes.removeValue((fr.cesi.libgdx.entities.models.WallTBox) selectedModel, false);
        }else if(selectedModel instanceof fr.cesi.libgdx.entities.models.FloorTBox) {
            floorTouchBoxes.removeValue((fr.cesi.libgdx.entities.models.TouchBox) selectedModel, false);
        }
    }

    /**
     * Active la selection pour un modèle touché par l'utilisateur
     * @param modeli
     */
    public void selectModel(MaderaModel modeli) {
        // Si c'est une WallTouchBox qui est sélectionné on laisse le mur en mode sélection
        if(!(modeli instanceof WallTBox)) {
            e3DService.setModelInactive();
        }
        e3DService.setModelActive(modeli);
    }

    public void unSelectModel() {
        if(e3DService.getSelectedModel() != null) {
            e3DService.setModelInactive();
        }
    }

    public void setAndroidInputAdapter(InputAdapter androidInputAdapter) {
        this.androidInputAdapter = androidInputAdapter;
    }

    public void setAndroidListener(GestureDetector.GestureListener androidListener) {
        this.androidListener = androidListener;
    }

    public PerspectiveCamera getCamera() {
        return camera;
    }

    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public MaderaModel getSelectedModel() {
        return e3DService.selectedModel;
    }


    public void exit() {
        Gdx.app.exit();
    }
}
