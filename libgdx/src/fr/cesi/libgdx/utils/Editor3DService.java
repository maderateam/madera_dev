package fr.cesi.libgdx.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;
import java.util.Comparator;

import fr.cesi.libgdx.Editor3D;
import fr.cesi.libgdx.entities.models.Carpentry;
import fr.cesi.libgdx.entities.models.Divider;
import fr.cesi.libgdx.entities.models.FloorTBox;
import fr.cesi.libgdx.entities.models.MaderaModel;
import fr.cesi.libgdx.entities.models.SimpleModel;
import fr.cesi.libgdx.entities.models.Slab;
import fr.cesi.libgdx.entities.models.Wall;
import fr.cesi.libgdx.entities.models.WallTBox;
import fr.cesi.libgdx.entities.vectors.WallPartCenter;
import fr.cesi.libgdx.misc.exceptions.IncoherentCollisionDetectionException;

/**
 * Classe permettant le manipulation des modèles 3D sur le plan
 */
public class Editor3DService {
    public final static int X_AXIS = 0;
    public final static int Y_AXIS = 1;
    public final static int Z_AXIS = 2;
    public static final int X_PLUS = 0;
    public static final int X_LESS = 1;
    public static final int Z_PLUS = 2;
    public static final int Z_LESS = 3;
    public MaderaModel selectedModel;
    public Material GRAY_BASIC;
    public Material ROYAL_BASIC;
    public Material ROYAL_ALPHA05;
    public Material WHITE_ALPHA05;
    public Material WHITE_ALPHA03;
    private Editor3D editor;
    private ModelBuilder modelBuilder;
    private AssetManager assets;

    public Editor3DService(Editor3D editor, AssetManager assets) {
        this.editor = editor;
        this.modelBuilder = new ModelBuilder();
        this.assets = assets;
        GRAY_BASIC = new Material(ColorAttribute.createDiffuse(Color.GRAY));
        ROYAL_BASIC = new Material(ColorAttribute.createDiffuse(Color.ROYAL));
        WHITE_ALPHA05 = new Material(ColorAttribute.createDiffuse(Color.WHITE));
        WHITE_ALPHA05.set(new BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA, 0.5f));
        WHITE_ALPHA03 = new Material(ColorAttribute.createDiffuse(Color.WHITE));
        WHITE_ALPHA03.set(new BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA, 0.3f));
        ROYAL_ALPHA05 = new Material(ColorAttribute.createDiffuse(Color.ROYAL));
        ROYAL_ALPHA05.set(new BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA, 0.5f));
    }

    /**
     * Créé une dalle de sol en fonction des coordonnées en entré
     * @param posX
     * @param posY
     * @param posZ
     * @return
     */
    public Slab createFloorPart(float posX, float posY, float posZ) {
        Material back = editor.floor_mat != null ? editor.floor_mat : GRAY_BASIC;
        Model floor_m = createFloorWithBuilder(editor.FLOOR_SIZE, editor.FLOOR_THIN, back);
        Slab floorPart = new Slab(floor_m);
        floorPart.setFrontMat(back);
        floorPart.transform.rotate(Vector3.X, 90);
        floorPart.transform.trn(new Vector3(posX * editor.FLOOR_SIZE, posY * editor.FLOOR_SIZE, posZ * editor.FLOOR_SIZE));
        BoundingBox box = new BoundingBox();
        floorPart.calculateBoundingBox(box);
        box.mul(floorPart.transform);
        floorPart.setBox(box);
        floorPart.setCenter(box.getCenter(new Vector3()));
        floorPart.setOriginalMat(GRAY_BASIC);
        return floorPart;
    }

    /**
     * Fonction de création du modèle 3D pour la dalle de sol carré
     * @param size taille d'un coté
     * @param thin épaisseur sur l'axe Y
     * @param mat texture à appliquer
     * @return le modèle de la dalle de sol
     */
    public Model createFloorWithBuilder(float size, float thin, Material mat) {
        int attr = VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal | VertexAttributes.Usage.TextureCoordinates;
        modelBuilder.begin();
        modelBuilder.node().id = "main";
        modelBuilder.part("front", GL20.GL_TRIANGLES, attr, mat)
                .rect(-0.5f * size, -size / 2, -thin / 2, -0.5f * size, size / 2, -thin / 2, 0.5f * size, size / 2, -thin / 2, 0.5f * size, -size / 2, -thin / 2, 0, 0, -1);
        modelBuilder.part("back", GL20.GL_TRIANGLES, attr, mat)
                .rect(-0.5f * size, -size / 2, thin / 2, 0.5f * size, -size / 2, thin / 2, 0.5f * size, size / 2, thin / 2, -0.5f * size, size / 2, thin / 2, 0, 0, 1);
        modelBuilder.part("bottom", GL20.GL_TRIANGLES, attr, mat)
                .rect(0.5f * size, -size / 2, -thin / 2, 0.5f * size, -size / 2, thin / 2, -0.5f * size, -size / 2, thin / 2, -0.5f * size, -size / 2, -thin / 2, 0, -1, 0);
        modelBuilder.part("top", GL20.GL_TRIANGLES, attr, mat)
                .rect(-0.5f * size, size / 2, -thin / 2, -0.5f * size, size / 2, thin / 2, 0.5f * size, size / 2, thin / 2, 0.5f * size, size / 2, -thin / 2, 0, 1, 0);
        modelBuilder.part("left", GL20.GL_TRIANGLES, attr, mat)
                .rect(-0.5f * size, -size / 2, thin / 2, -0.5f * size, size / 2, thin / 2, -0.5f * size, size / 2, -thin / 2, -0.5f * size, -size / 2, -thin / 2, -1, 0, 0);
        modelBuilder.part("right", GL20.GL_TRIANGLES, attr, mat)
                .rect(0.5f * size, -size / 2, -thin / 2, 0.5f * size, size / 2, -thin / 2, 0.5f * size, size / 2, thin / 2, 0.5f * size, -size / 2, thin / 2, 1, 0, 0);
        Model box = modelBuilder.end();
        return box;
    }

    /**
     * Méthode de création des murs à partir des dalles de sols
     * @param floorParts ensemble des dalles composant le sol
     */
    public Array<Wall> createAndDispatchWall(Array<Slab> floorParts) {
        ArrayList<WallPartCenter> wallCenters = new ArrayList<WallPartCenter>();
        ArrayList<Vector3> centers = new ArrayList<Vector3>();
        // On sotcke tous les centres de plques dans une liste
        for (MaderaModel model : floorParts) {
            centers.add(model.transform.getTranslation(new Vector3()));
        }
        // On les parcours et on créé 4 autres point dans les 4 direction sur X et Z
        // Vérifie ensuite si une de ces point coincide avec un centre
        // Si Aucun point n'est trouvé on place un mur extérieur entre le centre parcouru dans
        // la liste en le point créé sur l'axe
        // Si un centre coincide avec un des point créés sur l'axe on y place un emplacement de cloison
        for (int i = 0; i < centers.size(); i++) {
            /**
             * Deplacement de la moitié de la largeur de la dalle sur l'axe des X et des Z,
             *  pour detecter une éventuelle plaque voisine.
             * - Si pas de plaque alors on place un mur
             * - Si il y a une plque on cré un emplacement de cloison
             */
            // On créé des points que l'on déplace sur X, et Z
            Vector3 XPlus = new Vector3(centers.get(i).cpy()).add(editor.FLOOR_SIZE, 0, 0);
            Vector3 XLess = new Vector3(centers.get(i).cpy()).add(-editor.FLOOR_SIZE, 0, 0);
            Vector3 ZPlus = new Vector3(centers.get(i).cpy()).add(0, 0, editor.FLOOR_SIZE);
            Vector3 ZLess = new Vector3(centers.get(i).cpy()).add(0, 0, -editor.FLOOR_SIZE);

            if (!centers.contains(XPlus)) {
                // add 0.005 on 0.500 to put wall in floor
                // On créé un mur
                WallPartCenter center = new WallPartCenter(XPlus.cpy().add(-editor.FLOOR_SIZE / 2, editor.WALL_HEIGHT / 2, 0f));
                center.setRotated(true);
                wallCenters.add(center);
            } else {
                // On crée un emplcement de cloison
                createFloorTBoxFromFloor(centers.get(i), X_PLUS);
            }
            if (!centers.contains(XLess)) {
                WallPartCenter center = new WallPartCenter(XLess.cpy().add(editor.FLOOR_SIZE / 2, editor.WALL_HEIGHT / 2, 0f));
                center.setRotated(true);
                wallCenters.add(center);
            } else {
                createFloorTBoxFromFloor(centers.get(i), X_LESS);
            }
            if (!centers.contains(ZPlus)) {
                WallPartCenter center = new WallPartCenter(ZPlus.cpy().add(0f, editor.WALL_HEIGHT / 2, -editor.FLOOR_SIZE / 2));
                wallCenters.add(center);
            } else {
                createFloorTBoxFromFloor(centers.get(i), Z_PLUS);
            }
            if (!centers.contains(ZLess)) {
                WallPartCenter center = new WallPartCenter(ZLess.cpy().add(0f, editor.WALL_HEIGHT / 2, editor.FLOOR_SIZE / 2));
                wallCenters.add(center);
            } else {
                createFloorTBoxFromFloor(centers.get(i), Z_LESS);
            }
        }
        return createWallFromCenters(wallCenters);
    }

    /**
     * Création du modèle translucide permettant de créer une cloison à parir du centre d'une dalle de sol
     * @param centerOfFloor centre de la dalle de sol
     * @param axis axe le long du quel on place la cloison
     */
    private void createFloorTBoxFromFloor(Vector3 centerOfFloor, int axis) {
        Vector3 posOfTBox = centerOfFloor.cpy();
        switch (axis) {
            case X_PLUS:
                posOfTBox.add(editor.FLOOR_SIZE / 2, .95f, 0);
                break;
            case X_LESS:
                posOfTBox.add(-editor.FLOOR_SIZE / 2, .95f, 0);
                break;
            case Z_PLUS:
                posOfTBox.add(0f, .95f, editor.FLOOR_SIZE / 2);
                break;
            case Z_LESS:
                posOfTBox.add(0f, .95f, -editor.FLOOR_SIZE / 2);
                break;
        }
        if (!floorTBoxAllReadyExist(posOfTBox)) {

            if (axis == X_PLUS || axis == X_LESS) {
                editor.floorTouchBoxes.add(placeFloorTBoxOnWorld(posOfTBox, true));
            } else {
                editor.floorTouchBoxes.add(placeFloorTBoxOnWorld(posOfTBox, false));
            }
        }
    }

    /**
     * Vérifie si une touchBox est déjà placée à cette coordonnées
     * @param posOfTBox position ou l'on doit chercher la FloorTBox
     * @return true si vrai false sinon
     */
    private boolean floorTBoxAllReadyExist(Vector3 posOfTBox) {
        for (MaderaModel fTBox : editor.floorTouchBoxes) {
            if (fTBox.getCenter().x == posOfTBox.x
                    && fTBox.getCenter().y == posOfTBox.y
                    && fTBox.getCenter().z == posOfTBox.z) {
                return true;
            }
        }
        return false;
    }

    /**
     * Création d'un mur à partir des centres des différentes portion de mur
     * adjascentes au dalles de sol collées au mur à créer.
     * @param points centre des portions du mur à créer
     * @return une liste de mur
     */
    private Array<Wall> createWallFromCenters(ArrayList<WallPartCenter> points) {
        float maxX = getMaxMinByAxis(points, X_AXIS, true);
        float maxZ = getMaxMinByAxis(points, Z_AXIS, true);
        Array<Wall> walls = new Array<Wall>();
        for (float x = -editor.FLOOR_SIZE / 2; x <= maxX; x += editor.FLOOR_SIZE) {
            Array<WallPartCenter> sameX = new Array();
            for (float z = 0f; z <= maxZ; z += editor.FLOOR_SIZE) {
                for (WallPartCenter point : points) {
                    if (point.x == x && point.z == z) {
                        sameX.add(point);
                    }
                }
            }
            // Check if wall touch each others and merge them if they do
            if (sameX.size > 0) {
                walls.addAll(mergeWallPartsCenter(sameX, Z_AXIS));
            }
        }
        for (float z = -editor.FLOOR_SIZE / 2; z <= maxZ; z += editor.FLOOR_SIZE) {
            Array<WallPartCenter> sameZ = new Array();
            for (float x = 0f; x <= maxX; x += editor.FLOOR_SIZE) {
                for (WallPartCenter point : points) {
                    if (point.x == x && point.z == z) {
                        sameZ.add(point);
                    }
                }
            }
            // Check if wall touch each others and merge them if they do
            if (sameZ.size > 0) {
                walls.addAll(mergeWallPartsCenter(sameZ, X_AXIS));
            }
        }
        return walls;
    }

    /**
     * Fusionne différentes portion d'un mur en un seul
     * @param points centre des différentes portions de mur à fusionner
     * @param axis axe sur le long duquel le mur est placé
     * @return une liste de mur
     */
    private Array<Wall> mergeWallPartsCenter(Array<WallPartCenter> points, int axis) {
        Array<Wall> walls = new Array<Wall>();
        points = sortPointByAxis(points, axis);
        ArrayList<Array<WallPartCenter>> groupedPartsByWall = groupPartsByWall(points, axis);
        for (Array<WallPartCenter> wallPoints : groupedPartsByWall) {
            Vector3 center = new Vector3();
            float x = 0;
            float y = 0;
            float z = 0;
            int i;
            boolean rotate = false;
            for (i = 0; i < wallPoints.size; i++) {
                x += wallPoints.get(i).x;
                y += wallPoints.get(i).y;
                z += wallPoints.get(i).z;
                rotate = wallPoints.get(i).isRotated();
            }
            center.set(x / i, y / i, z / i);
            Model wallModel;
            wallModel = createWallWithBuilder(i, editor.WALL_THIN, editor.WALL_HEIGHT, GRAY_BASIC);
            Wall wall = new Wall(wallModel);
            wall.transform.trn(center);
            if (rotate) {
                wall.transform.rotate(Vector3.Y, 90);
                wall.setRotated(true);
            }
            BoundingBox box = new BoundingBox();
            wall.calculateBoundingBox(box);
            box.mul(wall.transform);
            wall.setBox(box);
            wall.setCenter(wall.transform.getTranslation(new Vector3()));
            wall.setOriginalMat(GRAY_BASIC);
            walls.add(wall);
        }
        return walls;
    }

    /**
     * Créé le modèle de mur
     * @param nbPart nombre de protion pour le mur
     * @param thin épaisseur du mur
     * @param height hateur du mur
     * @param defaultMat texture pour ce mur
     * @return le modèle du mur
     */
    private Model createWallWithBuilder(int nbPart, float thin, float height, Material defaultMat) {
        int attr = VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal | VertexAttributes.Usage.TextureCoordinates;
        modelBuilder.begin();
        modelBuilder.node().id = "main";
        modelBuilder.part("front", GL20.GL_TRIANGLES, attr, defaultMat)
                .rect(-0.5f * nbPart, -height / 2, -thin / 2, -0.5f * nbPart, height / 2, -thin / 2, 0.5f * nbPart, height / 2, -thin / 2, 0.5f * nbPart, -height / 2, -thin / 2, 0, 0, -1);
        modelBuilder.part("back", GL20.GL_TRIANGLES, attr, defaultMat)
                .rect(-0.5f * nbPart, -height / 2, thin / 2, 0.5f * nbPart, -height / 2, thin / 2, 0.5f * nbPart, height / 2, thin / 2, -0.5f * nbPart, height / 2, thin / 2, 0, 0, 1);
        modelBuilder.part("bottom", GL20.GL_TRIANGLES, attr, defaultMat)
                .rect(0.5f * nbPart, -height / 2, -thin / 2, 0.5f * nbPart, -height / 2, thin / 2, -0.5f * nbPart, -height / 2, thin / 2, -0.5f * nbPart, -height / 2, -thin / 2, 0, -1, 0);
        modelBuilder.part("top", GL20.GL_TRIANGLES, attr, defaultMat)
                .rect(-0.5f * nbPart, height / 2, -thin / 2, -0.5f * nbPart, height / 2, thin / 2, 0.5f * nbPart, height / 2, thin / 2, 0.5f * nbPart, height / 2, -thin / 2, 0, 1, 0);
        modelBuilder.part("left", GL20.GL_TRIANGLES, attr, defaultMat)
                .rect(-0.5f * nbPart, -height / 2, thin / 2, -0.5f * nbPart, height / 2, thin / 2, -0.5f * nbPart, height / 2, -thin / 2, -0.5f * nbPart, -height / 2, -thin / 2, -1, 0, 0);
        modelBuilder.part("right", GL20.GL_TRIANGLES, attr, defaultMat)
                .rect(0.5f * nbPart, -height / 2, -thin / 2, 0.5f * nbPart, height / 2, -thin / 2, 0.5f * nbPart, height / 2, thin / 2, 0.5f * nbPart, -height / 2, thin / 2, 1, 0, 0);
        Model box = modelBuilder.end();
        return box;
    }

    /**
     * Regroupe les portions de mur par mur
     * @param sortedParts ensemble des centre des portions de mur classé suivant leur position sur l'axe
     * @param axis axe sur lequel le mur est aligné
     * @return
     */
    private ArrayList<Array<WallPartCenter>> groupPartsByWall(Array<WallPartCenter> sortedParts, int axis) {
        ArrayList<Array<WallPartCenter>> groupedParts = new ArrayList<Array<WallPartCenter>>();
        groupedParts.add(new Array<WallPartCenter>());
        groupedParts.get(0).add(sortedParts.get(0));
        if (axis == X_AXIS) {
            float axisValue = sortedParts.get(0).x;
            for (int i = 1; i < sortedParts.size; i++) {
                if (sortedParts.get(i).x - editor.FLOOR_SIZE == axisValue) {
                    groupedParts.get(groupedParts.size() - 1).add(sortedParts.get(i));
                } else {
                    Array<WallPartCenter> newGroup = new Array<WallPartCenter>();
                    newGroup.add(sortedParts.get(i));
                    groupedParts.add(newGroup);
                }
                axisValue++;
            }
        } else if (axis == Z_AXIS) {
            float axisValue = sortedParts.get(0).z;
            for (int i = 1; i < sortedParts.size; i++) {
                if (sortedParts.get(i).z - editor.FLOOR_SIZE == axisValue) {
                    groupedParts.get(groupedParts.size() - 1).add(sortedParts.get(i));
                } else {
                    Array<WallPartCenter> newGroup = new Array<WallPartCenter>();
                    newGroup.add(sortedParts.get(i));
                    groupedParts.add(newGroup);
                }
                axisValue++;
            }
        }
        return groupedParts;
    }

    /**
     * Classe les points sur un axe donné
     * @param pointList liste des point à classer
     * @param axis axe le long du quel on veut classer les point
     * @return la liste des points classé
     */
    private Array<WallPartCenter> sortPointByAxis(Array<WallPartCenter> pointList, int axis) {
        switch (axis) {
            case X_AXIS:
                pointList.sort(new PointComparatorOnX());
                break;
            case Y_AXIS:
                pointList.sort(new PointComparatorOnY());
                break;
            case Z_AXIS:
                pointList.sort(new PointComparatorOnZ());
                break;
        }
        return pointList;
    }

    /**
     * Retourne parmis la liste des point en argument la valeur min ou max sur l'axe en argument
     * @param pointList liste des point dont on veut le min ou max
     * @param axis axe concerné
     * @param max vaut true si on veut le max, false si on veut le min
     * @return la valeur max sur cet axe
     */
    private float getMaxMinByAxis(ArrayList<WallPartCenter> pointList, int axis, boolean max) {
        float result = max ? Float.MIN_VALUE : Float.MAX_VALUE;
        for (Vector3 points : pointList) {
            switch (axis) {
                case X_AXIS:
                    if (max) {
                        result = result < points.x ? points.x : result;
                    } else {
                        result = result > points.x ? points.x : result;
                    }
                    break;
                case Y_AXIS:
                    if (max) {
                        result = result < points.y ? points.y : result;
                    } else {
                        result = result > points.y ? points.y : result;
                    }
                    break;
                case Z_AXIS:
                    if (max) {
                        result = result < points.z ? points.z : result;
                    } else {
                        result = result > points.z ? points.z : result;
                    }
                    break;
            }
        }
        return result;
    }

    /**
     * Récupère le modèle 3D à partir du chemin du fichier correspondant
     * @param modelpath chemin du fichier de modèle
     * @param modelClass classe du modèle qu l'on souhaite obtenir
     * @return le modèle créé
     */
    public SimpleModel getModelIFromPath(String modelpath, Class modelClass) {
        editor.setLoading(true);
        Model model = assets.get(modelpath, Model.class);
        SimpleModel smodel = null;
        if (modelClass.equals(Divider.class)) {
            smodel = new Divider(model);
        } else if (modelClass.equals(Carpentry.class)) {
            smodel = new Carpentry(model);
        }
        editor.setLoading(false);
        return smodel;
    }

    /**
     * Place le modèle dans le monde sur le point en argument
     * @param smodel simpleModel à placer
     * @param center point sur lequel on veut le placer
     */
    public void placeModelOnWorld(SimpleModel smodel, Vector3 center) {
        if (selectedModel.isRotated()) {
            smodel.transform.rotate(Vector3.Y, 90);
            smodel.setRotated(true);
        }
        smodel.transform.trn(center);
        smodel.setCenter(center);
    }

    /**
     * Détermine la position d'un modèle en fonction de la cvaluer du placement en argument
     * @param placement
     * @return le point sur lequel placer le modèle
     */
    public Vector3 calculPosFromPlacement(Float placement) {
        Vector3 center = selectedModel.transform.getTranslation(new Vector3());
        float length = getLenghtOfModel(selectedModel);
        // Détermine le centre du mur pour y = selectedModel.center.y
        if (selectedModel.isRotated()) {
            // Détermine le sens de déplacement du point pour le placement en relatif
            if (getDirectionOfPlacement(selectedModel, X_AXIS)) {
                center.add(0.010f, 0, 0);
                center.z = (center.z + (length / 2)) - length * placement;
            } else {
                center.add(-0.010f, 0, 0);
                center.z = (center.z - (length / 2)) + length * placement;
            }
        } else {
            // Détermine le sens de déplacement du point pour le plcement en relatif
            if (getDirectionOfPlacement(selectedModel, Z_AXIS)) {
                center.add(0, 0, 0.010f);
                center.x = (center.x + (length / 2)) - length * placement;
            } else {
                center.add(0, 0, -0.010f);
                center.x = (center.x - (length / 2)) + length * placement;
            }
        }
        return center;
    }

    /**
     * Renvoi la longuer max d'un modèle sur l'axe X ou Y selon comment il a été tourné
     * @param selectedModel modèle sélectionné
     * @return la lmongueur max
     */
    public float getLenghtOfModel(MaderaModel selectedModel) {
        BoundingBox box = new BoundingBox();
        selectedModel.calculateBoundingBox(box);
        return Math.abs(box.getCorner000(new Vector3()).x - box.getCorner100(new Vector3()).x);
    }

    /**
     * Détermine si le plcement relatif du slot doit de faire dans le sens positif ou négatif sur l'axe
     * @param selectedModel
     * @param axis
     * @return true si c'est dans le sens ositif, flse sinon
     */
    private Boolean getDirectionOfPlacement(MaderaModel selectedModel, int axis) {
        Vector3 centerOfWall = selectedModel.transform.getTranslation(new Vector3());
        Vector3 centerOfFloor = null;
        try {
            centerOfFloor = getCenterOfAdjancentFloor(selectedModel);
            if (axis == X_AXIS) {
                return centerOfWall.x > centerOfFloor.x;
            } else if (axis == Z_AXIS) {
                return centerOfWall.z > centerOfFloor.z;
            }
        } catch (IncoherentCollisionDetectionException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Renvoi le centre du modèle d'une des dalles de sol adjascentes à ce modèle
     * @param selectedModel modèle dont on veut rtécupérer le centre d'une des dalles adjacentes
     */
    private Vector3 getCenterOfAdjancentFloor(MaderaModel selectedModel) throws IncoherentCollisionDetectionException {
        BoundingBox boxSelected = new BoundingBox();
        selectedModel.calculateBoundingBox(boxSelected);
        Vector3 center = new Vector3();
        for (MaderaModel modeli : editor.floorParts) {
            BoundingBox modelBox = new BoundingBox();
            modeli.calculateBoundingBox(modelBox);
            if (boxSelected.intersects(modelBox)) {
                return modeli.transform.getTranslation(center);
            }
        }
        throw new IncoherentCollisionDetectionException("Le mur n'est en collision avec aucune dalle de sol !");
    }

    public WallTBox drawWallTouchBox(Vector3 center) {
        return placeWallTBoxOnWorld(center, selectedModel.isRotated());
    }

    /**
     * Renvoi le modèle sélectionné
     * @return le modèle sélectionné
     */
    public MaderaModel getSelectedModel() {
        return selectedModel;
    }

    /**
     * Supprime les carpentries et/ou les WallTouvhBox présents sur le mur sélectionné
     */
    public void deleteModelsOnSelectedWall() {
        BoundingBox selectedBox = selectedModel.calculateBoundingBox(new BoundingBox());
        selectedBox.mul(selectedModel.transform);
        Array<MaderaModel> models = new Array<MaderaModel>();
        models.addAll(editor.wallTouchBoxes);
        models.addAll(editor.carpentries);
        for (MaderaModel model : models) {
            // TODO apply box.mul after carpentry placement
            BoundingBox box = model.calculateBoundingBox(new BoundingBox());
            box.mul(model.transform);
            if (box.intersects(selectedBox)) {
                if (model instanceof fr.cesi.libgdx.entities.models.WallTBox) {
                    editor.wallTouchBoxes.removeValue((fr.cesi.libgdx.entities.models.WallTBox) model, true);
                } else {
                    editor.carpentries.removeValue((Carpentry) model, true);
                }
            }
        }
    }

    /**
     * Renvoit la liste des WallTouchBox en collision avec le modèle du mur passé en argument
     * @param wall Modèle du mur
     * @return
     */
    private Array<MaderaModel> getWallTBoxFromWall(MaderaModel wall) {
        Array<MaderaModel> models = new Array<MaderaModel>();
        for (fr.cesi.libgdx.entities.models.WallTBox touchBox : editor.wallTouchBoxes) {
            if (touchBox.getBox().intersects(wall.getBox())) {
                models.add(touchBox);
            }
        }
        return models;
    }

    /**
     * Renvoi la liste des WallTBox ou des Carpentry en collision avec modèle
     *
     * @return la liste des WallTBox et des Carpentry en collision
     * @throws IncoherentCollisionDetectionException si aucun composant n'est retourné
     */
    public Array<MaderaModel> getAdjoiningWallTBoxOrCarpentries(MaderaModel selectedModel) throws IncoherentCollisionDetectionException {
        Array<MaderaModel> models = new Array<MaderaModel>();
        if (selectedModel.getClass().equals(fr.cesi.libgdx.entities.models.WallTBox.class) || selectedModel.getClass().equals(Carpentry.class)) {
            Wall wall = getWallCollidedFromModel(selectedModel);
            if (wall == null) {
                throw new IncoherentCollisionDetectionException("Aucun mur adjacent au slot trouvé");
            }
            models.addAll(getWallTBoxOnWall(wall));
            models.addAll(getCarpentriesOnWall(wall));
            return models;
        } else {
            models.addAll(getWallTBoxOnWall(selectedModel));
            models.addAll(getCarpentriesOnWall(selectedModel));
            return models;
        }
    }

    /**
     * Renvoi le mur en collision avec le modèle en argument
     *
     * @param model modèle dont on veut récupérer le mur en collision
     * @return le mur en collision avec le modèle
     */
    private Wall getWallCollidedFromModel(MaderaModel model) {
        for (Wall wall : editor.walls) {
            if (model.getBox().intersects(wall.getBox())) {
                return wall;
            }
        }
        return null;
    }

    /**
     * Retourne la liste des WallTBox du mur en argument
     * @param wall
     * @return
     */
    private Array<MaderaModel> getWallTBoxOnWall(MaderaModel wall) {
        Array<MaderaModel> models = new Array<MaderaModel>();
        for (WallTBox tbox : editor.wallTouchBoxes) {
            if (tbox.getBox().intersects(wall.getBox())) {
                models.add(tbox);
            }
        }
        return models;
    }

    /**
     * Retourne la liste des Carpentries du mur passé en argument
     *
     * @param wall
     * @return
     */
    private Array<MaderaModel> getCarpentriesOnWall(MaderaModel wall) {
        Array<MaderaModel> models = new Array<MaderaModel>();
        for (Carpentry carp : editor.carpentries) {
            if (carp.getBox().intersects(wall.getBox())) {
                models.add(carp);
            }
        }
        return models;
    }

    /**
     * Méthode de création d'un Divider
     * - Récupère le modèle 3D grace à son path
     * - Place le modèle sur le panel libGDX
     * - Met à jour sa BoundingBox une fois placé
     *
     * @param modelpath
     * @return
     */
    public Divider createDivider(String modelpath) {
        Divider divider = (Divider) getModelIFromPath(modelpath, Divider.class);
        Vector3 center = selectedModel.transform.getTranslation(new Vector3());
        center.y = editor.WALL_HEIGHT / 2;
        divider.setBox(divider.calculateBoundingBox(new BoundingBox()));
        divider.setCenter(center);
        Material newMat = GRAY_BASIC;
        Material oldMat = divider.materials.get(0);
        oldMat.clear();
        oldMat.set(GRAY_BASIC);
        divider.setOriginalMat(newMat);
        placeModelOnWorld(divider, center);
        divider.getBox().mul(divider.transform);
        editor.removeModel(selectedModel);
        return divider;
    }

    /**
     * Méthode de création d'un Carpentry
     * - Récupère le modèle 3D grace à son path
     * - Calcule sa position (sa hauteur)
     * - Place le modèle sur le panel libGDX
     * - Met à jour sa BoundingBox une fois placé
     *
     * @param modelPath
     * @return
     */
    public Carpentry createCarpentry(String modelPath, float heightPlacement) {
        Carpentry carpentry = (Carpentry) getModelIFromPath(modelPath, Carpentry.class);
        Vector3 center = calculPosition(heightPlacement);
        carpentry.setBox(carpentry.calculateBoundingBox(new BoundingBox()));
        carpentry.setCenter(center);
        placeModelOnWorld(carpentry, center);
        carpentry.getBox().mul(carpentry.transform);
        return carpentry;
    }

    /**
     * Calcule la psition du modèle selectionné en fonction du heigt placement
     * @param heightPlacement
     * @return la position
     */
    private Vector3 calculPosition(float heightPlacement) {
        Vector3 center = selectedModel.transform.getTranslation(new Vector3());
        float height = editor.WALL_HEIGHT * heightPlacement;
        center.y = height;
        return center;
    }

    /**
     * Créé et positionne une FLootTouchBox en fonction de son centre
     * et de sa possible rotation sur l'axe Y
     * @param center de la FloorTouchBox à créer
     * @param rotation vaut true si le modèle doit être tourné, false sinon
     * @return le modèle créé
     */
    public FloorTBox placeFloorTBoxOnWorld(Vector3 center, boolean rotation) {
        Model model = modelBuilder.createBox(editor.FLOOR_SIZE - 0.001f, editor.FLOOR_TBOX_HEIGHT, 0.05f, WHITE_ALPHA03.copy(),
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
        FloorTBox touchBox = new FloorTBox(model);
        if (rotation) {
            touchBox.transform.rotate(Vector3.Y, 90);
        }
        touchBox.setRotated(rotation);
        touchBox.transform.trn(center);
        BoundingBox box = new BoundingBox();
        touchBox.calculateBoundingBox(box);
        box.mul(touchBox.transform);
        touchBox.setBox(box);
        touchBox.setCenter(touchBox.transform.getTranslation(new Vector3()));
        return touchBox;
    }

    /**
     * Créé et positionne une FLootTouchBox en fonction de son centre
     * et de sa possible rotation sur l'axe Y
     * @param center de la FloorTouchBox à créer
     * @param rotated  vaut true si le modèle doit être tourné, false sinon
     * @return
     */
    public WallTBox placeWallTBoxOnWorld(Vector3 center, boolean rotated) {
        Model cylinder = modelBuilder.createCylinder(Editor3D.WALL_TBOX_WIDTH, Editor3D.WALL_TBOX_HEIGHT, Editor3D.WALL_TBOX_WIDTH,
                Editor3D.WALL_TBOX_DIVISION_NB, ROYAL_ALPHA05, VertexAttributes.Usage.Normal | VertexAttributes.Usage.Position);
        WallTBox wallTBox = new WallTBox(cylinder);
        wallTBox.transform.trn(center);
        if (rotated) {
            wallTBox.transform.rotate(Vector3.Z, 90);
        } else {
            wallTBox.transform.rotate(Vector3.X, 90);
        }
        wallTBox.setRotated(rotated);
        wallTBox.setCenter(wallTBox.transform.getTranslation(new Vector3()));
        BoundingBox box = new BoundingBox();
        wallTBox.calculateBoundingBox(box);
        box.mul(wallTBox.transform);
        wallTBox.setBox(box);
        wallTBox.setOriginalMat(WHITE_ALPHA05);
        return wallTBox;
    }

    /**
     * Vérifie si le Carpentry créé n'empiète pas sur un composant déjà existant
     * @param carpentry le carpentry à placer
     * @return true si le carpentry empiète sur un autre composant, false sinon
     * @throws IncoherentCollisionDetectionException Si aucun mur en collision avec le carpentry est trouvé
     */
    public boolean carpentryCollideAnother(Carpentry carpentry) throws IncoherentCollisionDetectionException {
        Wall wall = getWallCollidedFromModel(carpentry);
        if (wall == null) {
            throw new IncoherentCollisionDetectionException("Aucun mur n'a été trouvé en collision avec le carpentr à ajouter");
        }
        boolean collide = false;
        Array<MaderaModel> carpList = getCarpentriesOnWall(wall);
        if (selectedModel instanceof Carpentry) {
            carpList.removeValue(selectedModel, false);
        }
        for (MaderaModel model : carpList) {
            if (carpentry.getBox().intersects(model.getBox())) {
                collide = true;
            }
        }
        return collide;
    }

    /**
     * Passe le modèle en argument dans l'état actif changement de texture
     * @param modeli modèle à passer dans l'état actif
     */
    public void setModelActive(MaderaModel modeli) {
        selectedModel = modeli;
        if (modeli.getClass().equals(FloorTBox.class)) {
            Material newMat = new Material();
            newMat.set(new BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA, 0.3f));
            newMat.set(ColorAttribute.createDiffuse(Color.ROYAL));
            Material oldMat = modeli.materials.get(0);
            oldMat.clear();
            oldMat.set(newMat);
        } else if (selectedModel.getClass().equals(Wall.class)) {
            Array<MaderaModel> preservedModels = new Array<MaderaModel>();
            preservedModels.add(modeli);
            preservedModels.addAll(getWallTBoxFromWall(selectedModel));
            setAllTransparent(preservedModels, 0.1f);
        } else if (selectedModel.getClass().equals(Slab.class)) {
            Array<MaderaModel> preservedModels = new Array<MaderaModel>();
            preservedModels.addAll(editor.floorParts);
            preservedModels.addAll(editor.carpentries);
            setAllTransparent(preservedModels, 0.1f);
        }
    }

    /**
     * Passe le modèle en état inactif
     */
    public void setModelInactive() {
        selectedModel = null;
        resetMaterial();
    }

    /**
     * Rend tous les modèle transparent à l'excetption de ceux passé en arguments
     * @param preservedModels modèles à préserver de la transparence
     * @param alpha facteur de transparence
     */
    private void setAllTransparent(Array<MaderaModel> preservedModels, float alpha) {
        Array<MaderaModel> models = editor.getAllModels();
        models.removeAll(preservedModels, true);
        Material newMat = new Material();
        newMat.set(new BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA, alpha));
        newMat.set(ColorAttribute.createDiffuse(Color.WHITE));
        Gdx.app.log("madera", "Before loop in set all transparent");
        for (int i = 0; i < models.size; i++) {
            if (models.get(i).getClass().equals(Wall.class) || models.get(i).getClass().equals(Slab.class)) {
                models.get(i).getNode("main").parts.get(0).material = newMat;
                models.get(i).getNode("main").parts.get(1).material = newMat;
            } else if (models.get(i).getClass().equals(Divider.class)) {
                Material oldMat = models.get(i).materials.get(0);
                oldMat.clear();
                oldMat.set(newMat);
            }
        }
    }

    /**
     * Reinitialise la texture des modèles
     */
    public void resetMaterial() {
        for (int i = 0; i < editor.getAllModels().size; i++) {
            if (editor.getAllModels().get(i).getClass().equals(FloorTBox.class)) {
                Material mat = editor.getAllModels().get(i).materials.get(0);
                mat.clear();
                mat.set(WHITE_ALPHA03);
            } else if (editor.getAllModels().get(i).getClass().equals(Wall.class)) {
                Material front = ((Wall) editor.getAllModels().get(i)).getFrontMat();
                Material back = ((Wall) editor.getAllModels().get(i)).getBackMat();
                if (front != null) {
                    editor.getAllModels().get(i).getNode("main").parts.get(0).material = front;
                } else {
                    editor.getAllModels().get(i).getNode("main").parts.get(0).material = GRAY_BASIC;
                }
                if (back != null) {
                    editor.getAllModels().get(i).getNode("main").parts.get(1).material = back;
                } else {
                    editor.getAllModels().get(i).getNode("main").parts.get(1).material = GRAY_BASIC;
                }
            } else if (editor.getAllModels().get(i).getClass().equals(WallTBox.class)) {
                Material mat = editor.getAllModels().get(i).materials.get(0);
                mat.clear();
                mat.set(ROYAL_ALPHA05);
            } else if (editor.getAllModels().get(i).getClass().equals(Slab.class)) {
                Material mat = ((Slab) editor.getAllModels().get(i)).getFrontMat();
                if (mat != null) {
                    editor.getAllModels().get(i).getNode("main").parts.get(0).material = mat;
                } else {
                    editor.getAllModels().get(i).getNode("main").parts.get(0).material = GRAY_BASIC;
                }
                editor.getAllModels().get(i).getNode("main").parts.get(1).material = GRAY_BASIC;
            } else if (editor.getAllModels().get(i).getClass().equals(Divider.class)) {
                Material savedMat = ((Divider) editor.getAllModels().get(i)).getMainMat();
                Material mat = editor.getAllModels().get(i).materials.get(0);
                mat.clear();
                if (savedMat != null) {
                    mat.set(savedMat);
                } else {
                    mat.set(GRAY_BASIC);
                }
            }
        }
    }

    /**
     * Déssine la texture dont le chemin est passé en argument
     * @param coveringPath chemin vers le fichier de texture
     * @param in vaut true si cela concerne la texture intérieure, false sinon
     * @param out vaut true si cela concerne la texture extérieure, false sinon
     */
    public void drawTextureOnModels(String coveringPath, boolean in, boolean out) {
        FileHandle handle = new FileHandle("/data/data/fr.cesi.madera/files/" +coveringPath);
        Texture texture = new Texture(handle);
        TextureAttribute textAttr = new TextureAttribute(TextureAttribute.Diffuse, texture);
        Material material = new Material();
        material.set(textAttr);
        if (selectedModel instanceof Wall) {
            if (in) {
                editor.wall_in_mat = material;
                for (Wall wall : editor.walls) {
                    boolean inversed = wall.isRotated() ? getDirectionOfPlacement(wall,X_AXIS) : getDirectionOfPlacement(wall,Z_AXIS);
                    if(inversed) {
                        wall.setFrontMat(material);
                    }else{
                        wall.setBackMat(material);
                    }
                }
            }
            if (out) {
                editor.wall_out_mat = material;
                for (Wall wall : editor.walls) {
                    boolean inversed = wall.isRotated() ? getDirectionOfPlacement(wall, X_AXIS) : getDirectionOfPlacement(wall, Z_AXIS);
                    if (inversed) {
                        wall.setBackMat(material);
                    } else {
                        wall.setFrontMat(material);
                    }
                }
            }
        } else if (selectedModel instanceof Slab) {
            editor.floor_mat = material;
            for (Slab slab : editor.floorParts) {
                slab.setFrontMat(material);
            }
        }
        resetMaterial();
    }

    public boolean dividerPlacementIsPossible() {
        Array<MaderaModel> models = new Array();
        models.addAll(editor.walls);
        models.addAll(editor.dividers);
        boolean collision = false;
        for (MaderaModel model:models) {
            if(selectedModel.getBox().intersects(model.getBox())){
                collision = true;
            }
        }
        return collision;
    }
}


class PointComparatorOnX implements Comparator<WallPartCenter> {
    @Override
    public int compare(WallPartCenter a, WallPartCenter b) {
        if (a.x < b.x) return -1;
        if (a.x > b.x) return 1;
        return 0;
    }
}

class PointComparatorOnY implements Comparator<WallPartCenter> {
    @Override
    public int compare(WallPartCenter a, WallPartCenter b) {
        if (a.y < b.y) return -1;
        if (a.y > b.y) return 1;
        return 0;
    }
}

class PointComparatorOnZ implements Comparator<WallPartCenter> {
    @Override
    public int compare(WallPartCenter a, WallPartCenter b) {
        if (a.z < b.z) return -1;
        if (a.z > b.z) return 1;
        return 0;
    }
}
