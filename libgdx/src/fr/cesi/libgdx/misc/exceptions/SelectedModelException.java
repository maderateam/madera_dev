package fr.cesi.libgdx.misc.exceptions;

/**
 * Created by Remi on 28/12/2016.
 */
public class SelectedModelException extends Exception {
    public SelectedModelException(String s) {
        super(s);
    }
}
