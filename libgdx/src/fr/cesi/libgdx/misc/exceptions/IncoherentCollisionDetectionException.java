package fr.cesi.libgdx.misc.exceptions;

/**
 * Created by Doremus on 04/02/2017.
 */

public class IncoherentCollisionDetectionException extends Exception {
    public IncoherentCollisionDetectionException() {
    }

    public IncoherentCollisionDetectionException(String detailMessage) {
        super(detailMessage);
    }
}
