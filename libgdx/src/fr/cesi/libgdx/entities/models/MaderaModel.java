package fr.cesi.libgdx.entities.models;

import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;

/**
 * Représentartion d'un modèle 3D
 */
public class MaderaModel extends ModelInstance{
    private BoundingBox box;
    private Vector3 center;
    private Vector3 dimensions;
    private boolean rotated;
    private Material originalMat;

    public MaderaModel(Model model) {
        super(model);
    }


    public BoundingBox getBox() {
        return box;
    }

    public void setBox(BoundingBox box) {
        this.box = box;
    }

    public Vector3 getCenter() {
        return center;
    }

    public void setCenter(Vector3 center) {
        this.center = center;
    }

    public Vector3 getDimensions() {
        return dimensions;
    }

    public void setDimensions(Vector3 dimensions) {
        this.dimensions = dimensions;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof MaderaModel && ((MaderaModel) obj).getCenter().equals(this.getCenter());
    }

    public boolean isRotated() {
        return rotated;
    }

    public void setRotated(boolean rotated) {
        this.rotated = rotated;
    }

    public Material getOriginalMat() {
        return originalMat;
    }

    public void setOriginalMat(Material originalMat) {
        this.originalMat = originalMat;
    }


}
