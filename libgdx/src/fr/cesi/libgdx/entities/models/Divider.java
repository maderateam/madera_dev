package fr.cesi.libgdx.entities.models;

import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;

/**
 * Représentation d'une cloison
 */
public class Divider extends SimpleModel {
    private Material mainMat;
    public Divider(Model model) {
        super(model);
    }

    public Material getMainMat() {
        return mainMat;
    }

    public void setMainMat(Material mainMat) {
        this.mainMat = mainMat;
    }
}
