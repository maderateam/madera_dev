package fr.cesi.libgdx.entities.models;

import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;

/**
 * Représentation d'une dalle de sol carré
 */
public class Slab extends ComplexModel {
    private Material frontMat;
    public Slab(Model model) {
        super(model);
    }

    public Material getFrontMat() {
        return frontMat;
    }

    public void setFrontMat(Material frontMat) {
        this.frontMat = frontMat;
    }
}
