package fr.cesi.libgdx.entities.models;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.math.Vector3;

/**
 * Représentation d'un cylindr ede sol pour la sélection d'un slot
 */
public class TouchBox extends MaderaModel {

    // Centre de l'élement parent de cette touchBox
    private Vector3 parentCenter;

    public TouchBox(Model model) {
        super(model);
    }

    public Vector3 getParentCenter() {
        return parentCenter;
    }

    public void setParentCenter(Vector3 parentCenter) {
        this.parentCenter = parentCenter;
    }
}
