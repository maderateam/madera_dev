package fr.cesi.libgdx.entities.models;

import com.badlogic.gdx.graphics.g3d.Model;

/**
 * Représentation d'un composant de menuiserie sur le plan
 */
public class Carpentry extends SimpleModel {
    public Carpentry(Model model) {
        super(model);
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
