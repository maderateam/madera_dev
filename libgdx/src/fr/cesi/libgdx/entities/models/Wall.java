package fr.cesi.libgdx.entities.models;

import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;

/**
 * Représentation d'un mur
 */
public class Wall extends ComplexModel {
    private float lenght;
    private Material frontMat;
    private Material backMat;

    public Wall(Model model) {
        super(model);
    }

    public float getLenght() {
        return lenght;
    }

    public void setLenght(float lenght) {
        this.lenght = lenght;
    }

    public Material getFrontMat() {
        return frontMat;
    }

    public void setFrontMat(Material frontMat) {
        this.frontMat = frontMat;
    }

    public Material getBackMat() {
        return backMat;
    }

    public void setBackMat(Material backMat) {
        this.backMat = backMat;
    }
}
