package fr.cesi.libgdx.entities.models;

import com.badlogic.gdx.graphics.g3d.Model;

/**
 * Représentation d'un cylindre de sélection d'un slot
 */
public class FloorTBox extends TouchBox {
    public FloorTBox(Model model) {
        super(model);
    }
}
