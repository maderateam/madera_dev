package fr.cesi.libgdx.entities.models;

import com.badlogic.gdx.graphics.g3d.Model;

/**
 * Représentation d'un cylindre de selection sur un mur
 */
public class WallTBox extends TouchBox {
    public WallTBox(Model model) {
        super(model);
    }
}
