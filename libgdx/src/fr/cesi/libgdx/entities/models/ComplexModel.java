package fr.cesi.libgdx.entities.models;

import com.badlogic.gdx.graphics.g3d.Model;

import java.util.ArrayList;

/**
 * Représentation d'un modèle 3D pouvant accueillir d'untre modèles 3D
 */
public class ComplexModel extends MaderaModel {
    private ArrayList<fr.cesi.libgdx.entities.models.TouchBox> placedSlots;

    public ComplexModel(Model model) {
        super(model);
    }

    public ArrayList<fr.cesi.libgdx.entities.models.TouchBox> getPlacedSlots() {
        return placedSlots;
    }

    public void setPlacedSlots(ArrayList<fr.cesi.libgdx.entities.models.TouchBox> placedSlots) {
        this.placedSlots = placedSlots;
    }
}
