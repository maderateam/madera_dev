package fr.cesi.libgdx.entities.models;

import com.badlogic.gdx.graphics.g3d.Model;

/**
 * Représentation d'un modèle ne pouvant pas en accueillir un autre
 */
public class SimpleModel extends MaderaModel {
    private String pathToModelFile;

    public SimpleModel(Model model) {
        super(model);
    }

    public String getPathToModelFile() {
        return pathToModelFile;
    }

    public void setPathToModelFile(String pathToModelFile) {
        this.pathToModelFile = pathToModelFile;
    }
}
