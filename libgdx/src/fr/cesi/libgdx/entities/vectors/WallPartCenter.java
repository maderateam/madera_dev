package fr.cesi.libgdx.entities.vectors;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Représentation du centre d'une portion de mur
 */
public class WallPartCenter extends Vector3 {
    private boolean rotated;

    public WallPartCenter() {
    }

    public WallPartCenter(float[] values) {
        super(values);
    }

    public WallPartCenter(Vector2 vector, float z) {
        super(vector, z);
    }

    public WallPartCenter(Vector3 vector) {
        super(vector);
    }

    public WallPartCenter(float x, float y, float z) {
        super(x, y, z);
    }

    public boolean isRotated() {
        return rotated;
    }

    public void setRotated(boolean rotated) {
        this.rotated = rotated;
    }
}
