package fr.cesi.madera.misc.exceptions;

/**
 * Created by Joshua on 04/10/2016.
 */

public class NoPSlotFoundException extends Exception {

    public NoPSlotFoundException() {
    }

    public NoPSlotFoundException(String detailMessage) {
        super(detailMessage);
    }
}
