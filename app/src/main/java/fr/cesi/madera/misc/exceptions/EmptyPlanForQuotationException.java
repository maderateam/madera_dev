package fr.cesi.madera.misc.exceptions;

/**
 * Created by Joshua on 04/10/2016.
 */

public class EmptyPlanForQuotationException extends Exception {

    public EmptyPlanForQuotationException() {
    }

    public EmptyPlanForQuotationException(String detailMessage) {
        super(detailMessage);
    }
}
