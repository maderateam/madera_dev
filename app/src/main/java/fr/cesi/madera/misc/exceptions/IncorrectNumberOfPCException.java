package fr.cesi.madera.misc.exceptions;

/**
 * Created by Doremus on 10/02/2017.
 */

public class IncorrectNumberOfPCException extends Exception {
    public IncorrectNumberOfPCException() {
    }

    public IncorrectNumberOfPCException(String detailMessage) {
        super(detailMessage);
    }
}
