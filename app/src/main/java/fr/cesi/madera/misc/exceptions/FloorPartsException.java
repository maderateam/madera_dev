package fr.cesi.madera.misc.exceptions;

/**
 * Created by Joshua on 04/10/2016.
 */

public class FloorPartsException extends Exception {

    public FloorPartsException() {}

    public FloorPartsException(String detailMessage) {
        super(detailMessage);
    }
}
