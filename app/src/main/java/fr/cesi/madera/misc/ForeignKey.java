package fr.cesi.madera.misc;

/**
 * Created by Remi on 17/11/2016.
 */
public @interface ForeignKey {
    String table();
    String column();
}
