package fr.cesi.madera.misc;

/**
 * Classe de représentation d'une Vue SQLite
 * Created by Doremus on 17/11/2016.
 */

public class SQLiteView {
    private String name;
    private String className;
    private String query;

    public SQLiteView(String name, String query, String className) {
        this.name = name;
        this.query = query;
        this.className = className;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public final String getClassName() {
        return className;
    }
}
