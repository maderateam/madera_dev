package fr.cesi.madera.misc.exceptions;

/**
 * Created by Doremus on 22/12/2016.
 */
public class IncoherentPlacementDefinitionException extends Exception {

    public IncoherentPlacementDefinitionException(String detailMessage) {
        super(detailMessage);
    }

    public IncoherentPlacementDefinitionException() {
    }
}
