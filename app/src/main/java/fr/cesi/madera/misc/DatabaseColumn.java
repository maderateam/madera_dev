package fr.cesi.madera.misc;

/**
 * Annotation permettant de déterminer si un attribut doit faire l'objet
 * d'une création de colonne dans la table correspodante
 */

public @interface DatabaseColumn {
}
