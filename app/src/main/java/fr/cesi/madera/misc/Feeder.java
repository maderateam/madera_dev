package fr.cesi.madera.misc;

import android.content.Context;
import android.content.SharedPreferences;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import fr.cesi.madera.dao.daoClient.ClientDao;
import fr.cesi.madera.dao.daoCommercial.CommercialDao;
import fr.cesi.madera.dao.daoComponent.ComponentDao;
import fr.cesi.madera.dao.daoCovering.CoveringDao;
import fr.cesi.madera.dao.daoGamme.GammeDao;
import fr.cesi.madera.dao.daoParameter.ParameterDao;
import fr.cesi.madera.dao.daoPlan.PlanDao;
import fr.cesi.madera.dao.daoProject.ProjectDao;
import fr.cesi.madera.dao.daoSlot.SlotDao;
import fr.cesi.madera.entities.Client;
import fr.cesi.madera.entities.Component;
import fr.cesi.madera.entities.Covering;
import fr.cesi.madera.entities.Gamme;
import fr.cesi.madera.entities.Parameter;
import fr.cesi.madera.entities.Project;
import fr.cesi.madera.entities.Slot;

/**
 * Created by Doremus on 03/12/2016.
 */

public class Feeder {
    private Context ctx;
    private ComponentDao composDao;
    private SlotDao slotDao;
    private GammeDao gammeDAO;
    private CoveringDao coveDAO;
    private PlanDao planDao;
    private ParameterDao parameterDao;
    private CommercialDao comDao;
    private ArrayList<Component> carpentries = new ArrayList<>();
    private ArrayList<Component> dividers = new ArrayList<>();
    private ArrayList<Component> walls = new ArrayList<>();
    private ArrayList<Component> sols = new ArrayList<>();
    private ArrayList<Gamme> gammes = new ArrayList<>();
    private ArrayList<Slot> slots = new ArrayList<>();
    private ArrayList<Covering> coverings = new ArrayList<>();

    public Feeder(Context ctx) {
        this.ctx = ctx;
        composDao = new ComponentDao(ctx);
        slotDao = new SlotDao(ctx);
        gammeDAO = new GammeDao(ctx);
        coveDAO = new CoveringDao(ctx);
        planDao = new PlanDao(ctx);
        parameterDao = new ParameterDao(ctx);
        comDao = new CommercialDao(ctx);
    }

    public void insertLibgdxTests(){
        setParameters();
        //createClientAndProject();
        createGammes();
        createCarpentry(); // 22
        composDao.openDbIfClosed();
        for (Component compo:carpentries) {
            composDao.insertEntity(compo);
        }
        createDivider(); // 3
        for (Component compo:dividers) {
            composDao.insertEntity(compo);
        }
        createMur(); // 7
        for (Component compo:walls) {
            composDao.insertEntity(compo);
        }
        createSol(); // 1
        for (Component compo:sols) {
            composDao.insertEntity(compo);
        }
        slots = generateSamples(walls,carpentries);
        slotDao.openDbIfClosed();
        for(Slot slot : slots){
            slotDao.insertEntity(slot);
        }
        createCoverings();
        coveDAO.openDbIfClosed();
        for(Covering covering : coverings){
            coveDAO.insertEntity(covering);
        }
        SharedPreferences sharedpreferences = ctx.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean("feed", true);
        editor.commit();
    }

    private void createCoverings() {
        Covering cov1 = new Covering();
        cov1.setDenomination("Bois lazuré");
        cov1.setIconname("wood1.jpg");
        cov1.setPrice(45.5);
        coverings.add(cov1);

        Covering cov2 = new Covering();
        cov2.setDenomination("Béton brut");
        cov2.setIconname("beton1.jpg");
        cov2.setPrice(15);
        coverings.add(cov2);

        Covering cov3 = new Covering();
        cov3.setDenomination("Minéral clair");
        cov3.setIconname("mineral1.jpg");
        cov3.setPrice(25);
        coverings.add(cov3);


        Covering cov4 = new Covering();
        cov4.setDenomination("Brique rouge");
        cov4.setIconname("brique1.jpg");
        cov4.setPrice(53);
        coverings.add(cov4);

        Covering cov5 = new Covering();
        cov5.setDenomination("Metal indus");
        cov5.setIconname("metal1.jpg");
        cov5.setPrice(75);
        coverings.add(cov5);

        Covering cov6 = new Covering();
        cov6.setDenomination("CESI");
        cov6.setIconname("cesi.jpg");
        cov6.setPrice(200);
        coverings.add(cov6);
    }

    private void createGammes() {
        gammeDAO.openDbIfClosed();
        Gamme g1 = new Gamme();
        g1.setDenomination("PVC");
        gammeDAO.insertEntity(g1);
        gammes.add(g1);

        Gamme g2 = new Gamme();
        g2.setDenomination("Bois");
        gammeDAO.insertEntity(g2);
        gammes.add(g2);

        Gamme g3 = new Gamme();
        g3.setDenomination("Metal");
        gammeDAO.insertEntity(g3);
        gammes.add(g3);
        gammeDAO.close();
    }

    private void createClientAndProject() {
        Client cli = new Client();
        cli.setFirstname("Jean");
        cli.setLastname("Dupont");
        cli.setZipcode("12000");
        cli.setAdress("Route du Pouzet");
        cli.setCity("Le Vibal");
        cli.setMail("jean@test.com");
        cli.setTel("0638694550");
        cli.setIdcommercial(1);
        ClientDao cliDAO= new ClientDao<>(ctx);
        cliDAO.insertEntity(cli);
        cliDAO.close();

        Project proj = new Project();
        proj.setDatecreate(new Timestamp(new Date().getTime()));
        proj.setDateclosed(new Timestamp((new Date().getTime())));
        proj.setIdclient(cli.get_id());
        proj.setName("ProjectTest");
        proj.setDescription("De la description en veux-tu en voila !");
        proj.setIdcommercial(1);
        ProjectDao projDAO = new ProjectDao(ctx);
        projDAO.insertEntity(proj);
        projDAO.close();
    }

    public void createCarpentry(){

		// Ajout Carpentry
        // Fenetre 1
        Component fenetre1 = new Component();
        fenetre1.setPrice(50);
        fenetre1.setComplex(false);
        fenetre1.setDenomination("Inclined PVC 0.5x0.62");
        fenetre1.setHeight(0.5f);
        fenetre1.setWidth(0.62f);
        fenetre1.setFloor(false);
        fenetre1.setDivider(false);
        fenetre1.setIconname("SimpleInclinedWindowPVC_0_5x0_62.png");
        fenetre1.setModelpath("SimpleInclinedWindowPVC_0_5x0_62.g3db");
        fenetre1.setIdgamme(gammes.get(0).get_id());
        carpentries.add(fenetre1);

        // Fenetre 2
        Component fenetre2 = new Component();
        fenetre2.setPrice(50);
        fenetre2.setComplex(false);
        fenetre2.setDenomination("Inclined Wood 0.5x0.62");
        fenetre2.setHeight(0.5f);
        fenetre2.setWidth(0.62f);
        fenetre2.setFloor(false);
        fenetre2.setDivider(false);
        fenetre2.setIconname("SimpleInclinedWindowWood_0_5x0_62.png");
        fenetre2.setModelpath("SimpleInclinedWindowWood_0_5x0_62.g3db");
        fenetre2.setIdgamme(gammes.get(1).get_id());
        carpentries.add(fenetre2);
		
		// Fenetre 3
        Component fenetre3 = new Component();
        fenetre3.setPrice(50);
        fenetre3.setComplex(false);
        fenetre3.setDenomination("Round PVC 0.5x0.5");
        fenetre3.setHeight(0.5f);
        fenetre3.setWidth(0.5f);
        fenetre3.setFloor(false);
        fenetre3.setDivider(false);
        fenetre3.setIconname("SimpleRoundWindowPVC_0_5x0_5.png");
        fenetre3.setModelpath("SimpleRoundWindowPVC_0_5x0_5.g3db");
        fenetre3.setIdgamme(gammes.get(0).get_id());
        carpentries.add(fenetre3);
		
		// Fenetre 4
        Component fenetre4 = new Component();
        fenetre4.setPrice(50);
        fenetre4.setComplex(false);
        fenetre4.setDenomination("Round Wood 0.5x0.5");
        fenetre4.setHeight(0.5f);
        fenetre4.setWidth(0.5f);
        fenetre4.setFloor(false);
        fenetre4.setDivider(false);
        fenetre4.setIconname("SimpleRoundWindowWood_0_5x0_5.png");
        fenetre4.setModelpath("SimpleRoundWindowWood_0_5x0_5.g3db");
        fenetre4.setIdgamme(gammes.get(1).get_id());
        carpentries.add(fenetre4);
		
		// Fenetre 5
        Component fenetre5 = new Component();
        fenetre5.setPrice(50);
        fenetre5.setComplex(false);
        fenetre5.setDenomination("Simple PVC 0.5x0.5");
        fenetre5.setHeight(0.5f);
        fenetre5.setWidth(0.5f);
        fenetre5.setFloor(false);
        fenetre5.setDivider(false);
        fenetre5.setIconname("SimpleWindowPVC_0_5x0_5.png");
        fenetre5.setModelpath("SimpleWindowPVC_0_5x0_5.g3db");
        fenetre5.setIdgamme(gammes.get(0).get_id());
        carpentries.add(fenetre5);
		
		// Fenetre 6
        Component fenetre6 = new Component();
        fenetre6.setPrice(50);
        fenetre6.setComplex(false);
        fenetre6.setDenomination("Simple Wood 0.5x0.5");
        fenetre6.setHeight(0.5f);
        fenetre6.setWidth(0.5f);
        fenetre6.setFloor(false);
        fenetre6.setDivider(false);
        fenetre6.setIconname("SimpleWindowWood_0_5x0_5.png");
        fenetre6.setModelpath("SimpleWindowWood_0_5x0_5.g3db");
        fenetre6.setIdgamme(gammes.get(1).get_id());
        carpentries.add(fenetre6);
		
		// Fenetre 7
        Component fenetre7 = new Component();
        fenetre7.setPrice(50);
        fenetre7.setComplex(false);
        fenetre7.setDenomination("Doube PVC 0.5x0.95");
        fenetre7.setWidth(0.5f);
        fenetre7.setHeight(0.95f);
        fenetre7.setFloor(false);
        fenetre7.setDivider(false);
        fenetre7.setIconname("Double_Vert_Flat_Window_PVC_0_5x0_95.png");
        fenetre7.setModelpath("Double_Vert_Flat_Window_PVC_0_5x0_95.g3db");
        fenetre7.setIdgamme(gammes.get(0).get_id());
        carpentries.add(fenetre7);
		
		// Fenetre 8
        Component fenetre8 = new Component();
        fenetre8.setPrice(50);
        fenetre8.setComplex(false);
        fenetre8.setDenomination("Double Wood 0.5x0.95");
        fenetre8.setWidth(0.5f);
        fenetre8.setHeight(0.95f);
        fenetre8.setFloor(false);
        fenetre8.setDivider(false);
        fenetre8.setIconname("Double_Vert_Flat_Window_Wood_0_5x0_95.png");
        fenetre8.setModelpath("Double_Vert_Flat_Window_Wood_0_5x0_95.g3db");
        fenetre8.setIdgamme(gammes.get(1).get_id());
        carpentries.add(fenetre8);
		
		// Fenetre 9
        Component fenetre9 = new Component();
        fenetre9.setPrice(50);
        fenetre9.setComplex(false);
        fenetre9.setDenomination("Inclined Wood 0.5x0.62");
        fenetre9.setWidth(0.5f);
        fenetre9.setHeight(0.62f);
        fenetre9.setFloor(false);
        fenetre9.setDivider(false);
        fenetre9.setIconname("Double_Vert_Inclined_Window_PVC_0_5x1_07.png");
        fenetre9.setModelpath("Double_Vert_Inclined_Window_PVC_0_5x1_07.g3db");
        fenetre9.setIdgamme(gammes.get(1).get_id());
        carpentries.add(fenetre9);
		
		// Fenetre 10
        Component fenetre10 = new Component();
        fenetre10.setPrice(50);
        fenetre10.setComplex(false);
        fenetre10.setDenomination("Inclined Wood 0.5x1.07");
        fenetre10.setWidth(0.5f);
        fenetre10.setHeight(1.07f);
        fenetre10.setFloor(false);
        fenetre10.setDivider(false);
        fenetre10.setIconname("Double_Vert_Inclined_Window_Wood_0_5x1_07.png");
        fenetre10.setModelpath("Double_Vert_Inclined_Window_Wood_0_5x1_07.g3db");
        fenetre10.setIdgamme(gammes.get(1).get_id());
        carpentries.add(fenetre10);
		
		// Fentre 11
        Component fenetre11 = new Component();
        fenetre11.setPrice(50);
        fenetre11.setComplex(false);
        fenetre11.setDenomination("Round PVC 0.5x0.95");
        fenetre11.setWidth(0.5f);
        fenetre11.setHeight(0.95f);
        fenetre11.setFloor(false);
        fenetre11.setDivider(false);
        fenetre11.setIconname("Double_Vert_Round_Window_PVC_0_5x0_95.png");
        fenetre11.setModelpath("Double_Vert_Round_Window_PVC_0_5x0_95.g3db");
        fenetre11.setIdgamme(gammes.get(0).get_id());
        carpentries.add(fenetre11);
		
		// Fentre 12
        Component fenetre12 = new Component();
        fenetre12.setPrice(50);
        fenetre12.setComplex(false);
        fenetre12.setDenomination("Round Wood 0.5x0.95");
        fenetre12.setWidth(0.5f);
        fenetre12.setHeight(0.95f);
        fenetre12.setFloor(false);
        fenetre12.setDivider(false);
        fenetre12.setIconname("Double_Vert_Round_Window_Wood_0_5x0_95.png");
        fenetre12.setModelpath("Double_Vert_Round_Window_Wood_0_5x0_95.g3db");
        fenetre12.setIdgamme(gammes.get(1).get_id());
        carpentries.add(fenetre12);
		
		// Fentre 13
        Component fenetre13 = new Component();
        fenetre13.setPrice(50);
        fenetre13.setComplex(false);
        fenetre13.setDenomination("Flat PVC 0.95x0.5");
        fenetre13.setWidth(0.95f);
        fenetre13.setHeight(0.5f);
        fenetre13.setFloor(false);
        fenetre13.setDivider(false);
        fenetre13.setIconname("Double_Hori_Flat_Window_PVC_0_95x0_5.png");
        fenetre13.setModelpath("Double_Hori_Flat_Window_PVC_0_95x0_5.g3db");
        fenetre13.setIdgamme(gammes.get(0).get_id());
        carpentries.add(fenetre13);
		
		// Fentre 14
        Component fenetre14 = new Component();
        fenetre14.setPrice(50);
        fenetre14.setComplex(false);
        fenetre14.setDenomination("Flat Wood 0.95x0.5");
        fenetre14.setWidth(0.95f);
        fenetre14.setHeight(0.5f);
        fenetre14.setFloor(false);
        fenetre14.setDivider(false);
        fenetre14.setIconname("Double_Hori_Flat_Window_Wood_0_95x0_5.png");
        fenetre14.setModelpath("Double_Hori_Flat_Window_Wood_0_95x0_5.g3db");
        fenetre14.setIdgamme(gammes.get(1).get_id());
        carpentries.add(fenetre14);
		
		// Fentre 15
        Component fenetre15 = new Component();
        fenetre15.setPrice(50);
        fenetre15.setComplex(false);
        fenetre15.setDenomination("Inclined PVC 0.95x0.62");
        fenetre15.setWidth(0.95f);
        fenetre15.setHeight(0.62f);
        fenetre15.setFloor(false);
        fenetre15.setDivider(false);
        fenetre15.setIconname("Double_Hori_Inclined_Window_PVC_0_95x0_62.png");
        fenetre15.setModelpath("Double_Hori_Inclined_Window_PVC_0_95x0_62.g3db");
        fenetre15.setIdgamme(gammes.get(0).get_id());
        carpentries.add(fenetre15);
		
		// Fentre 16
        Component fenetre16 = new Component();
        fenetre16.setPrice(50);
        fenetre16.setComplex(false);
        fenetre16.setDenomination("Inclined Wood 0.95x0.62");
        fenetre16.setWidth(0.95f);
        fenetre16.setHeight(0.62f);
        fenetre16.setFloor(false);
        fenetre16.setDivider(false);
        fenetre16.setIconname("Double_Hori_Inclined_Window_Wood_0_95x0_62.png");
        fenetre16.setModelpath("Double_Hori_Inclined_Window_Wood_0_95x0_62.g3db");
        fenetre16.setIdgamme(gammes.get(1).get_id());
        carpentries.add(fenetre16);
		
		// Fentre 17
        Component fenetre17 = new Component();
        fenetre17.setPrice(50);
        fenetre17.setComplex(false);
        fenetre17.setDenomination("Round PVC 0.95x0.5");
        fenetre17.setWidth(0.95f);
        fenetre17.setHeight(0.5f);
        fenetre17.setFloor(false);
        fenetre17.setDivider(false);
        fenetre17.setIconname("Double_Hori_Round_Window_PVC_0_95x0_5.png");
        fenetre17.setModelpath("Double_Hori_Round_Window_PVC_0_95x0_5.g3db");
        fenetre17.setIdgamme(gammes.get(0).get_id());
        carpentries.add(fenetre17);
		
		// Fentre 18
        Component fenetre18 = new Component();
        fenetre18.setPrice(50);
        fenetre18.setComplex(false);
        fenetre18.setDenomination("Round Wood 0.95x0.5");
        fenetre18.setWidth(0.95f);
        fenetre18.setHeight(0.5f);
        fenetre18.setFloor(false);
        fenetre18.setDivider(false);
        fenetre18.setIconname("Double_Hori_Round_Window_Wood_0_95x0_5.png");
        fenetre18.setModelpath("Double_Hori_Round_Window_Wood_0_95x0_5.g3db");
        carpentries.add(fenetre18);

        Component porte1 = new Component();
        porte1.setPrice(50);
        porte1.setComplex(false);
        porte1.setDenomination("Door Gray 2.0x0.90");
        porte1.setWidth(0.903f);
        porte1.setHeight(2.028f);
        porte1.setFloor(false);
        porte1.setDivider(false);
        porte1.setIconname("Door_Metal.png");
        porte1.setModelpath("Door_Metal.g3db");
        porte1.setIdgamme(gammes.get(2).get_id());
        carpentries.add(porte1);

        Component porte2 = new Component();
        porte2.setPrice(50);
        porte2.setComplex(false);
        porte2.setDenomination("Door Black 2.0x0.90");
        porte2.setWidth(0.903f);
        porte2.setHeight(2.028f);
        porte2.setFloor(false);
        porte2.setDivider(false);
        porte2.setIconname("Door_Metal_Black.png");
        porte2.setModelpath("Door_Metal_Black.g3db");
        porte2.setIdgamme(gammes.get(2).get_id());
        carpentries.add(porte2);

        Component porte3 = new Component();
        porte3.setPrice(50);
        porte3.setComplex(false);
        porte3.setDenomination("Door White 2.0x0.90");
        porte3.setWidth(0.903f);
        porte3.setHeight(2.028f);
        porte3.setFloor(false);
        porte3.setDivider(false);
        porte3.setIconname("Door_PVC_White.png");
        porte3.setModelpath("Door_PVC_White.g3db");
        porte3.setIdgamme(gammes.get(0).get_id());
        carpentries.add(porte3);

        Component porte4 = new Component();
        porte4.setPrice(50);
        porte4.setComplex(false);
        porte4.setDenomination("Door Wood 2.0x0.90");
        porte4.setWidth(0.903f);
        porte4.setHeight(2.028f);
        porte4.setFloor(false);
        porte4.setDivider(false);
        porte4.setIconname("Door_Wood.png");
        porte4.setModelpath("Door_Wood.g3db");
        porte4.setIdgamme(gammes.get(1).get_id());
        carpentries.add(porte4);
    }

    private void createMur(){
        // Création des deux murs
        // Mur 1
        Component wall1 = new Component();
        wall1.setComplex(true);
        wall1.setDenomination("Mur x1");
        wall1.setNbslot(1);
        wall1.setFloor(false);
        wall1.setDivider(false);
        wall1.setIconname("wall1_icon.png");
        walls.add(wall1);

        // Mur 2
        Component wall2 = new Component();
        wall2.setComplex(true);
        wall2.setDenomination("Mur x2");
        wall2.setNbslot(2);
        wall2.setFloor(false);
        wall2.setDivider(false);
        wall2.setIconname("wall2_icon.png");
        walls.add(wall2);

        Component wall3 = new Component();
        wall3.setPrice(50);
        wall3.setComplex(true);
        wall3.setDenomination("Mur x3");
        wall3.setNbslot(3);
        wall3.setFloor(false);
        wall3.setDivider(false);
        wall3.setIconname("wall3_icon.png");
        walls.add(wall3);

        Component wall4 = new Component();
        wall4.setPrice(50);
        wall4.setComplex(true);
        wall4.setDenomination("Mur x4");
        wall4.setNbslot(4);
        wall4.setFloor(false);
        wall4.setDivider(false);
        wall4.setIconname("wall4_icon.png");
        walls.add(wall4);

        Component wall5 = new Component();
        wall5.setPrice(50);
        wall5.setComplex(true);
        wall5.setDenomination("Mur x5");
        wall5.setNbslot(5);
        wall5.setFloor(false);
        wall5.setDivider(false);
        wall5.setIconname("wall5_icon.png");
        walls.add(wall5);

        Component wall6 = new Component();
        wall6.setPrice(50);
        wall6.setComplex(true);
        wall6.setDenomination("Mur x6");
        wall6.setNbslot(6);
        wall6.setFloor(false);
        wall6.setDivider(false);
        wall6.setIconname("wall6_icon.png");
        walls.add(wall6);

        Component wall7 = new Component();
        wall7.setPrice(50);
        wall7.setComplex(true);
        wall7.setDenomination("Mur x7");
        wall7.setNbslot(7);
        wall7.setFloor(false);
        wall7.setDivider(false);
        wall7.setIconname("wall7_icon.png");
        walls.add(wall7);
    }

    private void createSol(){
         // Création du sol pour libGDX
        Component floor4 = new Component();
        floor4.setPrice(50);
        floor4.setComplex(true);
        floor4.setDenomination("Sol Test LibGDX");
        floor4.setNbslot(14);
        floor4.setFloor(true);
        floor4.setDivider(false);
        floor4.setIconname("floor_l.png");
        floor4.setCoordinates("(0,0,0):(1,0,0):(2,0,0):(3,0,0):(0,0,1):(1,0,1):(2,0,1):" +
                "(3,0,1):(0,0,2):(1,0,2):(2,0,2):(3,0,2):(0,0,3):(1,0,3)");
        sols.add(floor4);


    }

    private void createDivider(){
        // Création des dividers
        // Divider 1
        Component div1 = new Component();
        div1.setPrice(50);
        div1.setComplex(false);
        div1.setDenomination("Porte intérieure ronde");
        div1.setFloor(false);
        div1.setDivider(true);
        div1.setIconname("divider1_icon.png");
        div1.setModelpath("doorDividerRound.g3db");
       dividers.add(div1);

        // Divider2
        Component div2 = new Component();
        div2.setPrice(50);
        div2.setComplex(false);
        div2.setDenomination("Porte interieure Droite");
        div2.setFloor(false);
        div2.setDivider(true);
        div2.setIconname("divider2_icon.png");
        div2.setModelpath("doorDivider.g3db");
        dividers.add(div2);
        // Divider2
        Component div3 = new Component();
        div3.setPrice(50);
        div3.setComplex(false);
        div3.setDenomination("Cloison pleine");
        div3.setFloor(false);
        div3.setDivider(true);
        div3.setIconname("divider3_icon.png");
        div3.setModelpath("plainDivider.g3db");
        dividers.add(div3);
    }

    private ArrayList<Slot> generateSamples(ArrayList<Component> walls, ArrayList<Component> carpentries){
        ArrayList<Slot> slots = new ArrayList<>();
        for(Component wall : walls){
            for(int i = 0 ; i < wall.getNbslot();i++){
                Random rand = new Random();
                int nbDiffrentSlot = rand.nextInt(9)+1;
                ArrayList<Component> allReadyPickCompo = new ArrayList<>();
                for (int j = 0 ; j < nbDiffrentSlot ; j++){
                    Slot slot = new Slot();
                    Component carp = null;
                    do{
                        carp = carpentries.get(rand.nextInt(carpentries.size()-1));
                    }while(allReadyPickCompo.contains(carp));
                    allReadyPickCompo.add(carp);
                    slot.setIdchildcomponent(carp.get_id());
                    slot.setIdparentcomponent(wall.get_id());
                    slot.setNumofslot(i);
                    if(carp.getHeight() < 1.5){
                        slot.setHeightplacement(0.65f);
                    }else{
                        slot.setHeightplacement(0);
                    }
                    Float width = (1f/(wall.getNbslot()+1f))*(i+1f);
                    slot.setWidthplacement(width);
                    slots.add(slot);
                }
            }
        }
        return slots;
    }
        public void setParameters(){

            Parameter param = new Parameter();
            param.setData("20");
            param.setName("tax");
            param.setType("double");
            parameterDao.insertEntity(param);

            Parameter param1 = new Parameter();
            param1.setData("2 rue de la République");
            param1.setName("CompanyAdresse");
            param1.setType("string");
            parameterDao.insertEntity(param1);

            Parameter param2 = new Parameter();
            param2.setData("Madera");
            param2.setName("CompanyName");
            param2.setType("string");
            parameterDao.insertEntity(param2);

            Parameter param3 = new Parameter();
            param3.setData("31560");
            param3.setName("CompanyZip");
            param3.setType("string");
            parameterDao.insertEntity(param3);

            Parameter param4 = new Parameter();
            param4.setData("Shouflour");
            param4.setName("CompanyVille");
            param4.setType("string");
            parameterDao.insertEntity(param4);

            Parameter param5 = new Parameter();
            param5.setData("0836656565");
            param5.setName("CompanyTel");
            param5.setType("string");
            parameterDao.insertEntity(param5);

            Parameter param6 = new Parameter();
            param6.setData("25.5");
            param6.setName("wallPrice");
            param6.setType("double");
            parameterDao.insertEntity(param6);

        }


}
