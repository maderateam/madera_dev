package fr.cesi.madera.misc;

/**
 * Classe reponsable de la génération du script de création des tables de la base de données cliente
 * Created by Porridge on 31/10/2016.
 */

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

import fr.cesi.madera.entities.Commercial;
import fr.cesi.madera.entities.Entity;
import fr.cesi.madera.entities.SynchronisedEntity;


public class DataBaseCreator<T extends Entity>{

	public static ArrayList<Class> entities;
	public static HashMap<Class,String> matchType = new HashMap<Class,String>();
	public static DataBaseUtils dataUtils = new DataBaseUtils();

	public DataBaseCreator(){}

    /**
     * Cette fonction génère les requêtes de création des tables
     * @return Un tableau contenant toutes les requêtes
     */
	public static ArrayList<String> sqliteQueryGenerator(){
		ArrayList<String> script = new ArrayList<String>();
		StringBuilder sharedTableQuery = new StringBuilder();

		for (Class entity : entities){
            // Liste des attributs de la classe et de ses superclasses
			ArrayList<Field> attributes = new ArrayList<Field>();
			attributes.addAll(dataUtils.getRecFieldFromSuperClass(entity,true));
            // Query des contraintes de type foreign key
            StringBuilder foreignConstraint = new StringBuilder();
            for(int i = 0 ; i < attributes.size() ; i++) {
                ForeignKey foreyKeyAnnot = attributes.get(i).getAnnotation(ForeignKey.class);
                if (foreyKeyAnnot != null) {
                    foreignConstraint.append(" CONSTRAINT fk_" + entity.getSimpleName().toLowerCase() + "_" + foreyKeyAnnot.table());
                    foreignConstraint.append(" FOREIGN KEY(" + attributes.get(i).getName().toLowerCase() + ")");
                    foreignConstraint.append(" REFERENCES " + foreyKeyAnnot.table() + "(_id)");
                }
            }
            // Query de création d'une table
			StringBuilder classQuery = new StringBuilder();
			classQuery.append("CREATE TABLE " + entity.getSimpleName().toLowerCase() + " (");

			for(int i = 0 ; i < attributes.size() ; i++){
				classQuery.append(attributes.get(i).getName().toLowerCase() + " ");
				if (attributes.get(i).getName().toLowerCase().equals("_id")){
					classQuery.append("INTEGER PRIMARY KEY AUTOINCREMENT, ");
				}else{
					classQuery.append(matchType.get(attributes.get(i).getType()));
                    if(i < attributes.size()-1 ||foreignConstraint.length() > 0){
                        classQuery.append(", ");
                    }
				}
			}
            classQuery.append(foreignConstraint);
			classQuery.append(");");
			script.add(classQuery.toString());
		}
		return script;
	}

    /**
     * Fonction de création des vues associés aux table de l'application en fonction de l'id du commercial
     * @param com servant a créer les vues
     * @return la liste des requêtes pour la création des vues
     */
	public ArrayList<String> createSQLiteViewsQuery(Commercial com){
        ArrayList<String> listView = new  ArrayList<String>();
        for (Class thisClass: entities) {
            if(thisClass.getSuperclass().equals(SynchronisedEntity.class)){
                String className = thisClass.getSimpleName().toLowerCase();
                StringBuilder query = new StringBuilder();
                query.append("CREATE VIEW " + getNameOfView(className,com) + " AS ");
                query.append("SELECT * FROM " + className + " ");
                query.append("WHERE idcommercial = " + com.get_id() + " ");
				query.append("AND hidden = 'false'" + ";");
                listView.add(query.toString());
            }
        }
        return listView;
    }

    /**
     * Fonction de génération des requêtes de suppression des vues pour ce commercial
     * @param com
     * @return
     */
    public ArrayList<String> deleteSQLiteViewsQuery(Commercial com){
        ArrayList<String> listView = new  ArrayList<String>();
        for (Class thisClass: entities) {
            if(thisClass.getSuperclass().equals(SynchronisedEntity.class)){
                String className = thisClass.getSimpleName().toLowerCase();
                StringBuilder query = new StringBuilder();
                query.append("DROP VIEW IF EXISTS " + getNameOfView(className,com) + ";");
                listView.add(query.toString());
            }
        }
        return listView;
    }

    /**
     * Renvoit le nom de la vue correspondant à cette classe et à ce commercials
     * @param className nom de la classe correspondant au nom de la table
     * @param com commercial concerné par la vue
     * @return le nom de la vue
     */
    public String getNameOfView(String className, Commercial com){
        String name = com.getFirstname().substring(0,4) + "_" + com.getLastname() + "_" + className;
        return name;
    }
}