package fr.cesi.madera.misc;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import org.apache.commons.lang3.StringUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import fr.cesi.madera.entities.Entity;

/**
 * Created by Doremus on 15/11/2016.
 */

public class DataBaseUtils<T extends Entity> {

    public static HashMap<String,Method> cursMethods = new HashMap<String,Method>();

    public DataBaseUtils() {
        try {
            cursMethods.put("int", Cursor.class.getDeclaredMethod("getInt",int.class));
            cursMethods.put("Interger", Cursor.class.getDeclaredMethod("getInt",int.class));
            cursMethods.put("String", Cursor.class.getDeclaredMethod("getString",int.class));
            cursMethods.put("char", Cursor.class.getDeclaredMethod("getString",int.class));
            cursMethods.put("float", Cursor.class.getDeclaredMethod("getFloat",int.class));
            cursMethods.put("Float", Cursor.class.getDeclaredMethod("getFloat",int.class));
            cursMethods.put("long", Cursor.class.getDeclaredMethod("getLong",int.class));
            cursMethods.put("Long", Cursor.class.getDeclaredMethod("getLong",int.class));
            cursMethods.put("Double", Cursor.class.getDeclaredMethod("getDouble",int.class));
            cursMethods.put("double", Cursor.class.getDeclaredMethod("getDouble",int.class));
            cursMethods.put("Boolean",Cursor.class.getDeclaredMethod("getString",int.class));
            cursMethods.put("boolean",Cursor.class.getDeclaredMethod("getString",int.class));
            cursMethods.put("Timestamp",Cursor.class.getDeclaredMethod("getLong",int.class));
        }catch(NoSuchMethodException e){
            Log.e("madera","Error getting method from Cursor => DatabaseCreator ");
        }
    }

    public ContentValues getValuesFromEntity(Entity entity){

        ContentValues values = new ContentValues();
        ArrayList<Field> attrs = new ArrayList<Field>();
        for (Field field : getRecFieldFromSuperClass(entity.getClass(),false)) {
            Annotation databaseAnnot = field.getAnnotation(DatabaseColumn.class);
            if(databaseAnnot != null){
                attrs.add(field);
            }
        }
        for (int i = 0 ; i < attrs.size(); i++){
            try {
                Object value;
                Method getter = null;
                if(attrs.get(i).getType().equals(boolean.class)||
                        attrs.get(i).getType().equals(Boolean.class)){
                    getter = entity.getClass().getMethod("is" + StringUtils.capitalize(attrs.get(i).getName()));
                } else {
                    getter = entity.getClass().getMethod("get" + StringUtils.capitalize(attrs.get(i).getName()));
                }
                value = getter.invoke(entity);
                if(value!=null) {
                    if(attrs.get(i).getType().equals(Timestamp.class)){
                        values.put(attrs.get(i).getName().toLowerCase(), ((Timestamp)value).getTime());
                    }else {
                        values.put(attrs.get(i).getName().toLowerCase(), value.toString());
                    }
                }else{
                    values.put(attrs.get(i).getName().toLowerCase(), "NULL");
                }
            }catch(NoSuchMethodException e){
                e.printStackTrace();
            } catch (InvocationTargetException e ) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return values;
    }
    public ArrayList<T> cursorToEntities(Class entityClass, Cursor curs) {
        ArrayList<T> entities = new ArrayList<T>();
        ArrayList<Field> fields = new ArrayList<Field>();
        fields.addAll(getRecFieldFromSuperClass(entityClass,true));
        curs.moveToFirst();
        while(!curs.isAfterLast()){
            T entity = null;
            try {
                entity = (T) entityClass.getConstructor().newInstance();
            }catch(InvocationTargetException | NoSuchMethodException | InstantiationException | IllegalAccessException e){
                Log.e("madera",e.getMessage());
            }
            for (Field field : fields) {
                int index = curs.getColumnIndex(field.getName().toLowerCase());
                try {
                    if(field.getType().equals(Boolean.class)||field.getType().equals(boolean.class)) {
                        boolean value =  cursMethods.get(field.getType().getSimpleName()).invoke(curs, index).equals("true");
                        entityClass.getMethod("set" + StringUtils.capitalize(field.getName()), field.getType())
                                .invoke(entity, value);
                    }else if(field.getType().equals(Timestamp.class)){
                        entityClass.getMethod("set" + StringUtils.capitalize(field.getName()), field.getType())
                                .invoke(entity, new Timestamp((long)cursMethods.get(field.getType().getSimpleName()).invoke(curs, index)));
                    }else if(field.getType().equals(char.class)){
                        char value =  cursMethods.get(field.getType().getSimpleName()).invoke(curs, index).toString().charAt(0);
                        entityClass.getMethod("set" + StringUtils.capitalize(field.getName()), field.getType())
                                .invoke(entity, value);
                    }else{
                        entityClass.getMethod("set" + StringUtils.capitalize(field.getName()), field.getType())
                                .invoke(entity, cursMethods.get(field.getType().getSimpleName()).invoke(curs, index));
                    }
                }catch(IllegalAccessException e){
                    Log.e("madera",e.getMessage());
                }catch(InvocationTargetException e){
                    Log.e("madera","Invocation target exception fieldName : " + field.getName() + " type : " + field.getType() + " index : " + index
                            + " entityclass : " + entityClass.getSimpleName() + " cursor column count : " + curs.getColumnCount() + " msg : " +  e.getMessage());
                    e.printStackTrace();
                }catch(NoSuchMethodException e){
                    Log.e("madera","no such method : "+e.getMessage());
                }
            }
            curs.moveToNext();
            entities.add(entity);
        }
        return entities;
    }

    /**
     *
     * @param thisClass
     * @return
     */
    public ArrayList<Field> getRecFieldFromSuperClass(Class thisClass,boolean withId){
        ArrayList<Field> fieldList = new ArrayList<Field>();

        Class superClass = thisClass.getSuperclass();
        if(superClass != null){
            fieldList.addAll(getRecFieldFromSuperClass(superClass,withId));
        }
        for (Field field : thisClass.getDeclaredFields()) {
            Annotation databaseAnnot = field.getAnnotation(DatabaseColumn.class);
            if(databaseAnnot != null || (withId && field.getName().equals("_id"))){
                fieldList.add(field);
            }
        }
        return fieldList;
    }

    public ArrayList<T> sortEntity(ArrayList unsortedList){

        Collections.sort(unsortedList, new Comparator<Entity>() {
            @Override
            public int compare(Entity entity1, Entity entity2) {
                return entity1.compareTo(entity2);
            }
        });
        return unsortedList;
    }
}
