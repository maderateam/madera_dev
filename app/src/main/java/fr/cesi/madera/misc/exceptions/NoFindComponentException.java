package fr.cesi.madera.misc.exceptions;

/**
 * Created by Remi on 02/01/2017.
 */
public class NoFindComponentException extends Exception {
    public NoFindComponentException() {
    }

    public NoFindComponentException(String detailMessage) {
        super(detailMessage);
    }
}
