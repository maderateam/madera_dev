package fr.cesi.madera.misc.exceptions;

/**
 * Created by Joshua on 04/10/2016.
 */

public class AlreadySynchronisedException extends Exception {

    public AlreadySynchronisedException() {}

    public AlreadySynchronisedException(String detailMessage) {
        super(detailMessage);
    }
}
