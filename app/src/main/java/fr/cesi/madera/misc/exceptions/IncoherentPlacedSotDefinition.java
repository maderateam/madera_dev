package fr.cesi.madera.misc.exceptions;

/**
 * Created by Doremus on 05/03/2017.
 */

public class IncoherentPlacedSotDefinition extends Exception {
    public IncoherentPlacedSotDefinition() {
    }

    public IncoherentPlacedSotDefinition(String detailMessage) {
        super(detailMessage);
    }
}
