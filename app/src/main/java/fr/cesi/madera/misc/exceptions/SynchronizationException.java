package fr.cesi.madera.misc.exceptions;

/**
 * Created by Doremus on 04/04/2017.
 */

public class SynchronizationException extends Exception {
    public SynchronizationException() {
    }

    public SynchronizationException(String detailMessage) {
        super(detailMessage);
    }
}
