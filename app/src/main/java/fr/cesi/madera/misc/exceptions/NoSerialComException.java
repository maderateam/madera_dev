package fr.cesi.madera.misc.exceptions;

/**
 * Created by Doremus on 18/04/2017.
 */

public class NoSerialComException extends Exception {
    public NoSerialComException() {
    }

    public NoSerialComException(String detailMessage) {
        super(detailMessage);
    }
}
