package fr.cesi.madera.misc;

import java.text.ParseException;
import java.util.ArrayList;

import fr.cesi.madera.entities.Component;

/**
 * Classe utilitaire permettant d'extraire les coordonnées des dalles de sol
 * à partir d'une chaine de caractère
 */

public class CoordinateExtractor {

    /**
     * Renvoi un tableau de couple de coordonnées à partir d'un composant sol
     * @param floor composant dont on veut connaitre les coordonnées des dalles constitutives
     * @return la liste des couples de coordonnées des dalles pour ce sol
     * @throws ParseException si une erreur dans la chaine est présent
     */
    public static ArrayList<ArrayList<Integer>> getCoordsFromFloor(Component floor) throws ParseException {
        if(floor == null || floor.getCoordinates().equals("")){
            throw new ParseException("Floor Parts coordinates doesn't exists",0);
        }
        String textCoords = floor.getCoordinates();
        String[] tokens = textCoords.split(":");
        ArrayList<ArrayList<Integer>> coordinates = new ArrayList<>();
        for (int i = 0 ; i < tokens.length ; i++){
            ArrayList<Integer> coord = new ArrayList<>();
            String[] coordText = tokens[i].substring(1,tokens[i].length()-1).split(",");
            if(coordText.length != 3){
                throw new ParseException("Floor Parts coordinates doesn't exists",i);
            }
            coord.add(Integer.parseInt(coordText[0]));
            coord.add(Integer.parseInt(coordText[1]));
            coord.add(Integer.parseInt(coordText[2]));
            coordinates.add(coord);
        }
        return coordinates;
    }
}
