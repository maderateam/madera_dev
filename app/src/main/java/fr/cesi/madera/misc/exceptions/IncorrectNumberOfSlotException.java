package fr.cesi.madera.misc.exceptions;

/**
 * Created by Remi on 02/01/2017.
 */
public class IncorrectNumberOfSlotException extends Exception {

    public IncorrectNumberOfSlotException() {
    }

    public IncorrectNumberOfSlotException(String detailMessage) {
        super(detailMessage);
    }
}
