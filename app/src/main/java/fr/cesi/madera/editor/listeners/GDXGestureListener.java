package fr.cesi.madera.editor.listeners;

import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;

import fr.cesi.libgdx.entities.models.MaderaModel;
import fr.cesi.madera.services.EditorAService;

/**
 * Cette classe fait le lien entre l'EventDispatcher et le TouchModelFinder
 * Elle récupère les coordonnées de clics à l'écran
 * Created by Doremus on 04/02/2017.
 */

public class GDXGestureListener implements GestureDetector.GestureListener {

    private EditorAService aService;
    private EventDispatcher eDispatcher;
    private TouchedModelFinder touchedModelFinder;

    public GDXGestureListener(EditorAService aService) {
        this.aService = aService;
        this.eDispatcher = new EventDispatcher(aService);
        this.touchedModelFinder = new TouchedModelFinder(aService);

    }

    @Override
    public boolean touchDown(float screenX, float screenY, int pointer, int button) {
        MaderaModel modeli = touchedModelFinder.getTouchedModel((int) screenX, (int) screenY);
        if(aService.editorA.state == aService.editorA.COVERING_STATE){
            if (modeli != null) {
                eDispatcher.selectCoveringForModel(modeli);
            }
        }else if(aService.editorA.state != aService.editorA.LOCK_STATE) {
            if (modeli == null) {
                aService.displayPlanPartsInLog();
            } else {
                eDispatcher.dispatchEventByModelType(modeli);
            }
        }
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        if (aService.editorA.state != aService.editorA.LOCK_STATE) {
            MaderaModel modeli = touchedModelFinder.getTouchedModel((int) x, (int) y);
            if (modeli != null) {
                eDispatcher.dispatchLongEventByModelType(modeli);
            }
        }
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void pinchStop() {

    }
}
