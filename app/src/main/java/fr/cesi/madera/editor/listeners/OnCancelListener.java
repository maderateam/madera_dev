package fr.cesi.madera.editor.listeners;

import android.view.View;

import fr.cesi.madera.services.EditorAService;
import fr.cesi.madera.views.activities.EditorActivity;

/**
 * Cette reçoit les évènements du clic sur le bouton annuler de l'éditeur
 * et appelle différentes méthodes en fonction de l'état de l'éditeur
 * Created by Doremus on 04/02/2017.
 */

public class OnCancelListener implements View.OnClickListener, View.OnLongClickListener {
    private EditorActivity editorA;
    private EditorAService aService;

    public OnCancelListener(EditorActivity editorA, EditorAService aService) {
        this.editorA = editorA;
        this.aService = aService;
    }

    @Override
    public void onClick(View v) {
        switch (editorA.state){
            case EditorActivity.FLOOR_STATE:
                editorA.state = EditorActivity.EMPTY_STATE;
                aService.clearEditor();
                editorA.showFloors();
                break;
            case EditorActivity.DIVIDER_STATE:
                editorA.state = EditorActivity.SELECTION_STATE;
                aService.unSelectComponent();
                editorA.clearListView();
            case EditorActivity.WALL_STATE:
                editorA.state = EditorActivity.SELECTION_STATE;
                aService.unSelectComponent();
                editorA.clearListView();
                break;
            case EditorActivity.CARPENTRIES_STATE:
                editorA.state = EditorActivity.SELECTION_STATE;
                aService.unSelectComponent();
                editorA.clearListView();
                break;
            case EditorActivity.COVERING_STATE:
                aService.unSelectComponent();
                aService.resetCoveringLayout();
                break;
            case EditorActivity.SELECTION_STATE:
                aService.unSelectComponent();
            default:
        }
    }

    @Override
    public boolean onLongClick(View v) {
        aService.deletePlan();
        return false;
    }
}
