package fr.cesi.madera.editor.listeners;

import android.os.Handler;

import com.badlogic.gdx.math.Vector3;

import fr.cesi.libgdx.entities.models.Carpentry;
import fr.cesi.libgdx.entities.models.Divider;
import fr.cesi.libgdx.entities.models.FloorTBox;
import fr.cesi.libgdx.entities.models.MaderaModel;
import fr.cesi.libgdx.entities.models.Slab;
import fr.cesi.libgdx.entities.models.Wall;
import fr.cesi.libgdx.entities.models.WallTBox;
import fr.cesi.madera.misc.exceptions.IncorrectNumberOfPCException;
import fr.cesi.madera.misc.exceptions.IncorrectNumberOfSlotException;
import fr.cesi.madera.services.EditorAService;

/**
 * Récupère l'évènement du toucher sur le panel de l'éditeur
 * Created by Doremus on 04/02/2017.
 */

public class EventDispatcher {
    private EditorAService aService;

    public EventDispatcher(EditorAService aService) {
        this.aService = aService;
    }

    /**
     * Execute différentes action en fonction du type du modèle touché par l'utilisateur
     * @param modeli modèle touché par l'utilisateur
     */
    public void dispatchEventByModelType(final MaderaModel modeli) {
        // Initialisation du mécanisme de rattachement du Thread au Thread main d'android
        Handler mainHandler = new Handler(aService.editorA.getMainLooper());
        if (modeli.getClass().equals(FloorTBox.class)) {
            onFloorTBoxClicked(modeli, mainHandler);
        } else if (modeli.getClass().equals(Wall.class)) {
            onWallClicked(modeli, mainHandler);
        } else if (modeli.getClass().equals(WallTBox.class)) {
            onWallTBoxClicked(modeli, mainHandler);
        } else if (modeli.getClass().equals(Carpentry.class)) {
            onCarpentryTouched(modeli, mainHandler);
        } else if (modeli.getClass().equals(Divider.class)) {
            onDividerTouched(modeli, mainHandler);
        }
    }

    /**
     * Réagit au clic sur un FloorTouchBox sur l'éditeur 3D
     * - Créé un PlacedSlot pour acueillir le divider
     * - Lie le PlacedSlot au sol
     * - Affiche la liste des Divider sur l'IHM Android
     *
     * @param modeli
     * @param mainHandler
     */
    private void onFloorTBoxClicked(final MaderaModel modeli, Handler mainHandler) {
        aService.selectedPCompo = aService.getGdxBridge().getPCompoByModel(modeli);
        // Si un model est déjà selectionné
        aService.editor.unSelectModel();
        //aService.selectedPCompo = null;
        // On passe l'éditeur en état DIVIDER
        aService.editorA.state = aService.editorA.DIVIDER_STATE;
        // On sélectionne le DIVIDER touché
        aService.editor.selectModel(modeli);
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                // On affiche la liste des divider sur l'activity
                aService.editorA.showDividers();
            }
        };
        mainHandler.post(myRunnable);
    }

    /**
     * Réagit au clic sur un mur sur l'éditeur 3D
     * - On selectionne le modèle
     * - On affiche la liste des murs
     *
     * @param modeli
     * @param mainHandler
     */
    public void onWallClicked(final MaderaModel modeli, Handler mainHandler) {
        // Si un model est déjà selectionné
        aService.selectedPSlot = null;
        aService.editor.selectModel(modeli);
        // On passe l'éditeur en état WALL
        aService.editorA.state = aService.editorA.WALL_STATE;
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                // On affiche la liste des divider sur l'activity
                aService.editorA.showWalls();
            }
        };
        mainHandler.post(myRunnable);
    }

    /**
     * Selectionne un slot quand on le touche
     * et affiche la liste des Carpentry compatibles
     * @param modeli
     * @param mainHandler
     */
    public void onWallTBoxClicked(final MaderaModel modeli, final Handler mainHandler) {
        // On passe l'éditeur en état CARPENTRIES
        aService.editorA.state = aService.editorA.CARPENTRIES_STATE;
        // On sélectionne le DIVIDER touché
        aService.editor.selectModel(modeli);
        try {
            aService.getGdxBridge().selectPlacedSlot(modeli);
        } catch (IncorrectNumberOfSlotException e) {
            e.printStackTrace();
        }
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                // On affiche la liste des divider sur l'activity
                aService.editorA.showCarpentries(modeli.getCenter());
            }
        };
        mainHandler.post(myRunnable);
    }

    /**
     * Sélectionne un carpentry quand on le touche
     * @param modeli modèle du carpentry à selectionner
     * @param mainHandler
     */
    private void onCarpentryTouched(MaderaModel modeli, Handler mainHandler) {
        // On sélectionne le Carpentry touché
        aService.editor.selectModel(modeli);
        // On récupère le PSLot lié à ce modèle et on le sélectionne
        try {
            aService.getGdxBridge().selectPlacedSlot(modeli);
        } catch (IncorrectNumberOfSlotException e) {
            e.printStackTrace();
        }

        try {
            aService.getGdxBridge().selectPCompoCarpByModel(modeli, aService.carpentries);
        } catch (IncorrectNumberOfPCException e) {
            e.printStackTrace();
        }
        // On récupère son centre
        final Vector3 centerOFPSlot = new Vector3(aService.selectedPSlot.getPosX(), aService.selectedPSlot.getPosY(), aService.selectedPSlot.getPosZ());
        // On affiche les carpentries compatibles avec ce Pslot
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                // On affiche la liste des divider sur l'activity
                aService.editorA.clearListView();
                aService.editorA.showCarpentries(centerOFPSlot);
            }
        };
        mainHandler.post(myRunnable);
    }

    /**
     * Sélectionne un divider quand on le touche
     * @param modeli modèle du divider touché
     * @param mainHandler
     */
    private void onDividerTouched(MaderaModel modeli, Handler mainHandler) {
        aService.editorA.state = aService.editorA.DIVIDER_STATE;
        // On sélectionne le Divider touché
        aService.editor.selectModel(modeli);
        // On récupère le PSLot lié à ce modèle et on le sélectionne
        aService.selectedPCompo = aService.getGdxBridge().getPCompoByModel(modeli);
        // On affiche les carpentries compatibles avec ce Pslot
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                // On affiche la liste des divider sur l'activity
                aService.editorA.clearListView();
                aService.editorA.showDividers();
            }
        };
        mainHandler.post(myRunnable);
    }

    /**
     * Réagit au clic long d'un modèle sur l'éditeur pour le supprimer
     * @param modeli
     */
    public void dispatchLongEventByModelType(MaderaModel modeli) {
        // Initialisation du mécanisme de rattachement du Thread au Thread main d'android
        Handler mainHandler = new Handler(aService.editorA.getMainLooper());
        if (modeli.getClass().equals(Carpentry.class)) {
            onCarpentryPress(modeli, mainHandler);
        } else if (modeli.getClass().equals(Divider.class)) {
            onDividerPress(modeli, mainHandler);
        }
    }

    /**
     * Supprime un divider quand on effectue un appuie long dessus
     * @param modeli modèle du divider
     * @param mainHandler
     */
    private void onDividerPress(final MaderaModel modeli, Handler mainHandler) {
        aService.getGdxBridge().removeDivider(modeli, aService.dividers);
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                aService.editorA.clearListView();
                aService.editorA.state = aService.editorA.SELECTION_STATE;
            }
        };
        mainHandler.post(myRunnable);
    }

    /**
     * Supprime un carpentry quand on effectue un appui long dessus
     * @param modeli modèle du carpentry
     * @param mainHandler
     */
    private void onCarpentryPress(final MaderaModel modeli, Handler mainHandler) {
        aService.getGdxBridge().removeCarpentry(modeli, aService.carpentries);
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                aService.editorA.clearListView();
                aService.editorA.state = aService.editorA.SELECTION_STATE;
            }
        };
        mainHandler.post(myRunnable);
    }

    /**
     * Réagit au clic sur un composant quand on a actier le mode "covering"
     * @param modeli modèle touché en mode covering
     */
    public void selectCoveringForModel(final MaderaModel modeli) {
        Handler mainHandler = new Handler(aService.editorA.getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                if (modeli instanceof Wall) {
                    aService.editorA.showCoverings();
                    aService.editor.selectModel(modeli);
                    aService.selectedPCompo = aService.getGdxBridge().getPCompoByModel(modeli);
                    aService.displayCoveringOption(aService.WALL_COVERING);
                } else if (modeli instanceof Slab) {
                    aService.editorA.showCoverings();
                    aService.editor.selectModel(modeli);
                    aService.selectedPCompo = aService.getGdxBridge().getPCompoByModel(modeli);
                    aService.displayCoveringOption(aService.SLAB_COVERING);
                }
            }
        };
        mainHandler.post(myRunnable);
    }
}
