package fr.cesi.madera.editor.listeners;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.utils.Array;

import fr.cesi.libgdx.entities.models.MaderaModel;
import fr.cesi.madera.services.EditorAService;
import fr.cesi.madera.views.activities.EditorActivity;

/**
 * Permet de trouver quel modèle 3D a été sélectionné en fonction des coordonnées du clic sur l'écran
 * Created by Doremus on 04/02/2017.
 */

public class TouchedModelFinder {

    private EditorAService aService;

    public TouchedModelFinder(EditorAService aService) {
        this.aService = aService;
    }

    /**
     * Renvoi le modèle 3D touché par l'utilisaeur
     * - Calcule la droite de projection des coordonnées du clic utilisateur dans le monde 3D de l'éditeur
     * - Détecte la colission des différents modèles 3D avec cette droite
     * - Renvoi le modèle qui coupe la droite au premier plan
     * @param screenX coordonnées X du click utilisateur sur l'écran
     * @param screenY coordonnées Y du click utilisateur sur l'écran
     * @return le modèle touché par l'utilisateur
     */
    public MaderaModel getTouchedModel(int screenX, int screenY) {
        //editor.getCamera().update();
        Ray ray = aService.editor.getCamera().getPickRay(screenX, screenY);
        double distance = -1;
        int result = -1;
        Vector3 position = new Vector3();
        // On filtre les modèles en fonction de l'état afin, d'accélerer la détection des collisions
        // et d'éviter d'éventuelles erreur dues à des imprécisions
        Array filteredModels = filterModelsByState();
        for (int i = 0; i < filteredModels.size; i++) {
            final MaderaModel instance = (MaderaModel) filteredModels.get(i);
            instance.transform.getTranslation(position);
            position.add(instance.getCenter());
            double dist2 = ray.origin.dst2(position);
            if (distance > 0f && dist2 > distance) continue;
            BoundingBox box = instance.getBox();
            if (Intersector.intersectRayBoundsFast(ray, box)) {
                result = i;
                distance = dist2;
            }
        }
        if (result == -1) {
            return null;
        }
        return (MaderaModel) filteredModels.get(result);
    }

    /**
     * Filtre la liste des modèles en fonction de l'état de l'éditeur.
     * @return la liste des modèles filtrés
     */
    private Array<MaderaModel> filterModelsByState(){
        Array filteredModels = new Array<>();
        switch (aService.editorA.state) {
            case EditorActivity.SELECTION_STATE:
                filteredModels.addAll(aService.editor.walls);
                filteredModels.addAll(aService.editor.dividers);
                filteredModels.addAll(aService.editor.floorTouchBoxes);
                break;
            case EditorActivity.WALL_STATE:
                filteredModels.addAll(aService.editor.getAdjoiningSlotsOrConponents());
                break;
            case EditorActivity.CARPENTRIES_STATE:
                // Récupération des autres slot attenant au même mur que le slot sélectionné
                filteredModels.addAll(aService.editor.getAdjoiningSlotsOrConponents());
                break;
            case EditorActivity.COVERING_STATE:
                filteredModels.addAll(aService.editor.walls);
                filteredModels.addAll(aService.editor.floorParts);
                break;
            default:
                filteredModels = aService.editor.getAllModels();
        }
        return  filteredModels;
    }
}
