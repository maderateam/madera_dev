package fr.cesi.madera.editor;

import android.view.View;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

import fr.cesi.libgdx.Editor3D;
import fr.cesi.madera.editor.listeners.CameraOnClickListener;
import fr.cesi.madera.entities.Component;
import fr.cesi.madera.entities.Covering;
import fr.cesi.madera.entities.Gamme;
import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.Project;
import fr.cesi.madera.services.ClientService;
import fr.cesi.madera.services.CoveringService;
import fr.cesi.madera.services.DiscountService;
import fr.cesi.madera.services.EditorAService;
import fr.cesi.madera.services.GammeService;
import fr.cesi.madera.services.PlanService;
import fr.cesi.madera.services.ProjectService;

/**
 * Created by Doremus on 03/04/2017.
 */

public abstract class EditorApplication extends AndroidApplication {

    // Etat d'vancement du plan
    public static final int EMPTY_STATE = -1;
    public static final int FLOOR_STATE = 0;
    public static final int DIVIDER_STATE = 1;
    public static final int WALL_STATE = 2;
    public static final int CARPENTRIES_STATE = 3;
    public static final int SELECTION_STATE = 4;
    public static final int LOCK_STATE = 5;
    public static final int COVERING_STATE = 6;

    public int state;

    public ArrayList<Component> floors;
    public ArrayList<Component> dividers;
    public ArrayList<Component> walls;
    public ArrayList<Component> carpentries;
    public ArrayList<Gamme> gammes;
    public ArrayList<Covering> coverings;

    /**
     * Mode d'affichage des infos des composants
     */
    public boolean modeInfoON = false;
    /**
     * Services appelés par l'éditeur
     */
    public EditorAService aService;

    protected PlanService planService;
    protected GammeService gammeService;
    protected DiscountService discService;
    protected ProjectService projectService;
    protected ClientService clientService;
    protected CoveringService coveringService;
    /**
     * Editeur libGDX
     */
    protected Editor3D editor;
    /**
     * Plan si il est chargé au lancement de l'éditeur
     */
    protected Plan plan;
    /**
     * Gamme sélectionné pour le filtre
     */
    public Gamme selectedGamme;

    protected Project project;
    protected CameraOnClickListener cameraListener;

    /**
     * Initialise l'éditeur 3D
     */
    public abstract void initializeEditor();

    /**
     * Affiche le layout contenant les otpion pour le covering intérieur et extérieur
     */
    public abstract void  displayWallCoveringOption();

    /**
     * MAsque le layout contenant les option pour le covering intérieur et extérieur
     */
    public abstract void displaySlabCoveringOption();

    /**
     * Remet les switch de layout de covering option à zero
     */
    public abstract void resetCoveringLayout();

    /**
     * Affiche le popup de confirmation de suppression de tous les évènement du plan
     */
    public abstract void displayDeleteConfirmation();

    /**
     * Affiche la liste des component sol
     */
    public abstract void showFloors();

    /**
     * Renvoi true si le switch de coveing in est sur ON
     */
    public abstract boolean isCoveringIn();

    /**
     * Renvoi true si le switch de coveing out est sur ON
     */
    public abstract boolean isCovringOut();

    /**
     * Active le mode de visualisation
     */
    public abstract void setVisualisationModeON();

    /**
     * Renvoi un des widget de caméra
     * @return
     */
    public abstract View getArrowDown();

    /**
     * Renvoi un des widget de caméra
     * @return
     */
    public abstract View getArrowUp();

    /**
     * Renvoi un des widget de caméra
     * @return
     */
    public abstract View getArrowLeft();

    /**
     * Renvoi un des widget de caméra
     * @return
     */
    public abstract View getArrowRight();

    /**
     * Renvoi un des widget de caméra
     * @return
     */
    public abstract View getCancelCam();

    /**
     * Renvoi un des widget de caméra
     * @return
     */
    public abstract void clearListView();

    /**
     * Affiche la liste des dividers
     */
    public abstract void showDividers();

    /**
     * Affiche la liste des murs
     */
    public abstract void showWalls();

    /**
     * Affiche la liste des carpentries
     * @param center
     */
    public abstract void showCarpentries(Vector3 center);

    /**
     * Affiche la liste des covering
     */
    public abstract void showCoverings();

    /**
     * Active le mode de visualisation après sauvegarde et avant fermeture de l'éditeur
     */
    public abstract void setEndingVisualisationModeON();

    public abstract void setProject(Project proj);

    public abstract void createPlan(boolean b);
}
