package fr.cesi.madera.editor.listeners;

import android.view.View;
import android.widget.AdapterView;

import fr.cesi.madera.entities.Component;
import fr.cesi.madera.entities.Covering;
import fr.cesi.madera.misc.exceptions.FloorPartsException;
import fr.cesi.madera.services.EditorAService;
import fr.cesi.madera.views.activities.EditorActivity;

/**
 * Cette classe écoute les clics sur la liste verticale des component de l'éditeur
 * et dispatche les méthodes en fonction de l'état de l'éditeur
 * Created by Doremus on 04/02/2017.
 */

public class ComponentOnItemClickListener implements AdapterView.OnItemClickListener {
    private EditorActivity editorA;
    private EditorAService aService;

    public ComponentOnItemClickListener(EditorActivity editorA,EditorAService aService) {
        this.editorA = editorA;
        this.aService = aService;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Component compo;
        // On déduit quel type de composants sont dans la liste en fonction de l'état de l'éditeur
        switch (editorA.state){
            case EditorActivity.EMPTY_STATE:
                compo = editorA.floors.get(position);
                try {
                    aService.createFloor(compo);
                    editorA.clearListView();
                    editorA.state = EditorActivity.SELECTION_STATE;
                } catch (FloorPartsException e) {
                    e.printStackTrace();
                }
                break;
            case EditorActivity.FLOOR_STATE:
                compo = editorA.floors.get(position);
                try {
                    aService.createFloor(compo);
                    editorA.clearListView();
                    editorA.state = EditorActivity.SELECTION_STATE;
                } catch (FloorPartsException e) {
                    e.printStackTrace();
                }
                break;
            case EditorActivity.DIVIDER_STATE:
                Component divider = editorA.dividers.get(position);
                if(!editorA.modeInfoON) {
                    aService.createDivider(divider);
                    editorA.state = EditorActivity.SELECTION_STATE;
                    editorA.clearListView();
                }else{
                    editorA.showComponentInfos(divider);
                }
                break;
            case EditorActivity.WALL_STATE:
                Component wall = editorA.walls.get(position);
                if(!editorA.modeInfoON) {
                    try {
                        aService.createSlotOnWall(wall);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    editorA.showComponentInfos(wall);
                }
                break;
            case EditorActivity.CARPENTRIES_STATE:
                Component carpentry = editorA.carpentries.get(position);
                if(!editorA.modeInfoON) {
                    aService.createCarpentryOnSlot(carpentry);
                }else{
                    editorA.showComponentInfos(carpentry);
                }
                break;
            case  EditorActivity.COVERING_STATE:
                Covering cov = editorA.coverings.get(position);
                aService.applyCoveringOnModels(cov);
                aService.editorA.clearListView();
                break;
        }
    }
}
