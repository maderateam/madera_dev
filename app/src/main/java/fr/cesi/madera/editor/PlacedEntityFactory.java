package fr.cesi.madera.editor;

import android.content.Context;
import android.content.SharedPreferences;

import fr.cesi.madera.dao.daoPlacedComponent.PlacedComponentDao;
import fr.cesi.madera.dao.daoPlacedSlot.PlacedSlotDao;
import fr.cesi.madera.entities.Component;
import fr.cesi.madera.entities.PlacedComponent;
import fr.cesi.madera.entities.PlacedSlot;

/**
 * Classe responsable de la création des PC ou des PS
 * Created by Doremus on 05/03/2017.
 */

public class PlacedEntityFactory {
    private static PlacedEntityFactory singleton;
    private Context ctx;
    private static PlacedComponentDao pcDao;
    private static PlacedSlotDao psDao;
    private static long pcTempID = 0;
    private static long psTempID = 0;

    private PlacedEntityFactory(){}

    private PlacedEntityFactory(Context ctx) {
        this.ctx = ctx;
        pcDao = new PlacedComponentDao(ctx);
        psDao = new PlacedSlotDao(ctx);
    }

    /**
     * Création d'un placeComponent à partir du ocmposant en argument
     * @param compo
     * @return
     */
    public PlacedComponent createPCFromComponent(Component compo){
        PlacedComponent pc = new PlacedComponent();
        pc.setHidden(false);
        SharedPreferences sharedpreferences = ctx.getSharedPreferences("settings", Context.MODE_PRIVATE);
        long idCom = sharedpreferences.getLong("idCom",0);
        pc.setIdcommercial(idCom);
        if(compo != null) {
            pc.setIdcomponent(compo.get_id());
        }
        pc.set_id(getPCTempID());
        return pc;
    }

    /**
     * Création d'un PlacedSlot à partir du PlacedComponent parent
     * @param parentPCompo Component parent
     * @return le PlacedSlot créé
     */
    public PlacedSlot createPSFromPComponent(PlacedComponent parentPCompo){
        PlacedSlot ps = new PlacedSlot();
        ps.setIdparentpcomponent(parentPCompo.get_id());
        ps.setHidden(false);
        SharedPreferences sharedpreferences = ctx.getSharedPreferences("settings", Context.MODE_PRIVATE);
        long idCom = sharedpreferences.getLong("idCom",0);
        ps.setIdcommercial(idCom);
        ps.set_id(getPSTempID());
        return ps;
    }

    /**
     * Renvoi une instance unique de la factory de PC et PS et initialise les id temporaires
     * @param ctx
     * @return
     */
    public static PlacedEntityFactory getInstance(Context ctx){
        if(singleton == null){
            singleton = new PlacedEntityFactory(ctx);
            long tempIdStart = pcDao.getLastIdForEntity(PlacedComponent.class);
            pcTempID = tempIdStart;
            tempIdStart = psDao.getLastIdForEntity(PlacedSlot.class);
            psTempID = tempIdStart;
        }
        return singleton;
    }

    /**
     * Renvoi l'id temporaire des PC généré et incrémenté pour la création de l'arbre des dépendances
     * entre PC et PS avant validation du plan et sauvegarde en BD
     * @return
     */
    public long getPCTempID() {
        pcTempID += 1;
        return pcTempID;
    }

    /**
     * Renvoi l'id temporaire de PS généré et incrémenté pour la création de l'arbre des dépendances
     * entre PC et PS avant validation du plan et sauvegarde en BD
     * @return
     */
    public long getPSTempID() {
        psTempID+=1;
        return psTempID;
    }

    public void resetIds() {
        long tempIdStart = pcDao.getLastIdForEntity(PlacedComponent.class);
        pcTempID = tempIdStart;
        tempIdStart = psDao.getLastIdForEntity(PlacedSlot.class);
        psTempID = tempIdStart;
    }
}
