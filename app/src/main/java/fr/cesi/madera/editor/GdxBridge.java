package fr.cesi.madera.editor;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;
import java.util.Iterator;

import fr.cesi.libgdx.Editor3D;
import fr.cesi.libgdx.entities.models.MaderaModel;
import fr.cesi.libgdx.entities.models.Slab;
import fr.cesi.libgdx.entities.models.Wall;
import fr.cesi.libgdx.misc.exceptions.SelectedModelException;
import fr.cesi.madera.entities.Component;
import fr.cesi.madera.entities.Entity;
import fr.cesi.madera.entities.PlacedComponent;
import fr.cesi.madera.entities.PlacedSlot;
import fr.cesi.madera.misc.exceptions.IncorrectNumberOfPCException;
import fr.cesi.madera.misc.exceptions.IncorrectNumberOfSlotException;
import fr.cesi.madera.misc.exceptions.NoPSlotFoundException;
import fr.cesi.madera.services.ComponentService;
import fr.cesi.madera.services.EditorAService;

/**
 * Cette classe fait le  pont entre le module LibGDX et le module Android
 * Elle permet de lancer les méthode libgdx et de mettre en place un système d'attente sur le Thread main
 * Created by Doremus on 05/03/2017.
 */

public class GdxBridge {
    public static final int X_AXIS = 0;
    public static final int Y_AXIS = 1;
    public static final int Z_AXIS = 2;

    public static final int PC_DIVIDER = 0;
    public static final int PC_WALL = 1;
    public static final int PS_WALL = 3;

    private Editor3D editor;
    private EditorAService aService;
    private Context ctx;

    public GdxBridge(Editor3D editor, EditorAService aService, Context ctx) {
        this.editor = editor;
        this.aService = aService;
        this.ctx = ctx;
    }

    /**
     * Appel de la fonction de dessin d'un sol sur l'éditeur 3D
     * @param coordinates Coordonnées des dalles composant le sol
     * @param floorCoveringPath Chemin vers le fichier de texture du covering du sol
     * @param eFactory Classe de création des PC et PS
     * @return un tableau contenant tous les PlcedComponent créés
     */
    public ArrayList<PlacedComponent> call_drawFloor(final ArrayList<ArrayList<Integer>> coordinates, final String floorCoveringPath, final PlacedEntityFactory eFactory) {
        final ArrayList<PlacedComponent> wallsPC = new ArrayList<>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        for (ArrayList<Integer> coord : coordinates) {
                            editor.drawFloorPart(coord.get(0), coord.get(1), coord.get(2), floorCoveringPath);
                        }
                        Array<Wall> walls = editor.drawWallParts();
                        if(eFactory != null) {
                            for (int i = 0 ; i < walls.size ; i++) {
                                PlacedComponent pcWall = eFactory.createPCFromComponent(null);
                                pcWall.setPosX(walls.get(i).getCenter().x);
                                pcWall.setPosY(walls.get(i).getCenter().y);
                                pcWall.setPosZ(walls.get(i).getCenter().z);
                                pcWall.setLength(editor.e3DService.getLenghtOfModel(walls.get(i)));
                                wallsPC.add(pcWall);
                            }
                        }
                        synchronized (aService.lock2) {
                            aService.lock2.notify();
                        }
                    }
                });
                synchronized (aService.lock2) {
                    try {
                        aService.lock2.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                synchronized (aService.getLock1()) {
                    aService.getLock1().notify();
                }
            }
        }).start();
        synchronized (aService.getLock1()) {
            try {
                aService.getLock1().wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return wallsPC;
    }

    /**
     * Appel de la fonction de l'éditeur pour déssiner un divider
     * @param modelPath chemin vers le fichier de modèle du divider
     * @param pc PlacedComponent correspondant au divider
     * @return le PlacedComponent dont on a initialiser la position
     */
    public PlacedComponent call_drawDivider(final String modelPath, final PlacedComponent pc) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        Vector3 center = editor.drawDivider(modelPath);
                        pc.setPosX(center.x);
                        pc.setPosY(center.y);
                        pc.setPosZ(center.z);
                        aService.selectedPCompo = null;
                        synchronized (aService.lock2) {
                            aService.lock2.notify();
                        }
                    }
                });
                synchronized (aService.lock2) {
                    try {
                        aService.lock2.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                synchronized (aService.getLock1()) {
                    aService.getLock1().notify();
                }
            }
        }).start();
        synchronized (aService.getLock1()) {
            try {
                aService.getLock1().wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return pc;
    }

    /**
     * Appel de la méthode de l'éditeur pour le dessin des WallTouchBox
     * @param widthplacement placement relatif du WallTouchBox par rapport à la logueur du mur
     * @param pSlot PlacedSlot dont on veut initialiser la position
     * @return le PlacedSlot dont on aura initialisé la position
     */
    public PlacedSlot call_drawTouchBoxOnWall(final float widthplacement, final PlacedSlot pSlot) {
        // Main
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        Vector3 center = editor.drawTouchBoxOnWall(widthplacement);
                        pSlot.setPosX(center.x);
                        pSlot.setPosY(center.y);
                        pSlot.setPosZ(center.z);
                        synchronized (aService.lock2) {
                            aService.lock2.notify();
                        }
                    }
                });
                synchronized (aService.lock2) {
                    try {
                        aService.lock2.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                synchronized (aService.getLock1()) {
                    aService.getLock1().notify();
                }
            }
        }).start();
        synchronized (aService.getLock1()) {
            try {
                aService.getLock1().wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Log.i("madera", "DrawTOnWall pSlot coord x:" + pSlot.getPosX() + " y:" + pSlot.getPosY() + " z:" + pSlot.getPosZ());
        return pSlot;
    }

    /**
     * Appel de la méthode de suppression des éléments présent sur le mur sélectionné
     */
    public void call_deleteModelsOnSelectedWall() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            editor.deleteModelsOnSelectedWall();
                        } catch (SelectedModelException e) {
                            e.printStackTrace();
                        }
                        synchronized (aService.lock2) {
                            aService.lock2.notify();
                        }
                    }
                });
                synchronized (aService.lock2) {
                    try {
                        aService.lock2.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                synchronized (aService.getLock1()) {
                    aService.getLock1().notify();
                }
            }
        }).start();
        synchronized (aService.getLock1()) {
            try {
                aService.getLock1().wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Appel de la méthode de l'éditeur pour le dessin d'un carpentry
     * @param modelPath Chemin vers le fichier de mofèle du carpentry
     * @param heightPlacement Hauteur à laquell on va placer le carpentry
     * @param pcCarpentry PlacedComponent correspondant au Carpentry
     * @param withAdd Détermine si on doit ajouter le PlacedCompoent à la
     *                liste des PC carpentry ou si celui si y existe déjà et que l'on le met juste à jour
     */
    public void call_drawCarpentry(final String modelPath, final float heightPlacement, final PlacedComponent pcCarpentry, final boolean withAdd) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        // On demande à l'éditeur 3D de déssiner le carpentry et on récupère son centre
                        Vector3 center = editor.drawCarpentry(modelPath, heightPlacement);
                        if (center != null) {
                            pcCarpentry.setPosX(center.x);
                            pcCarpentry.setPosY(center.y);
                            pcCarpentry.setPosZ(center.z);
                            if (withAdd) {
                                aService.selectedPSlot.setIdchildpcomponent(pcCarpentry.get_id());
                                aService.carpentries.add(pcCarpentry);
                            }
                        } else {
                            // Si non , on affiche un message, via un Toast à lancer sur le thread UI Android
                            Handler mainHandler = new Handler(ctx.getMainLooper());
                            Runnable myRunnable = new Runnable() {
                                @Override
                                public void run() {
                                    CharSequence text = "Collision détéctée. Impossible de placer ce composant ici.";
                                    int duration = Toast.LENGTH_LONG;
                                    Toast toast = Toast.makeText(ctx, text, duration);
                                    toast.show();
                                }
                            };
                            mainHandler.post(myRunnable);
                        }
                        synchronized (aService.lock2) {
                            aService.lock2.notify();
                        }
                    }
                });
                synchronized (aService.lock2) {
                    try {
                        aService.lock2.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                synchronized (aService.getLock1()) {
                    aService.getLock1().notify();
                }
            }
        }).start();
        synchronized (aService.getLock1()) {
            try {
                aService.getLock1().wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Appel dela méthode d'effacement des éléments de l'éditeur
     */
    public void call_clearEditor() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                editor.clearEditor();
            }
        });
    }

    /**
     * Appel de la méthode de dé-sélection du modèlke sélectionné
     */
    public void call_unSelectModel() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                editor.unSelectModel();
            }
        });
    }

    /**
     * Renvoit le PlacedComponent créé en fonction du modèle en argument en fonction des coordonnées
     * du centre du modèle et des coordonées X Y Z du PlacedCompoent
     *
     * @param model modèle dont on veut récupérer le PC équivalent
     * @return le PC equivalent au modèle
     */
    public PlacedComponent getPCompoByModel(MaderaModel model) {
        int nbMach = 0;
        PlacedComponent result = null;
        Vector3 center = model.getCenter();
        for (PlacedComponent pCompo : aService.getAllPCompos()) {
            if (pCompo.getPosX() == center.x
                    && pCompo.getPosY() == center.y
                    && pCompo.getPosZ() == center.z) {
                result = pCompo;
                nbMach++;
            }
        }
        if (nbMach != 1) {
            Log.i("madera", "Nombre de PC trouvé null ou supérieur à 1 match : " + nbMach);
        }
        return result;
    }

    /**
     * Permet de récupérer un des cooronéées du centre d'un modèle
     * @param selectedModel modèle dont on veut récupérer une des coordonnées du centre
     * @param axis axe dont on veut connaitre la coordonnée
     * @return la valeur des coordonnées du centre du modèle sur cet axe
     */
    public float getPosXYZFromModel(MaderaModel selectedModel, int axis) {
        switch (axis) {
            case X_AXIS:
                return selectedModel.transform.getTranslation(new Vector3()).x;
            case Y_AXIS:
                return selectedModel.transform.getTranslation(new Vector3()).y;
            case Z_AXIS:
                return selectedModel.transform.getTranslation(new Vector3()).z;
        }
        return -1f;
    }

    /**
     * Selectionne le PlacedSlot corresponant au modèle sélectionné
     */
    public void selectPlacedSlot(MaderaModel model) throws IncorrectNumberOfSlotException {
        int nbMatch = 0;
        for (PlacedSlot pslot : aService.getAllPlacedSlots()) {
            if (pslot.getPosX() == model.getCenter().x
                    && pslot.getPosZ() == model.getCenter().z) {
                aService.selectedPSlot = pslot;
                nbMatch++;
            }
        }
        if (nbMatch != 1) {
            throw new IncorrectNumberOfSlotException("Nombre de PlacedSlot selectionné trouvé incorrect :" + nbMatch);
        }
    }

    /**
     * Récupère le PlacedComponent lié au component représenté par ce modèle
     *
     * @param modeli modèle sélectionné
     * @throws IncorrectNumberOfPCException
     */
    public void selectPCompoCarpByModel(MaderaModel modeli, ArrayList<PlacedComponent> carpentries) throws IncorrectNumberOfPCException {
        int nbMatch = 0;
        for (PlacedComponent pCompo : carpentries) {
            float y = modeli.getCenter().y == 0 ? editor.WALL_HEIGHT / 2 : modeli.getCenter().y;
            Vector3 centerOFPSlot = new Vector3(pCompo.getPosX(), y, pCompo.getPosZ());
            if (modeli.getBox().contains(centerOFPSlot)) {
                aService.selectedPCompo = pCompo;
                nbMatch++;
            }
        }
        if (nbMatch != 1) {
            throw new IncorrectNumberOfPCException("Plusieur ou aucun PlacedComponent lié au Carpentry n'ont été trouvés match : " + nbMatch);
        }
    }


    /**
     * Récupère le PlacedSlot dont les cooronnées du centre sont égale à celle du point center
     *
     * @param center centre du slot
     * @return le PlacedSlot correspondant
     * @throws NoPSlotFoundException Si aucun PlacedSlot correspondant n'a été trouvé
     */
    public PlacedSlot getPSlotFromCenter(Vector3 center, ArrayList<PlacedSlot> wallSlots) throws NoPSlotFoundException {
        // TODO fait la même chose que selectPlacedSlot
        PlacedSlot result = null;
        for (PlacedSlot pSlot : wallSlots) {
            if (pSlot.getPosX() == center.x
                    && pSlot.getPosY() == center.y
                    && pSlot.getPosZ() == center.z) {
                result = pSlot;
            }
        }
        if (result == null) {
            throw new NoPSlotFoundException("Aucon WallPSlot correspondant à celui selectionné sur " +
                    "le mur n'a été trouvé dans la liste");
        }
        return result;
    }

    /**
     * Supprime le PComponent sélectionné ainsi que le Component correspondant au modèle de Divider
     * et demande à l'éditeur de supprimer le modèle 3D du divider
     *
     * @param modeli
     */
    public void removeDivider(final MaderaModel modeli, ArrayList<PlacedComponent> dividers) {
        aService.selectedPCompo = getPCompoByModel(modeli);
        dividers.remove(aService.selectedPCompo);
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                editor.removeDivider(modeli);
                editor.unSelectModel();
            }
        });

    }

    /**
     * Supprime le PComponent sélectionné ainsi que le Component correspondant au modèle de Divider
     * et demande à l'éditeur de supprimer le modèle 3D du divider
     *
     * @param modeli
     */
    public void removeCarpentry(final MaderaModel modeli, ArrayList<PlacedComponent> carpentries) {
        aService.selectedPCompo = getPCompoByModel(modeli);
        for (Iterator<PlacedComponent> iter = carpentries.iterator(); iter.hasNext(); ) {
            PlacedComponent pc = iter.next();
            if (pc.get_id() == aService.selectedPCompo.get_id()) {
                iter.remove();
            }
        }
        for (PlacedSlot pSlot : aService.wallSlots) {
            if (pSlot.getIdchildpcomponent() == aService.selectedPCompo.get_id()) {
                pSlot.setIdchildpcomponent(0);
            }
        }

        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                editor.removeCarpentry(modeli);
                editor.unSelectModel();
            }
        });
    }


    /**
     * Charge les modèles des component portés par ces les PlacedComponent de la pcList
     * @param pcList liste de PC porteur des component dont on veut charger les modèles 3D
     */
    public void loadModelsFromPC(final ArrayList<PlacedComponent> pcList, final ComponentService compoService) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gdx.app.postRunnable(new Runnable() {
                    public void run() {
                        editor.setLoading(true);
                        for (PlacedComponent pcDiv : pcList) {
                            final Component divider = compoService.getCompoById(pcDiv.getIdcomponent());
                            editor.loadComponentModel(aService.getModelPath(divider));
                            editor.assets.finishLoading();
                        }
                        // Attente tant que les modèle 3D ne sont pas chargés
                        while (!editor.assets.update()) {
                        }
                        editor.setLoading(false);
                        synchronized (aService.lock2) {
                            aService.lock2.notify();
                        }
                    }
                });
                synchronized (aService.lock2) {
                    try {
                        aService.lock2.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                synchronized (aService.getLock1()) {
                    aService.getLock1().notify();
                }
            }
        }).start();
        synchronized (aService.getLock1()) {
            try {
                aService.getLock1().wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Appel de la méthode sélection d'un modèle à partir des coordonnées d'un PC ou d'un PS
     * @param psOrpc PC ou PS dont on veut sélectionner le modèle correspondant
     * @param typeOfElement Type du modèle que l'on veut sélectionner
     */
    public void selectModelFromPCorPS(Entity psOrpc, final int typeOfElement) {
        final Vector3 centerOfModel = getVectorFromPCorPS(psOrpc);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gdx.app.postRunnable(new Runnable() {
                    public void run() {
                        try {
                            switch (typeOfElement) {
                                case PC_DIVIDER:
                                    editor.selectModelAtCoord(centerOfModel, editor.FLOOR_TOUCHBOX);
                                    break;
                                case PC_WALL:
                                    editor.selectModelAtCoord(centerOfModel, editor.WALL);
                                    break;
                                case PS_WALL:
                                    editor.selectModelAtCoord(centerOfModel, editor.WALL_TOUCHBOX);
                                    break;
                            }
                        } catch (SelectedModelException e) {
                            e.printStackTrace();
                        }
                        synchronized (aService.lock2) {
                            aService.lock2.notify();
                        }
                    }
                });
                synchronized (aService.lock2) {
                    try {
                        aService.lock2.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                synchronized (aService.getLock1()) {
                    aService.getLock1().notify();
                }
            }
        }).start();
        synchronized (aService.getLock1()) {
            try {
                aService.getLock1().wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Créé un Vector3 à partir des coordonnées d'un PC ou PS
     * @param entity
     * @return
     */
    private Vector3 getVectorFromPCorPS(Entity entity) {
        Vector3 vect = new Vector3();
        if (entity instanceof PlacedComponent) {
            vect.x = ((PlacedComponent) entity).getPosX();
            vect.y = ((PlacedComponent) entity).getPosY();
            vect.z = ((PlacedComponent) entity).getPosZ();
        } else if (entity instanceof PlacedSlot) {
            vect.x = ((PlacedSlot) entity).getPosX();
            vect.y = ((PlacedSlot) entity).getPosY();
            vect.z = ((PlacedSlot) entity).getPosZ();
        }
        return vect;
    }

    /**
     * Appel de la fonction de dessin de la texture du covering sur un composant (mur ou sol)
     * @param coveringPath
     * @param in
     * @param out
     */
    public void call_drawTextureOnModels(final String coveringPath, final boolean in, final boolean out) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gdx.app.postRunnable(new Runnable() {
                    public void run() {
                        try {
                            editor.drawTextureOnModels(coveringPath, in, out);
                        } catch (SelectedModelException e) {
                            e.printStackTrace();
                        }
                        synchronized (aService.lock2) {
                            aService.lock2.notify();
                        }
                    }
                });
                synchronized (aService.lock2) {
                    try {
                        aService.lock2.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                synchronized (aService.getLock1()) {
                    aService.getLock1().notify();
                }
            }
        }).start();
        synchronized (aService.getLock1()) {
            try {
                aService.getLock1().wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Renvoi le type du covering à partir du modèle sélectionné
     * @return
     */
    public int getTypeOfCovering() {
        Class aClass = editor.getClassOfSelectedModel();
        if (aClass != null) {
            if (aClass.equals(Wall.class)) {
                return aService.WALL_COVERING;
            } else if (aClass.equals(Slab.class)) {
                return aService.SLAB_COVERING;
            }
        }
        return -1;
    }

    /**
     * Appel de la méthode de remise à zéro des texture des modèles
     */
    public void call_resetMaterial() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gdx.app.postRunnable(new Runnable() {
                    public void run() {
                            editor.e3DService.resetMaterial();
                        synchronized (aService.lock2) {
                            aService.lock2.notify();
                        }
                    }
                });
                synchronized (aService.lock2) {
                    try {
                        aService.lock2.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                synchronized (aService.getLock1()) {
                    aService.getLock1().notify();
                }
            }
        }).start();
        synchronized (aService.getLock1()) {
            try {
                aService.getLock1().wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void setVisualisationModeOn() {
        editor.VISU_MODE = true;
    }

    public void setVisualisationModeOff() {
        editor.VISU_MODE = false;
    }

    public boolean call_dividerPlacementIsPossible() {
        final boolean[] isPossible = {false};
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gdx.app.postRunnable(new Runnable() {
                    public void run() {
                        isPossible[0] = editor.e3DService.dividerPlacementIsPossible();
                        synchronized (aService.lock2) {
                            aService.lock2.notify();
                        }
                    }
                });
                synchronized (aService.lock2) {
                    try {
                        aService.lock2.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                synchronized (aService.getLock1()) {
                    aService.getLock1().notify();
                }
            }
        }).start();
        synchronized (aService.getLock1()) {
            try {
                aService.getLock1().wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return isPossible[0];
    }
}
