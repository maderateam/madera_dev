package fr.cesi.madera.editor.listeners;

import android.view.MotionEvent;
import android.view.View;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector3;

import fr.cesi.libgdx.Editor3D;
import fr.cesi.madera.editor.EditorApplication;

/**
 * Cette classe gère les mouvements de la caméra sur l'éditeur
 * effectués à l'aide des 4 flêche directionnelles
 * Created by Doremus on 10/03/2017.
 */

public class CameraOnClickListener implements View.OnTouchListener {
    public final static int UP = 0;
    public final static int DOWN = 1;
    public final static int LEFT = 2;
    public final static int RIGHT = 3;
    private EditorApplication editorA;
    private Editor3D editor3D;
    private boolean move = false;

    public CameraOnClickListener(EditorApplication editorA, Editor3D editor3D) {
        this.editorA = editorA;
        this.editor3D = editor3D;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            move = true;
            if (v.equals(editorA.getArrowDown())) {
                moveCameraOnDirection(DOWN);
            } else if (v.equals(editorA.getArrowUp())) {
                moveCameraOnDirection(UP);
            } else if (v.equals(editorA.getArrowLeft())) {
                moveCameraOnDirection(LEFT);
            } else if (v.equals(editorA.getArrowRight())) {
                moveCameraOnDirection(RIGHT);
            } else if (v.equals(editorA.getCancelCam())) {
                editor3D.getCamera().position.set(2f, 15f, 2f);
                editor3D.getCamera().lookAt(0, 0, 0);
                editor3D.getCamera().near = 1f;
                editor3D.getCamera().update();
            }
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            move = false;
        }
        return true;
    }

    /**
     * Cette méthode translate la caméra en fonction de la direction sur lequel on désire la bouger
     * @param direction direction dans laquelle on veut déplacer la caméra
     */
    public void moveCameraOnDirection(final int direction) {
        final Camera cam = editor3D.getCamera();

        Thread mover = new Thread() {
            public void run() {
                while (move) {
                    Gdx.app.postRunnable(new Runnable() {
                        public void run() {
                            switch (direction) {
                                case UP:
                                    Vector3 up = cam.up.cpy();
                                    cam.position.add(up.scl(0.05f));
                                    break;
                                case DOWN:
                                    Vector3 down = cam.up.cpy();
                                    cam.position.add(down.scl(-0.05f));
                                    break;
                                case LEFT:
                                    Vector3 left = cam.direction.cpy().crs(cam.up).nor();
                                    cam.position.add(left.scl(-0.05f));
                                    break;
                                case RIGHT:
                                    Vector3 right = cam.direction.cpy().crs(cam.up).nor();
                                    cam.position.add(right.scl(0.05f));
                                    break;
                            }
                            editor3D.getCamera().update();
                        }
                    });
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        mover.start();
    }
}
