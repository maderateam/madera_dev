package fr.cesi.madera.services;

import android.content.Context;

import java.util.ArrayList;

import fr.cesi.madera.dao.daoCovering.CoveringDao;
import fr.cesi.madera.entities.Covering;

/**
 * Service de gestion des méthodes de récupération
 * des coverings
 */

public class CoveringService {
    Context ctx;
    CoveringDao coveringDao;

    public CoveringService(Context ctx){
        this.ctx = ctx;
        coveringDao = new CoveringDao(ctx);
    }

    /**
     * Méthode de récupération de tous les coverings présent en base de données
     * Retourne list de covering
     * @return
     */
    public ArrayList<Covering> getAllCoverings(){
        coveringDao.openDbIfClosed();
        ArrayList<Covering> coverings = coveringDao.getAllEntities();
        return coverings;
    }

    /**
     * Méthode de récupération d'un covering en base de données
     * grâce à son ID
     * @param idcoveringin
     * @return
     */
    public Covering getCoveringById(long idcoveringin) {
        coveringDao.openDbIfClosed();
        return (Covering) coveringDao.getEntityById(idcoveringin,coveringDao.COVERING_CLASS);
    }
}
