package fr.cesi.madera.services;

import android.content.ContentValues;
import android.content.Context;

import fr.cesi.madera.dao.daoClient.ClientDao;
import fr.cesi.madera.dao.daoCommercial.CommercialDao;
import fr.cesi.madera.dao.daoPlacedComponent.PlacedComponentDao;
import fr.cesi.madera.dao.daoPlacedSlot.PlacedSlotDao;
import fr.cesi.madera.dao.daoPlan.PlanDao;
import fr.cesi.madera.dao.daoProject.ProjectDao;
import fr.cesi.madera.dao.daoQuotation.QuotationDao;
import fr.cesi.madera.dao.daoQuotationLine.QuotationLineDao;

/**
 * Service de restauration des entitées masquées
 */

public class RestaurationService {
    private Context ctx;

    public RestaurationService(Context ctx) {
        this.ctx = ctx;
    }

    public void restoreAllHiddenEntities(){
        ContentValues cv = new ContentValues();
        cv.put("hidden","false");
        ClientDao cliDao = new ClientDao(ctx);
        cliDao.openDbIfClosed();
        cliDao.getDb().update(cliDao.getSQLiteView(cliDao.CLIENT_CLASS_NAME),cv,"hidden='true'",null);

        CommercialDao comDao = new CommercialDao(ctx);
        comDao.openDbIfClosed();
        comDao.getDb().update(comDao.getSQLiteView(comDao.COMMERCIAL_CLASS_NAME),cv,"hidden='true'",null);

        PlacedComponentDao pcDao = new PlacedComponentDao(ctx);
        pcDao.openDbIfClosed();
        pcDao.getDb().update(pcDao.getSQLiteView(pcDao.PCOMPONENT_CLASS_NAME),cv,"hidden='true'",null);

        PlacedSlotDao psDao = new PlacedSlotDao(ctx);
        psDao.openDbIfClosed();
        psDao.getDb().update(psDao.getSQLiteView(psDao.PSLOT_CLASS_NAME),cv,"hidden='true'",null);

        PlanDao planDao = new PlanDao(ctx);
        planDao.openDbIfClosed();
        planDao.getDb().update(planDao.getSQLiteView(planDao.PLAN_CLASS_NAME),cv,"hidden='true'",null);

        ProjectDao projDao = new ProjectDao(ctx);
        projDao.openDbIfClosed();
        projDao.getDb().update(projDao.getSQLiteView(projDao.PROJECT_CLASS_NAME),cv,"hidden='true'",null);

        QuotationDao quotDao = new QuotationDao(ctx);
        quotDao.openDbIfClosed();
        quotDao.getDb().update(quotDao.getSQLiteView(quotDao.QUOTATION_CLASS_NAME),cv,"hidden='true'",null);

        QuotationLineDao quotLDao = new QuotationLineDao(ctx);
        quotLDao.openDbIfClosed();
        quotLDao.getDb().update(quotLDao.getSQLiteView(quotLDao.QUOTATION_LINE_CLASS_NAME),cv,"hidden='true'",null);
    }
}
