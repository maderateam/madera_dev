package fr.cesi.madera.services;

import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;

import fr.cesi.libgdx.Editor3D;
import fr.cesi.libgdx.misc.exceptions.SelectedModelException;
import fr.cesi.madera.dao.daoPlacedComponent.PlacedComponentDao;
import fr.cesi.madera.dao.daoPlacedSlot.PlacedSlotDao;
import fr.cesi.madera.editor.EditorApplication;
import fr.cesi.madera.editor.GdxBridge;
import fr.cesi.madera.editor.PlacedEntityFactory;
import fr.cesi.madera.entities.Component;
import fr.cesi.madera.entities.Covering;
import fr.cesi.madera.entities.PlacedComponent;
import fr.cesi.madera.entities.PlacedSlot;
import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.Slot;
import fr.cesi.madera.misc.CoordinateExtractor;
import fr.cesi.madera.misc.exceptions.FloorPartsException;
import fr.cesi.madera.misc.exceptions.IncorrectNumberOfSlotException;
import fr.cesi.madera.misc.exceptions.NoFindComponentException;

/**
 * Created by Doremus on 11/12/2016.
 */

public class EditorAService {

    public static final int WALL_COVERING = 0;
    public static final int SLAB_COVERING = 1;

    public EditorApplication editorA;
    public Editor3D editor;

    public ComponentService compoService;
    public SlotService slotService;
    public PComponentService pcService;
    public PSlotService psService;
    public CoveringService covService;

    public PlacedSlot selectedPSlot;
    public PlacedComponent selectedPCompo;

    public PlacedComponent pcFloor;
    public ArrayList<PlacedComponent> carpentries = new ArrayList<>();
    public ArrayList<PlacedSlot> psFloorList = new ArrayList<>();
    public ArrayList<PlacedComponent> dividers = new ArrayList<>();
    public ArrayList<PlacedComponent> walls = new ArrayList<>();
    public ArrayList<PlacedSlot> wallSlots = new ArrayList<>();
    /**
     * Revêtement intérieur des murs
     */
    public Covering wall_in_covering;
    /**
     * Revêtement extérieur des murs
     */
    public Covering wall_out_covering;
    /**
     * Revêtement des cloisons
     */
    public Covering divider_covering;
    /**
     * Revêtement du sol
     */
    public Covering floor_covering;
    public Object lock2 = new Object();
    private Plan plan;
    private PlacedComponentDao pcDao;
    private PlacedSlotDao psDao;
    private Object lock1 = new Object();
    private PlacedEntityFactory eFactory;
    private GdxBridge gdxBridge;


    public EditorAService(EditorApplication editorA, Editor3D editor, Plan plan) {
        this.plan = plan;
        this.editorA = editorA;
        this.editor = editor;
        this.compoService = new ComponentService(editorA);
        this.pcService = new PComponentService(editorA);
        this.psService = new PSlotService(editorA);
        this.slotService = new SlotService(editorA);
        this.covService = new CoveringService(editorA);
        this.eFactory = PlacedEntityFactory.getInstance(editorA);
        pcDao = new PlacedComponentDao(editorA);
        psDao = new PlacedSlotDao(editorA);
        gdxBridge = new GdxBridge(editor, this, editorA);
    }

    /**
     * Méthode de création d'un sol à partir des dalles qui le compose
     * - Récupère l'ensemble des coordonnées des dalles qui composent le sol
     * - Créée le PC pour le sol
     * - Pour chaque dalle :
     * --Appelle la méthode LIBGDX de dessin de la dalle à partir de ses coordonées
     *
     * @param floor Composant Sol à créer
     * @throws FloorPartsException
     */
    public void createFloor(Component floor) throws FloorPartsException {
        try {
            final ArrayList<ArrayList<Integer>> coordinates = CoordinateExtractor.getCoordsFromFloor(floor);
            eFactory.createPCFromComponent(floor);
            pcFloor = eFactory.createPCFromComponent(floor);
            if (floor_covering != null) {
                pcFloor.setIdcoveringin(floor_covering.get_id());
            }
            walls = gdxBridge.call_drawFloor(coordinates,
                    getCoveringPath(floor_covering),
                    eFactory);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void loadFloor(Component floor) throws FloorPartsException {
        try {
            final ArrayList<ArrayList<Integer>> coordinates = CoordinateExtractor.getCoordsFromFloor(floor);
            gdxBridge.call_drawFloor(coordinates, getCoveringPath(floor_covering), null);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Méthode de création d'une cloison à partir du composant sélectionné par l'utilisateur
     * @param divider
     */
    public void createDivider(final Component divider) {
        if(gdxBridge.call_dividerPlacementIsPossible()){
            if (selectedPCompo != null) {
                deleteExistingPCInList(dividers, selectedPCompo);
            }
            PlacedComponent pcDivider = eFactory.createPCFromComponent(divider);
            if (divider_covering != null) {
                pcDivider.setIdcoveringin(divider_covering.get_id());
            }
            gdxBridge.call_drawDivider(getModelPath(divider), pcDivider);
            dividers.add(pcDivider);
        }else{
            // Si non , on affiche un message, via un Toast à lancer sur le thread UI Android
            Handler mainHandler = new Handler(editorA.getMainLooper());
            Runnable myRunnable = new Runnable() {
                @Override
                public void run() {
                    CharSequence text = "Impossible de placer une cloison ici.";
                    int duration = Toast.LENGTH_LONG;
                    Toast toast = Toast.makeText(editorA, text, duration);
                    toast.show();
                }
            };
            mainHandler.post(myRunnable);
            gdxBridge.call_unSelectModel();
        }
    }

    public void loadDivider(PlacedComponent pcDiv) {
        Component divider = compoService.getCompoById(pcDiv.getIdcomponent());
        gdxBridge.selectModelFromPCorPS(pcDiv, gdxBridge.PC_DIVIDER);
        gdxBridge.call_drawDivider(getModelPath(divider), pcDiv);
    }


    /**
     * Création des slots sur le mur à partir de l'élément selectionné par l'utilisateur
     *
     * @param wall
     */
    public void createSlotOnWall(final Component wall) throws Exception {
        try {
            // On récupère un slot pour chaque numéro (de slot) dans la liste des slots liées à ce mur
            final ArrayList<Slot> slots = slotService.getDistinctSlotByWall(wall);
            // On teste si le mur est assez grand pour accueillir ce nombre de slot
            if (wallPlacementIsPossible(wall, slots.size())) {
                // On tente de récupérer un PC qui aurait été précédemment créé pour le modèle sélectionné
                PlacedComponent wallCompo = gdxBridge.getPCompoByModel(editor.e3DService.selectedModel);
                // Si il existe...
                if (wallCompo != null) {
                    removeSubWallComponents(wallCompo);
                    // On supprime tous les slot ET/OU carpentries déssinés sur le mur dans l'éditeur
                    gdxBridge.call_deleteModelsOnSelectedWall();

                    // On set l'idComponent du PC avec celui du wall en argument
                    wallCompo.setIdcomponent(wall.get_id());

                } else {
                    wallCompo = eFactory.createPCFromComponent(wall);
                    // On set la position du PC mur
                    wallCompo.setPosX(gdxBridge.getPosXYZFromModel(editor.e3DService.selectedModel, gdxBridge.X_AXIS));
                    wallCompo.setPosY(gdxBridge.getPosXYZFromModel(editor.e3DService.selectedModel, gdxBridge.Y_AXIS));
                    wallCompo.setPosZ(gdxBridge.getPosXYZFromModel(editor.e3DService.selectedModel, gdxBridge.Z_AXIS));
                    if (wall_in_covering != null) {
                        wallCompo.setIdcoveringin(wall_in_covering.get_id());
                    }
                    if (wall_out_covering != null) {
                        wallCompo.setIdcoveringout(wall_out_covering.get_id());
                    }
                    walls.add(wallCompo);
                }

                // Pour chaque slot on sette ses propriétés
                for (final Slot slot : slots) {
                    final PlacedSlot pSlot = eFactory.createPSFromPComponent(wallCompo);
                    gdxBridge.call_drawTouchBoxOnWall(slot.getWidthplacement(), pSlot);
                    // On fixe le numéro de ce PSlot
                    pSlot.setNumofslot(slot.getNumofslot());
                    // Peut importe quel slot sera effectivement utilisé pour ce placedSlot
                    // On sette un id pour pouvoir utiliser le widthPlacement de ce slot
                    // Lors de l'import du plan si ce placedSlot esr vide
                    pSlot.setIdslot(slot.get_id());
                    boolean present = false;
                    for (PlacedSlot ps : wallSlots) {
                        if (ps.getPosX() == pSlot.getPosX() && ps.getPosY() == pSlot.getPosY() && ps.getPosZ() == pSlot.getPosZ()) {
                            present = true;
                            throw new Exception("Duplicate Slot !");
                        }
                    }if(!present){
                        wallSlots.add(pSlot);
                    }
                }
            } else {
                // Si non , on affiche un message, via un Toast à lancer sur le thread UI Android
                Handler mainHandler = new Handler(editorA.getMainLooper());
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        CharSequence text = "Mur trop petit pour ce nombre de slot";
                        int duration = Toast.LENGTH_LONG;
                        Toast toast = Toast.makeText(editorA, text, duration);
                        toast.show();
                    }
                };
                mainHandler.post(myRunnable);
            }
        } catch (IncorrectNumberOfSlotException incorrectNumberOfSlotException) {
            incorrectNumberOfSlotException.printStackTrace();
        }
    }

    private void loadSlotOnWall(ArrayList<PlacedSlot> pSlots) {
        for (PlacedSlot pslot : pSlots) {
            Slot slot = slotService.getSlotById(pslot.getIdslot());
            gdxBridge.call_drawTouchBoxOnWall(slot.getWidthplacement(), pslot);
        }
    }


    /**
     * Détermine si il est possible de placer ce composant wall sur cette portion de mur
     * * Récupère la taille du plus petit composant pour chaque numéro de slot du mur.
     * * Calcule le cumul de ces taille et ajoute pour chaque slot une marge : CARPENTRY_MARGIN
     * * Si la largeur du mur est supérieure à la largeur cumulé de chacun des plus petits composants
     * * alors le placement du mur est possible
     * @param wall   mur que l'on veut placer
     * @param nbSlot nombre de slot distinct porté par ce mur
     * @return True si le placement est possible, false sinon
     */
    private boolean wallPlacementIsPossible(Component wall, int nbSlot) {
        float totalMinWidth = 0;
        for (int i = 0; i < nbSlot; i++) {
            float minForSlotI = Float.MAX_VALUE;
            for (Component compo : compoService.getAllChildFromComponent(wall, i)) {
                minForSlotI = minForSlotI > compo.getWidth() ? compo.getWidth() : minForSlotI;
            }
            totalMinWidth += minForSlotI + editor.CARPENTRY_MARGIN;
        }
        try {
            return totalMinWidth < editor.getLenghtOfSelectedModel();
        } catch (SelectedModelException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Supprime tous les PlacedComponent portés par ce mur ainsi que les PlacedSlot faisant la liaison
     * @param wallCompo PC mur que l'on veut vider
     */
    private void removeSubWallComponents(PlacedComponent wallCompo) {
        // On récupère tous les PS lié à ce PC
        ArrayList<PlacedSlot> psListToRemove = getWallPSByParentPC(wallCompo);
        // On supprime tous les pc liés à ces différents ps
        for (PlacedSlot ps : psListToRemove) {
            for (Iterator<PlacedComponent> iterator = carpentries.iterator(); iterator.hasNext(); ) {
                PlacedComponent carp = iterator.next();
                if (carp.get_id() == ps.getIdchildpcomponent()) {
                    // Suprime le carpentry de la liste
                    iterator.remove();
                }
            }
        }
        // On supprime tous les PS lié à ce PC
        for (Iterator<PlacedSlot> iterator = wallSlots.iterator(); iterator.hasNext(); ) {
            PlacedSlot ps = iterator.next();
            for (PlacedSlot psToRem : psListToRemove) {
                if (ps.get_id() == psToRem.get_id()) {
                    iterator.remove();
                }
            }
        }
    }

    /**
     * Créé un PlacedComposant portant un composant Simple et demande à l'éditeur 3D de déssiner un carpentry
     * à l'emplacement du PlacedSlot sélectionné
     * @param carpentry Composant simple à placer sur le plan
     */
    public void createCarpentryOnSlot(final Component carpentry) {
        // Supprime le PC existant à cet endroit si le carpentry
        // doit prendre la place d'un Carpentry existant
        if (selectedPCompo != null) {
            deleteExistingPCInList(carpentries, selectedPCompo);
        }
        final PlacedComponent pcCarpentry = eFactory.createPCFromComponent(carpentry);
        // On lie PlacedSlot qui accueillera ce PlcedComponnent au slot
        Component wall = null;
        try {
            wall = getParentComponentFromPlacedSlot(selectedPSlot);
        } catch (NoFindComponentException e) {
            e.printStackTrace();
        }
        final Slot slot = slotService.getIDSlotForThisComponent(wall, carpentry, selectedPSlot.getNumofslot());
        selectedPSlot.setIdslot(slot.get_id());
        gdxBridge.call_drawCarpentry(getModelPath(carpentry), slot.getHeightplacement(), pcCarpentry, true);
    }

    private void loadCarpentryOnSlot(Component carpentry, Slot slot, PlacedComponent pcCarpentry) {
        gdxBridge.call_drawCarpentry(getModelPath(carpentry), slot.getHeightplacement(), pcCarpentry, false);
    }

    /**
     * Supprime le PC de la PCList si il y est présent
     * @param pcList     liste dans laquelle ajouter le pc
     * @param pcToDelete PlacedComponant à supprimer de la liste
     */
    private void deleteExistingPCInList(ArrayList<PlacedComponent> pcList, PlacedComponent pcToDelete) {
        for (Iterator<PlacedComponent> iter = pcList.iterator(); iter.hasNext(); ) {
            PlacedComponent pc = iter.next();
            if (pc.getPosX() == pcToDelete.getPosX()
                    && pc.getPosY() == pcToDelete.getPosY()
                    && pc.getPosZ() == pcToDelete.getPosZ()) {
                iter.remove();
            }
        }
    }

    /**
     * Récupère le composant porté par le PC auquel le placedSlot est lié
     * @param selectedPSlot Slopt dont ont veut récupérer le composant lié à son parent
     * @return le composant lié au parent du PlacedSlot
     * @throws NoFindComponentException Si aucun composant n'a été trouvé
     */
    private Component getParentComponentFromPlacedSlot(PlacedSlot selectedPSlot) throws NoFindComponentException {
        long idCompo = 0;
        byte match = 0;
        for (PlacedComponent pCompo : walls) {
            if (pCompo.get_id() == selectedPSlot.getIdparentpcomponent()) {
                idCompo = pCompo.getIdcomponent();
                match++;
            }
        }
        if (match != 1) {
            throw new NoFindComponentException("Erreur dans la récupération du composant parent du placedSlot match = " + match);
        }
        return compoService.getCompoById(idCompo);
    }

    /**
     * Renvoit le chemin complet du modèle pour le composant sélectionné en fonction de son type
     *
     * @param compo composant dont on veut récupérer le chemin du modèle 3D
     * @return le chemin du modèle 3D
     */
    public String getModelPath(Component compo) {
        String path = "";
        path += "/Madera/";
        if (compo.isDivider()) {
            path += "dividers";
        } else if (!compo.isComplex()) {
            path += "carpentries";
        }
        return path + "/" + +compo.get_id() + "/" + compo.getModelpath();
    }

    private String getCoveringPath(Covering covering) {
        if(covering != null) {
            return "Madera/covering/" + covering.getIconname();
        }
        return null;
    }


    private ArrayList<PlacedSlot> getWallPSByParentPC(PlacedComponent pc) {
        ArrayList<PlacedSlot> psList = new ArrayList<>();
        for (PlacedSlot slot : wallSlots) {
            if (slot.getIdparentpcomponent() == pc.get_id()) {
                psList.add(slot);
            }
        }
        return psList;
    }

    /**
     * Remet à zéro la liste de PC et PC créées pour les modèles et demande à l'éditeur 3D de
     * supprimer tous les modèles 3D
     */
    public void clearEditor() {
        eFactory.resetIds();
        pcFloor = null;
        psFloorList = new ArrayList<>();
        dividers = new ArrayList<>();
        walls = new ArrayList<>();
        wallSlots = new ArrayList<>();
        selectedPSlot = null;
        selectedPCompo = null;
        gdxBridge.call_clearEditor();

    }

    /**
     * Affiche la liste des composant compatibles avec ce PlacedSlot
     *
     * @param pSlot, PlacedSlot dont on veut récupérer la liste des Composants compatibles
     * @return la liste des composnants compatibles
     */
    public ArrayList<Component> getCarpentriesBySlot(PlacedSlot pSlot) throws NoFindComponentException {
        long idComponent = -1;
        for (PlacedComponent pc : walls) {
            Log.i("madera","getCarpentriesBySlot : " + pc.get_id() + " == " + pSlot.getIdparentpcomponent()  );
            if (pc.get_id() == pSlot.getIdparentpcomponent()) {
                idComponent = pc.getIdcomponent();
            }
        }
        if (idComponent == -1) {
            throw new NoFindComponentException("Aucun id de composant trouvé pour le mur lié au slot selectionné");
        }
        return compoService.getCarpentriesBySlotNumber(idComponent, pSlot.getNumofslot());
    }


    public void displayCoveringOption(int typeOfCovering) {
        switch (typeOfCovering) {
            case WALL_COVERING:
                editorA.displayWallCoveringOption();
                break;
            case SLAB_COVERING:
                editorA.displaySlabCoveringOption();
                break;
        }
    }

    public void applyCoveringOnModels(Covering cov) {
        int typeOfCovering = gdxBridge.getTypeOfCovering();
        if(typeOfCovering != -1) {

            boolean in = editorA.isCoveringIn();
            boolean out = editorA.isCovringOut();
            switch (typeOfCovering) {
                case WALL_COVERING:
                    for (PlacedComponent pCompo : walls) {
                        if (in) {
                            pCompo.setIdcoveringin(cov.get_id());
                        }
                        if (out) {
                            pCompo.setIdcoveringout(cov.get_id());
                        }
                    }
                    break;
                case SLAB_COVERING:
                    pcFloor.setIdcoveringin(cov.get_id());
                    break;
            }
            String coveringPath = "Madera/covering/" + cov.getIconname();
            gdxBridge.call_drawTextureOnModels(coveringPath, in, out);
        }
    }

    /**
     * Désactive la sélection du modèke précédemment sélectionné
     */
    public void unSelectComponent() {
        selectedPSlot = null;
        selectedPCompo = null;
        gdxBridge.call_unSelectModel();
    }

    /**
     * Supprime tous les éléments du plan de l'éditeur existant en base
     * Afin d'éviter les doublons
     */
    public void removeExistingPlanParts(Plan plan) {
        pcService.removePCFromPlanId(plan.get_id());
        psService.removePSFromPlanId(plan.get_id());
    }

    /**
     * Sauvegarde le plan en argument dans la base de données
     * * Parcours la hiérarchie des PlacedComponent / PlacedSlot à l'aide des idTemporaires
     * * Sauve en base au fur et à mesure les éléments et remplace les id temporaires par ceux de la base
     *
     * @param plan Plan à sauver en base
     */
    public void savePlanComponents(Plan plan) {
        // On insère le sol
        pcFloor.setIdplan(plan.get_id());
        pcDao.insertEntity(pcFloor);
        // On insère les dividers
        for (Iterator<PlacedComponent> div_iterator = dividers.iterator(); div_iterator.hasNext();) {
            PlacedComponent pcDiv = div_iterator.next();
            pcDiv.setIdplan(plan.get_id());
            pcDao.insertEntity(pcDiv);
            div_iterator.remove();
        }
        // Pour chaque PC mur
        for (Iterator<PlacedComponent> wall_iterator = walls.iterator(); wall_iterator.hasNext();) {
            PlacedComponent pcWall = wall_iterator.next();
            // On sauve l'id temporaire du pcWall
            pcWall.setIdplan(plan.get_id());
            long tempId = pcWall.get_id();
            // On insère le mur en base
            pcDao.insertEntity(pcWall);
            // On récupère tous les PS du mur en se basant sur l'id temporaire du mur
            for (Iterator<PlacedSlot> wslot_iterator = wallSlots.iterator(); wslot_iterator.hasNext();) {
                PlacedSlot psWall = wslot_iterator.next();
                if (psWall.getIdparentpcomponent() == tempId) {
                    if (psWall.getIdchildpcomponent() == 0) {
                        psWall.setIdplan(plan.get_id());
                        psWall.setIdparentpcomponent(pcWall.get_id());
                    } else {
                        // Pour chacun d'eux, on récupère le PC carp correspondant
                        for (Iterator<PlacedComponent> carp_iterator = carpentries.iterator(); carp_iterator.hasNext();) {
                            PlacedComponent pcCarp = carp_iterator.next();
                            if (pcCarp.get_id() == psWall.getIdchildpcomponent()) {
                                // On insère le pcCarp en base
                                pcCarp.setIdplan(plan.get_id());
                                pcDao.insertEntity(pcCarp);
                                // On sette le PS Wall et on l'insère en base
                                psWall.setIdplan(plan.get_id());
                                psWall.setIdparentpcomponent(pcWall.get_id());
                                psWall.setIdchildpcomponent(pcCarp.get_id());
                                carp_iterator.remove();
                            }
                        }
                    }
                    psDao.insertEntity(psWall);
                    wslot_iterator.remove();
                }
            }
            wall_iterator.remove();
        }
        activeVisualization(true);
        editorA.setEndingVisualisationModeON();
    }

    /**
     * Permet de charger un plan existant en base, ainsi que tous ses sus composants
     * @param plan
     */
    public void loadPlan(Plan plan) {
        // Création du sol
        Component sol = compoService.getSolFromPlanId(plan.get_id());
        if(sol != null) {
            PlacedComponent pcSol = pcService.getFloorPCFromPlanIdAndCompoId(plan.get_id(), sol.get_id());
            // Récupération du covering du sol
            floor_covering = covService.getCoveringById(pcSol.getIdcoveringin());
            // On sette l'id du pc Sol
            pcFloor = pcSol;
            try {
                loadFloor(sol);
            } catch (FloorPartsException e) {
                e.printStackTrace();
            }
            // Récupération des PlacedComponent porteurs des Dividers
            ArrayList<PlacedComponent> loadedPCDividers = pcService.getDividersFromPlanId(plan.get_id());
            // Ajout du covering divider
            if (loadedPCDividers.size() > 0) {
                divider_covering = covService.getCoveringById(loadedPCDividers.get(0).getIdcoveringin());
            }
            // On les ajoute à la liste de PC dividers de l'éditeur
            for (PlacedComponent pcDiv : loadedPCDividers) {
                //pcDiv.set_id(eFactory.getPCTempID());
                dividers.add(pcDiv);
            }
            // Chargement des modèles 3D
            // Attente tant que les modèle 3D ne sont pas chargés
            gdxBridge.loadModelsFromPC(loadedPCDividers, compoService);
            // Placement des dividers sur le plan
            for (PlacedComponent pcDiv : dividers) {
                loadDivider(pcDiv);
            }
            placeWallsOnPlan(plan);
            placeCarpentries();
            setTempIdForAllPCandPS();
            gdxBridge.call_resetMaterial();
        }else{
            editorA.state = editorA.FLOOR_STATE;
            editorA.showFloors();
        }
    }

    private void placeWallsOnPlan(Plan plan) {
        // Récupération des coverinf des mur
        ArrayList<PlacedComponent> loadedPCWalls = pcService.getWallsFromPlanId(plan.get_id());
        if(loadedPCWalls.size() > 0){
            wall_in_covering = covService.getCoveringById(loadedPCWalls.get(0).getIdcoveringin());
            wall_out_covering = covService.getCoveringById(loadedPCWalls.get(0).getIdcoveringout());
        }
        for (PlacedComponent pcWall : loadedPCWalls) {
            walls.add(pcWall);
        }
        for (PlacedComponent pcWall : loadedPCWalls) {
            // Récupération de la liste des PlacedSlot liant les murs et les Carpentries
            ArrayList<PlacedSlot> loadedPSWalls = psService.getPSWallsFromWallAndPlanId(plan.get_id(), pcWall);
            for (PlacedSlot pSlot : loadedPSWalls) {
                wallSlots.add(pSlot);
            }
            gdxBridge.selectModelFromPCorPS(pcWall, gdxBridge.PC_WALL);
            if(pcWall.getIdcomponent() != 0){
                loadSlotOnWall(loadedPSWalls);
            }

        }
        gdxBridge.selectModelFromPCorPS(loadedPCWalls.get(0),gdxBridge.PC_WALL);
        if(wall_in_covering != null) {
            gdxBridge.call_drawTextureOnModels(getCoveringPath(wall_in_covering), true, false);
        }
        if(wall_out_covering != null) {
            gdxBridge.call_drawTextureOnModels(getCoveringPath(wall_out_covering), false, true);
        }
    }

    /**
     * @param
     */
    private void placeCarpentries() {
        // On récupère la liste des PC contenant les Carpentries
        for (PlacedSlot psWall : wallSlots) {
            if (psWall.getIdchildpcomponent() != 0) {
                PlacedComponent pcCarp = pcService.getPCompoById(psWall.getIdchildpcomponent());
                carpentries.add(pcCarp);
            }
        }
        // On charge leurs modèles
        gdxBridge.loadModelsFromPC(carpentries, compoService);
        // Pour chaque PlacedSlot
        for (PlacedSlot psWall : wallSlots) {
            if (psWall.getIdchildpcomponent() != 0) {
                // On récupère le PlacedComponent fils
                PlacedComponent pcCarp = pcService.getPCompoById(psWall.getIdchildpcomponent());
                if (pcCarp != null) {
                    gdxBridge.selectModelFromPCorPS(psWall, gdxBridge.PS_WALL);
                    selectedPSlot = psWall;
                    Component carp = compoService.getCompoById(pcCarp.getIdcomponent());
                    Slot slot = slotService.getSlotById(psWall.getIdslot());
                    // On place le modèle du carpentry sur le plan
                    loadCarpentryOnSlot(carp, slot, pcCarp);
                }
            }
        }
    }

    private void setTempIdForAllPCandPS() {
        // On sette tempId for the floor
        pcFloor.set_id(eFactory.getPCTempID());
        // On sette tempId for dividers
        for (int i = 0 ; i < dividers.size() ; i++) {
            dividers.get(i).set_id(eFactory.getPCTempID());
        }
        // Pour chaque mur
        for (int i = 0 ; i < walls.size() ; i++) {
            // On sauve l'id du wall pour filtrer les PSlot
            long oldWallId = walls.get(i).get_id();
            // On sette l'id du pcWall
            walls.get(i).set_id(eFactory.getPCTempID());
            for (int j = 0 ; j < wallSlots.size() ; j++) {
                // On récupère les PS du pcWall
                if (wallSlots.get(j).getIdparentpcomponent() == oldWallId) {
                    if(wallSlots.get(j).getIdchildpcomponent() != 0){
                        for (int k = 0 ; k < carpentries.size() ; k++) {
                            // On récupère le PCCarp lié au PSWall
                            if (wallSlots.get(j).getIdchildpcomponent() == carpentries.get(k).get_id()) {
                                carpentries.get(k).set_id(eFactory.getPCTempID());
                                wallSlots.get(j).setIdparentpcomponent(walls.get(i).get_id());
                                wallSlots.get(j).setIdchildpcomponent(carpentries.get(k).get_id());
                                wallSlots.get(j).set_id(eFactory.getPSTempID());
                            }
                        }
                    }else if (wallSlots.get(j).getIdchildpcomponent() == 0) {
                        wallSlots.get(j).setIdparentpcomponent(walls.get(i).get_id());
                        wallSlots.get(j).set_id(eFactory.getPSTempID());
                    }
                }

            }
        }
    }


    public ArrayList<PlacedComponent> getAllPCompos() {
        ArrayList<PlacedComponent> result = new ArrayList<>();
        result.addAll(walls);
        result.addAll(dividers);
        result.addAll(carpentries);
        return result;
    }

    public ArrayList<PlacedSlot> getAllPlacedSlots() {
        ArrayList<PlacedSlot> result = new ArrayList<>();
        result.addAll(psFloorList);
        result.addAll(wallSlots);
        return result;
    }


    public ArrayList<Component> getAllFloors() {
        return compoService.getAllFloors();
    }

    public ArrayList<Component> getAllDividers() {
        return compoService.getAllDividers();
    }

    public ArrayList<Component> getAllWalls() {
        return compoService.getAllWalls();
    }

    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    public GdxBridge getGdxBridge() {
        return gdxBridge;
    }

    public Object getLock1() {
        return lock1;
    }

    public void displayPlanPartsInLog() {
        if (pcFloor != null) {
            Log.i("madera", "= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =");
            Log.i("madera", "========= FLOOR ==========");
            Log.i("madera", "PC Floor id : " + pcFloor.get_id() + " idCompo :" + pcFloor.getIdcomponent());
            Log.i("madera", "========= DIVIDERS ==========");
            for (PlacedComponent pc : dividers) {
                Log.i("madera", "PC Dividers id : " + pc.get_id() + " idCompo : " + pc.getIdcomponent());
            }
            Log.i("madera", "========= WALLS ==========");
            for (PlacedComponent pc : walls) {
                Log.i("madera", "PC Walls id : " + pc.get_id() + " idCompo : " + pc.getIdcomponent());
            }
            Log.i("madera", "========= CARPENTRIES ==========");
            for (PlacedComponent pc : carpentries) {
                Log.i("madera", "PC Carpentries id : " + pc.get_id() + " idCompo : " + pc.getIdcomponent());
            }
            Log.i("madera", "========= PLACED SLOT ==========");
            for (PlacedSlot ps : wallSlots) {
                Log.i("madera", "PS WALL id : " + ps.get_id() + " idChild : " + ps.getIdchildpcomponent() + " idParent : " + ps.getIdparentpcomponent()
                        + " idSlot : " + ps.getIdslot() + " numOfSlot :" + ps.getNumofslot() + " center : (" + ps.getPosX() + "," + ps.getPosY() + "," + ps.getPosZ() + ")");
            }
        }
    }

    public void resetCoveringLayout() {
        editorA.resetCoveringLayout();
    }

    public void deletePlan() {
        editorA.displayDeleteConfirmation();
    }

    public void deleteAllCompoOnPlan() {
        pcFloor = null;
        walls = new ArrayList<>();
        dividers = new ArrayList<>();
        carpentries = new ArrayList<>();
        wallSlots = new ArrayList<>();
        psFloorList = new ArrayList<>();
        gdxBridge.call_clearEditor();
        editorA.state = editorA.EMPTY_STATE;
        editorA.showFloors();
    }

    public boolean planIsEmpty() {
        return pcFloor == null;
    }

    public void activeVisualization(boolean actived) {
        if(actived) {
            gdxBridge.setVisualisationModeOn();
        }else{
            gdxBridge.setVisualisationModeOff();
        }
    }
}
