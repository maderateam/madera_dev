package fr.cesi.madera.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import fr.cesi.madera.dao.EntityDao;
import fr.cesi.madera.dao.daoBatch.BatchDao;
import fr.cesi.madera.entities.Batch;
import fr.cesi.madera.entities.Entity;
import fr.cesi.madera.entities.SynchronisedEntity;
import fr.cesi.madera.misc.exceptions.NoSerialComException;

/**
 * Created by Doremus on 18/04/2017.
 */

public class BatchService {
    public final static char INSERT_OP = 'I';
    public final static char UPDATE_OP = 'U';
    public final static char DELETE_OP = 'D';
    private Context ctx;

    public BatchService(Context context) {
        ctx = context;
    }

    public void registerOperation(Entity entity, char operation) throws NoSerialComException {
        Log.i("madera","Add bacth for entity class : " + entity.getClass().getSimpleName() + " operation : " + operation);
        final SharedPreferences sharedpreferences = ctx.getSharedPreferences("settings", Context.MODE_PRIVATE);
        String serialCom = sharedpreferences.getString("comserial", null);
        if(serialCom == null){
            throw new NoSerialComException("Aucun serial inscrit en base pour ce commercial");
        }
        Batch batch = new Batch();
        batch.setComserial(serialCom);
        batch.setOperation(operation);
        batch.setDateoperation(new Timestamp(new Date().getTime()));
        batch.setEntityclass(entity.getClass().getSimpleName());
        batch.setIdentity(entity.get_id());
        BatchDao dao = new BatchDao(ctx);
        dao.insertEntity(batch);
    }

    public String getJsonFromBatches(){
        BatchDao bachDao = new BatchDao(ctx);
        ArrayList<Batch> batches = bachDao.getAllEntities();
        StringBuilder jsonString = new StringBuilder("");
        jsonString.append("{\"batches\":[");
        for(Batch batch : batches){
            jsonString.append(createJsonStringFromBatch(batch));
            jsonString.append(",");
        }
        jsonString.deleteCharAt(jsonString.length()-1);
        jsonString.append("]}");
        Log.i("madera","Output batch " + jsonString.toString());

        return jsonString.toString();
    }

    public String createJsonStringFromBatch(Batch batch){
        try {
            Class entityClass = Class.forName("fr.cesi.madera.entities." + StringUtils.capitalize(batch.getEntityclass()));
            EntityDao eDao = new EntityDao(ctx) {
                @Override
                public ArrayList getAllEntities() {
                    return null;
                }
            };
            final SharedPreferences sharedpreferences = ctx.getSharedPreferences("settings", Context.MODE_PRIVATE);
            String serialCom = sharedpreferences.getString("comserial", null);
            Entity entity = eDao.getEntityById(batch.getIdentity(),entityClass);
            if(entity != null) {
                ((SynchronisedEntity) entity).setLastsync(new Timestamp(new Date().getTime()));
            }
            ObjectMapper mapper = new ObjectMapper();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:s");
            mapper.setDateFormat(df);
            String entityJson = mapper.writeValueAsString(entity);
            StringBuilder jsonBuilder = new StringBuilder("{");
            jsonBuilder
                    .append("\"id\":\"").append(batch.get_id()).append("\",")
                    .append("\"comserial\":\"").append(serialCom).append("\",")
                    .append("\"dateoperation\":\"").append(batch.getDateoperation()).append("\",")
                    .append("\"entityclass\":\"").append(batch.getEntityclass()).append("\",")
                    .append("\"operation\":\"").append(batch.getOperation()).append("\",")
                    .append("\"identity\":\"").append(batch.getIdentity()).append("\",")
                    .append("\"entity\":").append(entityJson)
                    .append("}");

            return jsonBuilder.toString();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Entity getEntityFromBatch(Batch batch) {
        Entity entity =  null;
        try {
            Class entityClass = Class.forName("fr.cesi.madera.entities." + StringUtils.capitalize(batch.getEntityclass()));
            ObjectMapper mapper = new ObjectMapper();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:s");
            mapper.setDateFormat(df);
            entity = (Entity) mapper.readValue(batch.getEntity(),entityClass);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return entity;
    }

    public void deleteAllBatch() {
        BatchDao dao = new BatchDao(ctx);
        ArrayList<Batch> batches = dao.getAllEntities();
        for (Batch batch :batches) {
            dao.deleteEntity(batch);
        }
    }
}
