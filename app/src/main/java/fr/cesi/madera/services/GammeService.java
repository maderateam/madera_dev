package fr.cesi.madera.services;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import fr.cesi.madera.dao.daoGamme.GammeDao;
import fr.cesi.madera.entities.Gamme;

/**
 * Service de gestion de récupération de gamme
 * depuis la base de données
 */

public class GammeService {

    private GammeDao gammeDAO;

    public GammeService(Context ctx) {
        gammeDAO = new GammeDao(ctx);
    }

    /**
     * Méthode de récupération des gammes depuis la base de données
     * retourne une liste de gammes
     * @return
     */
    public ArrayList<Gamme> getAllGammes(){
        gammeDAO.openDbIfClosed();
        Cursor curs = gammeDAO.getDb().rawQuery("SELECT * FROM " + gammeDAO.GAMME_CLASS_NAME,null);
        ArrayList<Gamme> gammeList = gammeDAO.getDataUtils().cursorToEntities(gammeDAO.GAMME_CLASS,curs);
        curs.close();
        return gammeList;
    }

    /**
     * Méthode de récupération du nom d'une gamme
     * à partir de son ID
     * @param idgamme
     * @return
     */
    public String getNameFromGammeID(long idgamme) {
        gammeDAO.openDbIfClosed();
        Gamme gamme = (Gamme) gammeDAO.getEntityById(idgamme,gammeDAO.GAMME_CLASS);
        gammeDAO.close();
        if(gamme != null){
            return gamme.getDenomination();
        }
        return "";
    }
}
