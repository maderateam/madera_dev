package fr.cesi.madera.services;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import fr.cesi.madera.dao.daoComponent.ComponentDao;
import fr.cesi.madera.dao.daoSlot.SlotDao;
import fr.cesi.madera.entities.Component;

/**
 * Created by Doremus on 09/12/2016.
 */

public class ComponentService {
    private ComponentDao compoDao;
    private SlotDao slotDao;
    private Context ctx;
    private SlotService slotService;

    public ComponentService(Context ctx){
        this.ctx = ctx;
        compoDao = new ComponentDao(ctx);
        slotDao = new SlotDao(ctx);
        this.slotService = new SlotService(ctx);
    }

    /**
     * Récupération en BD de tous les component pouvant être placé dans
     * un des slots fils du component compo
     * Si le niuméro du slot est != -1, alors on ne récupère que les component pouvant être placés
     * dans le slot portant le numéro numOfSlot
     * @param compo Composant dont on veut obtenir les composant fils
     * @param numOfSlot Numéro du slot, si on ne veut que ses coomposants
     * @return la liste des composant fils
     */
    public ArrayList<Component> getAllChildFromComponent(Component compo, int numOfSlot){
        slotDao.openDbIfClosed();
        ArrayList<Component> listOfCompos = new ArrayList<>();
        ArrayList<Integer> listOfSlotIDs = new ArrayList<>();
        Cursor curs = null;
        if(numOfSlot > 0) {
            curs = slotDao.getDb().rawQuery("SELECT DISTINCT idchildcomponent FROM " + slotDao.SLOT_CLASS_NAME
                    + " WHERE idparentcomponent = ? AND numofslot = ?", new String[]{String.valueOf(compo.get_id()),String.valueOf(numOfSlot)});
        }else{
            curs = slotDao.getDb().rawQuery("SELECT DISTINCT idchildcomponent FROM " + slotDao.SLOT_CLASS_NAME
                    + " WHERE idparentcomponent = ?", new String[]{String.valueOf(compo.get_id())});
        }
        try {
            while (curs.moveToNext()) {
                listOfSlotIDs.add(curs.getInt(0));
            }
        } finally {
            curs.close();
        }
        for (Integer id: listOfSlotIDs) {
            compoDao.openDbIfClosed();
            Cursor curs2 = compoDao.getDb().rawQuery("SELECT * FROM " + compoDao.COMPONENT_CLASS_NAME
                    + " WHERE _id = ?",new String[]{String.valueOf(id)});
            listOfCompos.add((Component)compoDao.getDataUtils().cursorToEntities(compoDao.COMPONENT_CLASS,curs2).get(0));
            curs2.close();
        }
        return listOfCompos;
    }

    /**
     * Méthode de récupétation de tous les sols depuis la base de données
     * retourne une liste de Component
     * @return
     */
    public ArrayList<Component> getAllFloors(){
        ArrayList<Component> listOfCompos;
        compoDao.openDbIfClosed();
        Cursor curs = compoDao.getDb().rawQuery("SELECT * FROM " + compoDao.COMPONENT_CLASS_NAME
                + " WHERE floor = ?",new String[]{String.valueOf(true)});
        listOfCompos = compoDao.getDataUtils().cursorToEntities(compoDao.COMPONENT_CLASS,curs);
        curs.close();
        return listOfCompos;
    }

    /**
     * Méthode de récupération de tous les types de dividers depuis la base de données
     * retourne une liste de Component
     * @return
     */
    public ArrayList<Component> getAllDividers(){
        ArrayList<Component> listOfCompos;
        compoDao.openDbIfClosed();
        Cursor curs = compoDao.getDb().rawQuery("SELECT * FROM " + compoDao.COMPONENT_CLASS_NAME
                + " WHERE divider = ?",new String[]{String.valueOf(true)});
        listOfCompos = compoDao.getDataUtils().cursorToEntities(compoDao.COMPONENT_CLASS,curs);
        curs.close();
        return listOfCompos;
    }

    /**
     * Méthode de récupération de tous les types de mur depuis la base de données
     * retourne une liste de Component
     * @return
     */
    public ArrayList<Component> getAllWalls() {
        ArrayList<Component> listOfCompos;
        compoDao.openDbIfClosed();
        Cursor curs = compoDao.getDb().rawQuery("SELECT * FROM " + compoDao.COMPONENT_CLASS_NAME
                + " WHERE complex = ? AND floor = ? AND divider = ?",new String[]{"true","false","false"});
        listOfCompos = compoDao.getDataUtils().cursorToEntities(compoDao.COMPONENT_CLASS,curs);
        curs.close();
        return listOfCompos;
    }

    /**
     * Retorune la liste des composant compatibles avec le slot lié au component dont l'id est idComponent et
     * dont le numéro de slot = number
     * @param idComponent id du composant auquel le slot est lié
     * @param number numéro du slot
     * @return la liste des composant compatibles avec ce slot
     */
    public ArrayList<Component> getCarpentriesBySlotNumber(long idComponent, int number){
        slotDao.openDbIfClosed();
        ArrayList<Component> result = new ArrayList<>();
        // Récupération de la liste des id des composant fils des slots ayant pour parent id, l'id idComponent et pour numéro de slot number
        ArrayList<Long> childCompoIds = slotService.getCompoIDBySlotAndNumber(idComponent,number);
        for (Long idcompo:childCompoIds) {
            Component compo = (Component) compoDao.getEntityById(idcompo,Component.class);
            result.add(compo);
        }
        return result;
    }

    /**
     * Méthode de récupération d'un composant en base de données
     * grâce à son ID
     * @param idCompo
     * @return
     */
    public Component getCompoById(long idCompo) {
        return (Component) compoDao.getEntityById(idCompo,Component.class);
    }

    /**
     * Récupère le composant sol du plan dont l'id est passé en paramètre
     * @param id du plan dont on veut récupérer le sol
     * @return le composant sol
     */
    public Component getSolFromPlanId(long id) {
        compoDao.openDbIfClosed();
        Cursor curs = compoDao.getDb().rawQuery(
                " SELECT c.* FROM component AS c" +
                        " JOIN placedcomponent AS pc ON c._id = pc.idcomponent " +
                        " WHERE pc.idplan = ? " +
                        " AND c.floor = ?" +
                        " AND c.complex = ?",new String[]{String.valueOf(id),"true","true"});
        ArrayList<Component> components = compoDao.getDataUtils().cursorToEntities(Component.class,curs);
        curs.close();
        if(components.size() > 0){
            return components.get(0);
        }
        return null;
    }
}
