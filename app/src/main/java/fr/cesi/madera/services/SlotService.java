package fr.cesi.madera.services;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import fr.cesi.madera.dao.daoSlot.SlotDao;
import fr.cesi.madera.entities.Component;
import fr.cesi.madera.entities.Slot;
import fr.cesi.madera.misc.DataBaseCreator;
import fr.cesi.madera.misc.exceptions.IncorrectNumberOfSlotException;

/**
 * Service de gestion des méthodes relatives
 * à la manipulation des slots
 */
public class SlotService {

    private Context ctx;
    private SlotDao slotDao;

    public SlotService(Context ctx) {
        this.ctx = ctx;
        slotDao = new SlotDao(ctx);
    }

    /**
     * Récupère un slot distinct pour chaque emplacement sur le mur
     * - Si le mur possède 3 emplacement le nombre de slot disctint doit ètre égal a 3
     * @param wall
     * @return le même nombre de slot distinct que
     * @throws IncorrectNumberOfSlotException
     */
    public ArrayList<Slot> getDistinctSlotByWall(Component wall)throws IncorrectNumberOfSlotException {
        int nbOfSlot = wall.getNbslot();
        if(nbOfSlot <= 0){
            throw new IncorrectNumberOfSlotException("Nombre de slot pour ce mur <= 0 ");
        }
        ArrayList<Slot> slots = getSlotsByComplexComponent(wall);
        ArrayList<Slot> result = new ArrayList<>();
        for(int i = 0 ; i < nbOfSlot ; i++){
            for (Slot slot:slots) {
                if(slot.getNumofslot() == i && result.size() < nbOfSlot){
                    result.add(slot);
                    break;
                }
            }
        }
        if(result.size() != wall.getNbslot()){
            throw new IncorrectNumberOfSlotException("Le nombre de slot distinct par leur numéro récupéré en " +
                    "base est différent du nombre d'emplacement pour ce mur");
        }
        return  result;
    }

    /**
     * Récupère le slot correspondant à ce mur pour ce composant et ayant le numéro numberOfSlot
     * @param wall mur portant le slot
     * @param carpentry composant lié au mur via le slot
     * @param numberOfSLot numéro du slot sur le mur
     * @return le slot correspondant
     */
    public Slot getIDSlotForThisComponent(Component wall, Component carpentry, int numberOfSLot) {
        slotDao.openDbIfClosed();
        Cursor curs = slotDao.getDb().rawQuery("SELECT * FROM " + slotDao.SLOT_CLASS_NAME
                + " WHERE idparentcomponent = ? AND idchildcomponent = ? AND numofslot = ? "
                ,new String[]{String.valueOf(wall.get_id()),String.valueOf(carpentry.get_id()),String.valueOf(numberOfSLot)});
        ArrayList<Slot> results = DataBaseCreator.dataUtils.cursorToEntities(slotDao.SLOT_CLASS,curs);
        curs.close();
        return results.get(0);
    }

    /**
     * Méthode de récupération d'un id par son composant mère et son nombre
     * @param idComponent
     * @param number
     * @return
     */
    public ArrayList<Long> getCompoIDBySlotAndNumber(long idComponent, int number){
        slotDao.openDbIfClosed();
        ArrayList<Long> childCompoIds = new ArrayList<>();
        Cursor curs = slotDao.getDb().rawQuery("SELECT idchildcomponent FROM " + slotDao.SLOT_CLASS_NAME
                + " WHERE idparentcomponent = ? AND numofslot = ?",new String[]{String.valueOf(idComponent),String.valueOf(number)});
        try {
            while (curs.moveToNext()) {
                childCompoIds.add(curs.getLong(0));
            }
        } finally {
            curs.close();
        }
        return  childCompoIds;
    }

    /**
     * Méthode de récupération d'un Slot en fonction de son mur
     * @param wall
     * @return
     */
    public ArrayList<Slot> getSlotsByComplexComponent(Component wall) {
        slotDao.openDbIfClosed();
        return slotDao.getSlotsFromComponent(wall);
    }

    /**
     * Méthode de récupération d'un slot par son ID
     * @param idslot
     * @return
     */
    public Slot getSlotById(long idslot) {
       slotDao.openDbIfClosed();
        return (Slot) slotDao.getEntityById(idslot,slotDao.SLOT_CLASS);
    }
}
