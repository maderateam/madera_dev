package fr.cesi.madera.services;

import android.content.Context;
import android.database.Cursor;

import java.util.Date;

import fr.cesi.madera.dao.daoDiscount.DiscountDao;

/**
 * Created by Doremus on 10/03/2017.
 */

public class DiscountService {
    DiscountDao discDAO;
    public DiscountService(Context ctx) {
        discDAO = new DiscountDao(ctx);
    }

    /**
     * Méthode de récupération d'un discounte disponible
     * en fonction du composant, de la gamme et de la date de validité du discount.
     * @param compoId
     * @param gammeID
     * @return
     */
    public float getDiscountByCompoOrGammeId(long compoId,long gammeID) {
        discDAO.openDbIfClosed();
        long thisDate = new Date().getTime();
        Cursor curs = discDAO.getDb().rawQuery("SELECT MAX(rate) FROM " + discDAO.DISCOUNT_CLASS_NAME +
                " WHERE idcompo = ? OR idgamme = ? AND dateexpir > ?",new String[]{compoId+"",gammeID+"",thisDate+""});
        curs.moveToFirst();
        float discount = curs.getFloat(0);
        curs.close();
        return discount;
    }
}
