package fr.cesi.madera.services;

import android.content.Context;

import fr.cesi.madera.dao.daoParameter.ParameterDao;

/**
 * Service de récupération
 * des données de la table parameter
 */

public class ParameterService {

    Context ctx;
    ParameterDao parameterDaoDAO;

    public ParameterService(Context ctx) {
        this.ctx = ctx;
        parameterDaoDAO = new ParameterDao(ctx);
    }

    /**
     * Méthode de récupération d'un paramètre
     * via son nom
     * @param parameterName
     * @return
     */
    public Object getParameterValue(String parameterName) {
        return parameterDaoDAO.getParameterValueFromName(parameterName);
    }
}
