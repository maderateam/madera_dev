package fr.cesi.madera.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;

import java.util.ArrayList;

import fr.cesi.madera.dao.daoPlacedComponent.PlacedComponentDao;
import fr.cesi.madera.entities.Component;
import fr.cesi.madera.entities.PlacedComponent;

/**
 * Service comprenant les methodes relatives
 * à la gestion et à la récupération des PlacedComponents
 */

public class PComponentService {

    private Context ctx;
    private PlacedComponentDao pcDao;

    public PComponentService(Context ctx) {
        this.ctx = ctx;
        this.pcDao = new PlacedComponentDao(ctx);
    }

    /**
     * Méthode de création de placedComponent à partir d'un composant
     * @param compo
     * @return
     */
    public PlacedComponent createPCFromComponent(Component compo){
        PlacedComponent pc = new PlacedComponent();
        pc.setHidden(false);
        SharedPreferences sharedpreferences = ctx.getSharedPreferences("settings", Context.MODE_PRIVATE);
        long idCom = sharedpreferences.getLong("idCom",0);
        pc.setIdcommercial(idCom);
        pc.setIdcomponent(compo.get_id());
        return pc;
    }

    /**
     * Méthode de récupération du placedComponent sol
     * à partir du plan et de l'id composant du sol
     * @param planId
     * @param floorCompoId
     * @return
     */
    public PlacedComponent getFloorPCFromPlanIdAndCompoId(long planId, long floorCompoId) {
        pcDao.openDbIfClosed();
        Cursor curs = pcDao.getDb().rawQuery(
                " SELECT pc.* FROM placedcomponent AS pc" +
                        " JOIN component AS c ON c._id = pc.idcomponent" +
                        " WHERE pc.idplan = ? " +
                        " AND pc.idcomponent = ?" +
                        " AND c.floor = ?",new String[]{String.valueOf(planId),String.valueOf(floorCompoId),"true"});
        PlacedComponent pc = (PlacedComponent) pcDao.getDataUtils().cursorToEntities(PlacedComponent.class,curs).get(0);
        curs.close();
        return pc;

    }

    /**
     * Méthode de récupération des dividers
     * en fonction du plan
     * @param id
     * @return
     */
    public ArrayList<PlacedComponent> getDividersFromPlanId(long id) {
        pcDao.openDbIfClosed();
        Cursor curs = pcDao.getDb().rawQuery(
                " SELECT pc.* FROM placedcomponent AS pc" +
                        " JOIN component AS c ON c._id = pc.idcomponent " +
                        " WHERE pc.idplan = ? " +
                        " AND c.divider = ?",new String[]{String.valueOf(id),"true"});
        ArrayList<PlacedComponent> pcDivides = pcDao.getDataUtils().cursorToEntities(PlacedComponent.class,curs);
        curs.close();
        return pcDivides;
    }

    /**
     * Méthode de récupération des murs d'un plan
     * à partir de son ID
     * @param id
     * @return
     */
    public ArrayList<PlacedComponent> getWallsFromPlanId(long id) {
        pcDao.openDbIfClosed();
        Cursor curs = pcDao.getDb().rawQuery(
                " SELECT pc.* FROM placedcomponent AS pc" +
                        " JOIN component AS c ON c._id = pc.idcomponent " +
                        " WHERE pc.idplan = ? " +
                        " AND c.floor = ?" +
                        " AND c.divider = ?" +
                        " AND c.complex = ?" +
                        " UNION " +
                        " SELECT pc.* FROM placedcomponent AS pc" +
                        " WHERE pc.idplan = ? " +
                        " AND pc.idcomponent = 0 " +
                        " AND pc.length > 0"
                        ,new String[]{id+"","false","false","true",id+""});
        ArrayList<PlacedComponent> pcDivides = pcDao.getDataUtils().cursorToEntities(PlacedComponent.class,curs);
        curs.close();
        return pcDivides;
    }

    /**
     * Méthode de récupération d'un placedComponent
     * par son ID
     * @param idPC
     * @return
     */

    public PlacedComponent getPCompoById(long idPC) {
        pcDao.openDbIfClosed();
        PlacedComponent pc = (PlacedComponent) pcDao.getEntityById(idPC,PlacedComponent.class);
        return pc;
    }

    /**
     * Méthode de suppression d'un placedComponent
     * Par son ID
     * @param id
     */
    public void removePCFromPlanId(long id) {
        pcDao.openDbIfClosed();
        pcDao.getDb().delete(pcDao.PCOMPONENT_CLASS_NAME,"idplan = ?",new String[]{String.valueOf(id)});
        pcDao.close();
    }
}
