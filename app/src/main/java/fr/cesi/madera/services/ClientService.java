package fr.cesi.madera.services;
import android.content.Context;

import java.util.ArrayList;

import fr.cesi.madera.dao.daoClient.ClientDao;
import fr.cesi.madera.dao.daoPlan.PlanDao;
import fr.cesi.madera.dao.daoProject.ProjectDao;
import fr.cesi.madera.entities.Client;
import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.Project;

/**
 * Service de gestion des méthodes de manipulation des clients
 *
 */

public class ClientService {
    ClientDao clientDAO;
    ProjectDao projDAO;
    PlanDao planDAO;
    Context ctx;

    public ClientService(Context ctx) {
        this.ctx = ctx;
        clientDAO = new ClientDao(ctx);
        projDAO = new ProjectDao(ctx);
        planDAO = new PlanDao(ctx);
    }

    /**
     * Methode de récupération d'un client en base
     * grâce à son ID
     * @param idclient
     * @return
     */
    public Client getClientById(long idclient) {
        return (Client) clientDAO.getEntityById(idclient,Client.class);
    }

    /**
     * Supprime (cache si déjà synchronisé) le client en argument
     * et supprime (cache si déjà synchronisé)tous ses projets et plan.
     * @param cli client que l'on veut supprimer
     */
    public void deleteCliAndRelatedProjects(Client cli) {
        // Suppression des projets et des plans
        projDAO.openDbIfClosed();
        ArrayList<Project> projects = projDAO.getProjectsByClient(cli);
        for (Project proj:projects) {
            ArrayList<Plan> plans = planDAO.getPlansByProject(proj);
            for (Plan plan:plans) {
                planDAO.deleteEntity(plan);
            }
            projDAO.deleteEntity(proj);
        }
        clientDAO.deleteEntity(cli);
    }

    /**
     * Méthode de récupération du nom et du prénom du client
     * grâce à son ID
     * @param idclient
     * @return
     */
    public String getClientNameById(long idclient) {
        clientDAO.openDbIfClosed();
        Client cli = (Client) clientDAO.getEntityById(idclient,clientDAO.CLIENT_CLASS);
        if(cli != null){
            return cli.getFirstname() + " " + cli.getLastname();
        }
        return "";
    }

    /**
     * Méthode de récupération d'un client en base
     * à partir de son projet
     * @param projet
     * @return
     */
    public Client getClientByProject(Project projet) {
        return clientDAO.getClientByProject(projet);
    }
}
