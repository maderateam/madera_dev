package fr.cesi.madera.services;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import fr.cesi.madera.dao.daoComponent.ComponentDao;
import fr.cesi.madera.dao.daoPlacedSlot.PlacedSlotDao;
import fr.cesi.madera.dao.daoSlot.SlotDao;
import fr.cesi.madera.entities.Component;
import fr.cesi.madera.entities.PlacedComponent;
import fr.cesi.madera.entities.PlacedSlot;
import fr.cesi.madera.entities.Slot;
import fr.cesi.madera.misc.exceptions.IncoherentPlacementDefinitionException;

/**
 * Service de gestion des méthode de manipulation
 * des placedSlot
 */

public class PSlotService {

    private Context ctx;
    private SlotDao slotDao;
    private ComponentDao compoDao;
    private PlacedSlotDao psDao;

    public PSlotService(Context ctx) {
        this.ctx = ctx;
        slotDao= new SlotDao(ctx);
        compoDao = new ComponentDao(ctx);
        psDao = new PlacedSlotDao(ctx);
    }


    /**
     * Méthode de récupération des slots d'un mur
     * @param wall
     * @return
     * @throws IncoherentPlacementDefinitionException
     */
    public ArrayList<Float> getSlotPlacement(Component wall) throws IncoherentPlacementDefinitionException {
        ArrayList<Float> placmtList = new ArrayList<>();
        ArrayList<Slot> slots = slotDao.getSlotsFromComponent(wall);
        for (Slot slot:slots) {
            if(!placmtList.contains(slot.getWidthplacement())){
                placmtList.add((float) slot.getWidthplacement());
            }
        }
        if(placmtList.size() != wall.getNbslot()){
            throw new IncoherentPlacementDefinitionException("Le nombre de positionnement différents " +
                    "ne coïncide pas avec le nombre de slots portés par le mur");
        }
        return  placmtList;
    }

    /**
     * Méthode de récupération des placedSlot depuis l'id du plan et un placedComponent wall
     * @param id
     * @param wall
     * @return
     */
    public ArrayList<PlacedSlot> getPSWallsFromWallAndPlanId(long id, PlacedComponent wall) {
        psDao.openDbIfClosed();
        Cursor curs = psDao.getDb().rawQuery(
                " SELECT ps.* FROM placedslot AS ps" +
                        " WHERE ps.idparentpcomponent = ?" +
                        " AND ps.idplan = ?",new String[]{String.valueOf(wall.get_id()),String.valueOf(id)});
        ArrayList<PlacedSlot> psWall = psDao.getDataUtils().cursorToEntities(PlacedSlot.class,curs);
        curs.close();
        return psWall;
    }

    /**
     * suppression des placedSlots à partir de l'id d'un plan
     * @param id
     */
    public void removePSFromPlanId(long id) {
        psDao.openDbIfClosed();
        psDao.getDb().delete(psDao.PSLOT_CLASS_NAME,"idplan = ?",new String[]{String.valueOf(id)});
        psDao.close();
    }
}
