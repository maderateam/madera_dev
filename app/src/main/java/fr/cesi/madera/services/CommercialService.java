package fr.cesi.madera.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;

import fr.cesi.madera.dao.EntityDao;
import fr.cesi.madera.dao.daoCommercial.CommercialDao;
import fr.cesi.madera.entities.Commercial;
import fr.cesi.madera.misc.exceptions.SynchronizationException;
import fr.cesi.madera.synchronization.SyncLauncher;

/**
 * Service permettant la connexion et l'attribution d'une tablette
 * à un commercial
 */

public class CommercialService {
    public CommercialDao comDAO;
    private Context ctx;

    public CommercialService(Context ctx) {
        this.ctx = ctx;
        comDAO = new CommercialDao(ctx);
    }

    /**
     * Méthode de connexion d'un commercial à son compte
     * et d'ajout d'une adresse mac à son compte pour
     * assignation d'une tablette au commercial
     * @param mail
     * @param pwd
     * @return
     */
    public Commercial connectCommercial(String mail, String pwd) throws SynchronizationException{
        SyncLauncher syncLauncher = new SyncLauncher(ctx);
        final SharedPreferences sharedpreferences = ctx.getSharedPreferences("settings", Context.MODE_PRIVATE);
        String serialcom = sharedpreferences.getString("comserial", null);
        Commercial com = null;
        if(serialcom == null){
            serialcom = Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);
            com = syncLauncher.getCommercial(mail,pwd,serialcom);
            if(com != null){
                EntityDao.setLockBatch(true);
                comDAO.insertEntity(com);
                EntityDao.setLockBatch(false);

            }else{
                throw new SynchronizationException("Erreur dans la récupération du commercial");
            }
        }
        return com;
    }
}
