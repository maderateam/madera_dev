package fr.cesi.madera.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import fr.cesi.madera.dao.daoComponent.ComponentDao;
import fr.cesi.madera.dao.daoCovering.CoveringDao;
import fr.cesi.madera.dao.daoDiscount.DiscountDao;
import fr.cesi.madera.dao.daoParameter.ParameterDao;
import fr.cesi.madera.dao.daoPlacedComponent.PlacedComponentDao;
import fr.cesi.madera.dao.daoPlan.PlanDao;
import fr.cesi.madera.dao.daoQuotation.QuotationDao;
import fr.cesi.madera.dao.daoQuotationLine.QuotationLineDao;
import fr.cesi.madera.entities.Component;
import fr.cesi.madera.entities.Covering;
import fr.cesi.madera.entities.PlacedComponent;
import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.Quotation;
import fr.cesi.madera.entities.QuotationLine;

/**
 * Created by Joshua on 10/02/2017.
 * Ce service a pour but de permettre la création des devis(Quotations)
 * des lignes de ce devis ( QuotationsLine)
 * et de la récupération de certaines informations nécéssaire à la réalisation du devis
 */

public class QuotationService {

    private QuotationDao quotationDao;
    private Context ctx;
    private ComponentDao compoDao;
    private PlacedComponentDao placedComponentDao;
    private DiscountDao discountDao;
    private QuotationLineDao quotationLineDao;
    private CoveringDao coveringDao;
    private PlanDao planDao;
    private ParameterDao parameterDao;

    public QuotationService(Context ctx) {
        this.ctx = ctx;
        this.compoDao = new ComponentDao(ctx);
        this.compoDao.open();
        this.quotationDao = new QuotationDao(ctx);
        this.quotationDao.open();
        this.placedComponentDao = new PlacedComponentDao(ctx);
        this.placedComponentDao.open();
        this.discountDao = new DiscountDao(ctx);
        this.discountDao.open();
        this.quotationLineDao = new QuotationLineDao(ctx);
        this.quotationLineDao.open();
        this.coveringDao = new CoveringDao(ctx);
        this.coveringDao.open();
        this.planDao = new PlanDao(ctx);
        this.planDao.open();
        this.parameterDao = new ParameterDao(ctx);
    }

    /**
     * Méthode de création et d'insertion d'un devis vide pour récupération d'id
     * @return
     */
    public long CreatePartialQuotation() {
        // Création du devis avec d'obtenir l'id en base pour les lignes du devis.
        Quotation quotation = new Quotation();
        //Envoi du devis partiellement remplis à la base afin d'obtenir un ID
        quotationDao.insertEntity(quotation);
        return quotation.get_id();
    }

    /**
     * Méthode de création de quotationLines Partielles
     * @param idQuotation
     * @param component
     * @return
     */
    // Création des QuotationsLine
    public QuotationLine CreateQuotationLine(long idQuotation, Component component) {
        QuotationLine quotationLine = new QuotationLine();
        quotationLine.setIdquotation(idQuotation);
        quotationLine.setDesignation(component.getDenomination());
        quotationLine.setIdcomponent(component.get_id());
        quotationLine.setUnitprice(component.getPrice());
        quotationLine.setIdgamme(component.getIdgamme());
        quotationLine.setDiscount(0);

        if (component.isComplex() == false) {
            quotationLine.setUnitofmeasure("U");
            quotationLine.setQuantity(1);
        } else {
            quotationLine.setUnitofmeasure("m²");
            quotationLine.setQuantity(component.getLength());
        }
        return quotationLine;
    }

    /**
     * Méthode de récupération de composants depuis un tableau de placedComponent
     * @param plan
     * @return
     */
    public ArrayList<Component> getQuotationComponents(Plan plan) {
        //Création de la liste de placedComponent du plan
        ArrayList<PlacedComponent> pComponentList = new ArrayList<PlacedComponent>();
        //Création de la liste de components du plan
        ArrayList<Component> componentList = new ArrayList<Component>();
        //Remplissage de la liste de placedComponent
        pComponentList = placedComponentDao.getPComponentByPlan(plan);
        for (int i = 0; i < pComponentList.size(); i++) {
            Component component = new Component();
            // Ajout du component à la liste de component du plan
            if (pComponentList.get(i).getIdcomponent() != 0) {
                component = compoDao.getComponentFromPComponent(pComponentList.get(i));
                if (component.isComplex()) {
                    if (component.isFloor()) {
                        int floor = getCoordsFromFloor(component);
                        component.setLength(floor);
                    } else {
                        component.setLength((float) pComponentList.get(i).getLength()*2.5f);
                    }
                }
                componentList.add(component);
            }
        }
        return componentList;
    }

    /**
     * Méthode de calcul d'un discount si necessaire
     * @param unitPrice
     * @param discount
     * @return
     */
    public double calculateDiscount(double unitPrice, double discount) {
        double unitPriceAfterDiscount = unitPrice - (unitPrice * discount / 100);
        return unitPriceAfterDiscount;
    }

    /**
     * // Méthode de calcul du montant avec tax
     * @param preTaxAmount
     * @param tax
     * @return
     */

    public double totalPriceWithTaxes(double preTaxAmount, double tax) {
        double taxCost = preTaxAmount * (tax / 100);
        double AllTaxIncluded = preTaxAmount + taxCost;
        return AllTaxIncluded;
    }

    /**
     * Création des quotationsLines partielles depuis le tableau de composants
     * @param componentsList
     * @param idQuotation
     * @return
     */
    private ArrayList<QuotationLine> makeQuotationLineFromComponent(ArrayList<Component> componentsList, long idQuotation) {

        ArrayList<QuotationLine> partialQuotationLines = new ArrayList<QuotationLine>();
        for (int i = 0; i < componentsList.size(); i++) {
            boolean alreadyExist = false;
            QuotationLine quotationLine = new QuotationLine();

            // On commence a créer un objet quotationLine en fonction du component
            quotationLine = CreateQuotationLine(idQuotation, componentsList.get(i));

            // Check si ajout de la quotationLine dans le tableau ou si ajout de quantitée
            // pour évité les doublons
            if (partialQuotationLines.size() == 0) {
                partialQuotationLines.add(quotationLine);
            } else {
                for (int e = 0; e < partialQuotationLines.size(); e++) {
                    //Log.i("MADERA COMPONENT", "ELSE");
                    if (partialQuotationLines.get(e).getIdcomponent() == quotationLine.getIdcomponent()) {
                        //Log.i("MADERA COMPONENT", "égalégal");
                        if (quotationLine.getUnitofmeasure() == "U") {
                            //Log.i("MADERA COMPONENT", "Unité U");
                            partialQuotationLines.get(e).setQuantity(partialQuotationLines.get(e).getQuantity() + 1);
                            alreadyExist = true;
                        } else {
                            //Log.i("MADERA COMPONENT", "mètres ²");
                            partialQuotationLines.get(e).setQuantity(partialQuotationLines.get(e).getQuantity() + quotationLine.getQuantity());
                            alreadyExist = true;
                        }
                    }
                }
                if (alreadyExist == false) {
                    partialQuotationLines.add(quotationLine);
                }
            }
        }
        return partialQuotationLines;
    }

    /**
     * Methode pour appliqué les calculs à chaques quotationLine
     * @param partialQuotationLine
     * @return
     */
    public ArrayList<QuotationLine> quotationLinesCalcul(ArrayList<QuotationLine> partialQuotationLine, boolean isInserted) {
        ArrayList<QuotationLine> quotationLines = new ArrayList<QuotationLine>();

        //Calcul du prix en fonction du nombre et des discounts disponible
        for (int i = 0; i < partialQuotationLine.size(); i++) {

            //Log.i("MADERA CALCUL DESIGN",partialQuotationLine.get(i).getDesignation());
            double discount = discountDao.getComponentDicount(partialQuotationLine.get(i).getIdcomponent(), partialQuotationLine.get(i).getIdgamme());

            // Ajout et calcul du prix unitaire en cas de disount (si discount il y a)
            if (discount != 0) {
                partialQuotationLine.get(i).setDiscount(discount);
                double disountedPrice = calculateDiscount(partialQuotationLine.get(i).getUnitprice(), partialQuotationLine.get(i).getDiscount());
                partialQuotationLine.get(i).setUnitprice(disountedPrice);
            }
            // calcul du total HT
            partialQuotationLine.get(i).setPretaxamount(partialQuotationLine.get(i).getQuantity() * partialQuotationLine.get(i).getUnitprice());

            // Récupération du pourcentage de tax dans la table paramètre
            ParameterDao parameterDao = new ParameterDao(ctx);
            double tax = (double) parameterDao.getParameterValueFromName("tax");
            partialQuotationLine.get(i).setTax(tax);

            //Calcul du montant total , tax incluse
            double allTaxIncluded = totalPriceWithTaxes(partialQuotationLine.get(i).getPretaxamount(), tax);
            partialQuotationLine.get(i).setAlltaxesincluded(allTaxIncluded);

            //insertion de la quotationLine complète dans la base
            QuotationLine finishedLine = new QuotationLine();
            finishedLine = partialQuotationLine.get(i);
            if (isInserted) {
                quotationLineDao.insertEntity(finishedLine);
            }

        }
        return partialQuotationLine;
    }

    /**
     * Méthode de calcul du prix total avec tax
     * @param quotationLines
     * @return
     */
    public double calculateTotalPriceWithTax(ArrayList<QuotationLine> quotationLines) {
        double totalPrice = 0;
        for (int i = 0; i < quotationLines.size(); i++) {
            totalPrice = totalPrice + quotationLines.get(i).getAlltaxesincluded();
        }
        return totalPrice;
    }

    /**
     * Méthode de calcul du prix total sans tax
     * @param quotationLines
     * @return
     */
    public double calculateTotalPriceWithoutTax(ArrayList<QuotationLine> quotationLines) {
        double totalPriceWT = 0;
        for (int i = 0; i < quotationLines.size(); i++) {
            totalPriceWT = totalPriceWT + quotationLines.get(i).getPretaxamount();
        }
        return totalPriceWT;
    }

    /**
     * Méthode de calcul du montant total économisé par les promotions
     * @param quotationLines
     * @return
     */
    public double totalDiscount(ArrayList<QuotationLine> quotationLines, ArrayList<Component> components) {
        double totalDiscount = 0;
        for (int i = 0; i < quotationLines.size(); i++) {
            if (quotationLines.get(i).getDiscount() > 0) {
                double moneySaved = 0;
                for (int e = 0; e < components.size(); e++) {
                    if (quotationLines.get(i).getIdcomponent() == components.get(e).get_id()) {
                        moneySaved = getDiscountDifference(quotationLines.get(i).getDiscount(), components.get(e).getPrice());
                    }
                }
                moneySaved = moneySaved * quotationLines.get(i).getQuantity();
                totalDiscount = totalDiscount + moneySaved;
            }
        }
        return totalDiscount;
    }

    /**
     * Méthode de calcul du discount par composant
     * @param discount
     * @param unitPrice
     * @return
     */
    public double getDiscountDifference(double discount, double unitPrice) {
        double moneySaved = 0;
        double didi = discount / 100;
        moneySaved = unitPrice * (discount / 100);
        return moneySaved;
    }

    /**
     * Méthode de récupération des coordonnées du sol
     *
     * @param component
     * @return
     */
    public int getCoordsFromFloor(Component component) {
        String getCoordinates = component.getCoordinates();
        String[] tokens = getCoordinates.split(":");
        return tokens.length;
    }


    /**
     * Méthode de calcul et de tri des coverings présent sur les éléments du plan
     * @param plan
     * @param idQuotation
     * @param isInserted
     * @return
     */
    private ArrayList<QuotationLine> createCoveringQuotationLines(Plan plan, long idQuotation, boolean isInserted) {

        ArrayList<QuotationLine> quotationLines = new ArrayList<QuotationLine>();
        ArrayList<QuotationLine> finalCoveringQuotationLines = new ArrayList<QuotationLine>();

        //Création de la liste de placedComponent du plan
        ArrayList<PlacedComponent> placedComponents = new ArrayList<PlacedComponent>();
        //Remplissage de la liste de placedComponent
        placedComponents = placedComponentDao.getPComponentByPlan(plan);
        for (int i = 0; i < placedComponents.size(); i++) {

            Component component = null;
            if (placedComponents.get(i).getIdcomponent() != 0) {
                component = (Component) compoDao.getEntityById(placedComponents.get(i).getIdcomponent(), Component.class);
            }

            if (component != null) {
                if (component.isFloor()) {
                    if (placedComponents.get(i).getIdcoveringin() != 0) {
                        int floor = getCoordsFromFloor(component);
                        Covering covering = (Covering) this.coveringDao.getEntityById(placedComponents.get(i).getIdcoveringin(), Covering.class);
                        QuotationLine quotationLine = new QuotationLine();
                        quotationLine.setIdcomponent(covering.get_id());
                        quotationLine.setCovering(true);
                        quotationLine.setUnitprice(covering.getPrice());
                        quotationLine.setDesignation("Revêtement : " + covering.getDenomination());
                        quotationLine.setIdquotation(idQuotation);
                        quotationLine.setQuantity(floor);
                        quotationLine.setUnitofmeasure("m²");
                        Double preTaxAmount = covering.getPrice() * floor;
                        quotationLine.setPretaxamount(preTaxAmount);
                        ParameterDao parameterDao = new ParameterDao(ctx);
                        double tax = (double) parameterDao.getParameterValueFromName("tax");
                        quotationLine.setTax(tax);
                        double TotalPriceWithTax = totalPriceWithTaxes(preTaxAmount, tax);
                        quotationLine.setAlltaxesincluded(TotalPriceWithTax);
                        quotationLines.add(quotationLine);
                    } else {

                        // On a un covering interieur pour ce placed component
                        if (placedComponents.get(i).getIdcoveringin() != 0) {
                            Covering covering = (Covering) this.coveringDao.getEntityById(placedComponents.get(i).getIdcoveringin(), Covering.class);
                            QuotationLine quotationLine = new QuotationLine();
                            quotationLine.setIdcomponent(covering.get_id());
                            quotationLine.setCovering(true);
                            quotationLine.setUnitprice(covering.getPrice());
                            quotationLine.setDesignation("Revêtement :" + covering.getDenomination());
                            quotationLine.setIdquotation(idQuotation);
                            quotationLine.setQuantity(placedComponents.get(i).getLength()*2.5f);
                            quotationLine.setUnitofmeasure("m²");
                            Double preTaxAmount = covering.getPrice() * placedComponents.get(i).getLength()*2.5f;
                            quotationLine.setPretaxamount(preTaxAmount);
                            ParameterDao parameterDao = new ParameterDao(ctx);
                            double tax = (double) parameterDao.getParameterValueFromName("tax");
                            quotationLine.setTax(tax);
                            double TotalPriceWithTax = totalPriceWithTaxes(preTaxAmount, tax);
                            quotationLine.setAlltaxesincluded(TotalPriceWithTax);
                            quotationLines.add(quotationLine);
                        }

                        if (placedComponents.get(i).getIdcoveringout() != 0) {
                            Covering covering = (Covering) this.coveringDao.getEntityById(placedComponents.get(i).getIdcoveringin(), Covering.class);
                            QuotationLine quotationLine = new QuotationLine();
                            quotationLine.setIdcomponent(covering.get_id());
                            quotationLine.setCovering(true);
                            quotationLine.setUnitprice(covering.getPrice());
                            quotationLine.setDesignation("Revêtement : " + covering.getDenomination());
                            quotationLine.setIdquotation(idQuotation);
                            quotationLine.setQuantity(placedComponents.get(i).getLength()*2.5f);
                            quotationLine.setUnitofmeasure("m²");
                            Double preTaxAmount = covering.getPrice() * placedComponents.get(i).getLength()*2.5f;
                            quotationLine.setPretaxamount(preTaxAmount);
                            ParameterDao parameterDao = new ParameterDao(ctx);
                            double tax = (double) parameterDao.getParameterValueFromName("tax");
                            quotationLine.setTax(tax);
                            double TotalPriceWithTax = totalPriceWithTaxes(preTaxAmount, tax);
                            quotationLine.setAlltaxesincluded(TotalPriceWithTax);
                            quotationLines.add(quotationLine);
                        }
                    }
                }
            } else {
                if (placedComponents.get(i).getIdcomponent() == 0) {
                    // On a un covering interieur pour ce placed component
                    if (placedComponents.get(i).getIdcoveringin() != 0) {
                        Covering covering = (Covering) this.coveringDao.getEntityById(placedComponents.get(i).getIdcoveringin(), Covering.class);
                        QuotationLine quotationLine = new QuotationLine();
                        quotationLine.setIdcomponent(covering.get_id());
                        quotationLine.setCovering(true);
                        quotationLine.setUnitprice(covering.getPrice());
                        quotationLine.setDesignation("Revêtement :" + covering.getDenomination());
                        quotationLine.setIdquotation(idQuotation);
                        quotationLine.setQuantity(placedComponents.get(i).getLength()*2.5f);
                        quotationLine.setUnitofmeasure("m²");
                        Double preTaxAmount = covering.getPrice() * placedComponents.get(i).getLength()*2.5f;
                        quotationLine.setPretaxamount(preTaxAmount);
                        ParameterDao parameterDao = new ParameterDao(ctx);
                        double tax = (double) parameterDao.getParameterValueFromName("tax");
                        quotationLine.setTax(tax);
                        double TotalPriceWithTax = totalPriceWithTaxes(preTaxAmount, tax);
                        quotationLine.setAlltaxesincluded(TotalPriceWithTax);
                        quotationLines.add(quotationLine);
                    }

                    if (placedComponents.get(i).getIdcoveringout() != 0) {
                        Covering covering = (Covering) this.coveringDao.getEntityById(placedComponents.get(i).getIdcoveringin(), Covering.class);
                        QuotationLine quotationLine = new QuotationLine();
                        quotationLine.setIdcomponent(covering.get_id());
                        quotationLine.setCovering(true);
                        quotationLine.setUnitprice(covering.getPrice());
                        quotationLine.setDesignation("Revêtement :" + covering.getDenomination());
                        quotationLine.setIdquotation(idQuotation);
                        quotationLine.setQuantity(placedComponents.get(i).getLength()*2.5f);
                        quotationLine.setUnitofmeasure("m²");
                        Double preTaxAmount = covering.getPrice() * placedComponents.get(i).getLength()*2.5f;
                        quotationLine.setPretaxamount(preTaxAmount);
                        ParameterDao parameterDao = new ParameterDao(ctx);
                        double tax = (double) parameterDao.getParameterValueFromName("tax");
                        quotationLine.setTax(tax);
                        double TotalPriceWithTax = totalPriceWithTaxes(preTaxAmount, tax);
                        quotationLine.setAlltaxesincluded(TotalPriceWithTax);
                        quotationLines.add(quotationLine);
                    }
                }
            }
        }

        // Boucle de tri des coverings
        boolean isAlreadyThere = false;
        for (int e = 0; e < quotationLines.size(); e++) {
            isAlreadyThere = false;
            if (finalCoveringQuotationLines.size() == 0) {
                finalCoveringQuotationLines.add(quotationLines.get(e));
            } else {
                for (int b = 0; b < finalCoveringQuotationLines.size(); b++) {
                    if (quotationLines.get(e).getIdcomponent() == finalCoveringQuotationLines.get(b).getIdcomponent()) {
                        isAlreadyThere = true;
                        finalCoveringQuotationLines.get(b).setQuantity(finalCoveringQuotationLines.get(b).getQuantity() + quotationLines.get(e).getQuantity());
                        finalCoveringQuotationLines.get(b).setPretaxamount(finalCoveringQuotationLines.get(b).getPretaxamount() + quotationLines.get(e).getPretaxamount());
                        finalCoveringQuotationLines.get(b).setAlltaxesincluded(finalCoveringQuotationLines.get(b).getAlltaxesincluded() + quotationLines.get(e).getAlltaxesincluded());
                    }
                }
                if (isAlreadyThere == false){
                    finalCoveringQuotationLines.add(quotationLines.get(e));
                }
            }
        }
        return finalCoveringQuotationLines;
    }

    public Quotation getQuotationByPlanId(long id) {
        quotationDao.openDbIfClosed();
        Cursor curs = quotationDao.getDb().rawQuery("SELECT * FROM " + quotationDao.getSQLiteView(quotationDao.QUOTATION_CLASS_NAME)
                + " WHERE idplan = ?", new String[]{id + ""});
        ArrayList<Quotation> entities = quotationDao.getDataUtils().cursorToEntities(quotationDao.QUOTATION_CLASS, curs);
        curs.close();
        if (entities.size() > 0) {
            return entities.get(0);
        }
        return null;
    }

    /**
     *
     * Méthode de calcul du prix HT sans ajout dans la base
     * @param plan
     * @return
     */
    public double getTempPriceHT(Plan plan) {

        // Récupération des composants nécéssaire à la réalisation du devis
        ArrayList<Component> componentList = new ArrayList<Component>();
        componentList = getQuotationComponents(plan);

        ArrayList<QuotationLine> quotationLines = new ArrayList<QuotationLine>();
        ArrayList<QuotationLine> quotationLinesCovering = new ArrayList<QuotationLine>();
        ArrayList<QuotationLine> quotationLinesSimplewall = new ArrayList<QuotationLine>();

        // Création d'une tableau de quotationLine partiel
        quotationLines = makeQuotationLineFromComponent(componentList, 0);
        quotationLinesCovering = createCoveringQuotationLines(plan, 0, false);
        quotationLinesSimplewall = generationSimpleWallQuotationLine(plan, 0);

        for (int i = 0; i < quotationLinesCovering.size(); i++) {
            quotationLines.add(quotationLinesCovering.get(i));
        }

        for (int i = 0; i < quotationLinesSimplewall.size(); i++) {
            quotationLines.add(quotationLinesSimplewall.get(i));
        }

        //calculs finaux de quotationLine
        quotationLines = quotationLinesCalcul(quotationLines, false);
        double htPrice;
        htPrice = calculateTotalPriceWithoutTax(quotationLines);
        return htPrice;
    }


    /**
     * Méthode de calcul temporaire de la TVA sans ajout dans la base
     * @param plan
     * @return
     */
    public double getTempTVA(Plan plan) {
        double htPrice = getTempPriceHT(plan);
        double ttcPrice = getTempTTC(plan);
        double tempTVA = ttcPrice - htPrice;
        return tempTVA;
    }

    /**
     * Méthode de calcul du total TTC sans ajout dans la base
     * @param plan
     * @return
     */
    public double getTempTTC(Plan plan) {
        // Récupération des composants nécéssaire à la réalisation du devis
        ArrayList<Component> componentList = new ArrayList<Component>();
        componentList = getQuotationComponents(plan);

        ArrayList<QuotationLine> quotationLines = new ArrayList<QuotationLine>();
        ArrayList<QuotationLine> quotationLinesCovering = new ArrayList<QuotationLine>();
        ArrayList<QuotationLine> quotationLinesSimplewall = new ArrayList<QuotationLine>();
        // Création d'une tableau de quotationLine partiel
        quotationLines = makeQuotationLineFromComponent(componentList, 0);
        quotationLinesCovering = createCoveringQuotationLines(plan, 0, false);
        quotationLinesSimplewall = generationSimpleWallQuotationLine(plan, 0);

        for (int i = 0; i < quotationLinesCovering.size(); i++) {
            quotationLines.add(quotationLinesCovering.get(i));
        }

        for (int i = 0; i < quotationLinesSimplewall.size(); i++) {
            quotationLines.add(quotationLinesSimplewall.get(i));
        }

        //calculs finaux de quotationLine
        quotationLines = quotationLinesCalcul(quotationLines, false);
        double ttcPrice;
        ttcPrice = calculateTotalPriceWithTax(quotationLines);
        return ttcPrice;
    }

    /**
     * Methode de création des murs simple
     * @param plan
     * @param idQuotation
     * @return
     */

    public ArrayList<QuotationLine> generationSimpleWallQuotationLine(Plan plan, long idQuotation) {

        ArrayList<PlacedComponent> pComponentList = new ArrayList<>();
        ArrayList<QuotationLine> quotationLines = new ArrayList<>();
        pComponentList = placedComponentDao.getPComponentByPlan(plan);

        for (int i = 0; i < pComponentList.size(); i++) {

            if (pComponentList.get(i).getIdcomponent() == 0) {

                if (quotationLines.size() == 0) {
                    QuotationLine quotationLine = new QuotationLine();
                    quotationLine.setIdcomponent(pComponentList.get(i).getIdcomponent());
                    quotationLine.setIdquotation(idQuotation);
                    quotationLine.setQuantity(pComponentList.get(i).getLength()*2.5f);
                    quotationLine.setUnitofmeasure("m²");
                    quotationLine.setDesignation("Mur simple");
                    Double prixMur = (Double) parameterDao.getParameterValueFromName("wallPrice");
                    Double preTaxAmount = pComponentList.get(i).getLength() * prixMur;
                    quotationLine.setPretaxamount(preTaxAmount);
                    quotationLine.setUnitprice(prixMur);
                    double tax = (Double) parameterDao.getParameterValueFromName("tax");
                    quotationLine.setTax(tax);
                    double TotalPriceWithTax = totalPriceWithTaxes(preTaxAmount, tax);
                    quotationLine.setAlltaxesincluded(TotalPriceWithTax);
                    quotationLines.add(quotationLine);
                } else {
                    quotationLines.get(0).setQuantity(quotationLines.get(0).getQuantity() + pComponentList.get(i).getLength()*2.5f);
                    Double prixMur = (Double) parameterDao.getParameterValueFromName("wallPrice");
                    Double preTaxAmount = pComponentList.get(i).getLength() * prixMur;
                    quotationLines.get(0).setPretaxamount(quotationLines.get(0).getPretaxamount() + preTaxAmount);
                    double tax = (Double) parameterDao.getParameterValueFromName("tax");
                    double totalPriceWithTax = totalPriceWithTaxes(preTaxAmount, tax);
                    quotationLines.get(0).setAlltaxesincluded(quotationLines.get(0).getAlltaxesincluded() + totalPriceWithTax);
                }
            }
        }
        return quotationLines;
    }

    /**
     * Methode globale de création de devis et de quotation lines
     * @param plan
     * @return
     */
    public ArrayList<QuotationLine> generateQuotation(Plan plan) {

        Quotation finalQuotation = new Quotation();
        quotationDao.insertEntity(finalQuotation);
        // Récupération des composants nécéssaire à la réalisation du devis
        ArrayList<Component> componentList = new ArrayList<Component>();
        componentList = getQuotationComponents(plan);

        ArrayList<QuotationLine> quotationLines = new ArrayList<QuotationLine>();
        ArrayList<QuotationLine> quotationLinesCovering = new ArrayList<QuotationLine>();
        ArrayList<QuotationLine> quotationLinesSimplewall = new ArrayList<QuotationLine>();

        // Création d'une tableau de quotationLine partiel
        quotationLines = makeQuotationLineFromComponent(componentList, finalQuotation.get_id());
        quotationLinesCovering = createCoveringQuotationLines(plan, finalQuotation.get_id(), true);
        quotationLinesSimplewall = generationSimpleWallQuotationLine(plan, finalQuotation.get_id());

        for (int i = 0; i < quotationLinesCovering.size(); i++) {
            quotationLines.add(quotationLinesCovering.get(i));
        }

        for (int i = 0; i < quotationLinesSimplewall.size(); i++) {
            quotationLines.add(quotationLinesSimplewall.get(i));
        }

        //calculs finaux de quotationLine
        quotationLines = quotationLinesCalcul(quotationLines, true);
        double ttcPrice;
        double htPrice;
        double moneySaved;

        ttcPrice = calculateTotalPriceWithTax(quotationLines);
        htPrice = calculateTotalPriceWithoutTax(quotationLines);
        moneySaved = totalDiscount(quotationLines, componentList);

        SharedPreferences sharedpreferences = ctx.getSharedPreferences("settings", Context.MODE_PRIVATE);
        long idCom = sharedpreferences.getLong("idCom", 0);
        finalQuotation.setIdcommercial(idCom);
        finalQuotation.setIdPlan(plan.get_id());
        finalQuotation.setDatecreated(new Timestamp(new Date().getTime()));
        finalQuotation.setValidated(false);
        finalQuotation.setPricewithouttax(htPrice);
        finalQuotation.setTotalDiscount(moneySaved);
        finalQuotation.setPricefinal(ttcPrice);

        double tax = (double) parameterDao.getParameterValueFromName("tax");
        finalQuotation.setTax(tax);
        quotationDao.updateEntity(finalQuotation);
        plan.setIdQuotation(finalQuotation.get_id());
        this.planDao.updateEntity(plan);
        return quotationLines;
    }

    public void deleteQuotation(Quotation quotation) {
        quotationLineDao.openDbIfClosed();
        Cursor curs = quotationLineDao.getDb().rawQuery("SELECT * FROM " + quotationLineDao.getSQLiteView(quotationLineDao.QUOTATION_LINE_CLASS_NAME)
        + " WHERE idquotation = ?",new String[]{""+quotation.get_id()});
        ArrayList<QuotationLine> quotLines = quotationLineDao.getDataUtils().cursorToEntities(quotationLineDao.QUOTATION_LINE_CLASS,curs);

        for (QuotationLine quotLine: quotLines) {
            quotationLineDao.deleteEntity(quotLine);
        }
        quotationDao.deleteEntity(quotation);
    }

    public Quotation getQuotationByPlan(Plan plan) {
        return quotationDao.getQuotationByPlan(plan);
    }

    public void updateEntity(Quotation quotation) {

        quotationDao.updateEntity(quotation);
    }
}
