package fr.cesi.madera.services;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import fr.cesi.madera.dao.daoComponent.ComponentDao;
import fr.cesi.madera.dao.daoPlacedComponent.PlacedComponentDao;
import fr.cesi.madera.dao.daoPlacedSlot.PlacedSlotDao;
import fr.cesi.madera.dao.daoPlan.PlanDao;
import fr.cesi.madera.entities.Component;
import fr.cesi.madera.entities.PlacedComponent;
import fr.cesi.madera.entities.PlacedSlot;
import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.Project;

/**
 * Service de gestion des méthodes relatives
 * à la manipulation des plans
 */

public class PlanService {
    /**
     * Contexte permettant d'instancier les DAO
     */
    private Context ctx;
    /**
     * DAO de manipulation de plan
     */
    private PlanDao planDAO;
    /**
     * DAO de manipulation des PlacedComponent
     */
    private PlacedComponentDao pCompoDAO;
    /**
     * DAO de manipulation des PlacedSlot
     */
    private PlacedSlotDao pSlotDAO;
    private ComponentDao compoDao;

    public PlanService(Context ctx) {
        this.ctx = ctx;
        planDAO = new PlanDao(ctx);
        planDAO.open();
        pCompoDAO = new PlacedComponentDao(ctx);
        pCompoDAO.open();
        pSlotDAO = new PlacedSlotDao(ctx);
        pSlotDAO.open();
        compoDao = new ComponentDao(ctx);
    }


    /**
     * Clonage d'un plan et de tous ses PlacedComponent et PlacedSlot fils et insertion en BD
     *
     * @param oldPlan
     * @return
     */
    public Plan addPlanToArchive(Plan oldPlan) {
        Plan newPlan = clonePlan(oldPlan);
        newPlan.setArchived(true);
        planDAO.updateEntity(newPlan);
        return newPlan;
    }

    /**
     * Clonage d'un Plan
     *
     * @param plan
     * @return
     */
    public Plan clonePlan(Plan plan) {
        Plan newPlan = new Plan();
        newPlan.setIdcommercial(plan.getIdcommercial());
        newPlan.setDescription(plan.getDescription());
        newPlan.setName(plan.getName());
        newPlan.setArchived(plan.isArchived());
        newPlan.setDatecreated(new Timestamp(plan.getDatecreated().getTime()));
        // On l'insère en BD et on fixe l'ID
        newPlan = (Plan) planDAO.insertEntity(newPlan);
        // On initialise la DAO PComponent
        pCompoDAO = new PlacedComponentDao(ctx);
        ArrayList<PlacedComponent> pCompos = pCompoDAO.getPComponentByPlan(plan);
        // Récupération de PComponent de premier niveau
        for (PlacedComponent pCompo : pCompos) {
            Component compo = (Component) compoDao.getEntityById(pCompo.getIdcomponent(), compoDao.COMPONENT_CLASS);
            if (pCompo.getIdcomponent() == 0 || compo.isDivider() || compo.isFloor()) {
                PlacedComponent pcDiv = clonePComponent(pCompo, newPlan);
                pCompoDAO.insertEntity(pcDiv);
            } else if (compo.isComplex()) {
                addPCompoAndSlotRecurs(newPlan, pCompo, null);
            }
        }
        return newPlan;
    }

    /**
     * Parcours récursif de l'arbre des PComposant et des PSlot qui consitue le Plan.
     * Ajout à chaque étape des élément cloné de l'arbre
     *
     * @param newPlan
     * @param pCompoCloneable
     * @param parentSlotCloned
     */
    private void addPCompoAndSlotRecurs(Plan newPlan, PlacedComponent pCompoCloneable, PlacedSlot parentSlotCloned) {
        // On clone le composant
        PlacedComponent pCompoCloned = clonePComponent(pCompoCloneable, newPlan);
        // On insère le pComposant cloné
        pCompoDAO.insertEntity(pCompoCloned);
        // Si c'est un composant de plus haut niveau alors il n'a pas de Slot parent
        if (parentSlotCloned != null) {
            pCompoCloned.setIdparentpslot(parentSlotCloned.get_id());
            parentSlotCloned.setIdchildpcomponent(pCompoCloned.get_id());
            pSlotDAO.updateEntity(parentSlotCloned);
            pCompoDAO.updateEntity(pCompoCloned);
        }

        // On récupère la liste des pSlot fils du pComponent
        for (PlacedSlot pSlotCloneable : (ArrayList<PlacedSlot>) pSlotDAO.getChildPSlotsFromPComponent(pCompoCloneable)) {
            // On clone chaue pSlot
            PlacedSlot pSlotCloned = clonePSlot(pSlotCloneable, newPlan);
            // lui associe en tant que père le pComposant cloné
            pSlotCloned.setIdparentpcomponent(pCompoCloned.get_id());
            // On l'insère en BD
            pSlotDAO.insertEntity(pSlotCloned);
            // On réupère le pComposant fils du pSlot pour qu'il soit cloné plus tard
            PlacedComponent childPCompoCloneable = pCompoDAO.getChildPComponentFromPSlot(pSlotCloneable);
            // On rapelle la fonction avec le pComposant à cloner et le pSlot qui vient de l'être
            if(childPCompoCloneable != null) {
                addPCompoAndSlotRecurs(newPlan, childPCompoCloneable, pSlotCloned);
            }
        }
    }

    /**
     * Clonage d'un PComponent
     *
     * @param oldCompo
     * @param newPlan
     * @return
     */
    private PlacedComponent clonePComponent(PlacedComponent oldCompo, Plan newPlan) {
        PlacedComponent newCompo = new PlacedComponent();
        newCompo.setIdcomponent(oldCompo.getIdcomponent());
        newCompo.setIdcommercial(oldCompo.getIdcommercial());
        newCompo.setIdplan(newPlan.get_id());
        newCompo.setLength(oldCompo.getLength());
        newCompo.setPosX(oldCompo.getPosX());
        newCompo.setPosY(oldCompo.getPosY());
        newCompo.setPosZ(oldCompo.getPosZ());
        newCompo.setLength(oldCompo.getLength());
        newCompo.setIdcoveringin(oldCompo.getIdcoveringin());
        newCompo.setIdcoveringout(oldCompo.getIdcoveringout());
        newCompo.setHidden(false);
        return newCompo;
    }

    private PlacedSlot clonePSlot(PlacedSlot oldSlot, Plan newPlan) {
        PlacedSlot newPSlot = new PlacedSlot();
        newPSlot.setHidden(false);
        newPSlot.setPosX(oldSlot.getPosX());
        newPSlot.setPosY(oldSlot.getPosY());
        newPSlot.setPosZ(oldSlot.getPosZ());
        newPSlot.setIdplan(newPlan.get_id());
        newPSlot.setNumofslot(oldSlot.getNumofslot());
        return newPSlot;
    }

    public void deletePlan(Plan plan) {
        planDAO.openDbIfClosed();
        if (plan.getLastsync() == null || plan.getLastsync().getTime() <= 0) {
            ArrayList<PlacedComponent> pCompos = pCompoDAO.getPComponentByPlan(plan);
            for (PlacedComponent pc : pCompos) {
                Log.i("madera", "Suppression du PC");
                pCompoDAO.deleteEntity(pc);
            }
            ArrayList<PlacedSlot> pSlots = pSlotDAO.getPSlotsByPlan(plan);
            for (PlacedSlot ps : pSlots) {
                Log.i("madera", "Suppression du PS");
                pSlotDAO.deleteEntity(ps);
            }
        }
        planDAO.deleteEntity(plan);
    }

    /**
     * Cérifie si le plan est vide
     *
     * @param plan
     * @return true si il est vide, false sinon
     */
    public boolean isPlanEmpty(Plan plan) {
        Cursor curs = planDAO.getDb().rawQuery("SELECT COUNT (*) FROM " + pCompoDAO.getSQLiteView(pCompoDAO.PCOMPONENT_CLASS_NAME) +
                " WHERE idplan = ?", new String[]{String.valueOf(plan.get_id())});
        int count = -1;
        if (curs.getCount() > 0) {
            curs.moveToFirst();
            count = curs.getInt(0);
        }
        curs.close();
        return count == 0;
    }

    /**
     * Insertion d'un plan
     * @param plan
     */
    public void insertPlan(Plan plan) {
        planDAO.insertEntity(plan);
    }

    /**
     * Méthode de récupération des plans archivés
     * @return
     */
    public List<Plan> listOfArchivedPlans() {
        planDAO.openDbIfClosed();
        return planDAO.listOfArchivedPlans();
    }

    /**
     * Méthode de récupération d'un liste de plan
     * d'un projet
     * @param projet
     * @return
     */
    public List<Plan> getPlansByProject(Project projet) {
        return planDAO.getPlansByProject(projet);
    }
}
