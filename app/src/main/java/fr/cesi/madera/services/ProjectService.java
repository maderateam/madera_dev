package fr.cesi.madera.services;

import android.content.Context;

import fr.cesi.madera.dao.daoProject.ProjectDao;
import fr.cesi.madera.entities.Project;

/**
 * Service de gestion des méthodes de manipulation des projets
 */

public class ProjectService {
    ProjectDao projDAO;
    public ProjectService(Context ctx) {
        projDAO = new ProjectDao(ctx);

    }

    /**
     * Méthode de récupération d'un projet par son ID
     * @param idProject
     * @return
     */
    public Project getProjectById(long idProject) {
        projDAO.openDbIfClosed();
        return (Project) projDAO.getEntityById(idProject,projDAO.PROJECT_CLASS);
    }
}
