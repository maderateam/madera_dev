package fr.cesi.madera.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import fr.cesi.libgdx.entities.models.ComplexModel;
import fr.cesi.madera.misc.DatabaseColumn;
import fr.cesi.madera.misc.ForeignKey;
import fr.cesi.madera.misc.NumericBooleanDeserializer;

/**
 * Composant d'un plan
 */
public class Component extends Entity implements Parcelable{
	@DatabaseColumn
    @JsonProperty
	private String denomination;
	@DatabaseColumn
    @JsonProperty
	@JsonDeserialize(using=NumericBooleanDeserializer.class)
    @JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
	private boolean complex;
	@DatabaseColumn
    @JsonProperty
	private float width;
	@DatabaseColumn
    @JsonProperty
	private float height;
	@DatabaseColumn
    @JsonProperty
	private float length;
	@DatabaseColumn
    @JsonProperty
	private double price;
	@DatabaseColumn
    @JsonProperty
	private int nbslot;
	@DatabaseColumn
    @JsonProperty
	@ForeignKey(table = "gamme",column = "_id")
	private long idgamme;
    @DatabaseColumn
    @JsonProperty
    @ForeignKey(table = "covering",column = "_id")
    private long idcovering;
	@DatabaseColumn
    @JsonProperty
	@JsonDeserialize(using=NumericBooleanDeserializer.class)
	private boolean floor;
	@DatabaseColumn
    @JsonProperty
	@JsonDeserialize(using=NumericBooleanDeserializer.class)
	private boolean divider;
    @JsonProperty
	@DatabaseColumn
	private String iconname;
    @JsonProperty
	@DatabaseColumn
	private String modelpath;
    @JsonProperty
	@DatabaseColumn
	private String coordinates;

    public Component() {
    }

    public Component(Boolean bool){
        this();
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public boolean isComplex() {
        return complex;
    }

    public void setComplex(boolean complex) {
        this.complex = complex;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getNbslot() {
        return nbslot;
    }

    public void setNbslot(int nbslot) {
        this.nbslot = nbslot;
    }

    public long getIdgamme() {
        return idgamme;
    }

    public void setIdgamme(long idgamme) {
        this.idgamme = idgamme;
    }

    public long getIdcovering() {
        return idcovering;
    }

    public void setIdcovering(long idcovering) {
        this.idcovering = idcovering;
    }

    public boolean isFloor() {
        return floor;
    }

    public void setFloor(boolean floor) {
        this.floor = floor;
    }

    public boolean isDivider() {
        return divider;
    }

    public void setDivider(boolean divider) {
        this.divider = divider;
    }

    public String getIconname() {
        return iconname;
    }

    public void setIconname(String iconname) {
        this.iconname = iconname;
    }

    public String getModelpath() {
        return modelpath;
    }

    public void setModelpath(String modelpath) {
        this.modelpath = modelpath;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public static Creator<Component> getCREATOR() {
        return CREATOR;
    }

    @Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest,flags);
		dest.writeString(this.denomination);
		dest.writeFloat(this.width);
		dest.writeFloat(this.height);
		dest.writeFloat(this.length);
		dest.writeDouble(this.price);
		dest.writeByte((byte) (this.complex ? 1 : 0));
		dest.writeInt(this.nbslot);
		dest.writeLong(this.idgamme);
        dest.writeLong(this.idcovering);
		dest.writeByte((byte) (this.floor ? 1 : 0));
        dest.writeByte((byte) (this.divider ? 1 : 0));
		dest.writeString(this.iconname);
		dest.writeString(this.modelpath);
		dest.writeString(this.coordinates);
	}
	public static final Creator<Component> CREATOR = new Creator<Component>() {
		@Override
		public Component createFromParcel(Parcel source)
		{
			return new Component(source);
		}
		@Override
		public Component[] newArray(int size)
		{
			return new Component[size];
		}
	};

	public Component(Parcel in) {
		super(in);
		this.denomination = in.readString();
		this.width = in.readFloat();
		this.height = in.readFloat();
		this.length = in.readFloat();
		this.price = in.readDouble();
		this.complex = in.readByte() != 0;
		this.nbslot = in.readInt();
		this.idgamme = in.readLong();
        this.idcovering = in.readLong();
		this.floor = in.readByte() != 0;
        this.divider = in.readByte() != 0;
		this.iconname = in.readString();
		this.modelpath = in.readString();
		this.coordinates = in.readString();
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof ComplexModel && ((Component)o).get_id() == get_id();
	}
}
