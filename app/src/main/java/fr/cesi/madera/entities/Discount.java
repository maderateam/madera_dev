/**
 * 
 */
package fr.cesi.madera.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.sql.Timestamp;

import fr.cesi.madera.misc.DatabaseColumn;
import fr.cesi.madera.misc.ForeignKey;

/**
 * Classe représentant une réduction applicable sur une gamme ou sur un composant
*/

public class Discount extends Entity implements Parcelable{

	@DatabaseColumn
	private double rate;
	@DatabaseColumn
	@ForeignKey(table = "gamme",column ="_id" )
	private long idgamme;
	@DatabaseColumn
	@ForeignKey(table = "component",column ="_id" )
	private long idcompo;
	@DatabaseColumn
	private Timestamp dateexpir;

	public Discount(Boolean boool){
		this();
	}

	public Discount(){}

	public Discount(double rate, long idgamme, long idcompo, Timestamp dateexpir) {
		this.rate = rate;
		this.idgamme = idgamme;
		this.idcompo = idcompo;
		this.dateexpir = dateexpir;
	}

	public Discount(long _id, double rate, long idgamme, long idcompo, Timestamp dateexpir) {
		super(_id);
		this.rate = rate;
		this.idgamme = idgamme;
		this.idcompo = idcompo;
		this.dateexpir = dateexpir;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public Timestamp getDateexpir() {
		return dateexpir;
	}

	public void setDateexpir(Timestamp dateexpir) {
		this.dateexpir = dateexpir;
	}

	public long getIdcompo() {
		return idcompo;
	}

	public void setIdcompo(long idcompo) {
		this.idcompo = idcompo;
	}

	public long getIdgamme() {
		return idgamme;
	}

	public void setIdgamme(long idgamme) {
		this.idgamme = idgamme;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest,flags);
        dest.writeDouble(this.rate);
		dest.writeLong(this.idgamme);
		dest.writeLong(this.idcompo);
		if(this.dateexpir != null) {
			dest.writeLong(this.dateexpir.getTime());
		}else{
			dest.writeLong(-1);
		}

	}
	public static final Creator<Discount> CREATOR = new Creator<Discount>()
	{
		@Override
		public Discount createFromParcel(Parcel source)
		{
			return new Discount(source);
		}

		@Override
		public Discount[] newArray(int size)
		{
			return new Discount[size];
		}
	};

	public Discount(Parcel in) {
		super(in);
        this.rate = in.readDouble();
		this.idgamme = in.readLong();
		this.idcompo = in.readLong();
		Long date = in.readLong();
		if(date != -1) {
			this.dateexpir = new Timestamp(date);
		}else{
			this.dateexpir = null;
		}

	}
}
