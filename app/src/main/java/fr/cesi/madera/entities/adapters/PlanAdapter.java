package fr.cesi.madera.entities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import fr.cesi.madera.R;
import fr.cesi.madera.entities.Plan;

/**
 * Created by Doremus on 16/10/2016.
 */

public class PlanAdapter extends ArrayAdapter<Plan> {
    public PlanAdapter(Context context, List<Plan> plans) {
        super(context, 0, plans);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.plan_list_cell,parent, false);
        }

        PlanViewHolder viewHolder = (PlanViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new PlanViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.compo_denom_cell_list);
            viewHolder.ref = (TextView) convertView.findViewById(R.id.plan_ref_cell_list);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List<Tweet> tweets
        Plan plan = getItem(position);

        //il ne reste plus qu'à remplir notre vue
        viewHolder.name.setText(plan.getName());
        // viewHolder.ref.setText(plan.getRef());

        return convertView;
    }

    private class PlanViewHolder{
        public TextView name;
        public TextView ref;
    }
}
