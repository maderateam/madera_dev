package fr.cesi.madera.entities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import fr.cesi.madera.R;
import fr.cesi.madera.entities.Client;

/**
 * Created by Doremus on 16/10/2016.
 */

public class ClientAdapter extends ArrayAdapter<Client> {
    public ClientAdapter(Context context, List<Client> clients) {
        super(context, 0, clients);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.client_list_cell,parent, false);
        }

        ClientViewHolder viewHolder = (ClientViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new ClientViewHolder();
            viewHolder.name_lastname = (TextView) convertView.findViewById(R.id.client_name_cell_list);
            viewHolder.ref = (TextView) convertView.findViewById(R.id.client_ref_cell_list);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List<Tweet> tweets
        Client client = getItem(position);

        //il ne reste plus qu'à remplir notre vue
        viewHolder.name_lastname.setText(client.getLastname() + " " +client.getFirstname());
        //viewHolder.ref.setText(client.getRef());

        return convertView;
    }

    private class ClientViewHolder{
        public TextView name_lastname;
        public TextView ref;
    }
}
