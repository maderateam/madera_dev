package fr.cesi.madera.entities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fr.cesi.madera.R;
import fr.cesi.madera.entities.Project;

/**
 * Created by Doremus on 16/10/2016.
 */

public class ProjectAdapter extends ArrayAdapter<Project> {
    public ProjectAdapter(Context context, List<Project> projects) {
        super(context, 0, projects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.project_list_cell,parent, false);
        }

        ProjectViewHolder viewHolder = (ProjectViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new ProjectViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.project_name_cell_list);
            viewHolder.ref = (TextView) convertView.findViewById(R.id.project_ref_cell_list);
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.project_icon_cell_list);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List<Tweet> tweets
        Project project = getItem(position);

        //il ne reste plus qu'à remplir notre vue
        viewHolder.name.setText(project.getName());
        //viewHolder.ref.setText(project.getRef());
        if(project.isClosed()){
            viewHolder.icon.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_project_closed_48dp));
        }else{
            viewHolder.icon.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_project_open_48dp));
        }


        return convertView;
    }

    private class ProjectViewHolder{
        public ImageView icon;
        public TextView name;
        public TextView ref;
    }
}
