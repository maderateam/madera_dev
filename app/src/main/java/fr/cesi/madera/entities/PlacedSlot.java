/**
 * 
 */
package fr.cesi.madera.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.sql.Timestamp;

import fr.cesi.madera.misc.DatabaseColumn;
import fr.cesi.madera.misc.ForeignKey;

/**
 * Classe représentnant un Slot une fois placé sur le plan
*/

public class PlacedSlot extends SynchronisedEntity implements Parcelable{
	@DatabaseColumn
	private float posX;
	@DatabaseColumn
	private float posY;
	@DatabaseColumn
	private float posZ;
	@DatabaseColumn
	@ForeignKey(table = "placedcomponent",column = "_id")
	private long idchildpcomponent;
	@DatabaseColumn
	@ForeignKey(table = "placedcomponent",column = "_id")
	private long idparentpcomponent;
	@DatabaseColumn
	@ForeignKey(table = "plan",column = "_id")
	private long idplan;
	@DatabaseColumn
	@ForeignKey(table = "slot",column = "_id")
	private long idslot;
    @DatabaseColumn
    private int numofslot;

	public PlacedSlot(Boolean boool){
		this();
	}
	public PlacedSlot(){}

	public PlacedSlot(float posX, float posY, float posZ, long idchildpcomponent, long idparentpcomponent, long idplan, long idslot, int numofslot) {
		this.posX = posX;
		this.posY = posY;
		this.posZ = posZ;
		this.idchildpcomponent = idchildpcomponent;
		this.idparentpcomponent = idparentpcomponent;
		this.idplan = idplan;
		this.idslot = idslot;
		this.numofslot = numofslot;
	}

	public PlacedSlot(long _id, Timestamp lastsync, boolean hidden, long idcommercial, float posX, float posY, float posZ, long idchildpcomponent, long idparentpcomponent, long idplan, long idslot, int numofslot) {
		super(_id, lastsync, hidden, idcommercial);
		this.posX = posX;
		this.posY = posY;
		this.posZ = posZ;
		this.idchildpcomponent = idchildpcomponent;
		this.idparentpcomponent = idparentpcomponent;
		this.idplan = idplan;
		this.idslot = idslot;
		this.numofslot = numofslot;
	}

	public PlacedSlot(Timestamp lastsync, boolean hidden, long idcommercial, float posX, float posY, float posZ, long idchildpcomponent, long idparentpcomponent, long idplan, long idslot, int numofslot) {
		super(lastsync, hidden, idcommercial);
		this.posX = posX;
		this.posY = posY;
		this.posZ = posZ;
		this.idchildpcomponent = idchildpcomponent;
		this.idparentpcomponent = idparentpcomponent;
		this.idplan = idplan;
		this.idslot = idslot;
		this.numofslot = numofslot;
	}

	/**
	 * @return the posX
	 */
	public float getPosX() {
		return posX;
	}
	/**
	 * @param posX the posX to set
	 */
	public void setPosX(float posX) {
		this.posX = posX;
	}
	/**
	 * @return the posY
	 */
	public float getPosY() {
		return posY;
	}
	/**
	 * @param posY the posY to set
	 */
	public void setPosY(float posY) {
		this.posY = posY;
	}
	/**
	 * @return the posZ
	 */
	public float getPosZ() {
		return posZ;
	}
	/**
	 * @param posZ the posZ to set
	 */
	public void setPosZ(float posZ) {
		this.posZ = posZ;
	}

	public long getIdparentpcomponent() {
		return idparentpcomponent;
	}

	public void setIdparentpcomponent(long idparentpcomponent) {
		this.idparentpcomponent = idparentpcomponent;
	}

	public long getIdchildpcomponent() {
		return idchildpcomponent;
	}

	public void setIdchildpcomponent(long idchildpcomponent) {
		this.idchildpcomponent = idchildpcomponent;
	}

	public long getIdplan() {
		return idplan;
	}

	public void setIdplan(long idplan) {
		this.idplan = idplan;
	}

    public long getIdslot() {
        return idslot;
    }

    public void setIdslot(long idslot) {
        this.idslot = idslot;
    }

    public int getNumofslot() {
        return numofslot;
    }

    public void setNumofslot(int numofslot) {
        this.numofslot = numofslot;
    }

    @Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest,flags);
		dest.writeFloat(this.posX);
		dest.writeFloat(this.posY);
		dest.writeFloat(this.posZ);
		dest.writeLong(this.idchildpcomponent);
		dest.writeLong(this.idparentpcomponent);
		dest.writeLong(this.idplan);
        dest.writeLong(this.idslot);
        dest.writeInt(this.numofslot);
	}
	public static final Creator<PlacedSlot> CREATOR = new Creator<PlacedSlot>(){
		@Override
		public PlacedSlot createFromParcel(Parcel source){
			return new PlacedSlot(source);
		}
		@Override
		public PlacedSlot[] newArray(int size){
			return new PlacedSlot[size];
		}
	};

	public PlacedSlot(Parcel in) {
		super(in);
		this.posX = in.readFloat();
		this.posY = in.readFloat();
		this.posZ = in.readFloat();
		this.idchildpcomponent = in.readLong();
		this.idparentpcomponent = in.readLong();
		this.idplan = in.readLong();
        this.idslot = in.readLong();
        this.numofslot = in.readInt();
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof PlacedComponent && ((PlacedComponent)o).get_id() == get_id();
	}
}
