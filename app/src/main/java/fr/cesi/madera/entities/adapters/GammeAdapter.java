package fr.cesi.madera.entities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import fr.cesi.madera.R;
import fr.cesi.madera.entities.Gamme;

/**
 * Created by Doremus on 16/10/2016.
 */

public class GammeAdapter extends ArrayAdapter<Gamme> {
    public GammeAdapter(Context context, List<Gamme> gammes) {
        super(context, 0, gammes);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.gamme_list_cell,parent, false);
        }

        GammeViewHolder viewHolder = (GammeViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new GammeViewHolder();
            viewHolder.denomination = (TextView) convertView.findViewById(R.id.gamme_denom_cell_list);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List<Tweet> tweets
        Gamme gamme = getItem(position);

        //il ne reste plus qu'à remplir notre vue
        viewHolder.denomination.setText(gamme != null ? gamme.getDenomination() : " Toutes les gammes");
        //viewHolder.ref.setText(client.getRef());

        return convertView;
    }

    private class GammeViewHolder{
        public TextView denomination;
    }
}
