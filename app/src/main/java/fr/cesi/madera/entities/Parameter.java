package fr.cesi.madera.entities;

import android.os.Parcel;
import android.os.Parcelable;

import fr.cesi.madera.misc.DatabaseColumn;

/**
 * Classe représentant un paramètre pour l'application
 * Created by Joshua on 09/02/2017.
 */

public class Parameter<T> extends Entity implements Parcelable {

    @DatabaseColumn
    private String name;

    @DatabaseColumn
    private String type;

    @DatabaseColumn
    private String data;

    public Parameter(){}

    public Parameter(Boolean boool){
        this();
    }

    public Parameter(String name, String type, String data) {
        this.name = name;
        this.type = type;
        this.data = data;
    }

    public Parameter(long _id, String name, String type, String data) {
        super(_id);
        this.name = name;
        this.type = type;
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    protected Parameter(Parcel in) {
        name = in.readString();
        type = in.readString();
        data = in.readString();
    }

    public static final Creator<Parameter> CREATOR = new Creator<Parameter>() {
        @Override
        public Parameter createFromParcel(Parcel in) {
            return new Parameter(in);
        }

        @Override
        public Parameter[] newArray(int size) {
            return new Parameter[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(type);
        dest.writeString(data);
    }
}
