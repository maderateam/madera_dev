package fr.cesi.madera.entities;
import android.os.Parcel;
import android.os.Parcelable;

import fr.cesi.madera.misc.DatabaseColumn;
import fr.cesi.madera.misc.ForeignKey;

/**
 * Classe représentant un slot accueillant un composant sur le plan
 */
public class Slot extends Entity implements Parcelable{

	@DatabaseColumn
	private float widthplacement;
	@DatabaseColumn
	private float heightplacement;
	@DatabaseColumn
	@ForeignKey(table = "component",column = "_id")
	private long idchildcomponent;
	@DatabaseColumn
	@ForeignKey(table = "component",column = "_id")
	private long idparentcomponent;
	@DatabaseColumn
	private int numofslot;

	public Slot(){}

	public Slot(Boolean boool){
		this();
	}

	public Slot(float widthplacement, float heightplacement, long idchildcomponent, long idparentcomponent, int numofslot) {
		this.widthplacement = widthplacement;
		this.heightplacement = heightplacement;
		this.idchildcomponent = idchildcomponent;
		this.idparentcomponent = idparentcomponent;
		this.numofslot = numofslot;
	}

	public Slot(long _id, float widthplacement, float heightplacement, long idchildcomponent, long idparentcomponent, int numofslot) {
		super(_id);
		this.widthplacement = widthplacement;
		this.heightplacement = heightplacement;
		this.idchildcomponent = idchildcomponent;
		this.idparentcomponent = idparentcomponent;
		this.numofslot = numofslot;
	}

	public float getWidthplacement() {
		return widthplacement;
	}

	public void setWidthplacement(float widthplacement) {
		this.widthplacement = widthplacement;
	}

	public long getIdchildcomponent() {
		return idchildcomponent;
	}

	public int getNumofslot() {
		return numofslot;
	}

	public void setNumofslot(int numofslot) {
		this.numofslot = numofslot;
	}

	public void setIdchildcomponent(long idchildcomponent) {
		if(idchildcomponent != 0 && idchildcomponent == this.idparentcomponent){
			throw new IllegalArgumentException("Child and parent ref are equals, stop for prevent loop child id : " +  idchildcomponent + " paret id : " + idparentcomponent);
		}
		this.idchildcomponent = idchildcomponent;
	}

	public long getIdparentcomponent() {
		return idparentcomponent;
	}

	public void setIdparentcomponent(long idParentComponent) {
		if(idParentComponent != 0 && idParentComponent == idchildcomponent){
			throw new IllegalArgumentException("Child and parent ref are equals, stop for prevent loop" );
		}
		this.idparentcomponent = idParentComponent;
	}

	public float getHeightplacement() {
		return heightplacement;
	}

	public void setHeightplacement(float heightplacement) {
		this.heightplacement = heightplacement;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest,flags);
		dest.writeFloat(heightplacement);
		dest.writeFloat(widthplacement);
		dest.writeLong(idchildcomponent);
		dest.writeLong(idparentcomponent);
		dest.writeInt(numofslot);
	}
	public static final Creator<Slot> CREATOR = new Creator<Slot>(){
		@Override
		public Slot createFromParcel(Parcel source){
			return new Slot(source);
		}
		@Override
		public Slot[] newArray(int size){
			return new Slot[size];
		}
	};

	public Slot(Parcel in) {
		super(in);
		this.heightplacement = in.readFloat();
		this.widthplacement = in.readFloat();
		this.idchildcomponent = in.readLong();
		this.idparentcomponent = in.readLong();
		this.numofslot = in.readInt();
	}
}