package fr.cesi.madera.entities.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import fr.cesi.madera.R;
import fr.cesi.madera.entities.Covering;

/**
 * Created by Doremus on 16/10/2016.
 */

public class CoveringAdapter extends ArrayAdapter<Covering> {
    private Context ctx;

    public CoveringAdapter(Context context, List<Covering> coverings) {
        super(context, 0, coverings);
        ctx = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.compo_list_cell, parent, false);
        }

        ComponentViewHolder viewHolder = (ComponentViewHolder) convertView.getTag();
        if (viewHolder == null) {
            viewHolder = new ComponentViewHolder();
            viewHolder.denomination = (TextView) convertView.findViewById(R.id.compo_denom_cell_list);
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.compo_icon_cell_list);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List<Component> compos
        Covering covering = getItem(position);

        //il ne reste plus qu'à remplir notre vue
        viewHolder.denomination.setText(covering.getDenomination());
        viewHolder.denomination.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        // Chargement de l'icone du component
        try {
//            // Récupération du chemin du fichier
            String path = "Madera/covering/" + covering.getIconname();
//
//            InputStream ims = ctx.getAssets().open(path);
            String absolutePath = ctx.getFilesDir().getCanonicalPath();
            absolutePath+="/" + path;
            InputStream ims = new FileInputStream(absolutePath);
            // load image as Drawable
            Drawable icon = Drawable.createFromStream(ims, null);
            // set image to ImageView
            viewHolder.icon.setImageDrawable(icon);
            viewHolder.icon.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        } catch (IOException ex) {
            Log.e("madera", "Connot get icon for this component " + ex.getMessage());
        }
        return convertView;
    }

    private class ComponentViewHolder {
        public TextView denomination;
        public ImageView icon;
    }
}
