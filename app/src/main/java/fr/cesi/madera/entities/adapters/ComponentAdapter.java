package fr.cesi.madera.entities.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import fr.cesi.madera.R;
import fr.cesi.madera.entities.Component;

/**
 * Created by Doremus on 16/10/2016.
 */

public class ComponentAdapter extends ArrayAdapter<Component> {
    private Context ctx;

    public ComponentAdapter(Context context, List<Component> compos) {
        super(context, 0, compos);
        ctx = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.compo_list_cell, parent, false);
        }

        ComponentViewHolder viewHolder = (ComponentViewHolder) convertView.getTag();
        if (viewHolder == null) {
            viewHolder = new ComponentViewHolder();
            viewHolder.denomination = (TextView) convertView.findViewById(R.id.compo_denom_cell_list);
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.compo_icon_cell_list);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List<Component> compos
        Component compo = getItem(position);

        //il ne reste plus qu'à remplir notre vue
        viewHolder.denomination.setText(compo.getDenomination());
        viewHolder.denomination.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        // Chargement de l'icone du component
        try {
            // Récupération du chemin du fichier
            String path = "";
            if (compo.isFloor()) {
                path = "Madera/floors/" + compo.get_id() + "/" + compo.getIconname();
            }else if (compo.isDivider()) {
                path = "Madera/dividers/" + compo.get_id() + "/" + compo.getIconname();
            }else if (compo.isComplex()) {
                path = "Madera/walls/" + compo.get_id() + "/" + compo.getIconname();
            }else if (!compo.isComplex()) {
                path = "Madera/carpentries/" + compo.get_id() + "/" + compo.getIconname();
            }
            String absolutePath = ctx.getFilesDir().getCanonicalPath();
            absolutePath+="/" + path;
            InputStream ims = new FileInputStream(absolutePath);
            // load image as Drawable
            Drawable icon = Drawable.createFromStream(ims, null);
            // set image to ImageView
            viewHolder.icon.setImageDrawable(icon);
            viewHolder.icon.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        } catch (IOException ex) {
            Log.e("madera", "Connot get icon for this component " + ex.getMessage());
        }
        return convertView;
    }

    private class ComponentViewHolder {
        public TextView denomination;
        public ImageView icon;
    }
}
