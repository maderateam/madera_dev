package fr.cesi.madera.entities;

import android.os.Parcel;
import android.os.Parcelable;

import fr.cesi.madera.misc.DatabaseColumn;

/**
 * Classe représentant un Covering (revêtement)
 */

public class Covering extends Entity implements Parcelable {

    @DatabaseColumn
    private double price;
    @DatabaseColumn
    private String iconname;
    @DatabaseColumn
    private String denomination;

    public Covering() {
    }

    public Covering(Parcel in) {
        super(in);
        this.price = in.readDouble();
        this.iconname = in.readString();
        this.denomination = in.readString();
    }

    public Covering(double price, String iconname, String denomination) {
        this.price = price;
        this.iconname = iconname;
        this.denomination = denomination;
    }

    public Covering(long _id, double price, String iconname, String denomination) {
        super(_id);
        this.price = price;
        this.iconname = iconname;
        this.denomination = denomination;
    }

    public Covering(Boolean boool){
        this();
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getIconname() {
        return iconname;
    }

    public void setIconname(String iconname) {
        this.iconname = iconname;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest,flags);
        dest.writeDouble(price);
        dest.writeString(iconname);
        dest.writeString(denomination);
    }
    public static final Creator<Covering> CREATOR = new Creator<Covering>()
    {
        @Override
        public Covering createFromParcel(Parcel source)
        {
            return new Covering(source);
        }

        @Override
        public Covering[] newArray(int size)
        {
            return new Covering[size];
        }
    };
}
