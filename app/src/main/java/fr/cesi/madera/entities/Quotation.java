/**
 * 
 */
package fr.cesi.madera.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.sql.Timestamp;

import fr.cesi.madera.misc.DatabaseColumn;
import fr.cesi.madera.misc.ForeignKey;
import fr.cesi.madera.misc.NumericBooleanDeserializer;

/**
 * Classe représentant un devis de maison
*/

public class Quotation extends SynchronisedEntity implements Parcelable {
	@DatabaseColumn
	@ForeignKey(table = "plan",column = "_id")
	private long idPlan;
	@DatabaseColumn
	private Timestamp datecreated;
	@DatabaseColumn
	private Timestamp datevalidated;
	@DatabaseColumn
	@JsonDeserialize(using=NumericBooleanDeserializer.class)
	private boolean validated;
	@DatabaseColumn
	private double pricewithouttax;
	@DatabaseColumn
	private double pricefinal;
	@DatabaseColumn
	private double tax;
	@DatabaseColumn
	private double totalDiscount;

	public Quotation() {
	}

	public Quotation(Boolean boool){
		this();
	}

	public Quotation(long idPlan, Timestamp datecreated, Timestamp datevalidated, boolean validated, double pricewithouttax, double pricefinal, double tax, double totalDiscount) {
		this.idPlan = idPlan;
		this.datecreated = datecreated;
		this.datevalidated = datevalidated;
		this.validated = validated;
		this.pricewithouttax = pricewithouttax;
		this.pricefinal = pricefinal;
		this.tax = tax;
		this.totalDiscount = totalDiscount;
	}

	public Quotation(long _id, Timestamp lastsync, boolean hidden, long idcommercial, long idPlan, Timestamp datecreated, Timestamp datevalidated, boolean validated, double pricewithouttax, double pricefinal, double tax, double totalDiscount) {
		super(_id, lastsync, hidden, idcommercial);
		this.idPlan = idPlan;
		this.datecreated = datecreated;
		this.datevalidated = datevalidated;
		this.validated = validated;
		this.pricewithouttax = pricewithouttax;
		this.pricefinal = pricefinal;
		this.tax = tax;
		this.totalDiscount = totalDiscount;
	}

	public Quotation(Timestamp lastsync, boolean hidden, long idcommercial, long idPlan, Timestamp datecreated, Timestamp datevalidated, boolean validated, double pricewithouttax, double pricefinal, double tax, double totalDiscount) {
		super(lastsync, hidden, idcommercial);
		this.idPlan = idPlan;
		this.datecreated = datecreated;
		this.datevalidated = datevalidated;
		this.validated = validated;
		this.pricewithouttax = pricewithouttax;
		this.pricefinal = pricefinal;
		this.tax = tax;
		this.totalDiscount = totalDiscount;
	}

	public long getIdPlan() {
		return idPlan;
	}

	public void setIdPlan(long idPlan) {
		this.idPlan = idPlan;
	}

	public Timestamp getDatecreated() {
		return datecreated;
	}

	public void setDatecreated(Timestamp datecreated) {
		this.datecreated = datecreated;
	}

	public Timestamp getDatevalidated() {
		return datevalidated;
	}

	public void setDatevalidated(Timestamp datevalidated) {
		this.datevalidated = datevalidated;
	}

	public boolean isValidated() {
		return validated;
	}

	public void setValidated(boolean validated) {
		this.validated = validated;
	}

	public double getPricewithouttax() {
		return pricewithouttax;
	}

	public void setPricewithouttax(double pricewithouttax) {
		this.pricewithouttax = pricewithouttax;
	}

	public double getPricefinal() {
		return pricefinal;
	}

	public void setPricefinal(double pricefinal) {
		this.pricefinal = pricefinal;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public double getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest,flags);
		dest.writeLong(idPlan);
        dest.writeByte((byte) (validated ? 1 : 0));
        if(datecreated !=null) {
            dest.writeLong(datecreated.getTime());
        }else{
            dest.writeLong(0);
        }
        if(datecreated != null){
            dest.writeLong(datevalidated.getTime());
        }else{
            dest.writeLong(0);
        }

        dest.writeDouble(pricewithouttax);
        dest.writeDouble(pricefinal);
        dest.writeDouble(tax);
        dest.writeDouble(totalDiscount);

	}
	public static final Creator<Quotation> CREATOR = new Creator<Quotation>()
	{
		@Override
		public Quotation createFromParcel(Parcel source)
		{
			return new Quotation(source);
		}

		@Override
		public Quotation[] newArray(int size)
		{
			return new Quotation[size];
		}
	};

	public Quotation(Parcel in) {
		super(in);
		idPlan = in.readLong();
		this.validated = in.readByte() != 0;
		Long date = in.readLong();
		if(date >0 ) {
			this.datecreated = new Timestamp(date);
		}else{
			this.datecreated = null;
		}
		date = in.readLong();
		if(date > 0) {
			this.datevalidated = new Timestamp(date);
		}else{
			this.datevalidated = null;
		}
        this.pricewithouttax = in.readDouble();
        this.pricefinal = in.readDouble();
        this.tax = in.readDouble();
        this.totalDiscount = in.readDouble();
    }
}