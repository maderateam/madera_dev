package fr.cesi.madera.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.sql.Timestamp;

import fr.cesi.madera.misc.DatabaseColumn;
import fr.cesi.madera.misc.ForeignKey;
import fr.cesi.madera.misc.NumericBooleanDeserializer;

/**
 * Classe mère de toutes les entité concernées par la synchronisation bi-directionelles
 */

public abstract class SynchronisedEntity extends Entity  implements Parcelable {
    @DatabaseColumn
    private Timestamp lastsync;
    @DatabaseColumn
    @JsonDeserialize(using=NumericBooleanDeserializer.class)
    private boolean hidden;
    @DatabaseColumn
    @ForeignKey(table = "commercial",column = "_id")
    private long idcommercial;

    public SynchronisedEntity() {}

    public SynchronisedEntity(Boolean boool){
        this();
    }

    public SynchronisedEntity(Timestamp lastsync, boolean hidden,long idcommercial) {
        this.lastsync = lastsync;
        this.hidden = hidden;
        this.idcommercial = idcommercial;
    }

    public SynchronisedEntity(long _id, Timestamp lastsync, boolean hidden, long idcommercial) {
        super(_id);
        this.lastsync = lastsync;
        this.hidden = hidden;
        this.idcommercial = idcommercial;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public Timestamp getLastsync() {
        return lastsync;
    }


    public void setLastsync(Timestamp lastsync) {
        this.lastsync = lastsync;
    }

    public long getIdcommercial() {
        return idcommercial;
    }

    public void setIdcommercial(long idcommercial) {
        this.idcommercial = idcommercial;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest,flags);
        if(this.lastsync != null) {
            dest.writeLong(this.lastsync.getTime());
        }else{
            dest.writeLong(-1);
        }
        dest.writeByte((byte) (hidden ? 1 : 0));
        dest.writeLong(idcommercial);
    }
    public static final Creator<Entity> CREATOR = new Creator<Entity>()
    {
        @Override
        public Entity createFromParcel(Parcel source)
        {
            return new Entity(source);
        }

        @Override
        public Entity[] newArray(int size)
        {
            return new Entity[size];
        }
    };

    public SynchronisedEntity(Parcel in) {
        super(in);
        Long date = in.readLong();
        if(date != -1) {
            this.lastsync = new Timestamp(date);
        }else{
            this.lastsync = null;
        }
        this.hidden = in.readByte() != 0;
        this.idcommercial = in.readLong();
    }

}
