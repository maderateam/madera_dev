/**
 * 
 */
package fr.cesi.madera.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.sql.Timestamp;

import fr.cesi.madera.misc.DatabaseColumn;
import fr.cesi.madera.misc.ForeignKey;
import fr.cesi.madera.misc.NumericBooleanDeserializer;

/**
 * Classe représentant un plan de maison
*/


public class Plan extends SynchronisedEntity implements Parcelable{

	@DatabaseColumn
	private String name;
	@DatabaseColumn
	private String description;
	@DatabaseColumn
	private Timestamp datecreated;
	@DatabaseColumn
	private Timestamp datevalidated;
	@DatabaseColumn
	@JsonDeserialize(using=NumericBooleanDeserializer.class)
	private boolean archived;
	@DatabaseColumn
	@ForeignKey(table = "project",column = "_id")
	private long idProject;
	@DatabaseColumn
	@ForeignKey(table = "quotation",column = "_id")
	private long idQuotation;

	public Plan(){}

	public Plan(Boolean boool){
		this();
	}

	public Plan(String name, String description, Timestamp datecreated, Timestamp datevalidated, boolean archived, long idProject, long idQuotation) {
		this.name = name;
		this.description = description;
		this.datecreated = datecreated;
		this.datevalidated = datevalidated;
		this.archived = archived;
		this.idProject = idProject;
		this.idQuotation = idQuotation;
	}

	public Plan(long _id, Timestamp lastsync, boolean hidden, long idcommercial, String name, String description, Timestamp datecreated, Timestamp datevalidated, boolean archived, long idProject, long idQuotation) {
		super(_id, lastsync, hidden, idcommercial);
		this.name = name;
		this.description = description;
		this.datecreated = datecreated;
		this.datevalidated = datevalidated;
		this.archived = archived;
		this.idProject = idProject;
		this.idQuotation = idQuotation;
	}

	public Plan(Timestamp lastsync, boolean hidden, long idcommercial, String name, String description, Timestamp datecreated, Timestamp datevalidated, boolean archived, long idProject, long idQuotation) {
		super(lastsync, hidden, idcommercial);
		this.name = name;
		this.description = description;
		this.datecreated = datecreated;
		this.datevalidated = datevalidated;
		this.archived = archived;
		this.idProject = idProject;
		this.idQuotation = idQuotation;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the name description
	 */
	public String getDescription() {return description;}

    /**
     * @param description
     */
    public void setDescription(String description) {this.description = description; }

    /**
	 * @return the datecreated
	 */
	public Timestamp getDatecreated() {
		return datecreated;
	}
	/**
	 * @param datecreated the datecreated to set
	 */
	public void setDatecreated(Timestamp datecreated) {
		this.datecreated = datecreated;
	}
	/**
	 * @return the datevalidated
	 */
	public Timestamp getDatevalidated() {
		return datevalidated;
	}
	/**
	 * @param datevalidated the datevalidated to set
	 */
	public void setDatevalidated(Timestamp datevalidated) {
		this.datevalidated = datevalidated;
	}

	public boolean isArchived() {
		return archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public long getIdProject() {
		return idProject;
	}

	public void setIdProject(long idProject) {
		this.idProject = idProject;
	}

	public long getIdQuotation() {
		return idQuotation;
	}

	public void setIdQuotation(long idQuotation) {
		this.idQuotation = idQuotation;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest,flags);
		dest.writeString(name);
		dest.writeString(description);
		dest.writeByte((byte) (archived ? 1 : 0));
		if(this.datecreated != null) {
			dest.writeLong(this.datecreated.getTime());
		}else{
			dest.writeLong(-1);
		}
		if(this.datevalidated != null) {
			dest.writeLong(this.datevalidated.getTime());
		}else{
			dest.writeLong(-1);
		}
		dest.writeLong(idProject);
		dest.writeLong(idQuotation);
	}
	public static final Creator<Plan> CREATOR = new Creator<Plan>()
	{
		@Override
		public Plan createFromParcel(Parcel source)
		{
			return new Plan(source);
		}

		@Override
		public Plan[] newArray(int size)
		{
			return new Plan[size];
		}
	};

	public Plan(Parcel in) {
		super(in);
		this.name = in.readString();
		this.description = in.readString();
		this.archived = in.readByte() != 0;
		Long date = in.readLong();
		if(date != -1) {
			this.datecreated = new Timestamp(date);
		}else{
			this.datecreated = null;
		}
		date = in.readLong();
		if(date != -1) {
			this.datevalidated = new Timestamp(date);
		}else{
			this.datevalidated = null;
		}
		idProject = in.readLong();
		idQuotation = in.readLong();
	}
}
