package fr.cesi.madera.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.sql.Timestamp;

import fr.cesi.madera.misc.DatabaseColumn;
import fr.cesi.madera.misc.ForeignKey;
import fr.cesi.madera.misc.NumericBooleanDeserializer;

/**
 * Classe représentant une liigne dans un devis de maison
 */

public class QuotationLine extends SynchronisedEntity implements Parcelable {

    @DatabaseColumn
    @ForeignKey(table = "quotation",column = "_id")
    private long idquotation;
    @DatabaseColumn
    private String designation;
    @DatabaseColumn
    private String unitofmeasure;
    @DatabaseColumn
    private float quantity;
    @DatabaseColumn
    private double unitprice;
    @DatabaseColumn
    private double discount;
    @DatabaseColumn
    private double pretaxamount;
    @DatabaseColumn
    private double tax;
    @DatabaseColumn
    private double alltaxesincluded;
    @DatabaseColumn
    @ForeignKey(table = "component", column = "_id")
    private long idcomponent;
    @DatabaseColumn
    @ForeignKey(table = "gamme", column = "_id")
    private long idgamme;
    @DatabaseColumn
    @JsonDeserialize(using=NumericBooleanDeserializer.class)
    private boolean covering;

    public QuotationLine(Boolean boool){
        this();
    }

    public QuotationLine(){
    }

    public QuotationLine(long idquotation, String designation, String unitofmeasure, float quantity, double unitprice, double discount, double pretaxamount, double tax, double alltaxesincluded, long idcomponent, long idgamme, boolean covering) {
        this.idquotation = idquotation;
        this.designation = designation;
        this.unitofmeasure = unitofmeasure;
        this.quantity = quantity;
        this.unitprice = unitprice;
        this.discount = discount;
        this.pretaxamount = pretaxamount;
        this.tax = tax;
        this.alltaxesincluded = alltaxesincluded;
        this.idcomponent = idcomponent;
        this.idgamme = idgamme;
        this.covering = covering;
    }

    public QuotationLine(long _id, Timestamp lastsync, boolean hidden, long idcommercial, long idquotation, String designation, String unitofmeasure, float quantity, double unitprice, double discount, double pretaxamount, double tax, double alltaxesincluded, long idcomponent, long idgamme, boolean covering) {
        super(_id, lastsync, hidden, idcommercial);
        this.idquotation = idquotation;
        this.designation = designation;
        this.unitofmeasure = unitofmeasure;
        this.quantity = quantity;
        this.unitprice = unitprice;
        this.discount = discount;
        this.pretaxamount = pretaxamount;
        this.tax = tax;
        this.alltaxesincluded = alltaxesincluded;
        this.idcomponent = idcomponent;
        this.idgamme = idgamme;
        this.covering = covering;
    }

    public QuotationLine(Timestamp lastsync, boolean hidden, long idcommercial, long idquotation, String designation, String unitofmeasure, float quantity, double unitprice, double discount, double pretaxamount, double tax, double alltaxesincluded, long idcomponent, long idgamme, boolean covering) {
        super(lastsync, hidden, idcommercial);
        this.idquotation = idquotation;
        this.designation = designation;
        this.unitofmeasure = unitofmeasure;
        this.quantity = quantity;
        this.unitprice = unitprice;
        this.discount = discount;
        this.pretaxamount = pretaxamount;
        this.tax = tax;
        this.alltaxesincluded = alltaxesincluded;
        this.idcomponent = idcomponent;
        this.idgamme = idgamme;
        this.covering = covering;
    }

    public long getIdquotation() {
        return idquotation;
    }

    public void setIdquotation(long idquotation) {
        this.idquotation = idquotation;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getUnitofmeasure() {
        return unitofmeasure;
    }

    public void setUnitofmeasure(String unitofmeasure) {
        this.unitofmeasure = unitofmeasure;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public double getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(double unitprice) {
        this.unitprice = unitprice;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getPretaxamount() {
        return pretaxamount;
    }

    public void setPretaxamount(double pretaxamount) {
        this.pretaxamount = pretaxamount;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getAlltaxesincluded() {
        return alltaxesincluded;
    }

    public void setAlltaxesincluded(double alltaxesincluded) {
        this.alltaxesincluded = alltaxesincluded;
    }

    public long getIdcomponent() {
        return idcomponent;
    }

    public void setIdcomponent(long idcomponent) {
        this.idcomponent = idcomponent;
    }

    public long getIdgamme() {
        return idgamme;
    }

    public void setIdgamme(long idgamme) {
        this.idgamme = idgamme;
    }

    public boolean isCovering() {
        return covering;
    }

    public void setCovering(boolean covering) {
        this.covering = covering;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest,flags);
        dest.writeLong(idquotation);
        dest.writeString(designation.toString());
        dest.writeString(unitofmeasure.toString());
        dest.writeFloat(quantity);
        dest.writeDouble(unitprice);
        dest.writeDouble(discount);
        dest.writeDouble(pretaxamount);
        dest.writeDouble(tax);
        dest.writeDouble(alltaxesincluded);
        dest.writeDouble(idcomponent);
        dest.writeDouble(idgamme);
        dest.writeByte((byte) (this.covering ? 1 : 0));

    }
    public static final Creator<QuotationLine> CREATOR = new Creator<QuotationLine>()
    {
        @Override
        public QuotationLine createFromParcel(Parcel source)
        {
            return new QuotationLine(source);
        }

        @Override
        public QuotationLine[] newArray(int size)
        {
            return new QuotationLine[size];
        }
    };

    public QuotationLine(Parcel in) {
        super(in);
        this.idquotation = in.readLong();
        this.designation = in.readString();
        this.unitofmeasure = in.readString();
        this.quantity = in.readFloat();
        this.unitprice = in.readDouble();
        this.discount = in.readDouble();
        this.pretaxamount = in.readDouble();
        this.tax = in.readDouble();
        this.alltaxesincluded = in.readDouble();
        this.idcomponent = in.readLong();
        this.idgamme = in.readLong();
        this.covering = in.readByte() != 0;
    }

    @Override
    public String toString() {
        return "QuotationLine{" +
                "idquotation=" + idquotation +
                ", designation='" + designation + '\'' +
                ", unitofmeasure='" + unitofmeasure + '\'' +
                ", quantity=" + quantity +
                ", unitprice=" + unitprice +
                ", discount=" + discount +
                ", pretaxamount=" + pretaxamount +
                ", tax=" + tax +
                ", alltaxesincluded=" + alltaxesincluded +
                ", idcomponent=" + idcomponent +
                ", idgamme=" + idgamme +
                '}';
    }
}
