/**
 * 
 */
package fr.cesi.madera.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.sql.Timestamp;

import fr.cesi.madera.misc.DatabaseColumn;
import fr.cesi.madera.misc.ForeignKey;

/**
 * Classe représentant un component une fois placé sur le plan
 */

public class PlacedComponent extends SynchronisedEntity implements Parcelable{

	@DatabaseColumn
	private float length;
	@DatabaseColumn
	private float posX;
	@DatabaseColumn
	private float posY;
	@DatabaseColumn
	private float posZ;
	@DatabaseColumn
	@ForeignKey(table = "plan",column = "_id")
	private long idplan;
    @DatabaseColumn
    @ForeignKey(table = "component",column = "_id")
    private long idcomponent;
	@DatabaseColumn
	@ForeignKey(table = "placedslot",column = "_id")
	private long idparentpslot;
	@DatabaseColumn
	@ForeignKey(table="covering",column="id")
	private long idcoveringin;
	@DatabaseColumn
	@ForeignKey(table="covering",column="id")
	private long idcoveringout;

	public PlacedComponent() {}

	public PlacedComponent(Boolean boool){
		this();
	}

	public PlacedComponent(float length, float posX, float posY, float posZ, long idplan, long idcomponent, long idparentpslot, long idcoveringin, long idcoveringout) {
		this.length = length;
		this.posX = posX;
		this.posY = posY;
		this.posZ = posZ;
		this.idplan = idplan;
		this.idcomponent = idcomponent;
		this.idparentpslot = idparentpslot;
		this.idcoveringin = idcoveringin;
		this.idcoveringout = idcoveringout;
	}

	public PlacedComponent(long _id, Timestamp lastsync, boolean hidden, long idcommercial, float length, float posX, float posY, float posZ, long idplan, long idcomponent, long idparentpslot, long idcoveringin, long idcoveringout) {
		super(_id, lastsync, hidden, idcommercial);
		this.length = length;
		this.posX = posX;
		this.posY = posY;
		this.posZ = posZ;
		this.idplan = idplan;
		this.idcomponent = idcomponent;
		this.idparentpslot = idparentpslot;
		this.idcoveringin = idcoveringin;
		this.idcoveringout = idcoveringout;
	}

	public PlacedComponent(Timestamp lastsync, boolean hidden, long idcommercial, float length, float posX, float posY, float posZ, long idplan, long idcomponent, long idparentpslot, long idcoveringin, long idcoveringout) {
		super(lastsync, hidden, idcommercial);
		this.length = length;
		this.posX = posX;
		this.posY = posY;
		this.posZ = posZ;
		this.idplan = idplan;
		this.idcomponent = idcomponent;
		this.idparentpslot = idparentpslot;
		this.idcoveringin = idcoveringin;
		this.idcoveringout = idcoveringout;
	}

	/**
	 * @return the scale
	 */
	public float getLength() {
		return length;
	}

	/**
	 * @param length the scale to set
	 */
	public void setLength(float length) {
		this.length = length;
	}
	/**
	 * @return the posX
	 */
	public float getPosX() {
		return posX;
	}
	/**
	 * @param posX the posX to set
	 */
	public void setPosX(float posX) {
		this.posX = posX;
	}
	/**
	 * @return the posY
	 */
	public float getPosY() {
		return posY;
	}
	/**
	 * @param posY the posY to set
	 */
	public void setPosY(float posY) {
		this.posY = posY;
	}
	/**
	 * @return the posZ
	 */
	public float getPosZ() {
		return posZ;
	}
	/**
	 * @param posZ the posZ to set
	 */
	public void setPosZ(float posZ) {
		this.posZ = posZ;
	}

	public long getIdplan() {
		return idplan;
	}

	public void setIdplan(long idplan) {
		this.idplan = idplan;
	}

    public long getIdcomponent() {
        return idcomponent;
    }

    public void setIdcomponent(long idcomponent) {
        this.idcomponent = idcomponent;
    }

	public long getIdparentpslot() {
		return idparentpslot;
	}

	public void setIdparentpslot(long idparentpslot) {
		this.idparentpslot = idparentpslot;
	}

	public long getIdcoveringin() {
		return idcoveringin;
	}

	public void setIdcoveringin(long idCoveringIn) {
		this.idcoveringin = idCoveringIn;
	}

	public long getIdcoveringout() {
		return idcoveringout;
	}

	public void setIdcoveringout(long idcoveringout) {
		this.idcoveringout = idcoveringout;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest,flags);
		dest.writeFloat(this.length);
		dest.writeFloat(this.posX);
		dest.writeFloat(this.posY);
		dest.writeFloat(this.posZ);
		dest.writeLong(this.idplan);
        dest.writeLong(this.idcomponent);
		dest.writeLong(this.idparentpslot);
		dest.writeLong(this.idcoveringin);
		dest.writeLong(this.idcoveringout);
	}
	public static final Creator<PlacedComponent> CREATOR = new Creator<PlacedComponent>(){
		@Override
		public PlacedComponent createFromParcel(Parcel source){
			return new PlacedComponent(source);
		}
		@Override
		public PlacedComponent[] newArray(int size){
			return new PlacedComponent[size];
		}
	};

	public PlacedComponent(Parcel in) {
		super(in);
		this.length = in.readFloat();
		this.posX = in.readFloat();
		this.posY = in.readFloat();
		this.posZ = in.readFloat();
		this.idplan = in.readLong();
        this.idcomponent = in.readLong();
		this.idparentpslot = in.readLong();
		this.idcoveringin = in.readLong();
		this.idcoveringout = in.readLong();
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof PlacedComponent && ((PlacedComponent)o).get_id() == get_id();
	}
}
