package fr.cesi.madera.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.sql.Timestamp;

import fr.cesi.madera.misc.DatabaseColumn;

/**
 * Classe représentant un client
*/

public class Client extends SynchronisedEntity implements Parcelable, Comparable<Entity>{

	@DatabaseColumn
	private String firstname;
	@DatabaseColumn
	private String lastname;
	@DatabaseColumn
	private String adress;
	@DatabaseColumn
	private String zipcode;
	@DatabaseColumn
	private String city;
	@DatabaseColumn
	private String tel;
	@DatabaseColumn
	private String mail;

	public Client(){}
	
	/**
	 * @param firstname
	 * @param lastname
	 * @param adresse
	 * @param zipcode
	 * @param city
	 * @param tel
	 * @param mail
	 */
	public Client(Timestamp lastsync, boolean hidden, long idcommercial, String firstname, String lastname, String adress, String zipcode, String city, String tel, String mail) {
		super(lastsync, hidden, idcommercial);
		this.firstname = firstname;
		this.lastname = lastname;
		this.adress = adress;
		this.zipcode = zipcode;
		this.city = city;
		this.tel = tel;
		this.mail = mail;
	}

	public Client(Boolean boool){
		this();
	}

	public Client(long _id, Timestamp lastsync, boolean hidden, long idcommercial, String firstname, String lastname, String adress, String zipcode, String city, String tel, String mail) {
		super(_id, lastsync, hidden, idcommercial);
		this.firstname = firstname;
		this.lastname = lastname;
		this.adress = adress;
		this.zipcode = zipcode;
		this.city = city;
		this.tel = tel;
		this.mail = mail;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest,flags);
		dest.writeString(firstname);
		dest.writeString(lastname);
		dest.writeString(adress);
		dest.writeString(zipcode);
		dest.writeString(city);
		dest.writeString(tel);
		dest.writeString(mail);

	}
	public static final Creator<Client> CREATOR = new Creator<Client>()
	{
		@Override
		public Client createFromParcel(Parcel source)
		{
			return new Client(source);
		}

		@Override
		public Client[] newArray(int size)
		{
			return new Client[size];
		}
	};

	public Client(Parcel in) {
        super(in);
		this.firstname = in.readString();
		this.lastname = in.readString();
		this.adress = in.readString();
		this.zipcode = in.readString();
		this.city = in.readString();
		this.tel = in.readString();
		this.mail = in.readString();
	}

	@Override
	public int compareTo(Entity client) {
		int lastNameComparison = this.lastname.compareTo(((Client)client).getLastname().toLowerCase());
		int firstNameComparison = this.firstname.compareTo(((Client)client).getFirstname().toLowerCase());
        return  lastNameComparison == 0 ? firstNameComparison : lastNameComparison;
	}
}
