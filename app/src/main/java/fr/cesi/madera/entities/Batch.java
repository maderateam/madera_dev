package fr.cesi.madera.entities;

import java.sql.Timestamp;

import fr.cesi.madera.misc.DatabaseColumn;
import fr.cesi.madera.misc.ForeignKey;

/**
 * Created by Doremus on 18/04/2017.
 */

public class Batch extends Entity {
    @DatabaseColumn
    @ForeignKey(table = "commercial",column = "comserial")
    private String comserial;
    @DatabaseColumn
    private Timestamp dateoperation;
    @DatabaseColumn
    private String entityclass;
    @DatabaseColumn
    private char operation;
    @DatabaseColumn
    private long identity;

    private String entity;

    public Batch() {}

    public Batch(Boolean boool){
        this();
    }

    public Batch(String comserial, Timestamp dateoperation, String entityclass, char operation, long identity, String entity) {
        this.comserial = comserial;
        this.dateoperation = dateoperation;
        this.entityclass = entityclass;
        this.operation = operation;
        this.identity = identity;
        this.entity = entity;
    }

    public Batch(long _id, String comserial, Timestamp dateoperation, String entityclass, char operation, long identity, String entity) {
        super(_id);
        this.comserial = comserial;
        this.dateoperation = dateoperation;
        this.entityclass = entityclass;
        this.operation = operation;
        this.identity = identity;
        this.entity = entity;
    }

    public String getComserial() {
        return comserial;
    }

    public void setComserial(String comserial) {
        this.comserial = comserial;
    }

    public Timestamp getDateoperation() {
        return dateoperation;
    }

    public void setDateoperation(Timestamp dateoperation) {
        this.dateoperation = dateoperation;
    }

    public String getEntityclass() {
        return entityclass;
    }

    public void setEntityclass(String entityclass) {
        this.entityclass = entityclass;
    }

    public char getOperation() {
        return operation;
    }

    public void setOperation(char operation) {
        this.operation = operation;
    }

    public long getIdentity() {
        return identity;
    }

    public void setIdentity(long identity) {
        this.identity = identity;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }
}
