/**
 *
 */
package fr.cesi.madera.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.sql.Timestamp;

import fr.cesi.madera.misc.DatabaseColumn;
import fr.cesi.madera.misc.ForeignKey;
import fr.cesi.madera.misc.NumericBooleanDeserializer;

/**
 * Classe représentant un projet de maison
 */

public class Project extends SynchronisedEntity implements Parcelable{

	@DatabaseColumn
	private String name;
	@DatabaseColumn
	private String description;
	@DatabaseColumn
	@JsonDeserialize(using=NumericBooleanDeserializer.class)
	private boolean closed;
	@DatabaseColumn
	private Timestamp datecreate;
	@DatabaseColumn
	private Timestamp dateclosed;
	@DatabaseColumn
	@ForeignKey(table = "client",column = "_id")
	private long idclient;

	public Project() {}

    /**
	 * @param name
	 * @param description
	 * @param closed
	 * @param datecreate
	 * @param dateclosed
	 * @param idclient
	 */
	public Project(String name, String description,
				   boolean closed, Timestamp datecreate, Timestamp dateclosed,long idclient) {
		this.name = name;
		this.description = description;
		this.closed = closed;
		this.datecreate = datecreate;
		this.dateclosed = dateclosed;
		this.idclient = idclient;
	}

	public Project(Boolean boool){
		this();
	}

    public Project(long _id, Timestamp lastsync, boolean hidden, long idcommercial, String name, String description, boolean closed, Timestamp datecreate, Timestamp dateclosed, long idclient) {
        super(_id, lastsync, hidden, idcommercial);
        this.name = name;
        this.description = description;
        this.closed = closed;
        this.datecreate = datecreate;
        this.dateclosed = dateclosed;
        this.idclient = idclient;
    }

    public Project(Timestamp lastsync, boolean hidden, long idcommercial, String name, String description, boolean closed, Timestamp datecreate, Timestamp dateclosed, long idclient) {
        super(lastsync, hidden, idcommercial);
        this.name = name;
        this.description = description;
        this.closed = closed;
        this.datecreate = datecreate;
        this.dateclosed = dateclosed;
        this.idclient = idclient;
    }

    /**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isClosed() {
		return closed;
	}

	public void setClosed(boolean closed) {
		this.closed = closed;
	}

	public Timestamp getDatecreate() {
		return datecreate;
	}

	public void setDatecreate(Timestamp datecreate) {
		this.datecreate = datecreate;
	}

	public Timestamp getDateclosed() {
		return dateclosed;
	}

	public void setDateclosed(Timestamp dateclosed) {
		this.dateclosed = dateclosed;
	}

	public long getIdclient() {
		return idclient;
	}

	public void setIdclient(long idclient) {
		this.idclient = idclient;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest,flags);
		dest.writeString(name);
		dest.writeString(description);
		dest.writeByte((byte) (closed ? 1 : 0));
		if(datecreate != null) {
			dest.writeLong(datecreate.getTime());
		}else{
            dest.writeLong(1);
        }
		if(dateclosed != null) {
			dest.writeLong(dateclosed.getTime());
		}else{
			dest.writeLong(1);
        }
		dest.writeLong(idclient);
	}
	public static final Creator<Project> CREATOR = new Creator<Project>()
	{
		@Override
		public Project createFromParcel(Parcel source)
		{
			return new Project(source);
		}

		@Override
		public Project[] newArray(int size)
		{
			return new Project[size];
		}
	};

	public Project(Parcel in) {
		super(in);
		this.name = in.readString();
		this.description = in.readString();
		this.closed = in.readByte() != 0;
        Long date = in.readLong();
        if(date != -1) {
			this.datecreate = new Timestamp(date);
        }else{
            this.datecreate = null;
        }

		date = in.readLong();
		if(date != -1) {
			this.dateclosed = new Timestamp(date);
		}else{
			this.dateclosed = null;
		}
		this.idclient = in.readLong();
	}

	@Override
	public int compareTo(Entity entity) {
		return this.getDatecreate().compareTo(((Project)entity).getDatecreate());
	}
}
