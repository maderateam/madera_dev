/**
 * 
 */
package fr.cesi.madera.entities;

import android.os.Parcel;
import android.os.Parcelable;

import fr.cesi.madera.misc.DatabaseColumn;

/**
* Classe représentant une gamme pour un component
*/

public class Gamme extends Entity implements Parcelable {

	@DatabaseColumn
	private String denomination;
	
	public Gamme(){}

	public Gamme(Boolean boool){
		this();
	}

	/**
	 * @param denomination
	 */
	public Gamme(String denomination) {
		this.denomination = denomination;
	}

	public Gamme(long _id, String denomination) {
		super(_id);
		this.denomination = denomination;
	}

	/**
	 * @return the denomination
	 */
	public String getDenomination() {
		return denomination;
	}
	/**
	 * @param denomination the denomination to set
	 */
	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest,flags);
		dest.writeString(this.denomination);
	}
	public static final Creator<Gamme> CREATOR = new Creator<Gamme>(){
		@Override
		public Gamme createFromParcel(Parcel source){
			return new Gamme(source);
		}
		@Override
		public Gamme[] newArray(int size){
			return new Gamme[size];
		}
	};

	public Gamme(Parcel in) {
		super(in);
		this.denomination = in.readString();
	}

	@Override
	public String toString() {
		return org.apache.commons.lang3.StringUtils.capitalize(denomination);
	}
}
