package fr.cesi.madera.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.sql.Timestamp;

import fr.cesi.madera.misc.DatabaseColumn;
import fr.cesi.madera.misc.NumericBooleanDeserializer;

/**
 * Classe représentant un commercial
*/

public class Commercial extends SynchronisedEntity implements Parcelable{

	@DatabaseColumn
	private String firstname;
	@DatabaseColumn
	private String lastname;
	@DatabaseColumn
	private String mail;
	@DatabaseColumn
	private String password;
	@DatabaseColumn
	private String comserial;
	@DatabaseColumn
	@JsonDeserialize(using=NumericBooleanDeserializer.class)
	private boolean vacation = false;
	
	public Commercial(){}

	public Commercial(Timestamp lastsync, boolean hidden, long idcommercial, String firstname, String lastname, String mail, String password, String comserial, boolean vacation) {
		super(lastsync, hidden, idcommercial);
		this.firstname = firstname;
		this.lastname = lastname;
		this.mail = mail;
		this.password = password;
		this.comserial = comserial;
		this.vacation = vacation;
	}

	public Commercial(Boolean boool){
		this();
	}

	public Commercial(long _id, Timestamp lastsync, boolean hidden, long idcommercial, String firstname, String lastname, String mail, String password, String comserial, boolean vacation) {
		super(_id, lastsync, hidden, idcommercial);
		this.firstname = firstname;
		this.lastname = lastname;
		this.mail = mail;
		this.password = password;
		this.comserial = comserial;
		this.vacation = vacation;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest,flags);
		dest.writeString(firstname);
		dest.writeString(lastname);
		dest.writeString(mail);
		dest.writeString(password);
		dest.writeString(comserial);
		dest.writeByte((byte) (vacation ? 1 : 0));
	}
	public static final Creator<Commercial> CREATOR = new Creator<Commercial>()
	{
		@Override
		public Commercial createFromParcel(Parcel source)
		{
			return new Commercial(source);
		}

		@Override
		public Commercial[] newArray(int size)
		{
			return new Commercial[size];
		}
	};

	public Commercial(Parcel in) {
		super(in);
		this.firstname = in.readString();
		this.lastname = in.readString();
		this.mail = in.readString();
		this.password = in.readString();
		this.comserial = in.readString();
		this.vacation = in.readByte() != 0;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getComserial() {
		return comserial;
	}

	public void setComserial(String comserial) {
		this.comserial = comserial;
	}

	public boolean isVacation() {
		return vacation;
	}

	public void setVacation(boolean vacation) {
		this.vacation = vacation;
	}
}
