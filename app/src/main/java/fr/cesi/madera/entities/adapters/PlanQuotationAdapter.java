package fr.cesi.madera.entities.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.cesi.madera.R;
import fr.cesi.madera.entities.Entity;
import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.Quotation;

/**
 * Created by Doremus on 16/10/2016.
 */

public class PlanQuotationAdapter extends ArrayAdapter<ArrayList<Entity>> {
    private ArrayList<ArrayList<Entity>> plans;
    public PlanQuotationAdapter(Context context, ArrayList<ArrayList<Entity>> plans) {
        super(context, 0, plans);
        this.plans = plans;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.plan_quot_list_cell, parent, false);

        }

        PlanQuotViewHolder viewHolder = (PlanQuotViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new PlanQuotViewHolder();
            viewHolder.planName = (TextView) convertView.findViewById(R.id.lbl_name_plan_quot_list);
            viewHolder.quotStatus = (TextView) convertView.findViewById(R.id.lbl_statut_quot_quot_list);
            viewHolder.ic_plan = (ImageView) convertView.findViewById(R.id.ic_plan_quot_list);
            viewHolder.ic_quot = (ImageView) convertView.findViewById(R.id.ic_quotation_quot_list);
            convertView.setTag(viewHolder);
        }
        List<Entity> row = getItem(position);

        viewHolder.ic_plan.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_border_right_white_48dp));
        viewHolder.planName.setText(((Plan)row.get(0)).getName());
        if(row.get(1) != null){
            Log.i("madera","Ya du devis là !");
            if(((Quotation)row.get(1)).isValidated()){
                viewHolder.quotStatus.setText(R.string.lbl_quot_valid);
            }else {
                viewHolder.quotStatus.setText(R.string.lbl_quot_wait);
            }
            viewHolder.ic_quot.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_devis_list_48dp));
        }else{
            Log.i("madera","Ya pas du devis là !");
            ImageView ic_quot = new ImageView(getContext());
            viewHolder.quotStatus.setText(R.string.lbl_quot_no_quot);
            ic_quot.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_no_quotation_48dp));
            ic_quot.setMinimumWidth(80);
            ic_quot.setMinimumHeight(80);
            viewHolder.ic_quot = ic_quot;
        }
        return convertView;
    }

    private class PlanQuotViewHolder{
        public ImageView ic_plan;
        public ImageView ic_quot;
        public TextView planName;
        public TextView quotStatus;
    }
}
