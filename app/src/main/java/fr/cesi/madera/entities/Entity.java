package fr.cesi.madera.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Classe représentant une entité dans l'application
 * Mère de toutes les entités gérée dans l'application coté métier
 * Created by Remi on 02/11/2016.
 */
public class Entity implements Parcelable, Comparable<Entity>{
    protected long _id;

    public Entity(){}

    public Entity(Boolean boool){
        this();
    }

    public Entity(long _id) {
        this._id = _id;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this._id);
    }
    public static final Creator<Entity> CREATOR = new Creator<Entity>()
    {
        @Override
        public Entity createFromParcel(Parcel source)
        {
            return new Entity(source);
        }

        @Override
        public Entity[] newArray(int size)
        {
            return new Entity[size];
        }
    };

    public Entity(Parcel in) {
        this._id = in.readLong();
    }


    @Override
    public int compareTo(Entity entity) {
        return 0;
    }
}
