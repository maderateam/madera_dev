package fr.cesi.madera.synchronization.httpResponse;

import java.util.ArrayList;

import fr.cesi.madera.entities.Batch;

/**
 * Created by Doremus on 24/04/2017.
 */

public class BatchResponse {
    private String status;
    private String msg;
    private String date;
    private String serialcom;
    private ArrayList<Batch> data;

    public BatchResponse() {
    }

    public BatchResponse(String status, String msg, String date, String serialcom, String data) {
        this.status = status;
        this.msg = msg;
        this.date = date;
        this.serialcom = serialcom;
    }

    public ArrayList<Batch> getData() {
        return data;
    }

    public void setData(ArrayList<Batch> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSerialcom() {
        return serialcom;
    }

    public void setSerialcom(String serialcom) {
        this.serialcom = serialcom;
    }
}
