package fr.cesi.madera.synchronization.httpResponse;

/**
 * Created by Doremus on 23/04/2017.
 */

public class Response {
    private String status;
    private String msg;
    private String date;
    private String serialcom;

    public Response() {
    }

    public Response(String status, String msg, String date, String serialcom) {
        this.status = status;
        this.msg = msg;
        this.date = date;
        this.serialcom = serialcom;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSerialcom() {
        return serialcom;
    }

    public void setSerialcom(String serialcom) {
        this.serialcom = serialcom;
    }
}
