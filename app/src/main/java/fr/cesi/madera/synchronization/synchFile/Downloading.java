package fr.cesi.madera.synchronization.synchFile;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by KH on 08/03/2017.
 */
//class permettant le télechargement d'un fichier sur le serveur distant

/**
 * La class AsyncTask dont hérite la class Downloading, permet de réaliser des tâches liés aux IHM de manière asynchrone
 * dans des Threads indépendantes de celle de l'IHM. L'avantage de l’AsyncTask est sa simplicité d’utilisation et d’implémentation.
 * La class AsyncTask s'execute dans deux Threads différents, la Thread IHM et et la Thread de Traitement.
 * Elle attend trois paramètres, le type de l'information qui est nécessaire au traitement,
 * le type de l'information qui est passé à sa tâche pour indiquer sa progression,
 * le type de l'information passé au code lorsque la tâche est finie.
 */
public class Downloading extends AsyncTask<String, String, Boolean> {

    private static final StringBuilder CONTENT_LINE = new StringBuilder();
    private static String fileUrl;
    private static String urlName = "url";
    private static DefaultHttpClient httpClient;
    private static HttpPost filePost;
    private static HttpResponse httpResponse;
    private static StatusLine statLine;
    private static String line;
    private static List<NameValuePair> nameValue;
    private static BufferedReader readerContent;
    private static OutputStream output = null;
    private static int count;
    private static String fileName;
    private static String pathFolder;
    private static File contentDownload;
    Context context;


    public Downloading() {
    }

    /**
     * Cette méthode est appelé une fois la méthode doInBackground est terminé
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    /**
     * Cette méthode s'exécute dans une autre Thread.
     * Elle reçoit un tableau d'objets lui permettant ainsi d'effectuer un traitement en série sur ces objets.
     * Seule cette méthode est exécutée dans une Thread à part, les autres méthodes s'exécutent dans la Thread de l'IHM .
     *
     * @param params
     * @return
     */
    @Override
    protected Boolean doInBackground(String... params) {
        //récupère les paramètres dans des variables de type string
        fileUrl = params[0];
        fileName = params[2];
        pathFolder = params[1];

        //creation de la connexion
        httpClient = new DefaultHttpClient();
        filePost = new HttpPost("http://aboucipu.fr/download.php");
        if(filePost == null){
            Log.e("madera", "Echec de la requête post");
            return false;
        }
        //déclaration des paramètres pour la méthode httpPost dans une variable de type ArrayList
        nameValue = new ArrayList<NameValuePair>(1);
        //Ajout de paramètre dans un tableau
        nameValue.add(new BasicNameValuePair(urlName, fileUrl));
        try {
            filePost.setEntity(new UrlEncodedFormEntity(nameValue));
            //exécute le lien url
            httpResponse = httpClient.execute(filePost);
            statLine = httpResponse.getStatusLine();
            int statusCode = statLine.getStatusCode();
            InputStream content = null;
            //Condition sur le code reponse envoyer par l'url
            if (statusCode == 200) {

                HttpEntity httpEntity = httpResponse.getEntity();
                try {
                    content = httpEntity.getContent();
                    //recupère le chemin dans lequel sera stocké le fichier télécharger dans une variable de type File
                    contentDownload = new File(pathFolder, fileName);
                    //Instanciantion de la class FileOutputStream qui permet l'utilisation de la méthode write().
                    output = new FileOutputStream(contentDownload);
                    //Instanciantion de la taille du fichier télécharger dans une variable de type bytes.
                    byte data[] = new byte[1024];
                    while ((count = content.read(data)) != -1) {
                        output.write(data, 0, count);
                    }
                    output.flush();
                    output.close();
                } catch (IOException e) {
                    Log.e("madera", "Le serveur n'a pas répondu correctement lors du transfert du fichier message :" + e.getMessage());
                    return false;
                }

                //recupère le contenu afin d'éffectué une lecture des ces données.
                readerContent = new BufferedReader(new InputStreamReader(content));

                //Lit ligne par ligne le contenu récupérer
                while ((line = readerContent.readLine()) != null) {
                    CONTENT_LINE.append(line + "\n");
                }
            } else {
                Log.e("madera", "Le serveur n'a pas répondu correctement lors du transfert du fichier");
                return false;
            }

        } catch (IOException e) {
            Log.e("madera", "Le serveur n'a pas répondu correctement lors du transfert du fichier  message :" + e.getMessage());
            return false;
        }
        return true;
    }

    protected void onPostExecute(String params) {
        //récupère les paramètres entrés lors de l'exécution de la class.

    }
}
