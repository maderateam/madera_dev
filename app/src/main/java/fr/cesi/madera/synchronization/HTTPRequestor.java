package fr.cesi.madera.synchronization;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Doremus on 20/04/2017.
 */

public class HTTPRequestor extends AsyncTask<Map<String,String>,Void,String> {

    public static final String URL = "http://www.aboucipu.fr/api_result.php";

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Map<String, String>... params) {
        //création de la connexion
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpost = new HttpPost(URL);
        if(httpost == null){
            Log.e("madera", "Echec de la requête post");
            return null;
        }

        //Déclaration des paramètres pour la méthode httpPost dans une variable de type ArrayList
        ArrayList<NameValuePair> nameValue = new ArrayList<>();
        //Ajout de paramètre dans un tableau
        for (Map.Entry<String, String> entry : params[0].entrySet()) {
            nameValue.add(new BasicNameValuePair(entry.getKey().toString(), entry.getValue().toString()));
        }
        try {
            httpost.setEntity(new UrlEncodedFormEntity(nameValue));
            //exécute le lien url avec les paramètres d'identification
            HttpResponse httpResponse = httpClient.execute(httpost);
            StatusLine statusLine = httpResponse.getStatusLine();

            int statusCodeHttp = statusLine.getStatusCode();
            //Condition sur le code de la reponse http du lien url
            if (statusCodeHttp == 200) {
                HttpEntity httpEntity = httpResponse.getEntity();
                try {

                    InputStream content = httpEntity.getContent();
                    BufferedReader readerContent = new BufferedReader(new InputStreamReader(content));
                    return readerContent.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else {
                Log.e("madera","Code return " + statusCodeHttp + " " + statusLine.getReasonPhrase());
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }
}
