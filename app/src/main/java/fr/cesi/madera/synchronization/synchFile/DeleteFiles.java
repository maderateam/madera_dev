package fr.cesi.madera.synchronization.synchFile;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by KH on 22/03/2017.
 */

public class DeleteFiles extends AsyncTask<String, String, String> {

    private static final String LOGIN_USER = "login";
    private static final String PASS_USER = "pass";
    private static String fileUrl;
    private static String urlName = "url";
    private static DefaultHttpClient httpClient;
    private static HttpPost filePost;
    private static HttpResponse httpResponse;
    private static StatusLine statLine;
    private static String reponse;
    private static List<NameValuePair> nameValue;
    private static BufferedReader readerContent;


    public DeleteFiles() {
    }

    /**
     * Cette méthode est appelé une fois la méthode doInBackground est terminé
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    /**
     * Cette méthode s'exécute dans une autre Thread.
     * Elle reçoit un tableau d'objets lui permettant ainsi d'effectuer un traitement en série sur ces objets.
     * Seule cette méthode est exécutée dans une Thread à part, les autres méthodes s'exécutent dans la Thread de l'IHM .
     *
     * @param params
     * @return
     */
    @Override
    protected String doInBackground(String... params) {

        //récupère les paramètres dans des variables de type string
        fileUrl = params[0];

        //creation de la connexion
        httpClient = new DefaultHttpClient();
        filePost = new HttpPost("http://aboucipu.fr/delete.php");
        if(filePost == null){
            Log.e("madera", "Echec de la requête post");
            return "error";
        }

        //déclaration des paramètres pour la méthode httpPost dans une variable de type ArrayList
        nameValue = new ArrayList<NameValuePair>(3);
        //Ajout de paramètre dans un tableau
        nameValue.add(new BasicNameValuePair(urlName, fileUrl));
        nameValue.add(new BasicNameValuePair(LOGIN_USER, "mamdera"));
        nameValue.add(new BasicNameValuePair(PASS_USER, "123"));
        try {
            filePost.setEntity(new UrlEncodedFormEntity(nameValue));
            //exécute le lien url
            httpResponse = httpClient.execute(filePost);
            statLine = httpResponse.getStatusLine();
            int statusCode = statLine.getStatusCode();
            InputStream content = null;
            //Condition sur le code reponse envoyer par l'url
            if (statusCode == 200) {
                HttpEntity httpEntity = httpResponse.getEntity();
                try {
                    content = httpEntity.getContent();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //recupère le contenu afin d'éffectué une lecture des ces données.
                readerContent = new BufferedReader(new InputStreamReader(content));
                reponse = readerContent.readLine();
            } else {
                return "error";
            }

        } catch (IOException e) {
            return "error";
        }
        return reponse;
    }


    protected void onPostExecute(String params) {
        //récupère les paramètres entrés lors de l'exécution de la class.

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}
