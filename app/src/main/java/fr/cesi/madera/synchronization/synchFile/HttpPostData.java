package fr.cesi.madera.synchronization;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by KH on 23/02/2017.
 */

//Cette class permet de générer un fichier json avec l'héritage de class AsyncTask
public class HttpPostData extends AsyncTask<String, Void, JSONObject> {
    private static JSONObject jObj = null;
    private static  DefaultHttpClient httpClient;
    private static HttpPost httpost;
    private static List<NameValuePair> nameValue;
    private static HttpResponse httpResponse;
    private static BufferedReader readerContent;
    private static int statusCodeHttp;
    private static StatusLine statusLine;
    private static HttpEntity httpEntity;
    private static InputStream content = null;

    public HttpPostData() {}
    /**
     * Cette méthode est appelé une fois la méthode doInBackground est terminé
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    /**
     * Cette méthode s'exécute dans une autre Thread.
     * Elle reçoit un tableau d'objets lui permettant ainsi d'effectuer un traitement en série sur ces objets.
     * Seule cette méthode est exécutée dans une Thread à part, les autres méthodes s'exécutent dans la Thread de l'IHM .
     * @param params
     * @return
     */
    @Override
    protected JSONObject doInBackground(String... params) {
        //création de la connexion
        httpClient = new DefaultHttpClient();
        httpost = new HttpPost("http://aboucipu.fr/api_result.php");
        if(httpost == null){
            Log.e("madera", "Echec de la requête post");
            return null;
        }

        //déclaration des paramètres pour la méthode httpPost dans une variable de type ArrayList
        nameValue = new ArrayList<NameValuePair>();
        //Ajout de paramètre dans un tableau
        nameValue.add(new BasicNameValuePair("comserial", "sqdsqdsqd"));
        nameValue.add(new BasicNameValuePair("method", "synchfile"));

        try {
            httpost.setEntity(new UrlEncodedFormEntity(nameValue));
            //exécute le lien url avec les paramètres d'identification
            httpResponse = httpClient.execute(httpost);
            statusLine = httpResponse.getStatusLine();

            statusCodeHttp = statusLine.getStatusCode();
            //Condition sur le code de la reponse http du lien url
            if (statusCodeHttp == 200) {

                httpEntity = httpResponse.getEntity();
                try {

                    content = httpEntity.getContent();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                readerContent = new BufferedReader(new InputStreamReader(content));

                // si redaLine renvoie une chaine vide créer exception
                try {
                    jObj = new JSONObject(readerContent.readLine());

                } catch (JSONException e) {e.printStackTrace();}
            } else {
                Log.i("madera","Code : " + statusCodeHttp);
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        //Récupération de l'object Json
        return jObj;
    }
}