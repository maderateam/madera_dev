package fr.cesi.madera.synchronization.synchData;

import android.content.Context;
import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import fr.cesi.madera.dao.EntityDao;
import fr.cesi.madera.entities.Batch;
import fr.cesi.madera.entities.Commercial;
import fr.cesi.madera.entities.Entity;
import fr.cesi.madera.misc.exceptions.SynchronizationException;
import fr.cesi.madera.services.BatchService;
import fr.cesi.madera.synchronization.HTTPRequestor;
import fr.cesi.madera.synchronization.httpResponse.BatchResponse;
import fr.cesi.madera.synchronization.httpResponse.Response;

/**
 * Created by Doremus on 19/04/2017.
 */

public class SynchEntity {
    private BatchService batchService;
    private EntityDao eDao;
    private Context ctx;

    public SynchEntity(Context ctx) {
        batchService = new BatchService(ctx);
        this.ctx = ctx;
        eDao = new EntityDao(ctx) {
            @Override
            public ArrayList getAllEntities() {return null;}
        };
    }

    public void synchronizedData(final String comserial) throws SynchronizationException{
        final Map<String,String> args = new HashMap<>();
        args.put("comserial",comserial);
        args.put("method","sync");
        BatchProcessor processor = new BatchProcessor() {
            @Override
            public Object process(ArrayList<Batch> batches) {
                processBatch(batches);
                return null;
            }
        };
        getDataFromServer(args,processor);
        pushDataToServer(comserial);
    }

    public Commercial firstLogin(String login, String pwd, String serial) throws SynchronizationException{
        Map<String,String> args = new HashMap<>();
        args.put("comserial",serial);
        args.put("method","login");
        args.put("login",login);
        args.put("pwd",pwd);
        BatchProcessor processor = new BatchProcessor() {
            @Override
            public Object process(ArrayList<Batch> batches) {
                return getCommercialFromBatch(batches);
            }
        };
        return (Commercial) getDataFromServer(args,processor);
    }

    private Commercial getCommercialFromBatch(ArrayList<Batch> data) {
        ObjectMapper mapper = new ObjectMapper();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:s");
        mapper.setDateFormat(df);
        Commercial com = null;
        try {
            com = mapper.readValue(data.get(0).getEntity(),Commercial.class);
        } catch (IOException e) {
            // TODO erreur
            e.printStackTrace();
        }
        Log.i("madera","Comm getcomFromBatch : " + com);
        return com;
    }

    public void initialLoad(String serial) throws SynchronizationException{
        Map<String,String> args = new HashMap<>();
        args.put("comserial",serial);
        args.put("method","initialLoad");
        BatchProcessor processor = new BatchProcessor() {
            @Override
            public Object process(ArrayList<Batch> batches) {
                processBatch(batches);
                return null;
            }
        };
        getDataFromServer(args,processor);
    }

    public Object getDataFromServer(Map<String,String> args,BatchProcessor processor) throws SynchronizationException{
        HTTPRequestor requestor = new HTTPRequestor();
        try {
            String resString = requestor.execute(args).get(30, TimeUnit.SECONDS);
            Log.i("madera","Message from getDataFromServer : " + resString + " map : " + args);
            ObjectMapper objectMapper = new ObjectMapper();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:s");
            objectMapper.setDateFormat(df);
            BatchResponse res = objectMapper.readValue(resString,BatchResponse.class);
            if(res != null && res.getStatus().equals("OK")){
                return processor.process(res.getData());
            }else{
                throw  new SynchronizationException("Error on initial Load request");
            }
        } catch (InterruptedException | ExecutionException e ){
            throw  new SynchronizationException("Error on initial Load request : " + e.getMessage());
        }catch(TimeoutException e) {
            throw  new SynchronizationException("Error on initial Load request : " + e.getMessage());
        } catch (IOException e) {
            throw  new SynchronizationException("Error on initial Load request : " + e.getMessage());
        }
    }
     // TODO suppr batch when ok
    private void pushDataToServer(String comserial) throws SynchronizationException{
        HTTPRequestor requestor = new HTTPRequestor();
        Map<String,String> args = new HashMap<>();
        args.put("comserial",comserial);
        args.put("method","pushBatches");
        String batchesString = batchService.getJsonFromBatches();
        if(batchesString != null && !batchesString.equals("")) {
            args.put("data", batchesString);
            try {
                String resString = requestor.execute(args).get(30, TimeUnit.SECONDS);
                Log.i("madera", "Message string : " + resString);
                ObjectMapper objectMapper = new ObjectMapper();
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:s");
                objectMapper.setDateFormat(df);
                try {
                    Response res = objectMapper.readValue(resString, Response.class);
                    if (res.getStatus().equals("OK")) {
                        Log.i("madera", "OK Push ! :" + res.getMsg());
                        batchService.deleteAllBatch();
                    } else {
                        Log.i("madera", "NOK Push ! :" + res.getMsg());
                        throw new SynchronizationException("Le serveur a répondu par une erreur");
                    }
                }catch(JsonParseException e){
                    throw new SynchronizationException("Erreur dans le parsing de la réponse");
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                throw new SynchronizationException("Error on initial Load request");
            } catch (TimeoutException e) {
                throw new SynchronizationException("Error on initial Load request");
            } catch (IOException e) {
                e.printStackTrace();
                throw new SynchronizationException("Error on initial Load request");
            }
        }
    }
    private boolean processBatch(ArrayList<Batch> res){
        EntityDao.setLockBatch(true);
        for (Batch batch: res) {
            processEntity(batch);
        }
        EntityDao.setLockBatch(false);
        return true;
    }

    private void processEntity(Batch batch) {
        Entity entity = batchService.getEntityFromBatch(batch);
        switch (batch.getOperation()){
            case BatchService.INSERT_OP:
                eDao.insertEntity(entity);
                break;
            case BatchService.DELETE_OP:
                eDao.deleteEntity(entity);
                break;
            case BatchService.UPDATE_OP:
                eDao.updateEntity(entity);
                break;
            default:
                Log.i("madera","Incorrect operation");
        }
    }

    interface BatchProcessor{
        Object process(ArrayList<Batch> batches);
    }
}
