package fr.cesi.madera.synchronization.synchFile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by KH on 11/02/2017.
 */
//Cette class permet la vérification d'une connexion internet sur une tablette
public class DeviceNetwork {
    private static ConnectivityManager conn = null;
    private static String TAG = "NET";
    private static NetworkInfo networtInfo;

    private DeviceNetwork() {
    }

    /**
     * Cette methode permet de vérifier la présence d'une connexion internet sur un support.
     *
     * @param ctx
     * @return
     */
    public static boolean networChecking(Context ctx) {
        /**
         * La methode getSystemService permet de récupérer le service de réseau
         */
        conn = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        // check la connexion internet de la tablette
        /**
         * La methode getActiveNetworkInfo() permet de récupérer l'information sur le réseau internet si il est active ou non.
         */
        networtInfo = conn.getActiveNetworkInfo();

        //Condition sur la présence d'une connexion internet
        if (networtInfo != null) {

            Log.e(TAG, "Network  available");
            /**
             * La méthode getType() permet de récupérer le type de connexion internet.
             * Condition sur ce type de connexion.
             */
            if (networtInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                Log.e(TAG, "Connection  WIFI");
                return true;
            } else if (networtInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                Log.e(TAG, "Connection  Mobile");
                return true;
            } else {
                return false;
            }

        } else {
            Log.e(TAG, "Error connection");
            return false;
        }
    }

    /**
     * La methode static networkActive_msg renvoie un message d'information si la connexion internet n'est pas active
     *
     * @param ctx
     */
    public static void networkUnactive_msg(Context ctx) {
        AlertDialog.Builder alertNetwork = new AlertDialog.Builder(ctx);
        alertNetwork.setMessage("Activez votre connexion internet")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })
                .create();
        alertNetwork.show();
    }
}
