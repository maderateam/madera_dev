package fr.cesi.madera.synchronization.synchFile;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import fr.cesi.madera.misc.exceptions.SynchronizationException;
import fr.cesi.madera.synchronization.HttpPostData;

/**
 * Created by KH on 02/03/2017.
 */

public class SynchFile {
    private static final SimpleDateFormat FORMAT_DATE = new SimpleDateFormat("dd-MM-yyyy");
    //Ces variable représentes une liste de fichiers de chaque composant
    private static JSONObject content = null, objContent = null;
    private static Context ctx;
    private static JSONArray contentFileArray;
    private static String jsonContentVal = "";
    private static String jsonContentValdate = "", newPath;
    private static File fileToDelete;
    private static DeleteFiles delFiles;
    //variable concernant le nom du dossier Madera lors du téléchargement des fichiers
    private static String homeDirConf = "/Madera/";
    //Cette Class permet la récupération du fchier json via une requête Http
    private static HttpPostData JSON_FILE;
    private static Date fileModifDate;
    private static String dateModifFileDevice, fileName, dateJsonFile, reponseFileServ;
    ;
    private static Downloading fileDownload;

    private static int fileCriteria;
    private static long dateModifDevice, dateJson;

    private static File[] allFilesInDevice;
    //Ces variables désignent les composants utiliser par le B.E
    private static File wallsComponent, dividersComponent, carpentriesComponent, floorsComponent, coveringComponent, wallsFiles;
    private static File dividersFiles, floorsFiles, carpentriesFiles, coveringFiles;
    private static File wallsDirInDevice, coveringDirInDevice, floorsDirInDevice, carpentriesDirInDevice, dividersDirInDevice;
    private ArrayList<String> component = new ArrayList<String>();


    public SynchFile(Context context) {
        this.ctx = context;
    }

    /**
     * Cette methode permet de récupérer un fichier json via un requette Http et d'en extraire le contenu.
     */
    public void getJson() throws JSONException {
        Log.i("madera","File dir : " + ctx.getFilesDir());
        try {
            JSON_FILE = new HttpPostData();
            content = JSON_FILE.execute().get(30, TimeUnit.SECONDS);
            if(content == null){
                throw new JSONException("Erreur de récupération du JSON content == null");
            }
            contentFileArray = content.getJSONArray("files");
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            throw new JSONException("Erreur de récupération du JSON");
        }

        for (int j = 0; j < contentFileArray.length(); j++) {
            objContent = contentFileArray.getJSONObject(j);
            jsonContentVal = objContent.getString("url");
            jsonContentValdate = objContent.getString("dateModif");
            component.add(jsonContentVal + "=" + jsonContentValdate);
        }

    }

    /**
     * Cette methode permet de télécharger les fichier sur le server afin de mettre le logiciel de création de maison
     * modulaire à jour
     */
    public void componentUpdate() throws SynchronizationException {
        Log.i("madera","In componentUpdate");
        try {
            getJson();
            Log.i("madera","In componentUpdate after getJson()");
        } catch (JSONException e) {
            throw new SynchronizationException("Erreur de récupération du JSONs serveur");
        }
        for (int u = 0; u < component.size(); u++) {
            Log.i("madera","Download file number : " + u + " on " + component.size() + " Name : " + component.get(u));
            //Log.i("madera","In componentUpdate LOOP");
            fileCriteria = component.get(u).lastIndexOf("/");
            String dir = component.get(u).substring(0, component.get(u).indexOf("/"));
            String subF = component.get(u).substring(0, fileCriteria + 1);
            String fileallfileName = component.get(u).substring(fileCriteria + 1);
            newPath = component.get(u).substring(0, component.get(u).indexOf("="));
            dateJsonFile = fileallfileName.substring(fileallfileName.indexOf("=") + 1);
            fileName = fileallfileName.substring(0, fileallfileName.lastIndexOf("="));
            switch (dir) {
                //Si dans la boucle on pointe sur un dossier walls on entre dans la condition
                //walls component
                case "walls":
                    wallsFiles = new File(ctx.getFilesDir() + homeDirConf + subF, fileName);
                    if (!wallsFiles.exists()) {
                        fileDownload = new Downloading();
                        wallsComponent = new File(ctx.getFilesDir() + homeDirConf + subF);
                        if (!wallsComponent.mkdir()) {
                            wallsComponent.mkdirs();
                        }
                        if (wallsComponent.exists()) {
                            AsyncTask<String,String,Boolean> task = fileDownload.execute(newPath, wallsComponent.toString(), fileName);
                            try {
                                if(!task.get(30, TimeUnit.SECONDS)){
                                    throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                }
                            } catch (InterruptedException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            } catch (ExecutionException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            } catch (TimeoutException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            }
                        }
                        break;
                    }

                    wallsDirInDevice = new File(ctx.getFilesDir() + homeDirConf + subF);
                    allFilesInDevice = new File(ctx.getFilesDir() + homeDirConf + subF).listFiles();
                    if (wallsDirInDevice.exists()) {
                        for (int q = 0; q < allFilesInDevice.length; q++) {
                            delFiles = new DeleteFiles();
                            fileDownload = new Downloading();

                            fileModifDate = new Date(allFilesInDevice[q].lastModified());
                            //Transformation du format de la date récupérer dans le fichier Json
                            dateModifFileDevice = FORMAT_DATE.format(fileModifDate);
                            try {
                                dateModifDevice = FORMAT_DATE.parse(dateModifFileDevice).getTime();
                                dateJson = FORMAT_DATE.parse(dateJsonFile).getTime();
                                fileToDelete = new File(ctx.getFilesDir() + homeDirConf + subF + allFilesInDevice[q].getName());
                                AsyncTask<String,String,String> deleteTask = delFiles.execute(subF + allFilesInDevice[q].getName());
                                if (deleteTask.get(30, TimeUnit.SECONDS).contains("false")) {
                                    Log.i("madera", "In delete :" + allFilesInDevice[q].getName());
                                    fileToDelete.delete();
                                    break;
                                }else if(deleteTask.get().contains("error")) {
                                    throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                }
                                //Condition sur la date du fichier distant et celui du server
                                if (dateJson > dateModifDevice) {
                                    wallsComponent = new File(ctx.getFilesDir() + homeDirConf + subF);
                                    AsyncTask<String,String,Boolean> task = fileDownload.execute(newPath, wallsComponent.toString(), fileName);
                                    try {
                                        if(!task.get(30, TimeUnit.SECONDS)){
                                            throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                        }
                                    } catch (InterruptedException e) {
                                        throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                    } catch (ExecutionException e) {
                                        throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                    } catch (TimeoutException e) {
                                        throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                    }
                                    break;
                                } else {
                                }
                            } catch (InterruptedException | ExecutionException | ParseException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            } catch (TimeoutException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            }
                            fileDownload.cancel(true);
                            delFiles.cancel(true);
                        }
                        break;
                    }
                    break;
                // floors component
                case "floors":
                    floorsFiles = new File(ctx.getFilesDir() + homeDirConf + subF, fileName);
                    if (!floorsFiles.exists()) {
                        fileDownload = new Downloading();
                        floorsComponent = new File(ctx.getFilesDir() + homeDirConf + subF);
                        if (!floorsComponent.mkdir()) {
                            floorsComponent.mkdirs();
                        }
                        if (floorsComponent.exists()) {
                            AsyncTask<String,String,Boolean> task = fileDownload.execute(newPath, floorsComponent.toString(), fileName);
                            try {
                                if(!task.get(30, TimeUnit.SECONDS)){
                                    throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                }
                            } catch (InterruptedException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            } catch (ExecutionException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            } catch (TimeoutException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            }
                        }
                        break;
                    }

                    floorsDirInDevice = new File(ctx.getFilesDir() + homeDirConf + subF);
                    allFilesInDevice = new File(ctx.getFilesDir() + homeDirConf + subF).listFiles();
                    if (floorsDirInDevice.exists()) {
                        for (int q = 0; q < allFilesInDevice.length; q++) {
                            delFiles = new DeleteFiles();
                            fileDownload = new Downloading();

                            fileModifDate = new Date(allFilesInDevice[q].lastModified());
                            //Transformation du format de la date récupérer dans le fichier Json
                            dateModifFileDevice = FORMAT_DATE.format(fileModifDate);
                            try {
                                dateModifDevice = FORMAT_DATE.parse(dateModifFileDevice).getTime();
                                dateJson = FORMAT_DATE.parse(dateJsonFile).getTime();
                                fileToDelete = new File(ctx.getFilesDir() + homeDirConf + subF + allFilesInDevice[q].getName());

                                AsyncTask<String,String,String> deleteTask = delFiles.execute(subF + allFilesInDevice[q].getName());
                                if (deleteTask.get(30, TimeUnit.SECONDS).contains("false")) {
                                    Log.i("Reponse path:", allFilesInDevice[q].getName());
                                    fileToDelete.delete();
                                    break;
                                }else if(deleteTask.get().contains("error")) {
                                    throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                }
                                //Condition sur la date du fichier distant et celui du server
                                if (dateJson > dateModifDevice) {
                                    floorsComponent = new File(ctx.getFilesDir() + homeDirConf + subF);
                                    AsyncTask<String,String,Boolean> task = fileDownload.execute(newPath, floorsComponent.toString(), fileName);
                                    try {
                                        if(!task.get(30, TimeUnit.SECONDS)){
                                            throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                        }
                                    } catch (InterruptedException e) {
                                        throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                    } catch (ExecutionException e) {
                                        throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                    } catch (TimeoutException e) {
                                        throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                    }
                                    Log.i("Reponse :", "Not Update");
                                    break;
                                }
                            } catch (InterruptedException | ExecutionException | ParseException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            } catch (TimeoutException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            }
                            fileDownload.cancel(true);
                            delFiles.cancel(true);
                        }
                        break;
                    }
                    break;
                //dividers component
                case "dividers":
                    dividersFiles = new File(ctx.getFilesDir() + homeDirConf + subF, fileName);
                    if (!dividersFiles.exists()) {
                        fileDownload = new Downloading();
                        dividersComponent = new File(ctx.getFilesDir() + homeDirConf + subF);
                        if (!dividersComponent.mkdir()) {
                            dividersComponent.mkdirs();
                        }
                        if (dividersComponent.exists()) {
                            AsyncTask<String,String,Boolean> task = fileDownload.execute(newPath, dividersComponent.toString(), fileName);
                            try {
                                if(!task.get(30, TimeUnit.SECONDS)){
                                    throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                }
                            } catch (InterruptedException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            } catch (ExecutionException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            } catch (TimeoutException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            }
                        }
                        break;
                    }

                    dividersDirInDevice = new File(ctx.getFilesDir() + homeDirConf + subF);
                    allFilesInDevice = new File(ctx.getFilesDir() + homeDirConf + subF).listFiles();
                    if (dividersDirInDevice.exists()) {
                        for (int q = 0; q < allFilesInDevice.length; q++) {
                            delFiles = new DeleteFiles();
                            fileDownload = new Downloading();

                            fileModifDate = new Date(allFilesInDevice[q].lastModified());
                            //Transformation du format de la date récupérer dans le fichier Json
                            dateModifFileDevice = FORMAT_DATE.format(fileModifDate);
                            try {
                                dateModifDevice = FORMAT_DATE.parse(dateModifFileDevice).getTime();
                                dateJson = FORMAT_DATE.parse(dateJsonFile).getTime();
                                fileToDelete = new File(ctx.getFilesDir() + homeDirConf + subF + allFilesInDevice[q].getName());

                                AsyncTask<String,String,String> deleteTask = delFiles.execute(subF + allFilesInDevice[q].getName());
                                if (deleteTask.get(30, TimeUnit.SECONDS).contains("false")) {
                                    Log.i("Reponse path:", allFilesInDevice[q].getName());
                                    fileToDelete.delete();
                                    break;
                                }else if(deleteTask.get().contains("error")) {
                                    throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                }
                                //Condition sur la date du fichier distant et celui du server
                                if (dateJson > dateModifDevice) {
                                    dividersComponent = new File(ctx.getFilesDir() + homeDirConf + subF);
                                    AsyncTask<String,String,Boolean> task = fileDownload.execute(newPath, dividersComponent.toString(), fileName);
                                    try {
                                        if(!task.get(30, TimeUnit.SECONDS)){
                                            throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                        }
                                    } catch (InterruptedException e) {
                                        throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                    } catch (ExecutionException e) {
                                        throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                    } catch (TimeoutException e) {
                                        throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                    }
                                    break;
                                }
                            } catch (InterruptedException | ExecutionException | ParseException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            } catch (TimeoutException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            }
                            fileDownload.cancel(true);
                            delFiles.cancel(true);
                        }
                        break;
                    }
                    break;
                //carpentries component

                case "covering":
                    coveringFiles = new File(ctx.getFilesDir() + homeDirConf + subF, fileName);
                    if (!coveringFiles.exists()) {
                        fileDownload = new Downloading();
                        coveringComponent = new File(ctx.getFilesDir() + homeDirConf + subF);
                        if (!coveringComponent.mkdir()) {
                            coveringComponent.mkdirs();
                        }
                        if (coveringComponent.exists()) {
                            AsyncTask<String,String,Boolean> task = fileDownload.execute(newPath, coveringComponent.toString(), fileName);
                            try {
                                if(!task.get(30, TimeUnit.SECONDS)){
                                    throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                }
                            } catch (InterruptedException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            } catch (ExecutionException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            } catch (TimeoutException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            }
                        }
                        break;
                    }

                    coveringDirInDevice = new File(ctx.getFilesDir() + homeDirConf + subF);
                    allFilesInDevice = new File(ctx.getFilesDir() + homeDirConf + subF).listFiles();
                    if (coveringDirInDevice.exists()) {
                        for (int q = 0; q < allFilesInDevice.length; q++) {
                            delFiles = new DeleteFiles();
                            fileDownload = new Downloading();

                            fileModifDate = new Date(allFilesInDevice[q].lastModified());
                            //Transformation du format de la date récupérer dans le fichier Json
                            dateModifFileDevice = FORMAT_DATE.format(fileModifDate);
                            try {
                                dateModifDevice = FORMAT_DATE.parse(dateModifFileDevice).getTime();
                                dateJson = FORMAT_DATE.parse(dateJsonFile).getTime();
                                fileToDelete = new File(ctx.getFilesDir() + homeDirConf + subF + allFilesInDevice[q].getName());

                                AsyncTask<String,String,String> deleteTask = delFiles.execute(subF + allFilesInDevice[q].getName());
                                if (deleteTask.get(30, TimeUnit.SECONDS).contains("false")) {
                                    Log.i("Reponse path:", allFilesInDevice[q].getName());
                                    fileToDelete.delete();
                                    break;
                                }else if(deleteTask.get().contains("error")) {
                                    throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                }
                                //Condition sur la date du fichier distant et celui du server
                                if (dateJson > dateModifDevice) {
                                    coveringComponent = new File(ctx.getFilesDir() + homeDirConf + subF);
                                    AsyncTask<String,String,Boolean> task = fileDownload.execute(newPath, coveringComponent.toString(), fileName);
                                    try {
                                        if(!task.get(30, TimeUnit.SECONDS)){
                                            throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                        }
                                    } catch (InterruptedException e) {
                                        throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                    } catch (ExecutionException e) {
                                        throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                    } catch (TimeoutException e) {
                                        throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                    }
                                    Log.i("Reponse :", "Not Update");
                                    break;
                                } else {
                                    Log.i("Reponse :", "File Update");
                                }
                            } catch (InterruptedException | ExecutionException | ParseException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            } catch (TimeoutException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            }
                            fileDownload.cancel(true);
                            delFiles.cancel(true);
                        }
                        break;
                    }
                    break;

                //covering component
                case "carpentries":
                    carpentriesFiles = new File(ctx.getFilesDir() + homeDirConf + subF, fileName);
                    if (!carpentriesFiles.exists()) {
                        fileDownload = new Downloading();
                        carpentriesComponent = new File(ctx.getFilesDir() + homeDirConf + subF);
                        if (!carpentriesComponent.mkdir()) {
                            carpentriesComponent.mkdirs();
                        }
                        if (carpentriesComponent.exists()) {
                            AsyncTask<String,String,Boolean> task = fileDownload.execute(newPath, carpentriesComponent.toString(), fileName);
                            try {
                                if(!task.get(30, TimeUnit.SECONDS)){
                                    throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                }
                            } catch (InterruptedException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            } catch (ExecutionException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            } catch (TimeoutException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            }
                        }
                        break;
                    }

                    carpentriesDirInDevice = new File(ctx.getFilesDir() + homeDirConf + subF);
                    allFilesInDevice = new File(ctx.getFilesDir() + homeDirConf + subF).listFiles();
                    if (carpentriesDirInDevice.exists()) {
                        for (int q = 0; q < allFilesInDevice.length; q++) {
                            delFiles = new DeleteFiles();
                            fileDownload = new Downloading();

                            fileModifDate = new Date(allFilesInDevice[q].lastModified());
                            //Transformation du format de la date récupérer dans le fichier Json
                            dateModifFileDevice = FORMAT_DATE.format(fileModifDate);
                            try {
                                dateModifDevice = FORMAT_DATE.parse(dateModifFileDevice).getTime();
                                dateJson = FORMAT_DATE.parse(dateJsonFile).getTime();
                                fileToDelete = new File(ctx.getFilesDir() + homeDirConf + subF + allFilesInDevice[q].getName());

                                AsyncTask<String,String,String> deleteTask = delFiles.execute(subF + allFilesInDevice[q].getName());
                                if (deleteTask.get(30, TimeUnit.SECONDS).contains("false")) {
                                    Log.i("Reponse path:", allFilesInDevice[q].getName());
                                    fileToDelete.delete();
                                    break;
                                }else if(deleteTask.get().contains("error")) {
                                    throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                }
                                //Condition sur la date du fichier distant et celui du server
                                if (dateJson > dateModifDevice) {
                                    carpentriesComponent = new File(ctx.getFilesDir() + homeDirConf + subF);
                                    AsyncTask<String,String,Boolean> task = fileDownload.execute(newPath, carpentriesComponent.toString(), fileName);
                                    try {
                                        if(!task.get(30, TimeUnit.SECONDS)){
                                            throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                        }
                                    } catch (InterruptedException e) {
                                        throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                    } catch (ExecutionException e) {
                                        throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                    } catch (TimeoutException e) {
                                        throw new SynchronizationException("Erreur lors du trasfert des fichier");
                                    }
                                    break;
                                }
                            } catch (InterruptedException | ExecutionException | ParseException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            } catch (TimeoutException e) {
                                throw new SynchronizationException("Erreur lors du trasfert des fichier");
                            }
                            fileDownload.cancel(true);
                            delFiles.cancel(true);
                        }
                        break;
                    }
                    break;
            }
        }
    }


}




