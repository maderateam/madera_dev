package fr.cesi.madera.synchronization;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;

import java.util.Calendar;

import fr.cesi.madera.entities.Commercial;
import fr.cesi.madera.misc.exceptions.SynchronizationException;
import fr.cesi.madera.synchronization.synchData.SynchEntity;
import fr.cesi.madera.synchronization.synchFile.DeviceNetwork;
import fr.cesi.madera.synchronization.synchFile.SynchFile;
import fr.cesi.madera.views.activities.AccueilActivity;

/**
 * Created by Doremus on 05/04/2017.
 */

public class SyncLauncher {
    private Context ctx;
    private static SynchFile downloadFile;
    private static SynchEntity syncData;
    private final Object lock = new Object();
    private Handler mainHandler;


    public SyncLauncher(Context ctx) {
        this.ctx = ctx;
        downloadFile = new SynchFile(ctx);
        syncData = new SynchEntity(ctx);
        mainHandler = new Handler(ctx.getMainLooper());
    }

    public void startSync(final String serialcom) {
        final ProgressDialog ringProgressDialog = ProgressDialog.show(ctx, "Synchronisation en cours...", "Téléchargement des fichiers...", true);
        ringProgressDialog.setCancelable(false);
        final Handler mainHandler = new Handler(ctx.getMainLooper());
        if (DeviceNetwork.networChecking(ctx)) {
            final Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        syncData.synchronizedData(serialcom);
                        //downloadFile.componentUpdate();
                        Runnable myRunnable = new Runnable() {
                            @Override
                            public void run() {
                                launchOKAlert("Synchonisation effectuée !");
                            }
                        };
                        mainHandler.post(myRunnable);
                    } catch (SynchronizationException e) {
                        DoItIfFailed doit = new DoItIfFailed() {
                            @Override
                            public void doIt(String comserial) {
                                startSync(serialcom);
                            }
                        };
                        launchErrorAlert("Erreur lors de la synchhronisation des données",serialcom,doit);
                    }
                }
            });
            dismissProgressBar(th,ringProgressDialog);

        } else {
            DeviceNetwork.networkUnactive_msg(ctx);
        }
    }
    public Commercial getCommercial(final String login, final String pwd, final String serialcom){
        final Commercial[] com = {null};
        if (DeviceNetwork.networChecking(ctx)) {
            final Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        com[0] = syncData.firstLogin(login,pwd,serialcom);

                    } catch (SynchronizationException e) {
//                        DoItIfFailed doIt = new DoItIfFailed() {
//                            @Override
//                            public void doIt(String comserial) {
//                                getCommercial(login,pwd,serialcom);
//                            }
//                        };
//                        launchErrorAlert("Erreur de connection",serialcom,doIt);
                    }
                    synchronized (lock) {
                        lock.notify();
                    }
                }
            });
            th.start();
        } else {
            DeviceNetwork.networkUnactive_msg(ctx);
        }
        synchronized (lock) {
            try {
                lock.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return com[0];
    }

    public void initialLoad(final String serialcom) {
        final ProgressDialog ringProgressDialog = ProgressDialog.show(ctx, "Synchronisation en cours...", "Téléchargement des fichiers...", true);
        ringProgressDialog.setCancelable(false);
        if (DeviceNetwork.networChecking(ctx)) {
            final Thread th = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        syncData.initialLoad(serialcom);
                        downloadFile.componentUpdate();
                        Runnable myRunnable = new Runnable() {
                            @Override
                            public void run() {
                                launchOKAlert("Synchonisation effectuée !");
                            }
                        };
                        mainHandler.post(myRunnable);
                    } catch (SynchronizationException e) {
                        DoItIfFailed doit = new DoItIfFailed() {
                            @Override
                            public void doIt(String comserial) {
                                initialLoad(serialcom);
                            }
                        };
                        launchErrorAlert("Erreur lors de la synchhronisation des données",serialcom,doit);
                    }
                }
            });
            dismissProgressBar(th,ringProgressDialog);
        } else {
            DeviceNetwork.networkUnactive_msg(ctx);
        }
    }

    public void launchErrorAlert(final String message, final String serialcom, final DoItIfFailed failure){
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                final AlertDialog.Builder builder1 = new AlertDialog.Builder(ctx);
                builder1.setMessage(message);
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "Rééssayer",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                failure.doIt(serialcom);
                            }
                        });
                builder1.setNegativeButton(
                        "Annuler",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                               dialog.dismiss();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        };
        mainHandler.post(myRunnable);
    }

    public void launchOKAlert(final String message){
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(ctx);
                builder1.setMessage(message);
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                SharedPreferences sharedpreferences = ctx.getSharedPreferences("settings", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                Calendar cal = Calendar.getInstance();
                                //cal.set(Calendar.DAY_OF_WEEK,1);
                                editor.putLong("lastSync", cal.getTime().getTime());
                                editor.commit();
                                Intent acc_intent = new Intent(ctx,AccueilActivity.class);
                                ctx.startActivity(acc_intent);
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        };
        mainHandler.post(myRunnable);
    }

    public void dismissProgressBar(final Thread th, final ProgressDialog ringProgressDialog ){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    th.start();
                    while (th.isAlive()) {
                        Log.i("madera","alive");
                        Thread.sleep(1000);
                    }
                    Runnable myRunnable = new Runnable() {
                        @Override
                        public void run() {
                            Log.i("madera","dismiss loading");
                            ringProgressDialog.dismiss();
                        }
                    };
                    mainHandler.post(myRunnable);
                } catch (Exception e) {
                    Log.i("madera", e.getMessage());
                }
            }
        }).start();
    }

    public interface DoItIfFailed{
        void doIt(String comserial);
    }
}
