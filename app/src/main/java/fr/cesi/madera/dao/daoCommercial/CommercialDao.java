package fr.cesi.madera.dao.daoCommercial;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import java.util.ArrayList;

import fr.cesi.madera.dao.IEntityDao;
import fr.cesi.madera.dao.SyncDao;
import fr.cesi.madera.entities.Commercial;

/**
 * Created by Joshua on 01/11/2016.
 */

public class CommercialDao<T>extends SyncDao implements IEntityDao,ICommercialDao {

    public static final Class COMMERCIAL_CLASS = Commercial.class;
    public static final String COMMERCIAL_CLASS_NAME = COMMERCIAL_CLASS.getSimpleName().toLowerCase();

    public CommercialDao(Context context) {
        super(context);
    }

    @Override
    public Commercial connectCommercialFirst(String mail, String password, String mac){
        Commercial com = getComByCredentials(mail,password);
        if(com != null) {
            com.setComserial(mac);
            updateEntity(com);
            return com;
        }
        return null;
    }
    @Override
    public Commercial getComByCredentials(String mail, String password){
        openDbIfClosed();
        Cursor cursorM = mDb.rawQuery("SELECT * FROM "+ COMMERCIAL_CLASS_NAME + " WHERE mail = ? AND password = ?",new String[]{mail,password});
        if(cursorM.getCount() == 1){
            Commercial com = (Commercial)dataUtils.cursorToEntities(Commercial.class,cursorM).get(0);
            cursorM.close();
            return com;
        }
        cursorM.close();
        return null;
    }

    @Override
    public boolean macAdressIsPresent(String macAdress){
        Cursor cursorM  = mDb.rawQuery("SELECT * FROM "+ COMMERCIAL_CLASS_NAME + " WHERE mac = ?",new String[]{macAdress});
        if(cursorM.getCount() <= 0){
            cursorM.close();
            return false;
        }
        cursorM.close();
        return true;
    }
    @Override
    public Commercial getCommercialBySerial(String serialcom) throws SQLException{
        openDbIfClosed();
        Cursor cursorM  = mDb.rawQuery("SELECT * FROM "+ COMMERCIAL_CLASS_NAME + " WHERE comserial = ?",new String[]{serialcom});
        if(cursorM.getCount() > 1){
            throw new SQLException("Plusieurs commerciaux trouvé avec la même adresse mac");
        }else if(cursorM.getCount() <= 0){
            cursorM.close();
            return null;
        }
        return (Commercial)dataUtils.cursorToEntities(Commercial.class,cursorM).get(0);
    }

    @Override
    public ArrayList getAllEntities() {
        return super.getAllEntities(COMMERCIAL_CLASS,COMMERCIAL_CLASS_NAME);
    }
}
