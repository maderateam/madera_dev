package fr.cesi.madera.dao.daoDiscount;

/**
 * Created by Joshua on 01/11/2016.
 */

public interface IDiscountDao {

    public double getComponentDicount(long idComponent, long idGamme);

}
