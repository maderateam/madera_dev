package fr.cesi.madera.dao.daoProject;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import fr.cesi.madera.dao.IEntityDao;
import fr.cesi.madera.dao.SyncDao;
import fr.cesi.madera.entities.Client;
import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.Project;

/**
 * Created by Joshua on 29/10/2016.
 */

public class ProjectDao<T> extends SyncDao implements IEntityDao, IProjectDao {

    public static final Class PROJECT_CLASS = Project.class;
    public static final String PROJECT_CLASS_NAME = PROJECT_CLASS.getSimpleName().toLowerCase();

    public ProjectDao(Context context) {
        super(context);
    }

    @Override
    public ArrayList getAllEntities() {
        openDbIfClosed();
        return super.getAllEntities(PROJECT_CLASS,PROJECT_CLASS_NAME);
    }

    @Override
    public ArrayList<Project> searchProjectsByName(String name){
        openDbIfClosed();
        Cursor curs = mDb.rawQuery("SELECT * FROM " + getSQLiteView(PROJECT_CLASS_NAME) + " WHERE name LIKE ? COLLATE NOCASE",new String[]{name + "%"});
        ArrayList<Project> listOfProject = dataUtils.cursorToEntities(PROJECT_CLASS,curs);
        curs.close();
        return listOfProject;
    }

    @Override
    public void addPlanToProject(Plan plan, Project project) {
        openDbIfClosed();
        String sqlRequest = "INSERT INTO project_plan ( project_id , plan_id ) VALUES (?,?)";
        mDb.rawQuery(sqlRequest,new String[]{String.valueOf(project.get_id()),String.valueOf(plan.get_id())});

    }

    @Override
    public void addClientToProject(Client client, Project project) {
        openDbIfClosed();
        String sqlRequest = "INSERT INTO client_project ( client_id, project_id ) VALUES (?,?)";
        mDb.rawQuery(sqlRequest,new String[]{String.valueOf(client.get_id()),String.valueOf(project.get_id())});

    }

    @Override
    public ArrayList<Project> getProjectsByClient(Client client) {
        openDbIfClosed();
        Cursor cursorM = mDb.rawQuery("SELECT * FROM " + getSQLiteView(PROJECT_CLASS_NAME) + " WHERE idclient =?",new String[]{String.valueOf(client.get_id())});
        ArrayList<Project> project = dataUtils.cursorToEntities(PROJECT_CLASS,cursorM);
        cursorM.close();
        return  project;
    }

    @Override
    public ArrayList<Project> getAllOpenedProjects() {
        openDbIfClosed();
        Cursor cursorM = mDb.rawQuery("SELECT * FROM "+ getSQLiteView(PROJECT_CLASS_NAME) + " WHERE closed = 'false'",null);
        return dataUtils.cursorToEntities(Project.class,cursorM);
    }

}
