package fr.cesi.madera.dao.daoSlot;

import java.util.ArrayList;

import fr.cesi.madera.entities.Component;
import fr.cesi.madera.entities.Slot;

/**
 * Created by Joshua on 01/11/2016.
 */

public interface ISlotDao {

    /**
     * Récupère la liste des slots liés à ce composant
     * @param component composant dont on veut récupérer les slots
     * @return la liste des slots correspondant à ce composant
     */
    public ArrayList<Slot> getSlotsFromComponent(Component component);
}
