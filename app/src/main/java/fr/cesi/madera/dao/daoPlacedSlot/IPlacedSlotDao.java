package fr.cesi.madera.dao.daoPlacedSlot;

import java.util.ArrayList;

import fr.cesi.madera.entities.PlacedComponent;
import fr.cesi.madera.entities.PlacedSlot;
import fr.cesi.madera.entities.Plan;

/**
 * Created by Joshua on 01/11/2016.
 */

public interface IPlacedSlotDao {
    /**
     * Méthode de récupération des placedSlots du placedComponent
     * @param placedComponent
     * @return
     */
    public ArrayList<PlacedSlot> getChildPSlotsFromPComponent(PlacedComponent placedComponent);

    /**
     * Récupération du pslot contenant ce placedComponent
     * @param placedComponent
     * @return le slot ou null si le PlacedComponent est de plus haut niveau
     */
    public ArrayList<PlacedSlot> getParentPSlotFromPComponent(PlacedComponent placedComponent);

    /**
     * Récupération de tous les PlacedSlots liés à ce plan
     * @param plan
     * @return
     */
    public ArrayList<PlacedSlot> getPSlotsByPlan(Plan plan);
}
