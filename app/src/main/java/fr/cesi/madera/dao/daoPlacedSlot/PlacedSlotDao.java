package fr.cesi.madera.dao.daoPlacedSlot;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import fr.cesi.madera.dao.IEntityDao;
import fr.cesi.madera.dao.SyncDao;
import fr.cesi.madera.entities.Entity;
import fr.cesi.madera.entities.PlacedComponent;
import fr.cesi.madera.entities.PlacedSlot;
import fr.cesi.madera.entities.Plan;

/**
 * Created by Joshua on 01/11/2016.
 */

public class PlacedSlotDao<T> extends SyncDao implements IEntityDao, IPlacedSlotDao {

    public static final Class PSLOT_CLASS = PlacedSlot.class;
    public static final String PSLOT_CLASS_NAME = PSLOT_CLASS.getSimpleName().toLowerCase();

    public PlacedSlotDao(Context context) {
        super(context);
    }


    @Override
    public Entity insertEntity(Entity entity) throws IllegalArgumentException{
        if(((PlacedSlot)entity).getIdchildpcomponent() == ((PlacedSlot)entity).getIdparentpcomponent()){
            throw new IllegalArgumentException("Child and parent ref are equals, stop for prevent loop ");
        }else{
            return super.insertEntity(entity);
        }
    }

    @Override
    public ArrayList getAllEntities() {
        return super.getAllEntities(PSLOT_CLASS,PSLOT_CLASS_NAME);

    }

    @Override
    public ArrayList<PlacedSlot> getChildPSlotsFromPComponent(PlacedComponent placedComponent) {
        openDbIfClosed();
        ArrayList<PlacedSlot> pSlotList = new ArrayList<PlacedSlot>();
        Cursor cursorM = mDb.rawQuery("SELECT * FROM " + getSQLiteView(PSLOT_CLASS_NAME +
                " WHERE idparentpcomponent = ?"),new String[]{String.valueOf(placedComponent.get_id())});
        pSlotList = dataUtils.cursorToEntities(PSLOT_CLASS,cursorM);
        cursorM.close();
        return pSlotList;
    }

    @Override
    public ArrayList<PlacedSlot> getParentPSlotFromPComponent(PlacedComponent placedComponent) {
        openDbIfClosed();
        ArrayList<PlacedSlot> pSlotList;
        Cursor cursorM = mDb.rawQuery("SELECT * FROM " + getSQLiteView(PSLOT_CLASS_NAME) +
                " WHERE _id = ?",new String[]{String.valueOf(placedComponent.getIdparentpslot())});
        pSlotList = dataUtils.cursorToEntities(PSLOT_CLASS,cursorM);
        cursorM.close();
        return pSlotList;
    }

    @Override
    public ArrayList<PlacedSlot> getPSlotsByPlan(Plan plan) {
        openDbIfClosed();
        ArrayList<PlacedSlot> pSlotsList;
        Cursor cursor = mDb.rawQuery("SELECT * FROM " + getSQLiteView(PSLOT_CLASS_NAME)
                + " WHERE idplan = ? ", new String[]{String.valueOf(plan.get_id())});
        pSlotsList = dataUtils.cursorToEntities(PSLOT_CLASS,cursor);
        cursor.close();
        return pSlotsList;
    }

}



