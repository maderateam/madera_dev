package fr.cesi.madera.dao.daoComponent;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import fr.cesi.madera.dao.EntityDao;
import fr.cesi.madera.dao.IEntityDao;
import fr.cesi.madera.entities.Component;
import fr.cesi.madera.entities.PlacedComponent;
import fr.cesi.madera.entities.Slot;

/**
 * Created by Joshua on 01/11/2016.
 */

public class ComponentDao extends EntityDao implements IEntityDao, IComponentDao{

    public static final Class COMPONENT_CLASS = Component.class;
    public static final String COMPONENT_CLASS_NAME = COMPONENT_CLASS.getSimpleName().toLowerCase();

    public ComponentDao(Context context) {
        super(context);
    }

    @Override
    public ArrayList getAllEntities() {
        openDbIfClosed();
        ArrayList<Component> compoList;
        Cursor cursorM = mDb.rawQuery("SELECT * FROM " + COMPONENT_CLASS_NAME,null);
        compoList = dataUtils.cursorToEntities(COMPONENT_CLASS,cursorM);
        cursorM.close();
        return compoList;
    }

    @Override
    public ArrayList<Component> getComponentFromSlot(Slot slot){
        openDbIfClosed();
        ArrayList<Component> compoList;
        Cursor cursorM = mDb.rawQuery("SELECT * FROM " + COMPONENT_CLASS_NAME + " WHERE _id = ?",
                new String[]{String.valueOf(slot.getIdparentcomponent())});
        compoList = dataUtils.cursorToEntities(COMPONENT_CLASS,cursorM);
        cursorM.close();
        return compoList;
    }

    @Override
    public Component getComponentFromPComponent(PlacedComponent pCompo) {
        openDbIfClosed();
        Component compo;
        Cursor cursor = mDb.rawQuery("SELECT * FROM " + COMPONENT_CLASS_NAME
                + " WHERE _id = ?", new String[]{String.valueOf(pCompo.getIdcomponent())});

        ArrayList<Component> components = dataUtils.cursorToEntities(COMPONENT_CLASS,cursor);

        if(components.size() > 0){
            compo = (Component) dataUtils.cursorToEntities(COMPONENT_CLASS,cursor).get(0);
        }else{
            return null;
        }
        cursor.close();
        return compo;
    }
}
