package fr.cesi.madera.dao.daoPlacedComponent;

import java.util.ArrayList;

import fr.cesi.madera.entities.PlacedComponent;
import fr.cesi.madera.entities.PlacedSlot;
import fr.cesi.madera.entities.Plan;

/**
 * Created by Joshua on 01/11/2016.
 */

public interface IPlacedComponent {
    /**
     * Méthode de récupération du PComponent contenu dans le PSlot
     * @param placedSlot
     * @return
     */
    public PlacedComponent getChildPComponentFromPSlot(PlacedSlot placedSlot);

    /**
     * Récupération du PComponent possédant le PSlot
     * @param placedSlot
     * @return
     */
    public PlacedComponent getParentPComponentFromPSlot(PlacedSlot placedSlot);

    /**
     * Récupère la liste des placedcomponent liés à ce plan
     * @param plan plan dont on veut récupérer les placedComonent
     * @return la liste des placedComponent si ils existent ou une liste vide sinon
     */
    public ArrayList<PlacedComponent> getPComponentByPlan(Plan plan);

}
