package fr.cesi.madera.dao.daoParameter;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import fr.cesi.madera.dao.EntityDao;
import fr.cesi.madera.entities.Parameter;

/**
 * Created by Joshua on 26/02/2017.
 */

public class ParameterDao extends EntityDao implements IParameterDao{

    public static final Class PARAMETER_CLASS = Parameter.class;
    public static final String PARAMETER_CLASS_NAME = PARAMETER_CLASS.getSimpleName().toLowerCase();

    public ParameterDao(Context context) {
        super(context);
    }

    @Override
    public ArrayList getAllEntities() {
        return null;
    }

    public void openDbIfClosed() {
        if (getDb() == null || !getDb().isOpen()) {
            open();
        }
    }
    @Override
    public Object getParameterValueFromName(String paramName) {
        openDbIfClosed();
        Parameter param = null;
        Cursor cursorM = mDb.rawQuery("SELECT * FROM " + PARAMETER_CLASS_NAME + " WHERE name = ?", new String[]{paramName});
        ArrayList params =  dataUtils.cursorToEntities(PARAMETER_CLASS, cursorM);
        if(params.size() > 0){
            param = (Parameter) dataUtils.cursorToEntities(PARAMETER_CLASS, cursorM).get(0);
        }else{
            return null;
        }
        Object value = null ;
        switch (param.getType().toLowerCase()) {
            case "boolean":
                value = Boolean.parseBoolean(param.getData());
                return value;
            case "string":
                value = String.valueOf(param.getData());
                return value;
            case "double":
                value = Double.parseDouble(param.getData());
                return value;
            case "int":
                value = Integer.parseInt(param.getData());
                return value;
            case "float":
                value = Float.parseFloat(param.getData());
                return value;
            case "long":
                value = Long.parseLong(param.getData());
                return value;
        }
        return value;
    }
}
