package fr.cesi.madera.dao.daoParameter;

/**
 * Created by Joshua on 26/02/2017.
 */

public interface IParameterDao {
    /**
     * Renvoit un objet contenant la valeur du parametre en fonction de son nom
     * @param paramName
     * @return
     */
    Object getParameterValueFromName(String paramName);
}
