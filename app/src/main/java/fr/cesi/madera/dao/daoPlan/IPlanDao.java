package fr.cesi.madera.dao.daoPlan;

import java.util.ArrayList;

import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.Project;

/**
 * Created by Joshua on 01/11/2016.
 */

public interface IPlanDao {

    /**
     * Méthode de récupération de la liste des plans archivés
     * @return
     */
    public ArrayList<Plan> listOfArchivedPlans();

    /**
     * Méthode de récupération de la liste de plan relative au projet
     * @param project
     * @return
     */
    public ArrayList<Plan> getPlansByProject(Project project);

    /**
     * Méthode de récupération d'un plan archivé par son ID
     * @param id
     * @return
     */
    public Plan getArchivedPlanById(String id);

}
