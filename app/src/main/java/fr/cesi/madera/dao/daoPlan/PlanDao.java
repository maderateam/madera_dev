package fr.cesi.madera.dao.daoPlan;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import fr.cesi.madera.dao.IEntityDao;
import fr.cesi.madera.dao.SyncDao;
import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.Project;

/**
 * Created by Joshua on 29/10/2016.
 */

public class PlanDao<T> extends SyncDao implements IEntityDao, IPlanDao {

    public static final Class PLAN_CLASS = Plan.class;
    public static final String PLAN_CLASS_NAME = PLAN_CLASS.getSimpleName().toLowerCase();

    public PlanDao(Context context) {
        super(context);
    }

    @Override
    public ArrayList getAllEntities() {
        return super.getAllEntities(PLAN_CLASS,PLAN_CLASS_NAME);

    }

    @Override
    public ArrayList<Plan> listOfArchivedPlans() {
        openDbIfClosed();
        ArrayList<Plan> allEntities;
        // TODO Check if boolean is literal or numeric in BD for true or false value
        Cursor cursorM = mDb.rawQuery("SELECT * FROM "+ getSQLiteView(PLAN_CLASS_NAME) +" WHERE archived = ?",new String[]{"true"});
        allEntities = dataUtils.cursorToEntities(Plan.class,cursorM);
        cursorM.close();
        return  allEntities;
    }

    @Override
    public ArrayList<Plan> getPlansByProject(Project project) {
        openDbIfClosed();
        ArrayList<Plan> planList;
        Cursor cursorM = mDb.rawQuery("SELECT * FROM " + getSQLiteView(PLAN_CLASS_NAME)
                + " WHERE idproject = ? AND hidden = ?", new String[]{""+project.get_id(),"false"});
        planList = dataUtils.cursorToEntities(PLAN_CLASS,cursorM);
        cursorM.close();
        return planList;
    }

    @Override
    public Plan getArchivedPlanById(String id) {
        return null;
    }
}
