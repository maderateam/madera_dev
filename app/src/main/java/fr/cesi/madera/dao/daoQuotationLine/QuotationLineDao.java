package fr.cesi.madera.dao.daoQuotationLine;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import fr.cesi.madera.dao.SyncDao;
import fr.cesi.madera.entities.QuotationLine;

/**
 * Created by Joshua on 21/02/2017.
 */

public class QuotationLineDao extends SyncDao implements IQuotationLineDao {


    public static final Class QUOTATION_LINE_CLASS = QuotationLine.class;
    public static final String QUOTATION_LINE_CLASS_NAME = QUOTATION_LINE_CLASS.getSimpleName().toLowerCase();

    public QuotationLineDao(Context context) {
        super(context);
    }

    @Override
    public ArrayList getAllEntities() {
        return null;
    }


    @Override
    public ArrayList<QuotationLine> getQuotationLines(long idQuotation) {
        openDbIfClosed();
        ArrayList<QuotationLine> quotationLineList;
        Cursor cursorM = mDb.rawQuery("SELECT * FROM " + QUOTATION_LINE_CLASS_NAME +" WHERE idquotation = ?", new String[]{String.valueOf(idQuotation)});
        quotationLineList = dataUtils.cursorToEntities(QUOTATION_LINE_CLASS,cursorM);
        cursorM.close();
        return quotationLineList;
    }
}
