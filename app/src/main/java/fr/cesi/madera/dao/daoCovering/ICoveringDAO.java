package fr.cesi.madera.dao.daoCovering;

/**
 * Created by Doremus on 09/04/2017.
 */

public interface ICoveringDAO {
    /**
     * Renvoi le prix d'un covering en fonction de l'id du covering
     * @param idCovering
     * @return le prix du covering
     */
    double getCoveringPrice(long idCovering);
}
