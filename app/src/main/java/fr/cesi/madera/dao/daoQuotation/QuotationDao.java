package fr.cesi.madera.dao.daoQuotation;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCantOpenDatabaseException;

import java.util.ArrayList;

import fr.cesi.madera.dao.IEntityDao;
import fr.cesi.madera.dao.SyncDao;
import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.Quotation;

/**
 * Created by Joshua on 29/10/2016.
 */

public class QuotationDao<T> extends SyncDao implements IEntityDao, IQuotationDao {

    public static final Class QUOTATION_CLASS = Quotation.class;
    public static final String  QUOTATION_CLASS_NAME =  QUOTATION_CLASS.getSimpleName().toLowerCase();

    public QuotationDao(Context context) {
        super(context);
    }

    @Override
    public ArrayList getAllEntities() {
        return super.getAllEntities(QUOTATION_CLASS,QUOTATION_CLASS_NAME);
    }


    @Override
    public Quotation validateQuotation(Quotation quotation) {
        openDbIfClosed();
        quotation.setValidated(true);
        // Vérification de l'ouverture à l'accès à la BDD
        // renvoi SQLiteCantOpenDatabaseException en cas d'erreur
        if(!mDb.isOpen()){
            throw new SQLiteCantOpenDatabaseException();
        }
        // Classe de récupération des values à insérer dans la base
        ContentValues values = new ContentValues();
        // Méthode générique de découpage d'une entity en modèle Colonne / value
        values = dataUtils.getValuesFromEntity(quotation);
        // Update de l'objet en base en fonction de son ID défini préalablement dans la base
        mDb.update(quotation.getClass().getSimpleName(),values,"_id="+quotation.get_id(),null);
        return null;
    }

    @Override
    public Quotation getQuotationByPlan(Plan plan) {
        openDbIfClosed();
        Cursor cursor = mDb.rawQuery("SELECT * FROM "+ getSQLiteView(QUOTATION_CLASS_NAME) +" WHERE idplan =?",new String[]{""+plan.getIdQuotation()});
        ArrayList<Quotation> quotation = dataUtils.cursorToEntities(QUOTATION_CLASS,cursor);
        cursor.close();
        if(quotation.size() > 0){
            return quotation.get(0);
        }
        return null;
    }

}
