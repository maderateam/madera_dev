package fr.cesi.madera.dao.daoDiscount;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import fr.cesi.madera.dao.EntityDao;
import fr.cesi.madera.dao.IEntityDao;
import fr.cesi.madera.entities.Discount;
import fr.cesi.madera.entities.Entity;

/**
 * Created by Joshua on 01/11/2016.
 */

public class DiscountDao<T> extends EntityDao implements IEntityDao, IDiscountDao {

    public static final Class DISCOUNT_CLASS = Discount.class;
    public static final String DISCOUNT_CLASS_NAME = DISCOUNT_CLASS.getSimpleName().toLowerCase();

    public DiscountDao(Context context) {
        super(context);
    }

    @Override
    public ArrayList getAllEntities() {
        openDbIfClosed();
        ArrayList<Entity> allEntities;
        Cursor cursorM = mDb.rawQuery("SELECT * FROM "+ DISCOUNT_CLASS_NAME,null);
        allEntities = dataUtils.cursorToEntities(DISCOUNT_CLASS,cursorM);
        cursorM.close();
        return allEntities;
    }

    @Override
    public double getComponentDicount(long idComponent, long idGamme) {
        openDbIfClosed();
        double discount = 0;
        Cursor cursorM = mDb.rawQuery("SELECT SUM(rate) AS rates FROM " + DISCOUNT_CLASS_NAME + " WHERE idcompo = ? OR idgamme = ?", new String[]{String.valueOf(idComponent), String.valueOf(idGamme)});
        if(cursorM.moveToFirst()){
            discount = cursorM.getDouble(cursorM.getColumnIndex("rates"));
        }
        return discount;
    }
}
