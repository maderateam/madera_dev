package fr.cesi.madera.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import fr.cesi.madera.entities.Batch;
import fr.cesi.madera.entities.Client;
import fr.cesi.madera.entities.Commercial;
import fr.cesi.madera.entities.Component;
import fr.cesi.madera.entities.Covering;
import fr.cesi.madera.entities.Discount;
import fr.cesi.madera.entities.Gamme;
import fr.cesi.madera.entities.Parameter;
import fr.cesi.madera.entities.PlacedComponent;
import fr.cesi.madera.entities.PlacedSlot;
import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.Project;
import fr.cesi.madera.entities.Quotation;
import fr.cesi.madera.entities.QuotationLine;
import fr.cesi.madera.entities.Slot;
import fr.cesi.madera.misc.DataBaseCreator;

/**
 * Created by Joshua on 02/11/2016.
 */

public class DataBaseHelper extends SQLiteOpenHelper {


    private static DataBaseHelper sInstance;
    private DataBaseCreator dataBaseCreator;


    /**
     * Constructor privé pour implémenter le singleton
     */
    private DataBaseHelper(Context context, String dataBaseName, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, dataBaseName, null, version);
        this.dataBaseCreator = new DataBaseCreator();
        // On établit la correspondance des types

        DataBaseCreator.matchType.put(String.class, "TEXT");
        DataBaseCreator.matchType.put(Integer.class, "INTEGER");
        DataBaseCreator.matchType.put(int.class, "INTEGER");
        DataBaseCreator.matchType.put(long.class, "INTEGER");
        DataBaseCreator.matchType.put(Long.class, "INTEGER");
        DataBaseCreator.matchType.put(Float.class, "REAL");
        DataBaseCreator.matchType.put(float.class, "REAL");
        DataBaseCreator.matchType.put(Double.class, "REAL");
        DataBaseCreator.matchType.put(double.class, "REAL");
        DataBaseCreator.matchType.put(Boolean.class, "TEXT");
        DataBaseCreator.matchType.put(boolean.class, "TEXT");
        DataBaseCreator.matchType.put(java.sql.Timestamp.class, "LONG");

        // On ajoute les classes dont on veut générer le script de création des tables SQLite
        DataBaseCreator.entities = new ArrayList<Class>();
        DataBaseCreator.entities.add(Client.class);
        DataBaseCreator.entities.add(Commercial.class);
        DataBaseCreator.entities.add(Component.class);
        DataBaseCreator.entities.add(Discount.class);
        DataBaseCreator.entities.add(Gamme.class);
        DataBaseCreator.entities.add(PlacedComponent.class);
        DataBaseCreator.entities.add(PlacedSlot.class);
        DataBaseCreator.entities.add(Plan.class);
        DataBaseCreator.entities.add(Project.class);
        DataBaseCreator.entities.add(Quotation.class);
        DataBaseCreator.entities.add(Slot.class);
        DataBaseCreator.entities.add(Covering.class);
        DataBaseCreator.entities.add(Parameter.class);
        DataBaseCreator.entities.add(QuotationLine.class);
        DataBaseCreator.entities.add(Batch.class);
    }

    /**
     * Renvoit une instance unique du DataBaseHelper,
     * si l'instance est à null parce qu'aucun appel a cette méthode n'a été fait,
     * créé l'instance, puis la renvoit
     * @param context
     * @param dataBaseName
     * @param factory
     * @param version
     * @return
     */
    public static synchronized DataBaseHelper getInstance(Context context, String dataBaseName, SQLiteDatabase.CursorFactory factory, int version) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new DataBaseHelper(context, dataBaseName, null, version);
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (String query: (ArrayList<String>)dataBaseCreator.sqliteQueryGenerator()) {
            db.execSQL(query);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (String query: (ArrayList<String>)dataBaseCreator.sqliteQueryGenerator()) {
            db.execSQL(query);
        }
    }

    public DataBaseCreator getDataBaseCreator() {
        return dataBaseCreator;
    }

}
