package fr.cesi.madera.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Remi on 02/08/2016.
 */
public abstract class DAOBase {

    protected final static int VERSION = 6;
    // Le nom du fichier qui représente ma base
    protected final static String NOM = "database.db";
    protected SQLiteDatabase mDb;
    protected DataBaseHelper mHandler;

    public DataBaseHelper getmHandler() {
        return mHandler;
    }
    public DAOBase(){}

    public DAOBase(Context pContext) {
        this.mHandler = DataBaseHelper.getInstance(pContext, NOM, null, VERSION);
    }

    public SQLiteDatabase open() {
        // Pas besoin de fermer la dernière base puisque getWritableDatabase s'en charge
        mDb = mHandler.getWritableDatabase();
        return mDb;

    }

    /**
     * Ferme la base de donnée et toutes les transactions en cours
     */
    public void close() {
        mDb.close();
    }

    /**
     * Renvoit un objet représentant la base de données
     * @return
     */
    public SQLiteDatabase getDb() {
        return mDb;
    }

    
}
