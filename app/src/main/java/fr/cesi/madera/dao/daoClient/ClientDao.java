package fr.cesi.madera.dao.daoClient;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import fr.cesi.madera.dao.IEntityDao;
import fr.cesi.madera.dao.SyncDao;
import fr.cesi.madera.entities.Client;
import fr.cesi.madera.entities.Project;

/**
 * Created by Joshua on 29/10/2016.
 */

public class ClientDao<T> extends SyncDao implements IEntityDao, IClientDao {

    public static final Class CLIENT_CLASS = Client.class;
    public static final String CLIENT_CLASS_NAME = CLIENT_CLASS.getSimpleName().toLowerCase();

    public ClientDao(Context context) {
        super(context);
    }

    @Override
    public ArrayList<Client> searchClientByName(String token) {
        openDbIfClosed();
        ArrayList<Client> listOfClients = new ArrayList<Client>();
        token.trim();
        Cursor cursorM;
        if(token.contains(" ")) {
            int foundIndex = token.indexOf(" ");
            String firstName = token.substring(0, foundIndex);
            String lastName = token.substring(foundIndex + 1);
            cursorM = mDb.rawQuery("SELECT * FROM " + getSQLiteView(CLIENT_CLASS_NAME) + " WHERE firstname LIKE ? OR lastname LIKE ? " + " COLLATE NOCASE UNION ALL SELECT * FROM " + getSQLiteView(CLIENT_CLASS_NAME) + " WHERE firstname LIKE ? OR lastname LIKE ? COLLATE NOCASE", new String[]{"%" + lastName,"%" + firstName,"%" + firstName,"%" + lastName});
        }else{
            cursorM = mDb.rawQuery("SELECT * FROM " + getSQLiteView(CLIENT_CLASS_NAME) + " WHERE firstname LIKE ?  OR lastname LIKE ? COLLATE NOCASE", new String[]{token + "%",token + "%"});
        }
        if(cursorM.getCount() > 0){
            listOfClients = dataUtils.cursorToEntities(CLIENT_CLASS,cursorM);
        }
        return listOfClients;
    }

    @Override
    public ArrayList getAllEntities() {
        return super.getAllEntities(CLIENT_CLASS,CLIENT_CLASS_NAME);
    }

    @Override
    public Client getClientByProject(Project project) {
        openDbIfClosed();
        Cursor cursorM = mDb.rawQuery("SELECT * FROM " + CLIENT_CLASS_NAME
                + " WHERE _id = ?", new String[]{String.valueOf(project.getIdclient())});
        ArrayList<Client> clients = dataUtils.cursorToEntities(CLIENT_CLASS,cursorM);
        cursorM.close();
        if(clients.size() > 0){
            return clients.get(0);
        }
        return null;
    }
}