package fr.cesi.madera.dao.daoProject;

import java.util.ArrayList;
import java.util.List;

import fr.cesi.madera.entities.Client;
import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.Project;

/**
 * Created by Joshua on 01/11/2016.
 */

public interface IProjectDao {

    /**
     * Méthode d'ajout d'un plan à un projet
     * @param plan
     */
    public void addPlanToProject(Plan plan, Project project);

    /**
     * Méthode d'ajout d'un client à un projet
     * @param client
     */
    public void addClientToProject(Client client, Project project);

    /**
     * Récupération de la liste des projets du client
     * @param client
     * @return
     */
    public List<Project> getProjectsByClient(Client client);

    /**
     * Récupération de tous les projets ouvert.
     * @return la liste des projets ouvert
     */
    public List<Project> getAllOpenedProjects();

    /**
     * Renvoit les projet dont le nom correspond à la chaine en argument
     * @param name
     * @return
     */
    public ArrayList<Project> searchProjectsByName(String name);
}
