package fr.cesi.madera.dao.daoCommercial;

import android.database.SQLException;

import fr.cesi.madera.entities.Commercial;

/**
 * Created by Joshua on 01/11/2016.
 */

public interface ICommercialDao {
    /**
     * Vérifie si les informations de connection du commercial sont correctes et le met à jour en
     * ajoutant son adresse mac.
     * @param mail Adresse mail du commercila
     * @param password mot de passe du commercial
     * @return le commercial mis à jour
     */
    public Commercial connectCommercialFirst(String mail, String password, String mac);

    /**
     * Récuprère le commercial correspondant à ces informations d'authentification
     * @param mail le mail du commercial
     * @param password le mot de passe du commercial
     * @return le commercial si les informations d'authentification sont exactes ou null sinon
     */
    public Commercial getComByCredentials(String mail, String password);

    /**
     * Vérifie si l'addresse mac passée en paramètres est présente au sein d'un des comptes commercial
     * @param macAdress adresse mac du commercial
     * @return True si l'adresse mac est présente, False sinon
     */
    public boolean macAdressIsPresent(String macAdress);

    /**
     * Récupère le commercial correspondant à cette adresse mac
     * @param serialcom adresse mac du commercial
     * @return le commercial correspondant
     * @throws SQLException si plusieurs commerciaux ont été trouvés
     */
    public Commercial getCommercialBySerial(String serialcom) throws SQLException;
}
