package fr.cesi.madera.dao.daoCovering;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import fr.cesi.madera.dao.EntityDao;
import fr.cesi.madera.dao.IEntityDao;
import fr.cesi.madera.entities.Component;
import fr.cesi.madera.entities.Covering;

/**
 * Created by Remi on 06/12/2016.
 */

public class CoveringDao extends EntityDao implements ICoveringDAO, IEntityDao{
    public static final Class COVERING_CLASS = Covering.class;
    public static final String COVERING_CLASS_NAME = COVERING_CLASS.getSimpleName().toLowerCase();

    public CoveringDao(Context context) {
        super(context);
    }

    @Override
    public ArrayList getAllEntities() {
        openDbIfClosed();
        ArrayList<Component> compoList;
        Cursor cursorM = mDb.rawQuery("SELECT * FROM " + COVERING_CLASS_NAME,null);
        compoList = dataUtils.cursorToEntities(COVERING_CLASS,cursorM);
        cursorM.close();
        return compoList;
    }
    @Override
    public double getCoveringPrice(long idCovering){
        openDbIfClosed();
        double coveringprice = 0;
        Cursor cursorM = mDb.rawQuery("SELECT price FROM " + COVERING_CLASS_NAME + " WHERE _id = ?", new String[]{String.valueOf(idCovering)});
        if(cursorM.moveToFirst()){
            coveringprice = cursorM.getDouble(cursorM.getColumnIndex("price"));
        }
        return coveringprice;
    }
}
