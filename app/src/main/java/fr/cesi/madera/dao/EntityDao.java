package fr.cesi.madera.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.util.Log;

import java.util.ArrayList;

import fr.cesi.madera.entities.Batch;
import fr.cesi.madera.entities.Entity;
import fr.cesi.madera.entities.SynchronisedEntity;
import fr.cesi.madera.misc.DataBaseUtils;
import fr.cesi.madera.misc.exceptions.NoSerialComException;
import fr.cesi.madera.services.BatchService;

/**
 * Created by Joshua on 03/11/2016.
 */

public abstract class EntityDao extends DAOBase implements IEntityDao {

    protected DataBaseUtils dataUtils = new DataBaseUtils();
    private BatchService batchService;
    private static boolean lockBatch = false;
    private Context ctx;

    public EntityDao(Context context){
        super(context);
        batchService = new BatchService(context);
        ctx = context;
    }

    @Override
    public Entity insertEntity(Entity entity) {
        openDbIfClosed();
        // Classe de récupération des values à insérer dans la base
        ContentValues values;
        // Méthode générique de découpage d'une entity en modèle Colonne / value
        values = dataUtils.getValuesFromEntity(entity);
        // Insertion des valeurs de l'objet dans la BDD et récupération de l'id défini pour l'entrée
        // dans la base
        if(getEntityById(entity.get_id(),entity.getClass()) != null){
            updateEntity(entity);
        }else{
            long insertId = mDb.insert(entity.getClass().getSimpleName(), null,values);
            // Attribution de l'id en base à l'objet en cours d'utilisation.
            entity.set_id(insertId);
            if(!isLockBatch() && !entity.getClass().equals(Batch.class) && entity instanceof SynchronisedEntity) {
                try {
                    Log.i("madera","Register Insert");
                    batchService.registerOperation(entity, BatchService.INSERT_OP);
                } catch (NoSerialComException e) {
                    e.printStackTrace();
                }
            }
        }
        Log.i("madera","Insert magel");
        return entity;
    }

    @Override
    public Entity updateEntity(Entity entity) throws SQLiteCantOpenDatabaseException  {
        openDbIfClosed();
        Log.i("madera","Update de ta mer");
        // Classe de récupération des values à insérer dans la base
        // Méthode générique de découpage d'une entity en modèle Colonne / value
        ContentValues values = dataUtils.getValuesFromEntity(entity);
        // Update de l'objet en base en fonction de son ID défini préalablement dans la base
        mDb.update(entity.getClass().getSimpleName(),values,"_id="+(entity).get_id(),null);
        Entity entityUpdated = getEntityById(entity.get_id(),entity.getClass());
        if(!isLockBatch() && !entity.getClass().equals(Batch.class) && entity instanceof SynchronisedEntity) {
            try {
                Log.i("madera","Register Update");
                batchService.registerOperation(entityUpdated, BatchService.UPDATE_OP);
            } catch (NoSerialComException e) {
                e.printStackTrace();
            }
        }
        return entityUpdated;
    }

    @Override
    public Entity getEntityById(long entityId, Class entityClass) throws SQLiteCantOpenDatabaseException {
        openDbIfClosed();
        Entity entity;
        Cursor cursorM = mDb.rawQuery("SELECT * FROM "+ entityClass.getSimpleName().toLowerCase() +" WHERE _id = ?",new String[]{String.valueOf(entityId)});
        ArrayList<Entity> entities = dataUtils.cursorToEntities(entityClass,cursorM);
        cursorM.close();
        if(entities.size()>0){
            return entities.get(0);
        }
        return null;
    }

    @Override
    public void deleteEntity(Entity entity) throws SQLiteCantOpenDatabaseException {
        openDbIfClosed();
        mDb.delete(entity.getClass().getSimpleName(),"_id = "+(entity).get_id(),null);
        if(!isLockBatch() && !entity.getClass().equals(Batch.class) && entity instanceof SynchronisedEntity) {
            try {
                Log.i("madera","Register Delete");
                batchService.registerOperation(entity, BatchService.DELETE_OP);
            } catch (NoSerialComException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public abstract ArrayList getAllEntities();

    @Override
    public void openDbIfClosed() {
        if(getDb() == null || !getDb().isOpen()){
            open();
        }
    }

    public DataBaseUtils getDataUtils() {
        return dataUtils;
    }

    public void setDataUtils(DataBaseUtils dataUtils) {
        this.dataUtils = dataUtils;
    }

    /**
     * Renvoi la valeur du denier id insérer en base pour le type d'entité en argument
     * @param aClass le type des entité dont on veut connaitre le dernier id en base
     * @return
     */
    public long getLastIdForEntity(Class aClass){
        openDbIfClosed();
        Cursor curs = mDb.rawQuery("select MAX(_id) from  " + aClass.getSimpleName().toLowerCase(),null);
        curs.moveToFirst();
        long id =curs.getLong(0);
        curs.close();
        return id;
    }

    public static boolean isLockBatch() {
        return lockBatch;
    }

    public static void setLockBatch(boolean lockBatch) {
        Log.i("madera","Lock bacthes : " + lockBatch);
        EntityDao.lockBatch = lockBatch;
    }
}
