package fr.cesi.madera.dao.daoSlot;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import fr.cesi.madera.dao.EntityDao;
import fr.cesi.madera.dao.IEntityDao;
import fr.cesi.madera.entities.Component;
import fr.cesi.madera.entities.Entity;
import fr.cesi.madera.entities.Slot;

/**
 * Created by Joshua on 01/11/2016.
 */

public class SlotDao<T> extends EntityDao implements IEntityDao, ISlotDao {

    public static final Class SLOT_CLASS = Slot.class;
    public static final String  SLOT_CLASS_NAME =  SLOT_CLASS.getSimpleName().toLowerCase();

    public SlotDao(Context context) {
        super(context);
    }

    @Override
    public ArrayList getAllEntities() {
        openDbIfClosed();
        ArrayList<Entity> allEntities;
        Cursor cursorM = mDb.rawQuery("SELECT * FROM "+ SLOT_CLASS_NAME,null);
        allEntities = dataUtils.cursorToEntities(SLOT_CLASS,cursorM);
        cursorM.close();
        return allEntities;
    }

    @Override
    public ArrayList<Slot> getSlotsFromComponent(Component component) {
        openDbIfClosed();
        ArrayList<Slot> slotList;
        Cursor cursorM = mDb.rawQuery("SELECT * FROM " + SLOT_CLASS_NAME + " WHERE idparentcomponent = ?",  new String[]{String.valueOf(component.get_id())});
        slotList = dataUtils.cursorToEntities(SLOT_CLASS,cursorM);
        cursorM.close();
        return  slotList;
    }


}
