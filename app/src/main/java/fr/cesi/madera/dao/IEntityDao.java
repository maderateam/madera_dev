package fr.cesi.madera.dao;

import java.util.ArrayList;

import fr.cesi.madera.entities.Entity;

/**
 * Created by Joshua on 29/10/2016.
 *
 * Interface permettant d'implémenter à nos DAO les méthodes type du CRUD
 *
 */

public interface IEntityDao<T extends Entity> {

    /**
     * Récupère une netité en fonctionde son id
     * @param entityId id de l'entté à récupérer
     * @return l'entité si elle existe, null sinon
     */
    public Entity getEntityById(long entityId, Class entityClass);

    /**
     *  Insère l'entité en base
     * @param entity entité à insérer
     */
    public Entity insertEntity(T entity);

    /**
     * Met à jour l'entité dans la base
     * @param entity entité à mettre à jour
     */
    public Entity updateEntity(T entity);

    /**
     * Supprime l'entité de la base
     * @param entity entité à supprimer
     * @return True si l'entité à étée supprimé, False sinon
     */
    public void deleteEntity(T entity);

    /**
     * Récupère la liste de toutes les entités
     * @return la liste des entité si elles existe, une liste vide sinon
     */
    public ArrayList<T> getAllEntities();

    /**
     * Vérifie si la connection avec la base de données est ouverte,
     * et l'ouvre si ce n'est as le cas
     */
    public void openDbIfClosed();





}
