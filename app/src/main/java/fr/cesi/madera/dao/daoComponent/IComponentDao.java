package fr.cesi.madera.dao.daoComponent;

import java.util.ArrayList;

import fr.cesi.madera.entities.Component;
import fr.cesi.madera.entities.PlacedComponent;
import fr.cesi.madera.entities.Slot;

/**
 * Created by Joshua on 01/11/2016.
 */

public interface IComponentDao {

    /**
     * Méthode de récupération des composents du slot
     * @param slot
     * @return
     */
    public ArrayList<Component> getComponentFromSlot(Slot slot);

    /**
     * Récupération d'un component à partir d'un placedCoimponent
     * @param pCompo
     * @return
     */
    public Component getComponentFromPComponent(PlacedComponent pCompo);
}
