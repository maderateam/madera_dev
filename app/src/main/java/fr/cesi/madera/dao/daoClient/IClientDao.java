package fr.cesi.madera.dao.daoClient;

import java.util.ArrayList;

import fr.cesi.madera.entities.Client;
import fr.cesi.madera.entities.Project;

/**
 * Created by Joshua on 01/11/2016.
 */

public interface IClientDao {

    /**
     * Recherche dans la table client si un ou plusieurs client porte un nom ET / OU un prénom similaire
     * au texte contenu dans charSequence
     * @param charSequence Chaine de caractère pouvant contenir tout ou partie du nom ET/OU du prénom d'un client
     * @return une liste de client ou une liste vide si aucun client n'est trouvé
     */
    ArrayList<Client> searchClientByName(String charSequence);

    /**
     * Retourne le client associé à un projet
     * @param project le projet dont on recherche le client
     * @return le client associé au projet
     */
    Client getClientByProject(Project project);

}
