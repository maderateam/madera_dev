package fr.cesi.madera.dao.daoPlacedComponent;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import fr.cesi.madera.dao.IEntityDao;
import fr.cesi.madera.dao.SyncDao;
import fr.cesi.madera.entities.PlacedComponent;
import fr.cesi.madera.entities.PlacedSlot;
import fr.cesi.madera.entities.Plan;

/**
 * Created by Joshua on 01/11/2016.
 */

public class PlacedComponentDao<T> extends SyncDao implements IEntityDao,IPlacedComponent {

    public static final Class PCOMPONENT_CLASS = PlacedComponent.class;
    public static final String PCOMPONENT_CLASS_NAME = PCOMPONENT_CLASS.getSimpleName().toLowerCase();

    public PlacedComponentDao(Context context) {
        super(context);
    }

    @Override
    public ArrayList getAllEntities() {
        return super.getAllEntities(PCOMPONENT_CLASS,PCOMPONENT_CLASS_NAME);

    }

    @Override
    public PlacedComponent getParentPComponentFromPSlot(PlacedSlot placedSlot) {
        openDbIfClosed();
        PlacedComponent pCompo;
        Cursor cursorM = mDb.rawQuery("SELECT * FROM " + getSQLiteView(PCOMPONENT_CLASS_NAME)
                + " WHERE idplacedslot = ?", new String[]{String.valueOf(placedSlot.getIdparentpcomponent())});
        pCompo = (PlacedComponent) dataUtils.cursorToEntities(PCOMPONENT_CLASS,cursorM).get(0);
        cursorM.close();
        return pCompo;

    }

    @Override
    public ArrayList<PlacedComponent> getPComponentByPlan(Plan plan) {
        openDbIfClosed();
        ArrayList<PlacedComponent> pCompoList;
        Cursor cursor = mDb.rawQuery("SELECT * FROM " + getSQLiteView(PCOMPONENT_CLASS_NAME)
        + " WHERE idplan = ? ", new String[]{String.valueOf(plan.get_id())});
        pCompoList = dataUtils.cursorToEntities(PCOMPONENT_CLASS,cursor);
        cursor.close();
        return pCompoList;
    }

    @Override
    public PlacedComponent getChildPComponentFromPSlot(PlacedSlot placedSlot) {
        openDbIfClosed();
        Cursor cursorM = mDb.rawQuery("SELECT * FROM " + getSQLiteView(PCOMPONENT_CLASS_NAME)
                + " WHERE idparentpslot = ?", new String[]{String.valueOf(placedSlot.get_id())});
        ArrayList<PlacedComponent> pCompos = dataUtils.cursorToEntities(PCOMPONENT_CLASS,cursorM);
        cursorM.close();
        if(pCompos.size() == 1){
            return pCompos.get(0);
        }
        return  null;
    }
}





