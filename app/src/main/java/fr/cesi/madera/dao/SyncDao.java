package fr.cesi.madera.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import fr.cesi.madera.entities.Commercial;
import fr.cesi.madera.entities.Entity;
import fr.cesi.madera.entities.SynchronisedEntity;
import fr.cesi.madera.misc.SQLiteView;

/**
 * Created by Joshua on 05/11/2016.
 */

public abstract class SyncDao extends EntityDao {

    private ArrayList<SQLiteView> listSQLiteView = new ArrayList<SQLiteView>();

    public SyncDao(Context context) {
        super(context);
    }

    @Override
    public void deleteEntity(Entity entity) {
        if(((SynchronisedEntity)entity).getLastsync() != null && ((SynchronisedEntity)entity).getLastsync().getTime() > 0){
            hide(entity);
        }else{
            super.deleteEntity(entity);
        }
    }

    public void hide(Entity entity){
        openDbIfClosed();
        ((SynchronisedEntity)entity).setHidden(true);
        // Méthode générique de découpage d'une entity en modèle Colonne / value
       updateEntity(entity);
    }

    /**
     * Créé le liste des vues à partir de la liste des entités
     * @param com
     */
    public void setViews(Commercial com){
        openDbIfClosed();
        ArrayList<String> listSQLiteViewAddQuery = getmHandler().getDataBaseCreator().createSQLiteViewsQuery(com);
        ArrayList<String> listSQLiteViewDropQuery = getmHandler().getDataBaseCreator().deleteSQLiteViewsQuery(com);
        for (String query: listSQLiteViewDropQuery){
            getDb().execSQL(query);
        }
        for (String query: listSQLiteViewAddQuery){
            getDb().execSQL(query);
        }
    }

    /**
     * Renvoi la vue pour une table donnée
     * @param entityClassName
     * @return
     */
    public String getSQLiteView(String entityClassName){
        openDbIfClosed();
        String cleanClassName = entityClassName.trim().toLowerCase();
        Cursor curs = mDb.rawQuery("SELECT name AS name from sqlite_master WHERE type = ? AND name LIKE ?",new String[]{"view", "%" + cleanClassName});
        if(curs.getCount() > 0) {
            curs.moveToFirst();
            return curs.getString(curs.getColumnIndex("name"));
        }
        return cleanClassName;
    }

    /**
     * Met à jour la liste des vues et ajoute les entity crées par le commercial com
     * @param com
     */
    public void upgradeViews(Commercial com){
        // TODO récupérer le commercial avec l'adresse mac de la tablette et y ajouter com
    }

    /**
     * Met a jour la liste des vues et la remet à zero en n'autorissant l'affichage des entity
     * pour le commercial com seulement
     * @param com
     */
    public void downgradeViews(Commercial com){
        // TODO si com vaut null on vire tous les com sauf celui qui a la même adresse mac que moi
    }

    /**
     * Récupère la liste des entités à condition qu'elle n'ai pas été hidden
     */
    public ArrayList<Entity> getAllEntities(Class entityClass, String entityClassName){
        openDbIfClosed();
        ArrayList<Entity> entityList;
        Cursor cursorM = mDb.rawQuery("SELECT * FROM " + getSQLiteView(entityClassName) +
        " WHERE hidden = ?", new String[]{"false"});
        entityList = dataUtils.cursorToEntities(entityClass,cursorM);
        cursorM.close();
        return entityList;
    }

}
