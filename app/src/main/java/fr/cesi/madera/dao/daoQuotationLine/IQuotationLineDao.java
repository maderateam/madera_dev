package fr.cesi.madera.dao.daoQuotationLine;

import java.util.ArrayList;

import fr.cesi.madera.entities.QuotationLine;

/**
 * Created by Joshua on 21/02/2017.
 */

public interface IQuotationLineDao {

    public ArrayList<QuotationLine> getQuotationLines(long idQuotation);

}
