package fr.cesi.madera.dao.daoQuotation;

import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.Quotation;

/**
 * Created by Joshua on 01/11/2016.
 */

public interface IQuotationDao {

    /**
     * Méthode permettant de d'activer la validation du devis
     */
    Quotation validateQuotation(Quotation quotation);

    /**
     * Méthode de récupération du devis d'un plan
     * @param plan
     * @return
     */
    Object getQuotationByPlan(Plan plan);
}
