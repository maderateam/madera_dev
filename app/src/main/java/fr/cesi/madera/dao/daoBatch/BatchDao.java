package fr.cesi.madera.dao.daoBatch;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;

import fr.cesi.madera.dao.EntityDao;
import fr.cesi.madera.entities.Batch;
import fr.cesi.madera.entities.Component;

/**
 * Created by Doremus on 18/04/2017.
 */

public class BatchDao extends EntityDao implements IBatchDao {
    public static final Class BATCH_CLASS = Batch.class;
    public static final String BATCH_CLASS_NAME = BATCH_CLASS.getSimpleName().toLowerCase();

    public BatchDao(Context context) {
        super(context);
    }

    @Override
    public ArrayList getAllEntities() {
        openDbIfClosed();
        ArrayList<Component> compoList;
        Cursor cursorM = mDb.rawQuery("SELECT * FROM " + BATCH_CLASS_NAME,null);
        compoList = dataUtils.cursorToEntities(BATCH_CLASS,cursorM);
        Log.i("madera","NB baych getAll entities :" + compoList.size());
        cursorM.close();
        return compoList;
    }
}
