package fr.cesi.madera.dao.daoGamme;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import fr.cesi.madera.dao.EntityDao;
import fr.cesi.madera.dao.IEntityDao;
import fr.cesi.madera.entities.Entity;
import fr.cesi.madera.entities.Gamme;

/**
 * Created by Joshua on 01/11/2016.
 */

public class GammeDao<T> extends EntityDao implements IEntityDao, IGammeDao{

    public static final Class GAMME_CLASS = Gamme.class;
    public static final String GAMME_CLASS_NAME = GAMME_CLASS.getSimpleName().toLowerCase();

    public GammeDao(Context context) {
        super(context);
    }

    @Override
    public ArrayList getAllEntities() {
        openDbIfClosed();
        ArrayList<Entity> allEntities ;
        Cursor cursorM = mDb.rawQuery("SELECT * FROM "+ GAMME_CLASS_NAME,null);
        allEntities = dataUtils.cursorToEntities(GAMME_CLASS,cursorM);
        cursorM.close();
        return allEntities;
    }
}
