package fr.cesi.madera.views.activities;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.math.Vector3;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.cesi.libgdx.Editor3D;
import fr.cesi.madera.R;
import fr.cesi.madera.editor.EditorApplication;
import fr.cesi.madera.editor.listeners.GDXGestureListener;
import fr.cesi.madera.editor.listeners.GDXInputAdapter;
import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.Project;
import fr.cesi.madera.services.ClientService;
import fr.cesi.madera.services.CoveringService;
import fr.cesi.madera.services.DiscountService;
import fr.cesi.madera.services.EditorAService;
import fr.cesi.madera.services.GammeService;
import fr.cesi.madera.services.PlanService;
import fr.cesi.madera.services.ProjectService;
import fr.cesi.madera.views.fragments.ChoixProjEditorDFragment;
import fr.cesi.madera.views.fragments.ModifInfoPlanDFragment;

/**
 * Created by Doremus on 05/11/2016.
 */
public class PlanActivity extends EditorApplication implements IDisplay {

    private Project project;
    private Plan plan;
    private Button btn_modif;
    private Button btn_edit;
    private Button btn_arch;
    private Button btn_suppr;
    private Button btn_estim;
    private Button btnAttr;
    private TextView lbl_name;
    private TextView lbl_date_crea;
    private TextView lbl_date_gen;
    private TextView lbl_desc;
    private LinearLayout layoutEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        plan = getIntent().getExtras().getParcelable("PLAN");
        project = getIntent().getExtras().getParcelable("PROJET");
        setContentView(R.layout.plan);
        setNavBarTitle();
        bindView();
        bindListener();
        refreshView();
        initializeEditor();
    }

    @Override
    public void initializeEditor() {
        // Création de l'éditeur graphique
        editor = new Editor3D();
        // Initialisation des services
        aService = new EditorAService(this, editor, plan);
        gammeService = new GammeService(this);
        planService = new PlanService(this);
        discService = new DiscountService(this);
        projectService = new ProjectService(this);
        clientService = new ClientService(this);
        coveringService = new CoveringService(this);
        // Initialisation des ecouteurs de click pour l'éditeur libgdx
        InputAdapter inputAdapter = new GDXInputAdapter(aService);
        editor.setAndroidInputAdapter(inputAdapter);
        GDXGestureListener gestureListener = new GDXGestureListener(aService);
        editor.setAndroidListener(gestureListener);
        // Initialisation de la vue Android
        bindView();
        bindListener();
        // Passage de l'éditeur en mode empty
        //state = EMPTY_STATE;
        // Lancement du panel de l'éditeur graphique
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        layoutEditor.addView(initializeForView(editor, config));
        state = LOCK_STATE;
        project = projectService.getProjectById(plan.getIdProject());
        aService.activeVisualization(true);
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                aService.loadPlan(plan);
            }
        });
        t.start();
    }

    @Override
    public void setNavBarTitle() {
        SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.menu_fragment);
        String comName = sharedpreferences.getString("comName", "");
        TextView lbl_com_name = (TextView) currentFragment.getView().findViewById(R.id.lbl_username_navbar);
        lbl_com_name.setText(comName);
        TextView title_navbar = (TextView) currentFragment.getView().findViewById(R.id.lbl_title_navbar);
        title_navbar.setText(R.string.lbl_title_plan_nav);
    }

    @Override
    public void bindView() {
        btn_modif = (Button) findViewById(R.id.btn_modif_plan_plan);
        btn_edit = (Button) findViewById(R.id.btn_edit_plan_plan);
        btn_suppr = (Button) findViewById(R.id.btn_suppr_plan_plan);
        btn_arch = (Button) findViewById(R.id.btn_arch_edit_plan);
        btn_estim = (Button) findViewById(R.id.btn_estimation);
        lbl_name = (TextView) findViewById(R.id.title_plan_plan);
        lbl_date_crea = (TextView) findViewById(R.id.date_crea_plan_plan);
        lbl_date_gen = (TextView) findViewById(R.id.date_gen_plan_plan);
        lbl_desc = (TextView) findViewById(R.id.desc_plan_plan);
        layoutEditor = (LinearLayout) findViewById(R.id.layout_editor);
        btnAttr = (Button) findViewById(R.id.btn_attr_plan_plan);
    }

    @Override
    public void refreshView() {
        lbl_name.setText(plan.getName());
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        if (plan.getDatecreated() != null && plan.getDatecreated().getTime() > 0) {
            lbl_date_crea.setText(formatter.format(plan.getDatecreated()));

        }
        if (plan.getDatevalidated() != null && plan.getDatevalidated().getTime() > 0) {
            lbl_date_gen.setText(formatter.format(plan.getDatevalidated()));
        }
        lbl_desc.setText(plan.getDescription());
        if (plan.isArchived()) {
            btn_edit.setVisibility(View.INVISIBLE);
            btn_estim.setVisibility(View.INVISIBLE);
            btn_arch.setVisibility(View.INVISIBLE);
        } else if (plan.getIdQuotation() > 0) {
            btn_edit.setVisibility(View.INVISIBLE);
            btn_suppr.setVisibility(View.INVISIBLE);
            btn_arch.setVisibility(View.VISIBLE);
            btnAttr.setVisibility(View.INVISIBLE);
        } else {
            btn_edit.setVisibility(View.VISIBLE);
            btn_estim.setVisibility(View.VISIBLE);
            btn_suppr.setVisibility(View.VISIBLE);
            btn_arch.setVisibility(View.VISIBLE);
            btnAttr.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void bindListener() {
        btn_modif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                ModifInfoPlanDFragment modifDialog = new ModifInfoPlanDFragment();
                modifDialog.setPlan(plan);
                modifDialog.show(fm, "");
            }
        });
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // On instancie l'IHM Projet
                Intent projet = new Intent(PlanActivity.this, EditorActivity.class);
                // On lui envoit le projet de la liste qui a été sélectionné
                projet.putExtra("PLAN", plan);
                startActivity(projet);
            }
        });
        btn_suppr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(PlanActivity.this);
                builder.setMessage(R.string.msg_confirm_plan_suppr)
                        .setPositiveButton(R.string.btn_supprimer, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                if (plan.getIdQuotation() <= 0) {
                                    PlanService planService = new PlanService(PlanActivity.this);
                                    Log.i("madera","Suppression du plan !");
                                    planService.deletePlan(plan);
                                    ProjectService projService = new ProjectService(PlanActivity.this);
                                    Project proj = projService.getProjectById(plan.getIdProject());
                                    Intent intent = new Intent(PlanActivity.this, ProjectActivity.class);
                                    intent.putExtra("PROJET", proj);
                                    startActivity(intent);
                                } else {
                                    CharSequence text = "Suppression d'un plan avec un devis impossible";
                                    int duration = Toast.LENGTH_LONG;
                                    Toast toast = Toast.makeText(PlanActivity.this, text, duration);
                                    toast.show();
                                }
                            }
                        })
                        .setNegativeButton(R.string.btn_annuler, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                // Create the AlertDialog object and return it
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        btn_arch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(PlanActivity.this);
                builder.setMessage(R.string.msg_confirm_plan_archiv)
                        .setPositiveButton(R.string.btn_valider, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                PlanService planService = new PlanService(PlanActivity.this);
                                planService.addPlanToArchive(plan);
                                CharSequence text = "Le plan a été ajouté aux archives";
                                int duration = Toast.LENGTH_LONG;
                                Toast toast = Toast.makeText(PlanActivity.this, text, duration);
                                toast.show();
                            }
                        })
                        .setNegativeButton(R.string.btn_annuler, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                // Create the AlertDialog object and return it
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        btn_estim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PlanActivity.this, EstimationActivity.class);
                intent.putExtra("PLAN", plan);
                startActivity(intent);
            }
        });
        btnAttr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                ChoixProjEditorDFragment dialog = new ChoixProjEditorDFragment();
                dialog.show(fm, "");
            }
        });
    }

    @Override
    public void displayWallCoveringOption() {
    }

    @Override
    public void displaySlabCoveringOption() {
    }

    @Override
    public void resetCoveringLayout() {
    }

    @Override
    public void displayDeleteConfirmation() {
    }

    @Override
    public void showFloors() {
    }

    @Override
    public boolean isCoveringIn() {
        return false;
    }

    @Override
    public boolean isCovringOut() {
        return false;
    }

    @Override
    public void setVisualisationModeON() {
    }

    @Override
    public View getArrowDown() {
        return null;
    }

    @Override
    public View getArrowUp() {
        return null;
    }

    @Override
    public View getArrowLeft() {
        return null;
    }

    @Override
    public View getArrowRight() {
        return null;
    }

    @Override
    public View getCancelCam() {
        return null;
    }

    @Override
    public void clearListView() {
    }

    @Override
    public void showDividers() {
    }

    @Override
    public void showWalls() {
    }

    @Override
    public void showCarpentries(Vector3 center) {
    }

    @Override
    public void showCoverings() {
    }

    @Override
    public void setEndingVisualisationModeON() {
    }

    @Override
    public void setProject(Project proj) {
        project = proj;
    }

    @Override
    public void createPlan(boolean b) {
        Plan newPlan = planService.clonePlan(plan);
        newPlan.setArchived(false);
        newPlan.setDatecreated(new Timestamp(new Date().getTime()));
        newPlan.setIdProject(project.get_id());
        FragmentManager fm = getFragmentManager();
        plan = newPlan;
        ModifInfoPlanDFragment modifDialog = new ModifInfoPlanDFragment();
        modifDialog.setPlan(newPlan);
        modifDialog.show(fm, "");
    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(PlanActivity.this,ProjectActivity.class);
        intent.putExtra("PROJET",project);
        startActivity(intent);
    }
}
