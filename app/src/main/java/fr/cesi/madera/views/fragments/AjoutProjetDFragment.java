package fr.cesi.madera.views.fragments;

import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.sql.Timestamp;
import java.util.Date;

import fr.cesi.madera.R;
import fr.cesi.madera.dao.daoProject.ProjectDao;
import fr.cesi.madera.entities.Client;
import fr.cesi.madera.entities.Project;
import fr.cesi.madera.views.activities.IDisplay;

/**
 * Gestion de la fenêtre modale d'ajout d'un projet
 */
public class AjoutProjetDFragment extends DialogFragment {

    private Client client;
    private EditText name;
    private EditText desc;
    private Button valider;
    private Button annuler;


    public AjoutProjetDFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.dialog_ajout_projet, container);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        // On désactive l'apparission du clavier dès l'ouverture de l'activity
        this.getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setCancelable(true);
        bindView();
        bindListener();
    }

    public void bindListener(){
        annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AjoutProjetDFragment.this.dismiss();
            }
        });
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!name.getText().toString().trim().equals("") && !desc.getText().toString().trim().equals("")) {
                    Project projet = new Project();
                    SharedPreferences sharedpreferences = getActivity().getSharedPreferences("settings", Context.MODE_PRIVATE);
                    long idCom = sharedpreferences.getLong("idCom",0);
                    projet.setIdcommercial(idCom);
                    projet.setName(name.getText().toString());
                    projet.setDescription(desc.getText().toString());
                    projet.setIdclient(client.get_id());
                    projet.setDatecreate(new Timestamp(new Date().getTime()));
                    projet.setClosed(false);
                    ProjectDao daoProj = new ProjectDao(getActivity());
                    daoProj.insertEntity(projet);
                    // Création du message de confirmation
                    CharSequence text = "Le projet a bien été créé !";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(getActivity(), text, duration);
                    toast.show();
                    ((IDisplay) getActivity()).refreshView();
                    AjoutProjetDFragment.this.dismiss();
                }else{
                    // Création du message de confirmation
                    CharSequence text = "L'un des champs est vide...";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(getActivity(), text, duration);
                    toast.show();
                }
            }
        });
    }

    private void bindView() {
        name = (EditText)getView().findViewById(R.id.txt_name_proj_add_proj);
        desc = (EditText)getView().findViewById(R.id.txt_desc_proj_add_proj);
        valider = (Button)getView().findViewById(R.id.btn_ajouter_add_proj);
        annuler = (Button)getView().findViewById(R.id.btn_annuler_add_proj);
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
