package fr.cesi.madera.views.fragments;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Pattern;

import fr.cesi.madera.R;
import fr.cesi.madera.dao.daoClient.ClientDao;
import fr.cesi.madera.entities.Client;
import fr.cesi.madera.views.activities.IDisplay;

/**
 * Gestion de l'affichage de la fenêtre modale de modification des informations de contact d'un client
 */
public class ModifContactClientDFragment extends DialogFragment {

    private Button btn_annuler;
    private Button btn_valider;
    private EditText tel;
    private EditText mail;
    private Client client;

    public ModifContactClientDFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.dialog_modif_contact_client, container);
        this.getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Holo_Light);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        bindView();
        if(client != null) {
            tel.setText(client.getTel());
            mail.setText(client.getMail());
        }
        btn_annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModifContactClientDFragment.this.dismiss();
            }
        });
        btn_valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean mailOK = android.util.Patterns.EMAIL_ADDRESS.matcher(mail.getText().toString()).matches();
                if(!mailOK){
                    mail.setTextColor(Color.RED);
                }else{
                    mail.setTextColor(Color.WHITE);
                }
                Pattern paternTel = Pattern.compile("^0[1-9]([-. ]?[0-9]{2}){4}");
                boolean telOK = paternTel.matcher(tel.getText()).matches();
                if(!telOK){
                    tel.setTextColor(Color.RED);
                }else{
                    tel.setTextColor(Color.WHITE);
                }
                if(telOK && mailOK) {
                    client.setTel(tel.getText().toString());
                    client.setMail(mail.getText().toString());
                    ClientDao daoCli = new ClientDao(getActivity());
                    daoCli.updateEntity(client);
                    // Création du message de confirmation
                    CharSequence text = "Le client a bien été mis à jour !";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(getActivity(), text, duration);
                    toast.show();
                    ((IDisplay) getActivity()).refreshView();
                    ModifContactClientDFragment.this.dismiss();
                }else{
                    // Création du message de confirmation
                    CharSequence text = "L'un des champs est vide...";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(getActivity(), text, duration);
                    toast.show();
                }
            }
        });
    }

    public void bindView(){
        btn_annuler = (Button) getView().findViewById(R.id.btn_annuler_modif_contact_dialog_cli);
        btn_valider = (Button) getView().findViewById(R.id.btn_valider_modif_address_dialog_cli);
        tel = (EditText) getView().findViewById(R.id.txt_modif_tel_dialog_cli);
        mail = (EditText) getView().findViewById(R.id.txt_modif_mail_dialog_cli);
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

}
