package fr.cesi.madera.views.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import fr.cesi.madera.R;
import fr.cesi.madera.dao.daoClient.ClientDao;
import fr.cesi.madera.dao.daoPlan.PlanDao;
import fr.cesi.madera.dao.daoQuotationLine.QuotationLineDao;
import fr.cesi.madera.entities.Client;
import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.Quotation;
import fr.cesi.madera.entities.QuotationLine;
import fr.cesi.madera.services.ParameterService;
import fr.cesi.madera.services.QuotationService;

/**
 * Created by El Maki on 10/02/2017.
 */

public class QuotationActivity extends Activity implements IDisplay {

    private TextView txt_plan_name;
    private TextView txt_society_name;
    private TextView txt_zip_society;
    private TextView txt_rue_society;
    private TextView txt_ville_society;
    private TextView txt_tel_society;
    private TextView txt_client_name;
    private TextView txt_zip_client;
    private TextView txt_rue_client;
    private TextView txt_ville_client;
    private TextView txt_tel_client;
    private TextView date_emission;
    private TextView date_last_edit;
    private TextView txt_final_ttc;
    private Button btn_retour_devis;
    private Button btn_suppr_devis;
    private Button btn_valider_devis;
    private TableLayout tbl_layout_devis;
    private String completName;
    private Quotation quotation;
    private Plan plan;
    private ArrayList<QuotationLine> quotationLines;

    private ArrayList<TableRow> tableRowList;
    private ArrayList<TextView> designationList;
    private ArrayList<TextView> uniteList  ;
    private ArrayList<TextView> quantiteList  ;
    private ArrayList<TextView> prix_unitaire_ht1List;
    private ArrayList<TextView> promotionList ;
    private ArrayList<TextView> prix_htList;
    private ArrayList<TextView> taxList ;
    private ArrayList<TextView> prix_ttcList ;



    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quotation);
        quotation = getIntent().getExtras().getParcelable("QUOTATION");
        plan = getIntent().getExtras().getParcelable("PLAN");
        PlanDao planDAO = new PlanDao(this);
        plan = (Plan) planDAO.getEntityById(plan.get_id(), Plan.class);
        this.quotationLines = new ArrayList<QuotationLine>();
        QuotationLineDao quotationLineDao = new QuotationLineDao(this);
        this.quotationLines = quotationLineDao.getQuotationLines(quotation.get_id());
        bindView();
        bindListener();
        refreshView();
        setNavBarTitle();
    }

    public void createRow(TableRow tableRow, TextView textView, String text, float weight)
    {
        Drawable cell_shape = getResources().getDrawable( R.drawable.cell_shape );
        textView.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT, weight));
        textView.setBackground(cell_shape);
        textView.setTextSize(24);
        textView.setTypeface(null, Typeface.BOLD);
        textView.setText(text);
        textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tableRow.addView(textView);
    }


    @Override
    public void setNavBarTitle() {
        SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.menu_fragment);
        String comName = sharedpreferences.getString("comName", "");
        TextView lbl_com_name = (TextView) currentFragment.getView().findViewById(R.id.lbl_username_navbar);
        lbl_com_name.setText(comName);
        TextView title_navbar = (TextView) currentFragment.getView().findViewById(R.id.lbl_title_navbar);
        title_navbar.setText(R.string.lbl_title_quota_nav);

    }

    @Override
    public void bindView() {

        btn_retour_devis = (Button) findViewById(R.id.btn_annuler_devis);
        txt_plan_name = (TextView) findViewById(R.id.nom_plan_quot);
        txt_society_name = (TextView) findViewById(R.id.nom_societe_quot);
        txt_zip_society = (TextView) findViewById(R.id.zip_societe_quot);
        txt_rue_society = (TextView) findViewById(R.id.rue_societe_quot);
        txt_ville_society = (TextView) findViewById(R.id.ville_societe_quot);
        txt_tel_society = (TextView) findViewById(R.id.tel_societe_quot);
        txt_client_name = (TextView) findViewById(R.id.nom_client_quot);
        txt_zip_client = (TextView) findViewById(R.id.zip_client_quot);
        txt_rue_client = (TextView) findViewById(R.id.rue_client_quot);
        txt_ville_client = (TextView) findViewById(R.id.ville_client_quot);
        txt_tel_client = (TextView) findViewById(R.id.tel_client_quot);
        date_emission = (TextView) findViewById(R.id.date_emission_quot);
        date_last_edit = (TextView) findViewById(R.id.date_last_edit_quot);
        txt_final_ttc = (TextView) findViewById(R.id.total_TTC_quota);
        tbl_layout_devis = (TableLayout) findViewById(R.id.table_devis_quot);
        btn_valider_devis = (Button) findViewById(R.id.btn_valider_devis);
        btn_suppr_devis = (Button) findViewById(R.id.btn_suppr_devis);
    }

    @Override
    public void refreshView() {

        Format date_formatter = new SimpleDateFormat("dd/MM/yyyy");
        if(tableRowList == null) {

            this.tableRowList = new ArrayList<TableRow>();
            this.designationList = new ArrayList<TextView>();
            this.uniteList = new ArrayList<TextView>();
            this.quantiteList = new ArrayList<TextView>();
            this.prix_unitaire_ht1List = new ArrayList<TextView>();
            this.promotionList = new ArrayList<TextView>();
            this.prix_htList = new ArrayList<TextView>();
            this.taxList = new ArrayList<TextView>();
            this.prix_ttcList = new ArrayList<TextView>();


            ClientDao clientDAO = new ClientDao(this);
            Client client = (Client) clientDAO.getEntityById(1, Client.class);
            completName = client.getFirstname() + " " + client.getLastname();

            ParameterService parameterService = new ParameterService(this);
            txt_society_name.setText((String) parameterService.getParameterValue("CompanyName"));
            txt_rue_society.setText((String) parameterService.getParameterValue("CompanyAdresse"));
            txt_ville_society.setText((String) parameterService.getParameterValue("CompanyZip"));
            txt_zip_society.setText((String) parameterService.getParameterValue("CompanyVille"));
            txt_tel_society.setText((String) parameterService.getParameterValue("CompanyTel"));

            txt_plan_name.setText(plan.getName());

            txt_client_name.setText(completName);
            txt_zip_client.setText(client.getZipcode());
            txt_rue_client.setText(client.getAdress());
            txt_ville_client.setText(client.getCity());
            txt_tel_client.setText(client.getTel());

            Date date = new Date();
            date.setTime(quotation.getDatecreated().getTime());
            date_emission.setText(date_formatter.format(date));

            for (int i = 0; i < quotationLines.size(); i++) {
                TableRow tableRow = new TableRow(this);
                tableRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT));
                tableRowList.add(tableRow);
                TextView designation = new TextView(this);
                designationList.add(designation);
                TextView unite = new TextView(this);
                uniteList.add(unite);
                TextView quantite = new TextView(this);
                quantiteList.add(quantite);
                TextView prix_unitaire_ht = new TextView(this);
                prix_unitaire_ht1List.add(prix_unitaire_ht);
                TextView promotion = new TextView(this);
                promotionList.add(promotion);
                TextView prix_ht = new TextView(this);
                prix_htList.add(prix_ht);
                TextView tax = new TextView(this);
                taxList.add(tax);
                TextView prix_ttc = new TextView(this);
                prix_ttcList.add(prix_ttc);

                createRow(tableRow, designation, quotationLines.get(i).getDesignation(), 2.0f);
                createRow(tableRow, unite, quotationLines.get(i).getUnitofmeasure(), 0.5f);
                createRow(tableRow, quantite, Float.toString(quotationLines.get(i).getQuantity()), 0.5f);
                createRow(tableRow, prix_unitaire_ht, Double.toString(quotationLines.get(i).getUnitprice()), 0.7f);
                createRow(tableRow, promotion, Double.toString(quotationLines.get(i).getDiscount()), 0.5f);
                createRow(tableRow, prix_ht, Double.toString(quotationLines.get(i).getPretaxamount()), 0.7f);
                createRow(tableRow, tax, Double.toString(quotationLines.get(i).getTax()), 0.5f);
                createRow(tableRow, prix_ttc, Double.toString(quotationLines.get(i).getAlltaxesincluded()), 0.7f);
                tbl_layout_devis.addView(tableRow);
            }

            txt_final_ttc.setText(" " + String.valueOf(new DecimalFormat("##.##").format(quotation.getPricefinal())) + " €");
        }

        if (quotation.getDatevalidated() != null) {
            Date date2 = new Date();
            date2.setTime(quotation.getDatevalidated().getTime());
            btn_valider_devis.setVisibility(View.INVISIBLE);
            btn_suppr_devis.setVisibility(View.INVISIBLE);
            date_last_edit.setText(date_formatter.format(date2));
        }

    }

    @Override
    public void bindListener() {
        btn_retour_devis.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuotationActivity.this, PlanActivity.class);
                intent.putExtra("PLAN", plan);
                startActivity(intent);
            }
        });
        btn_valider_devis.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                quotation.setDatevalidated(new Timestamp(new Date().getTime()));
                quotation.setValidated(true);
                QuotationService quotService = new QuotationService(QuotationActivity.this);
                quotService.updateEntity(quotation);
                refreshView();
                btn_suppr_devis.setVisibility(View.INVISIBLE);
                btn_valider_devis.setVisibility(View.INVISIBLE);
            }
        });
        btn_suppr_devis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(quotation.getDatevalidated() == null || quotation.getDatevalidated().getTime() <= 0){
                    QuotationService quotService =new QuotationService(QuotationActivity.this);
                    quotService.deleteQuotation(quotation);
                }else{
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(QuotationActivity.this);
                    builder1.setMessage("Impossible de supprimer ce devis car il a été validé par le client");
                    builder1.setCancelable(false);
                    builder1.setPositiveButton(
                            "Retour",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            }
        });

    }
}
