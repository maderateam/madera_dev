package fr.cesi.madera.views.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import fr.cesi.madera.R;
import fr.cesi.madera.dao.daoProject.ProjectDao;
import fr.cesi.madera.entities.Client;
import fr.cesi.madera.entities.Project;
import fr.cesi.madera.entities.adapters.ProjectAdapter;
import fr.cesi.madera.misc.DataBaseUtils;
import fr.cesi.madera.services.ClientService;
import fr.cesi.madera.views.fragments.AjoutProjetDFragment;
import fr.cesi.madera.views.fragments.ModifAddressClientDFragment;
import fr.cesi.madera.views.fragments.ModifContactClientDFragment;

/**
 * Gère l'affichage d'un client
 */
public class ClientActivity extends Activity implements IDisplay {

    private Button btn_new_projet;
    private Button btn_suppr_cli;
    private ListView projectList;
    private TextView nomUser;
    private TextView refUser;
    private TextView tel;
    private TextView mail;
    private TextView address;
    private TextView codePostal;
    private TextView ville;
    private Client client;
    private ArrayList<Project> list_projets = new ArrayList<Project>();
    private ProjectDao projectDao = new ProjectDao(this);
    private DataBaseUtils<Project> utils = new DataBaseUtils<Project>();
    private ClientService cliService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.client);
        bindView();
        // On désactive l'apparission du clavier dès l'ouverture de l'activity
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        // On récupère le client
        client = getIntent().getExtras().getParcelable("CLIENT");
        cliService = new ClientService(this);
        // On remplis les champs de l'IHM
        refreshView();
        bindListener();
        setNavBarTitle();
    }

    @Override
    public void bindListener() {
        btn_new_projet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                AjoutProjetDFragment modifDialog = new AjoutProjetDFragment();
                modifDialog.setClient(client);
                modifDialog.show(fm,"");
            }
        });
        projectList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent ficheProjet = new Intent(ClientActivity.this,ProjectActivity.class);
                ficheProjet.putExtra("PROJET",list_projets.get(position));
                startActivity(ficheProjet);
            }
        });
        btn_suppr_cli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(ClientActivity.this);
                builder.setMessage(R.string.msg_confirm_cli_delete)
                        .setPositiveButton(R.string.btn_valider, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                cliService.deleteCliAndRelatedProjects(client);
                                Intent clientIntent = new Intent(ClientActivity.this,ContactActivity.class);
                                startActivity(clientIntent);
                                finish();
                            }
                        })
                        .setNegativeButton(R.string.btn_annuler, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                // Create the AlertDialog object and return it
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    @Override
    public void refreshView() {
        // On récupère la liste des projets
        list_projets = utils.sortEntity(projectDao.getProjectsByClient(client));
        ProjectAdapter adapter = new ProjectAdapter(this,list_projets);
        projectList.setAdapter(adapter);
        // On remplis  l'IHM avec les infos client
        nomUser.setText(client.getFirstname() + " " + client.getLastname());
        //refUser.setText(client.getRef());
        tel.setText(client.getTel());
        mail.setText(client.getMail());
        address.setText(client.getAdress());
        codePostal.setText(client.getZipcode());
        ville.setText(client.getCity());
    }

    @Override
    public void setNavBarTitle() {
        SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.menu_fragment);
        String comName = sharedpreferences.getString("comName", "");
        TextView lbl_com_name = (TextView) currentFragment.getView().findViewById(R.id.lbl_username_navbar);
        lbl_com_name.setText(comName);
        TextView title_navbar = (TextView) currentFragment.getView().findViewById(R.id.lbl_title_navbar);
        title_navbar.setText(R.string.lbl_title_cli_nav);
    }

    @Override
    public void bindView() {
        btn_suppr_cli = (Button) findViewById(R.id.btn_suppr_cli);
        btn_new_projet = (Button) findViewById(R.id.btn_new_projet_cli);
        projectList = (ListView) findViewById(R.id.list_proj_cli);
        nomUser = (TextView) findViewById(R.id.lbl_name_cli);
        refUser = (TextView) findViewById(R.id.lbl_ref_cli);
        ville = (TextView) findViewById(R.id.lbl_ville_info_cli);
        tel = (TextView) findViewById(R.id.lbl_tel_info_cli);
        mail = (TextView) findViewById(R.id.lbl_mail_info_cli);
        address = (TextView) findViewById(R.id.lbl_addr_info_cli);
        codePostal = (TextView) findViewById(R.id.lbl_code_post_info_cli);
    }

    /**
     * Méthode déclaré dans le XML
     * Ouvre une boite de dialog permettant d'éditer les données correspondantes
     * @param v bouton pressé
     */
    public void openDialog(View v){
        FragmentManager fm = getFragmentManager();
        if(v.getId() == R.id.btn_sync_file){
            ModifContactClientDFragment modifDialog = new ModifContactClientDFragment();
            modifDialog.setClient(client);
            modifDialog.show(fm,"");
        }else if(v.getId() == R.id.btn_modif_addr_info_cli){
            ModifAddressClientDFragment modifDialog = new ModifAddressClientDFragment();
            modifDialog.setClient(client);
            modifDialog.show(fm,"");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshView();
    }
}
