package fr.cesi.madera.views.fragments;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Pattern;

import fr.cesi.madera.R;
import fr.cesi.madera.dao.daoClient.ClientDao;
import fr.cesi.madera.entities.Client;
import fr.cesi.madera.views.activities.IDisplay;

/**
 * Gestion de l'affichage de la fenêtre modale de modification de l'adresse d'un client
 */
public class ModifAddressClientDFragment extends DialogFragment {

    private Button btn_annuler;
    private Button btn_valider;
    private EditText address;
    private EditText codePostal;
    private  EditText ville;
    private Client client;

    public ModifAddressClientDFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.dialog_modif_address_client, container);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        // On désactive l'apparission du clavier dès l'ouverture de l'activity
        this.getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setCancelable(true);
        bindView();
        bindListener();
        if(client != null) {
            address.setText(client.getAdress());
            codePostal.setText(client.getZipcode());
            ville.setText(client.getCity());
        }

    }

    public void bindView(){
        btn_annuler = (Button) getView().findViewById(R.id.btn_annuler_modif_address_dialog_cli);
        btn_valider = (Button) getView().findViewById(R.id.btn_valider_modif_address_dialog_cli);
        address = (EditText) getView().findViewById(R.id.txt_modif_adress_dialog_cli);
        codePostal = (EditText) getView().findViewById(R.id.txt_modif_code_post_dialog_cli);
        ville = (EditText) getView().findViewById(R.id.txt_modif_ville_dialog_cli);
    }

    public void bindListener(){
        btn_annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModifAddressClientDFragment.this.dismiss();
            }
        });
        btn_valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pattern paternCodePost = Pattern.compile("^[0-9]{5}");
                boolean codePostOK = paternCodePost.matcher(codePostal.getText()).matches();
                if(!codePostOK){
                    codePostal.setTextColor(Color.RED);
                }else{
                    codePostal.setTextColor(Color.WHITE);
                }
                if(!ville.getText().toString().trim().equals("") && codePostOK
                        && !address.getText().toString().trim().equals("")) {
                    client.setCity(ville.getText().toString());
                    client.setZipcode(codePostal.getText().toString());
                    client.setAdress(address.getText().toString());
                    ClientDao daoCli = new ClientDao(getActivity());
                    daoCli.updateEntity(client);
                    // Création du message de confirmation
                    CharSequence text = "Le client a bien été mis à jour !";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(getActivity(), text, duration);
                    toast.show();
                    ((IDisplay) getActivity()).refreshView();
                    ModifAddressClientDFragment.this.dismiss();
                }else{
                    // Création du message de confirmation
                    CharSequence text = "L'un des champs est vide...";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(getActivity(), text, duration);
                    toast.show();
                }
            }
        });
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

}
