package fr.cesi.madera.views.activities;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import fr.cesi.madera.R;
import fr.cesi.madera.dao.daoClient.ClientDao;
import fr.cesi.madera.entities.Client;
import fr.cesi.madera.entities.adapters.ClientAdapter;
import fr.cesi.madera.misc.DataBaseUtils;

/**
 * Created by Doremus on 29/10/2016.
 */

public class ContactActivity extends Activity implements IDisplay {

    private ListView list_cli;
    private EditText name;
    private EditText lastname;
    private EditText tel;
    private EditText mail;
    private EditText address;
    private EditText code_postal;
    private EditText ville;
    private Button btn_ajout;
    private EditText txtSearch;
    private ArrayList<Client> clientList = new ArrayList<Client>();
    private ClientDao clientDao = new ClientDao(this);
    private ClientAdapter adapter;
    private DataBaseUtils<Client> utils = new DataBaseUtils<Client>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // On associe le layout XML à cette activity
        setContentView(R.layout.contact);
        // On désactive l'apparission du clavier dès l'ouverture de l'activity
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setNavBarTitle();
        // On bind chaque élément de la vue avec sa variable
        bindView();
        // On ajoute les listener sur les éléments intéractifs
        bindListener();
        // On récupère la liste des client
        refreshView();
    }


    @Override
    public void bindView() {
        list_cli = (ListView) findViewById(R.id.list_cli_ann);
        name = (EditText) findViewById(R.id.txt_name_ann);
        lastname = (EditText) findViewById(R.id.txt_lastname_ann);
        tel = (EditText) findViewById(R.id.txt_tel_ann);
        mail = (EditText) findViewById(R.id.txt_mail_ann);
        address = (EditText) findViewById(R.id.txt_addr_ann);
        code_postal = (EditText) findViewById(R.id.txt_code_post_ann);
        ville = (EditText) findViewById(R.id.txt_ville_ann);
        btn_ajout = (Button) findViewById(R.id.btn_add_cli_ann);
        txtSearch = (EditText) findViewById(R.id.txt_search_cli_ann);
    }

    @Override
    public void refreshView() {
        // On rempli la liste
        clientList = utils.sortEntity(clientDao.getAllEntities());
        // On créé l'adapter pour la liste des clients
        adapter = new ClientAdapter(ContactActivity.this,clientList);
        list_cli.setAdapter(adapter);

        if(clientList.size()>0){
            txtSearch.setHint("Votre recherche ...");
            txtSearch.setHintTextColor(Color.parseColor("#ffffff"));
            txtSearch.setEnabled(true);
        }else{
            txtSearch.setHint("Aucun client trouvé.");
            txtSearch.setHintTextColor(Color.parseColor("#ffffff"));
            txtSearch.setEnabled(false);
        }
        name.setText("");
        lastname.setText("");
        tel.setText("");
        mail.setText("");
        address.setText("");
        code_postal.setText("");
        ville.setText("");
        txtSearch.setText("");
    }

    @Override
    public void bindListener() {
        // On ajoute un écouteur sur la liste pour basculer vers l'activité client
        // dès que l'on clique sur un item de la liste
        list_cli.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent fiche_cli = new Intent(ContactActivity.this,ClientActivity.class);
                fiche_cli.putExtra("CLIENT",clientList.get(position));
                startActivity(fiche_cli);
            }
        });
        // On écoute le clic sur le bouton d'ajout d'un client
        btn_ajout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                boolean mailOK = android.util.Patterns.EMAIL_ADDRESS.matcher(mail.getText().toString()).matches();
//                if(!mailOK){
//                    mail.setTextColor(Color.RED);
//                }else{
//                    mail.setTextColor(Color.WHITE);
//                }
//                Pattern paternCodePost = Pattern.compile("^[0-9]{5}");
//                boolean codePostOK = paternCodePost.matcher(code_postal.getText()).matches();
//                if(!codePostOK){
//                    code_postal.setTextColor(Color.RED);
//                }else{
//                    code_postal.setTextColor(Color.WHITE);
//                }
//                Pattern paternTel = Pattern.compile("^0[1-9]([-. ]?[0-9]{2}){4}");
//                boolean telOK = paternTel.matcher(tel.getText()).matches();
//                if(!telOK){
//                    tel.setTextColor(Color.RED);
//                }else{
//                    tel.setTextColor(Color.WHITE);
//                }

                // Si les champ de nom, prenom tel et mail sont non vide on peut enregistrer le client
                if(!name.getText().toString().trim().equals("") && !lastname.getText().toString().trim().equals("")){
                    Client client = new Client();
                    client.setFirstname(name.getText().toString());
                    client.setLastname(lastname.getText().toString());
                    client.setMail(mail.getText().toString());
                    client.setTel(tel.getText().toString());
                    client.setCity(ville.getText().toString());
                    client.setAdress(address.getText().toString());
                    client.setZipcode(code_postal.getText().toString());
                    client.setHidden(false);
                    SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
                    long idCom = sharedpreferences.getLong("idCom",0);
                    client.setIdcommercial(idCom);
                    clientDao.insertEntity(client);
                    refreshView();
                    // Création du message de confirmation
                    CharSequence text = "Le client a bien été ajouté !";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(ContactActivity.this, text, duration);
                    toast.show();
                }else{
                    CharSequence text = "L'un des champs obligatoires n'a pas été rempli ...";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(ContactActivity.this, text, duration);
                    toast.show();
                }
            }
        });
        // On surveille la saisie de texte dans le champ de recherche
        txtSearch.addTextChangedListener(new TextWatcherOnSearchBar());
    }

    @Override
    public void setNavBarTitle() {
        SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.menu_fragment);
        String comName = sharedpreferences.getString("comName", "");
        TextView lbl_com_name = (TextView) currentFragment.getView().findViewById(R.id.lbl_username_navbar);
        lbl_com_name.setText(comName);
        TextView title_navbar = (TextView) currentFragment.getView().findViewById(R.id.lbl_title_navbar);
        title_navbar.setText(R.string.lbl_title_cont_nav);
    }


    /**
     * Listener sur l'EditText de recherche d'un client
     * Permet de rechercher un client en fonction du texte saisit dans l'éditText
     */
    private class TextWatcherOnSearchBar implements TextWatcher{

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // TODO voir projet Java début d'année pour la recherche caractère par caractère
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // TODO voir projet Java début d'année pour la recherche caractère par caractère
        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO voir projet Java début d'année pour la recherche caractère par caractère
        }
    }
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this,AccueilActivity.class);
        startActivity(intent);
    }
}
