package fr.cesi.madera.views.activities;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import fr.cesi.madera.R;
import fr.cesi.madera.dao.daoClient.ClientDao;
import fr.cesi.madera.dao.daoProject.ProjectDao;
import fr.cesi.madera.entities.Client;
import fr.cesi.madera.entities.Project;
import fr.cesi.madera.entities.adapters.ClientAdapter;
import fr.cesi.madera.entities.adapters.ProjectAdapter;

/**
 * Created by Doremus on 06/11/2016.
 */

public class RechercheActivity extends Activity implements IDisplay {

    private ListView list_cli;
    private ListView list_proj;
    private EditText txt_search;
    private ArrayList<Client> clients;
    private ArrayList<Project> projects;
    ClientDao clientDao = new ClientDao(RechercheActivity.this);
    ProjectDao projectDao = new ProjectDao(RechercheActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recherche);
        setNavBarTitle();
        bindView();
        bindListener();
        clients = clientDao.getAllEntities();
        projects = projectDao.getAllEntities();
        refreshView();
    }

    @Override
    public void setNavBarTitle() {
        SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.menu_fragment);
        String comName = sharedpreferences.getString("comName", "");
        TextView lbl_com_name = (TextView) currentFragment.getView().findViewById(R.id.lbl_username_navbar);
        lbl_com_name.setText(comName);
        TextView title_navbar = (TextView) currentFragment.getView().findViewById(R.id.lbl_title_navbar);
        title_navbar.setText(R.string.lbl_title_rech_nav);
    }

    @Override
    public void bindView() {
        list_cli = (ListView) findViewById(R.id.list_cli_rech) ;
        list_proj = (ListView) findViewById(R.id.list_proj_rech) ;
        txt_search = (EditText) findViewById(R.id.txt_rech_rech);
    }

    @Override
    public void refreshView() {
            ClientAdapter cliAdapter = new ClientAdapter(this, clients);
            list_cli.setAdapter(cliAdapter);
            ProjectAdapter projAdapter = new ProjectAdapter(this, projects);
            list_proj.setAdapter(projAdapter);
    }

    @Override
    public void bindListener() {
        txt_search.addTextChangedListener(new RechercheActivity.TextWatcherOnSearchBar());
        list_cli.setOnItemClickListener(new OnClientClickListener());
        list_proj.setOnItemClickListener(new OnProjClickListener());
    }

    /**
     * Listener sur l'EditText de recherche d'un client
     * Permet de rechercher un client en fonction du texte saisit dans l'éditText
     */
    private class TextWatcherOnSearchBar implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // TODO voir projet Java début d'année pour la recherche caractère par caractère
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            clients = clientDao.searchClientByName(s.toString().toLowerCase());
            projects = projectDao.searchProjectsByName(s.toString().toLowerCase());
            refreshView();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    private class OnClientClickListener implements android.widget.AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent planIntent = new Intent(RechercheActivity.this, ClientActivity.class);
            planIntent.putExtra("CLIENT", clients.get(position));
            startActivity(planIntent);
        }
    }

    private class OnProjClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent planIntent = new Intent(RechercheActivity.this, ProjectActivity.class);
            planIntent.putExtra("PROJET", projects.get(position));
            startActivity(planIntent);
        }
    }
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this,AccueilActivity.class);
        startActivity(intent);
    }
}
