package fr.cesi.madera.views.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import fr.cesi.madera.R;

/**
 * Gestion de l'affichage de la barre horizontal supérieure
 */

public class NavBarFragment extends Fragment {

    private ImageView menuIcon;
    private TextView lbl_username;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.navbar, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
                bindView();
        // On s'assure que le bind est bien été éffectué
        if(lbl_username != null){
            // TODO create method getUsername()
            // lbl_username.setText(getUsername());
        }
        menuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                MenuDialogFragment menuDialog = new MenuDialogFragment();
                menuDialog.show(fm,"");

            }
        });

    }

    /**
     * Permet de binder les éléments de la vue et les champ de l'activity
     */
    private void bindView() {
        menuIcon = (ImageView) getView().findViewById(R.id.ic_menu_navbar);
        lbl_username = (TextView) getView().findViewById(R.id.lbl_username_navbar);
    }
}
