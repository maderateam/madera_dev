package fr.cesi.madera.views.activities;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.cesi.madera.R;
import fr.cesi.madera.entities.Client;
import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.Project;
import fr.cesi.madera.entities.Quotation;
import fr.cesi.madera.services.ClientService;
import fr.cesi.madera.services.ProjectService;
import fr.cesi.madera.services.QuotationService;

/**
 * Gestion de l'affichage de l'estimation d'un plan
 */

public class EstimationActivity extends Activity implements IDisplay {

    private TextView txt_client_name;
    private TextView txt_plan_name;
    private TextView txt_rue_client;
    private TextView txt_zip_client;
    private TextView txt_ville_client;
    private TextView txt_tel_client;
    private TextView txt_etat_devis;
    private TextView txt_plan_name_info;
    private TextView txt_client_name_info;
    private TextView date_last_edit;
    private TextView prix_total_HT;
    private TextView prix_TVA;
    private TextView prix_total_TTC;
    private Button btn_voir_devis;
    private String completName;
    private Plan plan;
    private Quotation quotation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.estimation);
        plan = getIntent().getExtras().getParcelable("PLAN");
        bindView();
        bindListener();
        refreshView();
        setNavBarTitle();
    }

    @Override
    public void setNavBarTitle() {
        SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.menu_fragment);
        String comName = sharedpreferences.getString("comName", "");
        TextView lbl_com_name = (TextView) currentFragment.getView().findViewById(R.id.lbl_username_navbar);
        lbl_com_name.setText(comName);
        TextView title_navbar = (TextView) currentFragment.getView().findViewById(R.id.lbl_title_navbar);
        title_navbar.setText(R.string.lbl_title_esti_nav);
    }

    @Override
    public void bindView() {
        txt_client_name = (TextView) findViewById(R.id.nom_client_esti);
        txt_plan_name = (TextView) findViewById(R.id.nom_plan_esti);
        txt_rue_client = (TextView) findViewById(R.id.rue_client_esti);
        txt_zip_client = (TextView) findViewById(R.id.zip_client_esti);
        txt_ville_client = (TextView) findViewById(R.id.ville_client_esti);
        txt_tel_client = (TextView) findViewById(R.id.tel_client_esti);
        txt_etat_devis = (TextView) findViewById(R.id.etat_devis_esti);
        txt_client_name_info = (TextView) findViewById(R.id.nom_client_info);
        txt_plan_name_info = (TextView) findViewById(R.id.nom_plan_info);
        date_last_edit = (TextView) findViewById(R.id.date_crea_plan_esti);
        prix_total_HT = (TextView) findViewById(R.id.prix_total_HT_esti);
        prix_TVA = (TextView) findViewById(R.id.TVA_20_esti);
        prix_total_TTC = (TextView) findViewById(R.id.total_TTC_esti);
        btn_voir_devis = (Button) findViewById(R.id.btn_voir_devis_esti);
    }

    @Override
    public void refreshView() {
        ProjectService projService = new ProjectService(this);
        Project proj = projService.getProjectById(plan.getIdProject());
        ClientService cliService = new ClientService(this);
        Client client = cliService.getClientById(proj.getIdclient());
        QuotationService quotService = new QuotationService(this);
        // Peut être == null si aucun devis de généré pour ce plan
        quotation = quotService.getQuotationByPlanId(plan.get_id());

        completName = client.getFirstname() + " " + client.getLastname();
        txt_plan_name.setText(plan.getName());
        txt_client_name.setText(completName);
        txt_rue_client.setText(client.getAdress());
        txt_zip_client.setText(client.getZipcode());
        txt_ville_client.setText(client.getCity());
        txt_tel_client.setText(client.getTel());
//        txt_etat_devis.setText(quotation.isValidated()); false = non validé, true = validé
        txt_plan_name_info.setText(plan.getName());
        txt_client_name_info.setText(completName);
        Date date = new Date();
        date.setTime(plan.getDatecreated().getTime());
        Format date_formatter = new SimpleDateFormat("dd/MM/yyyy");
        date_last_edit.setText(date_formatter.format(date));
        double HT;
        double TTC;
        double TVA;
        if (quotation == null) {
            HT = quotService.getTempPriceHT(plan);
            TTC = quotService.getTempTTC(plan);
            TVA = quotService.getTempTVA(plan);
            btn_voir_devis.setText("Générer le devis");
            txt_etat_devis.setText("Pas de devis");
        } else {
            HT = quotation.getPricewithouttax();
            TVA = quotService.getTempTVA(plan);
            TTC = quotation.getPricefinal();
            btn_voir_devis.setText("Voir le devis");
            if(quotation.isValidated()){
                txt_etat_devis.setText("Devis validé");
            }else{
                txt_etat_devis.setText("Devis non validé");
            }
        }
        prix_total_HT.setText(String.valueOf(new DecimalFormat("##.##").format(HT)) + " €");
        prix_TVA.setText(String.valueOf(new DecimalFormat("##.##").format(TVA)) + " €");
        prix_total_TTC.setText(String.valueOf(new DecimalFormat("##.##").format(TTC)) + " €");
    }

    @Override
    public void bindListener() {
        btn_voir_devis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(quotation != null) {
                    Intent intent = new Intent(EstimationActivity.this, QuotationActivity.class);
                    intent.putExtra("PLAN", plan);
                    intent.putExtra("QUOTATION", quotation);
                    startActivity(intent);
                }else{
                    QuotationService quotationService = new QuotationService(EstimationActivity.this);
                    quotationService.generateQuotation(plan);
                    refreshView();
                }
            }
        });
    }
}
