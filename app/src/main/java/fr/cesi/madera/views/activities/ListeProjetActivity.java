package fr.cesi.madera.views.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import fr.cesi.madera.R;
import fr.cesi.madera.dao.daoProject.ProjectDao;
import fr.cesi.madera.entities.Client;
import fr.cesi.madera.entities.Project;
import fr.cesi.madera.entities.adapters.ProjectAdapter;
import fr.cesi.madera.misc.DataBaseUtils;
import fr.cesi.madera.views.fragments.ChoixCliContactDFragment;

/**
 * Gestion de l'affichage de la liste des projets
 */

public class ListeProjetActivity extends Activity implements IDisplay {

    private ListView listProjet;
    private TextView client_name;
    private Button btn_choix_cli;
    private Button btn_add_proj;
    private EditText txt_name;
    private EditText txt_desc;
    private EditText txt_search;
    private Client client;
    private ProjectDao projectDao = new ProjectDao(this);
    private ArrayList<Project> list_project = new ArrayList<>();
    private DataBaseUtils<Project> utils = new DataBaseUtils<Project>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.liste_projet);
        bindView();
        bindListener();
        setNavBarTitle();
        refreshView();
        // On désactive l'apparission du clavier dès l'ouverture de l'activity
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    public void setNavBarTitle() {
        SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.menu_fragment);
        String comName = sharedpreferences.getString("comName", "");
        TextView lbl_com_name = (TextView) currentFragment.getView().findViewById(R.id.lbl_username_navbar);
        lbl_com_name.setText(comName);
        TextView title_navbar = (TextView) currentFragment.getView().findViewById(R.id.lbl_title_navbar);
        title_navbar.setText(R.string.lbl_title_proj_nav);
    }

    @Override
    public void bindView() {
        listProjet = (ListView) findViewById(R.id.list_proj_listproj);
        btn_choix_cli = (Button) findViewById(R.id.btn_choix_cli_listproj);
        btn_add_proj = (Button) findViewById(R.id.btn_add_proj_listproj);
        txt_name = (EditText) findViewById(R.id.txt_name_proj_listproj);
        txt_desc = (EditText) findViewById(R.id.txt_desc_proj_listproj);
        txt_search = (EditText) findViewById(R.id.txt_search_proj_listproj);
        client_name = (TextView) findViewById(R.id.lbl_name_cli_list_proj);
    }

    @Override
    public void refreshView() {
        if(list_project.size()>0){
            txt_search.setHint("Votre recherche ...");
            txt_search.setHintTextColor(Color.parseColor("#ffffff"));
            txt_search.setEnabled(true);
        }else{
            txt_search.setHint("Aucun projet trouvé.");
            txt_search.setHintTextColor(Color.parseColor("#ffffff"));
            txt_search.setEnabled(false);
        }
        if(client == null){
            client_name.setText("Pas de client");
        }else{
            client_name.setText(client.getFirstname() + " " + client.getLastname());
        }
        list_project = utils.sortEntity(projectDao.getAllOpenedProjects());
        ProjectAdapter adapter = new ProjectAdapter(this,list_project);
        listProjet.setAdapter(adapter);
    }

    @Override
    public void bindListener() {
        // Au clic sur un projet de la liste
        listProjet.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                // On instancie l'IHM Projet
                Intent projet = new Intent(ListeProjetActivity.this, ProjectActivity.class);
                // On lui envoit le projet de la liste qui a été sélectionné
                projet.putExtra("PROJET",(Project)listProjet.getItemAtPosition(i));
                startActivity(projet);
            }
        });


        // Choix d'un cient dans une liste
        btn_choix_cli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                ChoixCliContactDFragment dialog = new ChoixCliContactDFragment();
                dialog.show(fm,"");
            }
        });
        // Ajout d'un projet
        btn_add_proj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Si les champ de nom, prenom tel et mail sont non vide on peut enregistrer le client
                if(!txt_name.getText().equals("") && client != null){
                    Project projet = new Project();
                    SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
                    long idCom = sharedpreferences.getLong("idCom",0);
                    projet.setIdcommercial(idCom);
                    projet.setIdclient(client.get_id());
                    projet.setName(txt_name.getText().toString());
                    projet.setHidden(false);
                    projet.setDescription(txt_desc.getText().toString());
                    projet.setDatecreate(new Timestamp(new Date().getTime()));
                    ProjectDao projectDao = new ProjectDao(ListeProjetActivity.this);
                    projectDao.insertEntity(projet);
                    refreshView();
                }else{
                    CharSequence text = "L'un des champs obligatoires n'a pas été rempli ...";
                    int duration = Toast.LENGTH_LONG;
                    Toast toast = Toast.makeText(ListeProjetActivity.this, text, duration);
                    toast.show();
                }
            }
        });
        // Recherche d'une projet dans la liste
        txt_search.addTextChangedListener(new ListeProjetActivity.TextWatcherOnSearchBar());
    }

    /**
     * Listener sur l'EditText de recherche d'un client
     * Permet de rechercher un client en fonction du texte saisit dans l'éditText
     */
    private class TextWatcherOnSearchBar implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // TODO voir projet Java début d'année pour la recherche caractère par caractère
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // TODO voir projet Java début d'année pour la recherche caractère par caractère
        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO voir projet Java début d'année pour la recherche caractère par caractère
        }
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
