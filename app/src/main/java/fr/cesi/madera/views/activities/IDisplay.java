package fr.cesi.madera.views.activities;

/**
 * Interface de définition des méthode d'affichage pour les activity
 */
public interface IDisplay {

    /**
     * Permet de définir le nom de la section dans la barre horizontale supérieure
     */
    public void setNavBarTitle();
    /**
     * Permet de lier les élément de la vue avec les widgets Java
     */
    public void bindView();

    /**
     * Provoque le refraichissement de certains éléments de la vue
     */
    public void refreshView();

    /**
     * Associe les listener sur les éléments intéractifs de la vue
     */
    public void bindListener();
}
