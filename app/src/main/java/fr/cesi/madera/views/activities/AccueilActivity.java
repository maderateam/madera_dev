package fr.cesi.madera.views.activities;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import fr.cesi.madera.R;
import fr.cesi.madera.dao.EntityDao;
import fr.cesi.madera.misc.Feeder;

/**
 * Gestion de l'affichage de la page d'accueil
 * Created by Porridge on 20/10/2016.
 */

public class AccueilActivity extends Activity implements IDisplay {
    //Variable en private
    private ImageView contacts;
    private ImageView create_plan;
    private ImageView recherche;
    private ImageView projet;
    private ImageView archive_plan;
    private ImageView settings;


    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accueil);
        bindView();
        bindListener();
        setNavBarTitle();
        //getFeeder();
    }

    @Override
    public void setNavBarTitle() {
        SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.menu_fragment);
        String comName = sharedpreferences.getString("comName", "");
        TextView lbl_com_name = (TextView) currentFragment.getView().findViewById(R.id.lbl_username_navbar);
        lbl_com_name.setText(comName);
        TextView lblTitleNavBar = (TextView) currentFragment.getView().findViewById(R.id.lbl_title_navbar);
        lblTitleNavBar.setText(R.string.lbl_title_acc_nav);

    }

    @Override
    public void bindView(){
        contacts = (ImageView) findViewById(R.id.img_contact_acc);
        create_plan = (ImageView) findViewById(R.id.img_plan_acc);
        recherche = (ImageView) findViewById(R.id.img_rech_acc);
        projet = (ImageView) findViewById(R.id.img_proj_acc);
        archive_plan = (ImageView) findViewById(R.id.img_arch_acc);
        settings = (ImageView) findViewById(R.id.img_param_acc);

    }

    @Override
    public void refreshView() {

    }
    @Override
    public void bindListener(){
        contacts.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(AccueilActivity.this, ContactActivity.class);
                startActivity(intent);
            }
        });
        create_plan.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(AccueilActivity.this,EditorActivity.class);
                startActivity(intent);
            }
        });
        recherche.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent recherche = new Intent(AccueilActivity.this,RechercheActivity.class);
                startActivity(recherche);
            }
        });
        projet.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
            Intent projet_liste = new Intent(AccueilActivity.this,ListeProjetActivity.class);
                startActivity(projet_liste);
            }
        });
        archive_plan.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(AccueilActivity.this, ArchivesActivity.class);
                startActivity(intent);
            }
        });
        settings.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(AccueilActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });
    }

    /**
     * Méthode permettant de remplir la base de données avec les data de tests
     */
    public void getFeeder() {
        SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        if (!sharedpreferences.getBoolean("feed", false)) {
            //EntityDao.setLockBatch(true);
            Feeder feed = new Feeder(AccueilActivity.this);
            feed.insertLibgdxTests();
            EntityDao.setLockBatch(false);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}





