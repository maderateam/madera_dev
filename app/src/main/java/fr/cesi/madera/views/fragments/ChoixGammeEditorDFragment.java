package fr.cesi.madera.views.fragments;

import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import fr.cesi.madera.R;
import fr.cesi.madera.dao.daoGamme.GammeDao;
import fr.cesi.madera.entities.Gamme;
import fr.cesi.madera.entities.adapters.GammeAdapter;
import fr.cesi.madera.views.activities.EditorActivity;

/**
 * Gestion de l'affichage de la fenêtre modale de selection de la gamme
 */
public class ChoixGammeEditorDFragment extends DialogFragment {

    private ListView listV_gamme;
    private ArrayList<Gamme> gammes;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.dialog_select_list_gamme, container);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        this.getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        bindView();
        refresfView();
        bindListener();
    }

    private void refresfView() {
        GammeDao gammeDao = new GammeDao(getActivity());
        gammeDao.openDbIfClosed();
        gammes = gammeDao.getAllEntities();
        gammes.add(0,null);
        GammeAdapter adapter = new GammeAdapter(getActivity(),gammes);
        listV_gamme.setAdapter(adapter);
    }

    private void bindListener() {
        listV_gamme.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Gamme gamme = (Gamme)listV_gamme.getItemAtPosition(i);
                ((EditorActivity)getActivity()).filterByGamme(gamme);
                dismiss();
            }
        });
    }

    private void bindView() {
        listV_gamme = (ListView) getView().findViewById(R.id.list_select_gamme_editor);
    }
}
