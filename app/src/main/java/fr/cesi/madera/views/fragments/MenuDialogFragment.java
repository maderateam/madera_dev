package fr.cesi.madera.views.fragments;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import fr.cesi.madera.R;
import fr.cesi.madera.views.activities.AccueilActivity;
import fr.cesi.madera.views.activities.ArchivesActivity;
import fr.cesi.madera.views.activities.ContactActivity;
import fr.cesi.madera.views.activities.EditorActivity;
import fr.cesi.madera.views.activities.ListeProjetActivity;
import fr.cesi.madera.views.activities.RechercheActivity;

/**
 * Gestion de l'affichage de la barre de menu verticale
 */
public class MenuDialogFragment extends DialogFragment {
    private ImageView img_accueil_menu;
    private ImageView img_annuaire_menu;
    private ImageView img_rech_menu;
    private ImageView img_editeur_menu;
    private ImageView img_project_menu;
    private ImageView img_arch_menu;
    private ImageView img_param_menu;
    private TextView lbl_accueil_menu;
    private TextView lbl_annuaire_menu;
    private TextView lbl_rech_menu;
    private TextView lbl_editeur_menu;
    private TextView lbl_projet_menu;
    private TextView lbl_arch_menu;
    private TextView lbl_param_menu;

    public MenuDialogFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.menu, container);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        this.getDialog().getWindow().setGravity(Gravity.LEFT | Gravity.TOP);
        this.getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        getDialog().setContentView(R.layout.menu);

        bindView();
    }

    /**
     * Permet de binder les éléments de la vue et les champ de l'activity
     */
    private void bindView() {
        MenuItemListener listener = new MenuItemListener();
        img_accueil_menu = (ImageView) getView().findViewById(R.id.img_accueil_menu);
        img_accueil_menu.setOnClickListener(listener);
        img_annuaire_menu = (ImageView) getView().findViewById(R.id.img_annuaire_menu);
        img_annuaire_menu.setOnClickListener(listener);
        img_rech_menu = (ImageView) getView().findViewById(R.id.img_rech_menu);
        img_rech_menu.setOnClickListener(listener);
        img_editeur_menu = (ImageView) getView().findViewById(R.id.img_editeur_menu);
        img_editeur_menu.setOnClickListener(listener);
        img_project_menu = (ImageView) getView().findViewById(R.id.img_project_menu);
        img_project_menu.setOnClickListener(listener);
        img_arch_menu = (ImageView) getView().findViewById(R.id.img_arch_menu);
        img_arch_menu.setOnClickListener(listener);
        img_param_menu = (ImageView) getView().findViewById(R.id.img_param_menu);
        img_param_menu.setOnClickListener(listener);
        lbl_accueil_menu = (TextView) getView().findViewById(R.id.lbl_accueil_menu);
        lbl_accueil_menu.setOnClickListener(listener);
        lbl_annuaire_menu = (TextView) getView().findViewById(R.id.lbl_annuaire_menu);
        lbl_annuaire_menu.setOnClickListener(listener);
        lbl_rech_menu = (TextView) getView().findViewById(R.id.lbl_rech_menu);
        lbl_rech_menu.setOnClickListener(listener);
        lbl_editeur_menu = (TextView) getView().findViewById(R.id.lbl_editeur_menu);
        lbl_editeur_menu.setOnClickListener(listener);
        lbl_projet_menu = (TextView) getView().findViewById(R.id.lbl_projet_menu);
        lbl_projet_menu.setOnClickListener(listener);
        lbl_arch_menu = (TextView) getView().findViewById(R.id.lbl_arch_menu);
        lbl_arch_menu.setOnClickListener(listener);
        lbl_param_menu = (TextView) getView().findViewById(R.id.lbl_param_menu);
        lbl_param_menu.setOnClickListener(listener);
    }

    private class MenuItemListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            Intent intent;
            switch(v.getId()){
                case R.id.img_accueil_menu:
                case R.id.lbl_accueil_menu:
                    intent = new Intent(getActivity(), AccueilActivity.class);
                    startActivity(intent);
                    break;
                case R.id.img_annuaire_menu:
                case R.id.lbl_annuaire_menu:
                    intent = new Intent(getActivity(), ContactActivity.class);
                    startActivity(intent);
                    break;
                case R.id.img_arch_menu:
                case R.id.lbl_arch_menu:
                    intent = new Intent(getActivity(), ArchivesActivity.class);
                    startActivity(intent);
                    break;
                case R.id.img_editeur_menu:
                case R.id.lbl_editeur_menu:
                    intent = new Intent(getActivity(), EditorActivity.class);
                    startActivity(intent);
                    break;
                case R.id.img_project_menu:
                case R.id.lbl_projet_menu:
                    intent = new Intent(getActivity(), ListeProjetActivity.class);
                    startActivity(intent);
                    break;
                case R.id.img_param_menu:
                case R.id.lbl_param_menu:
                    intent = new Intent(getActivity(), AccueilActivity.class);
                    startActivity(intent);
                    break;
                case R.id.img_rech_menu:
                case R.id.lbl_rech_menu:
                    intent = new Intent(getActivity(), RechercheActivity.class);
                    startActivity(intent);
                    break;
            }
        }
    }
}
