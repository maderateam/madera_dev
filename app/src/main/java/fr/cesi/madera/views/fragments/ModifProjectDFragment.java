package fr.cesi.madera.views.fragments;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.sql.Timestamp;

import fr.cesi.madera.R;
import fr.cesi.madera.dao.daoClient.ClientDao;
import fr.cesi.madera.dao.daoProject.ProjectDao;
import fr.cesi.madera.entities.Client;
import fr.cesi.madera.entities.Project;
import fr.cesi.madera.views.activities.ProjectActivity;

/**
 * Gestion de l'affichage de la fenêtre modale de modification des infos d'un projet
 */
public class ModifProjectDFragment extends DialogFragment {

    private Project project;
    private Project projectUpdated = new Project();
    private Client client;
    private EditText txt_name;
    private EditText txt_desc;
    private Button btn_client;
    private Button btn_valider;
    private Button btn_annuler;
    private TextView lbl_user;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.dialog_modif_projet, container);
        return view;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        this.getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        projectUpdated.set_id(project.get_id());
        //projectUpdated.setRef(project.getRef());
        projectUpdated.setLastsync(new Timestamp(project.getLastsync().getTime()));
        projectUpdated.setName(project.getName());
        projectUpdated.setDescription(project.getDescription());
        // TODO check if dateClosed is null
        projectUpdated.setDateclosed(new Timestamp(project.getDateclosed().getTime()));
        projectUpdated.setDatecreate(new Timestamp(project.getDatecreate().getTime()));
        projectUpdated.setClosed(project.isClosed());
        projectUpdated.setIdclient(project.get_id());
        bindView();
        bindListener();
        refresfView();
    }

    public void refresfView() {
        ClientDao clientDao = new ClientDao(getActivity());
        client = clientDao.getClientByProject(projectUpdated);
        txt_name.setHint(projectUpdated.getName());
        txt_desc.setHint(projectUpdated.getDescription());
        lbl_user.setText(client.getFirstname() + " " + client.getLastname());
    }

    private void bindListener() {
        btn_client.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                ChoixCliProjectDFragment modifDialog = new ChoixCliProjectDFragment();
                modifDialog.setProjet(projectUpdated);
                modifDialog.show(fm,"");

            }
        });
        btn_valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                project.setName(txt_name.getText().toString());
                project.setDescription(txt_desc.getText().toString());
                project.setIdclient(projectUpdated.getIdclient());
                ProjectDao daoProjet = new ProjectDao(getActivity());
                daoProjet.updateEntity(project);
                ((ProjectActivity)getActivity()).refreshView();
                dismiss();
            }
        });
        btn_annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void bindView() {
        txt_name = (EditText)getView().findViewById(R.id.txt_name_modif_project);
        txt_desc = (EditText) getView().findViewById(R.id.txt_desc_modif_proj);
        btn_client = (Button) getView().findViewById(R.id.btn_change_client);
        btn_valider = (Button) getView().findViewById(R.id.btn_valider_modif_proj);
        btn_annuler = (Button) getView().findViewById(R.id.btn_annuler_modif_proj);
        lbl_user = (TextView) getView().findViewById(R.id.lbl_name_client_proj);
    }

    public Project getProject() {
        return project;
    }
    public void setProject(Project project) {
        this.project = project;
    }

    public void setProjectUpdated(Project projectUpdated) {
        this.projectUpdated = projectUpdated;
    }
}
