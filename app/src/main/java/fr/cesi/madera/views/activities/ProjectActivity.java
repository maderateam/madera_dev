package fr.cesi.madera.views.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import fr.cesi.madera.R;
import fr.cesi.madera.dao.daoProject.ProjectDao;
import fr.cesi.madera.entities.Client;
import fr.cesi.madera.entities.Entity;
import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.Project;
import fr.cesi.madera.entities.adapters.PlanQuotationAdapter;
import fr.cesi.madera.services.ClientService;
import fr.cesi.madera.services.PlanService;
import fr.cesi.madera.services.QuotationService;
import fr.cesi.madera.views.fragments.ModifProjectDFragment;

/**
 * Created by Doremus on 01/11/2016.
 */

public class ProjectActivity extends Activity implements IDisplay {

    private TextView nomProjet;
    private TextView creationDate;
    private TextView closedDate;
    private TextView desc;
    private TextView clientName;
    private ImageView ic_statut;
    private TextView lbl_statut;
    private Button btnSuppr;
    private Button btnModif;
    private Button btnClot;
    private Button btnAjoutPlan;
    private Project projet;
    private Client client;
    private ListView plan_quot_list;
    private ClientService cliService;
    private PlanService planService;
    private QuotationService quotService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        projet = getIntent().getExtras().getParcelable("PROJET");
        setContentView(R.layout.projet);
        cliService = new ClientService(this);
        planService = new PlanService(this);
        quotService = new QuotationService(this);
        bindView();
        bindListener();
        if(projet != null){
            refreshView();
        }
        // On désactive l'apparission du clavier dès l'ouverture de l'activity
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setNavBarTitle();
    }


    @Override
    public void setNavBarTitle() {
        SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.menu_fragment);
        String comName = sharedpreferences.getString("comName", "");
        TextView lbl_com_name = (TextView) currentFragment.getView().findViewById(R.id.lbl_username_navbar);
        lbl_com_name.setText(comName);
        TextView title_navbar = (TextView) currentFragment.getView().findViewById(R.id.lbl_title_navbar);
        title_navbar.setText(R.string.lbl_title_proj);
    }

    @Override
    public void bindView() {
        plan_quot_list = (ListView) findViewById(R.id.list_plan_quot);
        nomProjet = (TextView) findViewById(R.id.lbl_name_proj);
        creationDate = (TextView) findViewById(R.id.lbl_created_date_proj);
        closedDate = (TextView) findViewById(R.id.lbl_closed_date_proj);
        desc = (TextView) findViewById(R.id.txt_desc_modif_proj);
        clientName = (TextView) findViewById(R.id.lbl_name_client_proj);
        btnSuppr = (Button) findViewById(R.id.btn_suppr_proj);
        btnModif = (Button) findViewById(R.id.btn_modif_proj);
        btnClot = (Button) findViewById(R.id.btn_clot_proj);
        lbl_statut = (TextView) findViewById(R.id.lbl_statut_proj);
        ic_statut = (ImageView) findViewById(R.id.img_status_proj);
        if(projet.isClosed()){
            lbl_statut.setText(R.string.lbl_statut_closed);
            ic_statut.setImageDrawable(getResources().getDrawable(R.drawable.ic_close_48dp));
        }else{
            lbl_statut.setText(R.string.lbl_statut_open);
            ic_statut.setImageDrawable(getResources().getDrawable(R.drawable.ic_open_48dp));
        }
        btnAjoutPlan = (Button) findViewById(R.id.btn_add_plan_proj);
    }

    @Override
    public void refreshView() {
        client = cliService.getClientByProject(projet);
        nomProjet.setText(projet.getName());
        //refProjet.setText(projet.getRef());
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        creationDate.setText(dateFormat.format(new Date(projet.getDatecreate().getTime())));
        if(!projet.isClosed()){
            closedDate.setText("");
            ic_statut.setImageDrawable(getResources().getDrawable(R.drawable.ic_open_48dp));
            lbl_statut.setText("Ouvert");
            btnClot.setText("CLOTURER");
        }else{
            closedDate.setText(dateFormat.format(projet.getDateclosed()));
            ic_statut.setImageDrawable(getResources().getDrawable(R.drawable.ic_close_48dp));
            lbl_statut.setText("Clôturé");
            btnClot.setText("RE-OUVRIR");
        }
        desc.setText(projet.getDescription());
        clientName.setText(client.getFirstname() + " " + client.getLastname());
        List<Plan> plans = planService.getPlansByProject(projet);
        ArrayList<ArrayList<Entity>> plans_quot = new ArrayList<ArrayList<Entity>>();
        for (Plan plan: plans) {
            plans_quot.add(new ArrayList<Entity>(Arrays.asList(plan,quotService.getQuotationByPlan(plan))));
        }
        final PlanQuotationAdapter planQuotAdapter = new PlanQuotationAdapter(this,plans_quot);
        plan_quot_list.setAdapter(planQuotAdapter);
        plan_quot_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent planIntent = new Intent(ProjectActivity.this,PlanActivity.class);
                planIntent.putExtra("PLAN",((ArrayList<Entity>)plan_quot_list.getItemAtPosition(i)).get(0));
                planIntent.putExtra("PROJET",projet);
                startActivity(planIntent);
            }
        });
    }

    @Override
    public void bindListener() {
        btnModif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                ModifProjectDFragment modifDialog = new ModifProjectDFragment();
                modifDialog.setProject(projet);
                modifDialog.show(fm,"ModifProjectDFragment");
            }
        });
        btnSuppr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(ProjectActivity.this);
                builder.setMessage(R.string.msg_confirm_proj_delete)
                        .setPositiveButton(R.string.btn_valider, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                ProjectDao projectDAO = new ProjectDao(ProjectActivity.this);
                                projectDAO.deleteEntity(projet);
                                finish();
                            }
                        })
                        .setNegativeButton(R.string.btn_annuler, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                // Create the AlertDialog object and return it
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        btnClot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(ProjectActivity.this);
                builder.setMessage(R.string.msg_confirm_proj_closure)
                        .setPositiveButton(R.string.btn_valider, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if(!projet.isClosed()) {
                                    projet.setClosed(true);
                                    projet.setDateclosed(new Timestamp(new Date().getTime()));
                                }else{
                                    projet.setClosed(false);
                                    projet.setDateclosed(null);
                                }
                                ProjectDao projetDAO = new ProjectDao(ProjectActivity.this);
                                projetDAO.updateEntity(projet);
                                ProjectActivity.this.refreshView();
                            }
                        })
                        .setNegativeButton(R.string.btn_annuler, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                // Create the AlertDialog object and return it
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        btnAjoutPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedpreferences = ProjectActivity.this.getSharedPreferences("settings", Context.MODE_PRIVATE);
                long idCom = sharedpreferences.getLong("idCom",0);
                Plan plan = new Plan();
                plan.setIdProject(projet.get_id());
                plan.setIdcommercial(idCom);
                plan.setDatecreated(new Timestamp(new Date().getTime()));
                plan.setName("Nouveau Plan");
                planService.insertPlan(plan);
                Intent intent = new Intent(ProjectActivity.this,PlanActivity.class);
                intent.putExtra("PLAN",plan);
                startActivity(intent);
            }
        });
    }
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this,AccueilActivity.class);
        startActivity(intent);
    }
}
