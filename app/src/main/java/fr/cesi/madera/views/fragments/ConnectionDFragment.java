package fr.cesi.madera.views.fragments;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import fr.cesi.madera.AndroidLauncher;
import fr.cesi.madera.R;
import fr.cesi.madera.entities.Commercial;
import fr.cesi.madera.misc.exceptions.SynchronizationException;
import fr.cesi.madera.services.CommercialService;

/**
 * Gestion de l'affichage de la fenêtre modale de conenction pour le commercial
 */

public class ConnectionDFragment extends DialogFragment {
    private OndialogDismissListener dismissListener;
    private CommercialService comService;
    private Button valider;
    private Button annuler;
    private EditText mail;
    private EditText password;
    private Commercial com;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.dialog_connection_com, container);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        this.getDialog().getWindow().setGravity(Gravity.CENTER);
        this.getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(false);
        bindView();
        bindListener();
    }

    private void bindListener() {
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("madera","Click Valider");
                if(mail.getText() == null ||  password.getText() == null
                        || mail.getText().equals("") || password.getText().equals("")){
                    CharSequence text = "Veuillez saisir votre mail ET votre mot de passe";
                    int duration = Toast.LENGTH_LONG;
                    Toast toast = Toast.makeText(getActivity(), text, duration);
                    toast.show();
                }else{
                    try {
                        comService = new CommercialService(getActivity());
                        Commercial com = comService.connectCommercial(mail.getText().toString(),password.getText().toString());
                        if(com != null){
                            ((AndroidLauncher)getActivity()).writeComToLocal(com);
                        }
                    } catch (SynchronizationException e) {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                        builder1.setMessage("Erreur lors de la connexion");
                        builder1.setCancelable(false);
                        builder1.setPositiveButton(
                                "Rééssayer",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        builder1.setNegativeButton(
                                "Annuler",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        getActivity().finish();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        e.printStackTrace();
                    }
                    dismiss();
                }
            }
        });
        annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                ((AndroidLauncher)getActivity()).finish();
            }
        });
    }

    @Override
    public void dismiss() {
        super.dismiss();
        dismissListener.onDismiss();
    }

    /**
     * Permet de binder les éléments de la vue et les champ de l'activity
     */
    private void bindView() {
        valider = (Button) getView().findViewById(R.id.btn_valider_co);
        annuler = (Button) getView().findViewById(R.id.btn_annuler_co);
        mail = (EditText) getView().findViewById(R.id.txt_mail_co);
        password = (EditText)getView().findViewById(R.id.txt_passwd_co);
    }

    public CommercialService getComService() {
        return comService;
    }

    public void setComService(CommercialService comService) {
        this.comService = comService;
    }

    public Commercial getCom() {
        return com;
    }

    public void setCom(Commercial com) {
        this.com = com;
    }

    public void setDismissListener(OndialogDismissListener dismissListener) {
        this.dismissListener = dismissListener;
    }
}
