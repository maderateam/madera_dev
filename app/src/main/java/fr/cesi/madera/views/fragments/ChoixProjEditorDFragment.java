package fr.cesi.madera.views.fragments;

import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import fr.cesi.madera.R;
import fr.cesi.madera.dao.daoProject.ProjectDao;
import fr.cesi.madera.editor.EditorApplication;
import fr.cesi.madera.entities.Project;
import fr.cesi.madera.entities.adapters.ProjectAdapter;
import fr.cesi.madera.views.activities.EditorActivity;

/**
 * Gestion de l'affichage de la fenêtre modale de selection d'un projet
 */
public class ChoixProjEditorDFragment extends DialogFragment {

    private ListView listV_proj;
    private ArrayList<Project> projects;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_select_list_proj, container);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        this.getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        bindView();
        refresfView();
        bindListener();
    }

    private void refresfView() {
        ProjectDao projectDao = new ProjectDao(getActivity());
        projects = projectDao.getAllEntities();
        ProjectAdapter adapter = new ProjectAdapter(getActivity(), projects);
        listV_proj.setAdapter(adapter);
    }

    private void bindListener() {
        listV_proj.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Project proj = (Project) listV_proj.getItemAtPosition(i);
                ((EditorApplication) getActivity()).setProject(proj);
                ((EditorApplication) getActivity()).createPlan(false);
                dismiss();
            }
        });

    }

    private void bindView() {
        listV_proj = (ListView) getView().findViewById(R.id.list_select_proj_editor);
    }

}
