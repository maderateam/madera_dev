package fr.cesi.madera.views.activities;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

import fr.cesi.madera.R;
import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.adapters.PlanAdapter;
import fr.cesi.madera.services.PlanService;

/**
 * Gère l'affichage de la liste des plans archivés
 */
public class ArchivesActivity extends Activity implements IDisplay {
    private ListView archive_list;
    private List<Plan> plan_list;
    private TextView txt_plan_name;
    private TextView txt_plan_date_crt;
    private TextView txt_plan_desc;
    private Format date_formatter = new SimpleDateFormat("dd/MM/yyyy");
    private Button btn_voir;

    private PlanService planService;

    private Plan activatedPlan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.archive);
        // Bind des différents éléments de la vue
        bindView();
        bindListener();
        // Remplissage de la liste
        planService = new PlanService(this);
        plan_list = planService.listOfArchivedPlans();
        // Bind de la liste à la ListView
        PlanAdapter adapter = new PlanAdapter(ArchivesActivity.this, plan_list);
        archive_list.setAdapter(adapter);
        // On simule un click sur le premier élément de la liste pour remplir
        // par défault les champs avec le premier item;
        if(plan_list.size()>0) {
            archive_list.performItemClick(archive_list, 0, 0);
        }else{
            finish();
            CharSequence text = "Aucune archive trouvée";
            int duration = Toast.LENGTH_LONG;
            Toast toast = Toast.makeText(this, text, duration);
            toast.show();
        }
        setNavBarTitle();
    }

    /**
     * Fonction permettant de binder chacun des éléments intéractif de la vue
     */
    public void bindView(){
        archive_list = (ListView) findViewById(R.id.archive_list);
        txt_plan_name = (TextView) findViewById(R.id.title_plan_arch);
        txt_plan_date_crt = (TextView) findViewById(R.id.date_crea_plan_arch);
        txt_plan_desc = (TextView) findViewById(R.id.desc_plan_arch);
        btn_voir = (Button) findViewById(R.id.btn_voir_plan_arch);
    }

    @Override
    public void setNavBarTitle() {
        SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.menu_fragment);
        String comName = sharedpreferences.getString("comName", "");
        TextView lbl_com_name = (TextView) currentFragment.getView().findViewById(R.id.lbl_username_navbar);
        lbl_com_name.setText(comName);
        TextView lblTitleNavBar = (TextView) currentFragment.getView().findViewById(R.id.lbl_title_navbar);
        lblTitleNavBar.setText(R.string.lbl_title_arch_nav);
    }

    @Override
    public void refreshView() {

    }

    @Override
    public void bindListener() {
        btn_voir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // On instancie l'IHM Projet
                Intent projet = new Intent(ArchivesActivity.this, PlanActivity.class);
                // On lui envoit le projet de la liste qui a été sélectionné
                projet.putExtra("PLAN",activatedPlan);
                startActivity(projet);
            }
        });
        // Ajout de l'écouteur sur la liste
        archive_list.setOnItemClickListener(new ArchiveOnClickListener());
    }

    /**
     * Ecouteur de la liste des plan archivés
     */
    private class ArchiveOnClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            activatedPlan= plan_list.get(position);
            txt_plan_name.setText(activatedPlan.getName());
            //txt_plan_ref.setText(plan_arch.getRef());
            txt_plan_date_crt.setText(date_formatter.format(activatedPlan.getDatecreated()));
            txt_plan_desc.setText(activatedPlan.getDescription());
        }
    }
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this,AccueilActivity.class);
        startActivity(intent);
    }
}
