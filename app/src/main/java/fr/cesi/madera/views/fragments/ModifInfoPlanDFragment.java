package fr.cesi.madera.views.fragments;

import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import fr.cesi.madera.R;
import fr.cesi.madera.dao.daoPlan.PlanDao;
import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.views.activities.IDisplay;

/**
 * Gestion de l'affichage de la fenêtre modale de modification des informations d'un plan
 */
public class ModifInfoPlanDFragment extends DialogFragment {

    private Plan plan;

    private EditText txt_name;
    private EditText txt_desc;
    private Button btn_valider;
    private Button btn_annuler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.dialog_modif_plan, container);
        return view;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        this.getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        bindView();
        bindListener();
        if(plan != null) {
            txt_name.setText(plan.getName());
            txt_desc.setText(plan.getDescription());
        }

    }

    private void bindView() {
        txt_name = (EditText) getView().findViewById(R.id.txt_edit_name_plan_arch);
        txt_desc = (EditText) getView().findViewById(R.id.txt_edit_desc_plan_arch);
        btn_valider = (Button) getView().findViewById(R.id.btn_val_modif_arch);
        btn_annuler = (Button) getView().findViewById(R.id.btn_ann_modif_arch);
    }

    private void bindListener(){
        btn_annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModifInfoPlanDFragment.this.dismiss();
            }
        });
        btn_valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plan.setName(txt_name.getText().toString());
                plan.setDescription(txt_desc.getText().toString());
                PlanDao planDao = new PlanDao(getActivity());
                planDao.updateEntity(plan);
                ((IDisplay)getActivity()).refreshView();
                ModifInfoPlanDFragment.this.dismiss();
            }
        });
    }

    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }
}
