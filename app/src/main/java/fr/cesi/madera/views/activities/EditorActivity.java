package fr.cesi.madera.views.activities;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.math.Vector3;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import fr.cesi.libgdx.Editor3D;
import fr.cesi.madera.R;
import fr.cesi.madera.editor.EditorApplication;
import fr.cesi.madera.editor.listeners.CameraOnClickListener;
import fr.cesi.madera.editor.listeners.ComponentOnItemClickListener;
import fr.cesi.madera.editor.listeners.GDXGestureListener;
import fr.cesi.madera.editor.listeners.GDXInputAdapter;
import fr.cesi.madera.editor.listeners.OnCancelListener;
import fr.cesi.madera.entities.Component;
import fr.cesi.madera.entities.Covering;
import fr.cesi.madera.entities.Gamme;
import fr.cesi.madera.entities.Plan;
import fr.cesi.madera.entities.Project;
import fr.cesi.madera.entities.adapters.ComponentAdapter;
import fr.cesi.madera.entities.adapters.CoveringAdapter;
import fr.cesi.madera.misc.exceptions.NoFindComponentException;
import fr.cesi.madera.misc.exceptions.NoPSlotFoundException;
import fr.cesi.madera.services.ClientService;
import fr.cesi.madera.services.CoveringService;
import fr.cesi.madera.services.DiscountService;
import fr.cesi.madera.services.EditorAService;
import fr.cesi.madera.services.GammeService;
import fr.cesi.madera.services.PlanService;
import fr.cesi.madera.services.ProjectService;
import fr.cesi.madera.views.fragments.ChoixGammeEditorDFragment;
import fr.cesi.madera.views.fragments.ChoixProjEditorDFragment;
import fr.cesi.madera.views.fragments.ModifInfoPlanEditorDFragment;

/**
 * Gestion de l'affichage de l'éditeur de plan
 */
public class EditorActivity extends EditorApplication {
    /**
     * Widget de gestion de la caméra
     */
    public ImageView arrowUp;
    public ImageView arrowDown;
    public ImageView arrowLeft;
    public ImageView arrowRight;
    public ImageView cancelCam;
    /**
     * Widgets du panel d'infos composant
     */
    public TextView txt_nom;
    public TextView txt_gamme;
    public TextView txt_prix;
    public TextView txt_reduction;
    public TextView txt_taille;
    public Switch sw_infos;
    public Switch sw_cov;
    public LinearLayout infoLayout;
    public LinearLayout infoPlanLayout;

    /**
     * Widget du panneau covering
     */
    public LinearLayout layoutCov;
    public Switch sw_out;
    public Switch sw_in;
    /**
     * Widget du panneau d'info plan
     */
    private TextView txtNomPlan;
    private TextView txtNomProj;
    private TextView txtNomCli;
    private TextView txtDateCrea;
    /**
     * Widgets de l'IHM
     */
    private FrameLayout editorPanel;
    private LinearLayout menuHorz;
    private FrameLayout menuVert;
    private ListView listView;
    private ImageView cancel;
    private Button btnSave;
    private Button btnGamme;
    private Button btnVisu;
    private Button btnEndVisu;

    private boolean fromPlanActivity = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editeur);
        setNavBarTitle();
        /// Récupération du plan si il existe
        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            plan = extra.getParcelable("PLAN");
            fromPlanActivity = true;
        }
        initializeEditor();
    }
    @Override
    public void initializeEditor(){
        // Création de l'éditeur graphique
        editor = new Editor3D();
        // Initialisation des services
        aService = new EditorAService(this, editor, plan);
        gammeService = new GammeService(this);
        planService = new PlanService(this);
        discService = new DiscountService(this);
        projectService = new ProjectService(this);
        clientService = new ClientService(this);
        coveringService = new CoveringService(this);
        // Initialisation des ecouteurs de click pour l'éditeur libgdx
        InputAdapter inputAdapter = new GDXInputAdapter(aService);
        editor.setAndroidInputAdapter(inputAdapter);
        GDXGestureListener gestureListener = new GDXGestureListener(aService);
        editor.setAndroidListener(gestureListener);
        // Initialisation de l'écouteur caméra
        cameraListener = new CameraOnClickListener(this, editor);
        // Initialisation de la vue Android
        bindView();
        bindListener();
        // Passage de l'éditeur en mode empty
        state = EMPTY_STATE;
        // Lancement du panel de l'éditeur graphique
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        editorPanel.addView(initializeForView(editor, config));
        // Si le plan est vide on affiche la liste des sols
        if (plan == null || planService.isPlanEmpty(plan)) {
            state = FLOOR_STATE;
            showFloors();
        } else {
            state = SELECTION_STATE;
            project = projectService.getProjectById(plan.getIdProject());
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    aService.loadPlan(plan);
                }
            });
            t.start();
            aService.displayPlanPartsInLog();
        }
        // Récupération de la liste des gammes
        gammes = gammeService.getAllGammes();
        // Masquage des layout d'infos et de covering
        infoLayout.setVisibility(View.INVISIBLE);
        infoPlanLayout.setVisibility(View.INVISIBLE);
        btnEndVisu.setVisibility(View.INVISIBLE);
        layoutCov.setVisibility(View.INVISIBLE);
        // Inscritpion des infos du plan si il y en a un de chargé
        showPlanInfos(plan);
        aService.activeVisualization(false);
    }

    public void setNavBarTitle() {
        SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.menu_fragment);
        String comName = sharedpreferences.getString("comName", "");
        TextView lbl_com_name = (TextView) currentFragment.getView().findViewById(R.id.lbl_username_navbar);
        lbl_com_name.setText(comName);
        TextView lblTitleNavBar = (TextView) currentFragment.getView().findViewById(R.id.lbl_title_navbar);
        lblTitleNavBar.setText(R.string.lbl_title_editor_nav);
    }

    public void showFloors() {
        floors = aService.getAllFloors();
        displayComponentList(floors, listView);
    }

    @Override
    public boolean isCoveringIn() {
        return sw_in.isChecked();
    }

    @Override
    public boolean isCovringOut() {
        return sw_out.isChecked();
    }

    public void showDividers() {
        dividers = filterCompoInListByGamme(aService.getAllDividers(), selectedGamme);
        for (Component divider : dividers) {
            editor.loadComponentModel(aService.getModelPath(divider));
        }
        displayComponentList(dividers, listView);
    }

    public void showWalls() {
        walls = filterCompoInListByGamme(aService.getAllWalls(), selectedGamme);
        displayComponentList(walls, listView);
    }

    public void showCarpentries(Vector3 centerOFSlot) {
        state = CARPENTRIES_STATE;
        try {
            carpentries = aService.getCarpentriesBySlot(aService.getGdxBridge().getPSlotFromCenter(centerOFSlot, aService.wallSlots));
            carpentries = filterCompoInListByGamme(carpentries, selectedGamme);
            //filterCompoInListByGamme(aService.getAllWalls(), selectedGamme);
            for (Component carpentry : carpentries) {
                editor.loadComponentModel(aService.getModelPath(carpentry));
            }
            displayComponentList(carpentries, listView);
        } catch (NoPSlotFoundException e) {
            e.printStackTrace();
        } catch (NoFindComponentException e) {
            e.printStackTrace();
        }
    }

    public void showCoverings() {
        coverings = coveringService.getAllCoverings();
        displayCoveringList(coverings, listView);
    }

    private void displayCoveringList(ArrayList<Covering> coverings, ListView listV) {
        CoveringAdapter adapter = new CoveringAdapter(this, coverings);
        listV.setAdapter(adapter);
        listV.invalidate();
    }

    public void displayComponentList(ArrayList<Component> list, ListView listV) {
        ComponentAdapter adapter = new ComponentAdapter(this, list);
        listV.setAdapter(adapter);
        listV.invalidate();
    }

    private void showPlanInfos(Plan plan) {
        if (plan != null) {
            txtNomPlan.setText(plan.getName());
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            txtDateCrea.setText(format.format(plan.getDatecreated()));
            if (project != null) {
                txtNomProj.setText(project.getName());
                txtNomCli.setText(clientService.getClientNameById(project.getIdclient()));
            } else {
                txtNomProj.setText("Archive");
            }
        } else {
            txtNomPlan.setText("Non défini");
            txtNomProj.setText("Non associé");
            txtNomCli.setText("Non associé");
        }
    }

    public void bindView() {
        cancel = (ImageView) findViewById(R.id.img_cancel_editor);
        editorPanel = (FrameLayout) findViewById(R.id.editor_layout);
        listView = (ListView) findViewById(R.id.listView_editor);
        btnSave = (Button) findViewById(R.id.btn_editor_save);
        btnVisu = (Button) findViewById(R.id.btn_editor_visu);
        btnGamme = (Button) findViewById(R.id.btn_editor_gamme);
        txt_nom = (TextView) findViewById(R.id.txt_nom_editor_info);
        txt_gamme = (TextView) findViewById(R.id.txt_gamme_editor_info);
        txt_prix = (TextView) findViewById(R.id.txt_prix_editor_info);
        txt_reduction = (TextView) findViewById(R.id.txt_red_editor_info);
        txt_taille = (TextView) findViewById(R.id.txt_taille_editor_info);
        sw_infos = (Switch) findViewById(R.id.sw_infos);
        sw_cov = (Switch) findViewById(R.id.sw_covering);
        infoLayout = (LinearLayout) findViewById(R.id.editor_info_layout);
        txtNomPlan = (TextView) findViewById(R.id.txt_nom_plan_editor);
        txtNomProj = (TextView) findViewById(R.id.txt_proj_plan_editor);
        txtNomCli = (TextView) findViewById(R.id.txt_client_plan_proj);
        txtDateCrea = (TextView) findViewById(R.id.txt_date_plan_editor);
        infoPlanLayout = (LinearLayout) findViewById(R.id.layout_info_plan_editor);
        menuHorz = (LinearLayout) findViewById(R.id.menu_hor_editor);
        menuVert = (FrameLayout) findViewById(R.id.menu_vert_editor);
        btnEndVisu = (Button) findViewById(R.id.btn_end_visu);
        arrowUp = (ImageView) findViewById(R.id.img_arrow_up_editor);
        arrowDown = (ImageView) findViewById(R.id.img_arrow_down_editor);
        arrowLeft = (ImageView) findViewById(R.id.img_arrow_left_editor);
        arrowRight = (ImageView) findViewById(R.id.img_arrow_right_editor);
        cancelCam = (ImageView) findViewById(R.id.img_cancel_cam_editor);
        layoutCov = (LinearLayout) findViewById(R.id.layout_covering_editor);
        sw_out = (Switch) findViewById(R.id.sw_covering_out);
        sw_in = (Switch) findViewById(R.id.sw_covering_in);
    }

    public void bindListener() {
        listView.setOnItemClickListener(new ComponentOnItemClickListener(this, aService));
        cancel.setOnClickListener(new OnCancelListener(this, aService));
        cancel.setOnLongClickListener(new OnCancelListener(this, aService));
        btnSave.setOnClickListener(new OnSavePlanListener());
        btnVisu.setOnClickListener(new OnButtonVisuClicked());
        btnEndVisu.setOnClickListener(new OnButtonEndVisuClicked());
        btnGamme.setOnClickListener(new OnGammeButtonClicked());
        sw_infos.setOnCheckedChangeListener(new OnSwitchInfoClicked());
        sw_cov.setOnCheckedChangeListener(new OnSwitchCoveringClicked());
        arrowDown.setOnTouchListener(cameraListener);
        arrowLeft.setOnTouchListener(cameraListener);
        arrowRight.setOnTouchListener(cameraListener);
        arrowUp.setOnTouchListener(cameraListener);
        cancelCam.setOnTouchListener(cameraListener);
        sw_out.setOnCheckedChangeListener(new OnSwitchOutClicked());
        sw_in.setOnCheckedChangeListener(new OnSwitchInClicked());
    }

    public void clearListView() {
        Log.i("madera","Clear ListView");
        listView.setAdapter(null);
    }

    /**
     * Lance la fenetre de sélection d'un projet
     */
    private void associateProject() {
        FragmentManager fm = getFragmentManager();
        ChoixProjEditorDFragment dialog = new ChoixProjEditorDFragment();
        dialog.show(fm, "");
    }

    /**
     * Insère le plan dans la base
     * @param plan
     */
    public void insertPlan(Plan plan) {
        planService.insertPlan(plan);

    }

    /**
     * Filtre la ist de composant affichée par gamme
     * @param gamme
     */
    public void filterByGamme(Gamme gamme) {
        Adapter adapter = listView.getAdapter();
        selectedGamme = gamme;
        if (adapter != null) {
            switch (state) {
                case CARPENTRIES_STATE:
                    if (aService.selectedPSlot != null) {
                        try {
                            carpentries = aService.getCarpentriesBySlot(aService.selectedPSlot);
                            carpentries = filterCompoInListByGamme(carpentries, selectedGamme);
                            for (Component carpentry : carpentries) {
                                editor.loadComponentModel(aService.getModelPath(carpentry));
                            }
                            displayComponentList(carpentries, listView);
                        } catch (NoFindComponentException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case WALL_STATE:
                    walls = aService.getAllWalls();
                    walls = filterCompoInListByGamme(walls, selectedGamme);
                    for (Component carpentry : walls) {
                        editor.loadComponentModel(aService.getModelPath(carpentry));
                    }
                    displayComponentList(walls, listView);
                    break;
                case DIVIDER_STATE:
                    dividers = aService.getAllDividers();
                    dividers = filterCompoInListByGamme(dividers, selectedGamme);
                    for (Component carpentry : dividers) {
                        editor.loadComponentModel(aService.getModelPath(carpentry));
                    }
                    displayComponentList(dividers, listView);
                    break;
            }
        }
        if(gamme != null) {
            int lengthOfGammeName = gamme.getDenomination().length() >= 11 ? 10 : gamme.getDenomination().length();
            btnGamme.setText(btnGamme.getText() + " : " + gamme.getDenomination().substring(0, lengthOfGammeName));
        }else{
            btnGamme.setText(R.string.txt_gamme);
        }
    }


    public ArrayList<Component> filterCompoInListByGamme(ArrayList<Component> unfilteredList, Gamme gamme) {
        if (gamme != null) {
            for (Iterator<Component> iter = unfilteredList.iterator(); iter.hasNext(); ) {
                Component compo = iter.next();
                if (compo.getIdgamme() != 0 && compo.getIdgamme() != gamme.get_id()) {
                    iter.remove();
                }
            }
        }
        return unfilteredList;
    }

    /**
     * Sauvegarde le plan en cours si il n'éxistait pas en BD
     * - Propose si l'utilisateur veut l'archiver ou l'associer à un projet.
     * - Créé le nouveau plan et le sauvegarde
     */
    private void chooseSaveOption() {
        // On demande si le commercial veut associer ce plan à un projet
        final AlertDialog.Builder builder = new AlertDialog.Builder(EditorActivity.this);
        builder.setMessage(R.string.msg_confirm_assoc_proj_editor)
                .setPositiveButton(R.string.btn_save, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        if (plan == null) {
                            EditorActivity.this.associateProject();
                        } else {
                            if (plan.isArchived()) {
                                EditorActivity.this.associateProject();
                            } else {
                                aService.removeExistingPlanParts(plan);
                                EditorActivity.this.savePlanComponents(plan);
                            }
                        }
                    }
                })
                .setNegativeButton(R.string.btn_archiver, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        // On l'archive
                        project = null;
                        EditorActivity.this.createPlan(true);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void savePlanComponents(Plan plan) {
        aService.savePlanComponents(plan);
    }

    public void createPlan(boolean archive) {
        SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        long idCom = sharedpreferences.getLong("idCom", 0);
        Plan plan = new Plan();
        plan.setIdcommercial(idCom);
        plan.setArchived(archive);
        plan.setDatecreated(new Timestamp(new Date().getTime()));
        long idProject = project != null ? project.get_id() : 0;
        plan.setIdProject(idProject);
        // Edition des infos du plan
        // Le plan sera insérer lors de la validation des infos du plan
        FragmentManager fm = getFragmentManager();
        ModifInfoPlanEditorDFragment modifDialog = new ModifInfoPlanEditorDFragment();
        modifDialog.setPlan(plan);
        modifDialog.show(fm, "");
    }

    public void showComponentInfos(Component compo) {
        txt_nom.setText(compo.getDenomination());
        txt_gamme.setText(gammeService.getNameFromGammeID(compo.getIdgamme()));
        txt_prix.setText(compo.getPrice() + " €");
        float discount = discService.getDiscountByCompoOrGammeId(compo.get_id(), compo.getIdgamme());
        txt_reduction.setText((discount * 100) + " %");
        txt_taille.setText(compo.getHeight() + " l x " + compo.getWidth() + " L");
    }

    public void displayDeleteConfirmation(){
        // Demande de confirmation pour la suppression de tous les éléments du plan
        final AlertDialog.Builder builder = new AlertDialog.Builder(EditorActivity.this);
        builder.setMessage(R.string.msg_confirm_suppr_plan_editor)
                .setPositiveButton(R.string.btn_supprimer, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        aService.deleteAllCompoOnPlan();
                    }
                })
                .setNegativeButton(R.string.btn_annuler, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void displayWallCoveringOption() {
        sw_out.setChecked(true);
        sw_in.setChecked(true);
        sw_out.setVisibility(View.VISIBLE);
        sw_in.setVisibility(View.VISIBLE);
        layoutCov.setVisibility(View.VISIBLE);
    }

    public void displaySlabCoveringOption() {
        sw_out.setVisibility(View.INVISIBLE);
        sw_in.setVisibility(View.INVISIBLE);
        layoutCov.setVisibility(View.INVISIBLE);
    }

    @Override
    public void resetCoveringLayout() {
        layoutCov.setVisibility(View.INVISIBLE);
    }


    public void setProject(Project project) {
        this.project = project;
    }

    public Plan getPlan() {
        return plan;
    }


    /**
     * Ecouteur sur le bouton de sauvegarde du plan
     */
    class OnSavePlanListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            // Si le plan n'est pas vide
            if(!aService.planIsEmpty()) {
                // Lancement de la méthode de choix entre la sauvegarde ou l'archivage du plan
                chooseSaveOption();
            }else{
                CharSequence text = "Impossible de sauvegarder un plan vide..";
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(EditorActivity.this, text, duration);
                toast.show();
            }
        }
    }

    /**
     * Ecouteur sur le bouton de sélection de la gamme
     */
    private class OnGammeButtonClicked implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            FragmentManager fm = getFragmentManager();
            ChoixGammeEditorDFragment dialog = new ChoixGammeEditorDFragment();
            dialog.show(fm, "");
        }
    }

    /**
     * Ecouteur de changement d'état du switch pour le mode d'affichage des infos des composants
     */
    private class OnSwitchInfoClicked implements CompoundButton.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            // On passe l'éditeur en mode infos si le switch est en mode ON
            if (isChecked) {
                infoLayout.setVisibility(View.VISIBLE);
                infoPlanLayout.setVisibility(View.VISIBLE);
            } else {
                infoLayout.setVisibility(View.INVISIBLE);
                infoPlanLayout.setVisibility(View.INVISIBLE);
            }
            modeInfoON = isChecked;
        }
    }

    /**
     * Ecouteur de changement d'état du switch pour le mode d'affichage des infos des composants
     */
    private class OnSwitchCoveringClicked implements CompoundButton.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            // On passe l'éditeur en mode infos si le switch est en mode ON
            if (isChecked) {
                if (state == EMPTY_STATE || state == FLOOR_STATE) {
                    CharSequence text = "Sélectionnez d'abord un sol";
                    int duration = Toast.LENGTH_LONG;
                    Toast toast = Toast.makeText(EditorActivity.this, text, duration);
                    toast.show();
                } else {
                    state = COVERING_STATE;
                    if (editor.getSelectedModel() != null) {
                        editor.unSelectModel();
                    }
                    clearListView();
                    //showCoverings();
                }
            } else {
                sw_out.setVisibility(View.INVISIBLE);
                sw_in.setVisibility(View.INVISIBLE);
                layoutCov.setVisibility(View.INVISIBLE);
                if (editor.getSelectedModel() != null) {
                    editor.unSelectModel();
                }
                clearListView();
                if(aService.planIsEmpty()) {
                    state = EMPTY_STATE;
                    showFloors();
                }else{
                    state = SELECTION_STATE;
                }
            }

        }
    }

    private class OnButtonVisuClicked implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            setVisualisationModeON();
        }
    }

    private class OnButtonEndVisuClicked implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            setVisualisationModeOFF();
        }
    }

    public void setVisualisationModeON(){
        menuHorz.setVisibility(View.INVISIBLE);
        menuVert.setVisibility(View.INVISIBLE);
        layoutCov.setVisibility(View.INVISIBLE);
        btnEndVisu.setVisibility(View.VISIBLE);
        aService.unSelectComponent();
        aService.activeVisualization(true);
        clearListView();
        sw_infos.setChecked(false);
        sw_cov.setChecked(false);
        state = LOCK_STATE;
    }

    @Override
    public View getArrowDown() {
        return arrowDown;
    }

    @Override
    public View getArrowUp() {
        return arrowUp;
    }

    @Override
    public View getArrowLeft() {
        return arrowLeft;
    }

    @Override
    public View getArrowRight() {
        return arrowRight;
    }

    @Override
    public View getCancelCam() {
        return cancelCam;
    }

    public void setVisualisationModeOFF(){
        menuHorz.setVisibility(View.VISIBLE);
        menuVert.setVisibility(View.VISIBLE);
        btnEndVisu.setVisibility(View.INVISIBLE);
        layoutCov.setVisibility(View.INVISIBLE);
        if(aService.planIsEmpty()){
            state = EMPTY_STATE;
            showFloors();
        }else{
            state = SELECTION_STATE;
        }
        aService.activeVisualization(false);
    }

    public void setEndingVisualisationModeON(){
        menuHorz.setVisibility(View.INVISIBLE);
        menuVert.setVisibility(View.INVISIBLE);
        btnEndVisu.setVisibility(View.VISIBLE);
        layoutCov.setVisibility(View.INVISIBLE);
        sw_cov.setChecked(false);
        aService.unSelectComponent();
        aService.activeVisualization(true);
        clearListView();
        sw_infos.setChecked(false);
        state = LOCK_STATE;
        btnEndVisu.setText("Fermer l'éditeur");
        btnEndVisu.setOnClickListener(new EndVisualiationBtnListener());
    }



    private class OnSwitchInClicked implements CompoundButton.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(!isChecked){
                if(!sw_out.isChecked()){
                    sw_out.setChecked(true);
                }
            }
        }
    }
    private class OnSwitchOutClicked implements CompoundButton.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(!isChecked){
                if(!sw_in.isChecked()){
                    sw_in.setChecked(true);
                }
            }
        }
    }

    private class EndVisualiationBtnListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            finish();
            if(fromPlanActivity){
                Intent intent = new Intent(EditorActivity.this, PlanActivity.class);
                intent.putExtra("PLAN",plan);
                startActivity(intent);
            }else {
                Intent intent = new Intent(EditorActivity.this, AccueilActivity.class);
                startActivity(intent);
            }

        }
    }
}
