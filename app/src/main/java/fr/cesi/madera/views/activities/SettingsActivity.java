package fr.cesi.madera.views.activities;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import fr.cesi.madera.R;
import fr.cesi.madera.dao.EntityDao;
import fr.cesi.madera.synchronization.SyncLauncher;

/**
 * Gestion de l'affichage des paramètres de l'application
 */
public class SettingsActivity extends Activity implements IDisplay {

    private Button btnSync;
    private Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parameter);
        bindView();
        bindListener();
        setNavBarTitle();
    }



    @Override
    public void setNavBarTitle() {
        SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.menu_fragment);
        String comName = sharedpreferences.getString("comName", "");
        TextView lbl_com_name = (TextView) currentFragment.getView().findViewById(R.id.lbl_username_navbar);
        lbl_com_name.setText(comName);
        TextView lblTitleNavBar = (TextView) currentFragment.getView().findViewById(R.id.lbl_title_navbar);
        lblTitleNavBar.setText(R.string.lbl_title_param_nav);
    }

    @Override
    public void bindView() {
        btnSync = (Button) findViewById(R.id.btn_sync_file);
        btnBack = (Button) findViewById(R.id.btn_back_parameter);
    }

    @Override
    public void refreshView() {
    }

    @Override
    public void bindListener() {
        btnSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
                String comserial = sharedpreferences.getString("comserial", null);
                SyncLauncher synchronizator = new SyncLauncher(SettingsActivity.this);
                Log.i("madera","Commserial : " + comserial);
                EntityDao.setLockBatch(true);
                synchronizator.startSync(comserial);
                EntityDao.setLockBatch(false);
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent acc_intent = new Intent(SettingsActivity.this,AccueilActivity.class);
                startActivity(acc_intent);
                finish();
            }
        });
    }
}
