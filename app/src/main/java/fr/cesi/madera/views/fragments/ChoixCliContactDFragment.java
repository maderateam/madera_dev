package fr.cesi.madera.views.fragments;

import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import fr.cesi.madera.R;
import fr.cesi.madera.dao.daoClient.ClientDao;
import fr.cesi.madera.entities.Client;
import fr.cesi.madera.entities.adapters.ClientAdapter;
import fr.cesi.madera.views.activities.ListeProjetActivity;

/**
 * Gestion de la fenêtre modale du choix d'ub client
 */
public class ChoixCliContactDFragment extends DialogFragment {

    private ListView listV_cli;
    private ArrayList<Client> clients;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_select_list_cli, container);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        this.getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        bindView();
        refresfView();
        bindListener();
    }

    private void refresfView() {
        ClientDao clientDao = new ClientDao(getActivity());
        clients = clientDao.getAllEntities();
        ClientAdapter adapter = new ClientAdapter(getActivity(), clients);
        listV_cli.setAdapter(adapter);
    }

    private void bindListener() {
        listV_cli.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Client client = (Client) listV_cli.getItemAtPosition(i);
                ((ListeProjetActivity) getActivity()).setClient(client);
                ((ListeProjetActivity) getActivity()).refreshView();
                dismiss();
            }
        });

    }

    private void bindView() {
        listV_cli = (ListView) getView().findViewById(R.id.list_select_cli_proj);
    }

}
