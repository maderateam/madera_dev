package fr.cesi.madera.views.fragments;

/**
 * Created by Doremus on 09/05/2017.
 */

public interface OndialogDismissListener {
    void onDismiss();
}
