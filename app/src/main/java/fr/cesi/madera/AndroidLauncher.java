package fr.cesi.madera;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.apache.commons.lang3.StringUtils;

import java.util.Calendar;

import fr.cesi.madera.dao.EntityDao;
import fr.cesi.madera.entities.Commercial;
import fr.cesi.madera.synchronization.SyncLauncher;
import fr.cesi.madera.views.activities.AccueilActivity;
import fr.cesi.madera.views.fragments.ConnectionDFragment;
import fr.cesi.madera.views.fragments.OndialogDismissListener;
import io.fabric.sdk.android.Fabric;

/**
 * Lance l'application Madera
 * Vérifie si l'utilisateur s'est déjà connecter sur cette tablette
 * et lande la synchronisation des fichier si besoin.
 */
public class AndroidLauncher extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        // Vérification connectivité réseau
        final SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        ConnectivityManager conn = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networtInfo = conn.getActiveNetworkInfo();
        // On check si il est connecté au réseau
        // Si non
        if (networtInfo == null) {
            Log.i("madera","Pas de réseau");
            // Déjà connecté
            if (sharedpreferences.getString("comserial", null) != null
                    && !sharedpreferences.getString("comserial", null).equals("")) {
                Log.i("madera","Déjà connecté");
                // Erreur au premier lancement on rééssaie ou on ferme l'appli
                CharSequence text = "Pas de rédeau, vous ne pourrez pas synchroniser.";
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(this, text, duration);
                toast.show();
                displayAcceuil();

            }else {
                Log.i("madera","Pas connecté");
                // Si pas encore connecté on affiche une erreur
                AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                builder1.setMessage("Erreur de connectivité, veuillez vous connecter au réseau pour le lancement intinial de l'application");
                builder1.setCancelable(false);
                builder1.setPositiveButton(
                        "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                finish();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
                alert11.getWindow().setLayout(600, 400);
            }
        } else {
            Log.i("madera","Ya du réseau");
            // Si présence du réseau
            if (networtInfo.getType() == ConnectivityManager.TYPE_WIFI || networtInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                if (sharedpreferences.getString("comserial", null) == null
                        || sharedpreferences.getString("comserial", null).equals("")) {
                    Log.i("madera", "Connected to WIFI or 3G");
                    displayConnectionFragment();
                    Log.i("madera","After Dialog launch");
                }else{
                    launchSynchroIfNecessary();
                }
            }
        }

    }

    /**
     * Méthode qui prend en charge le lancement de la synchronisation
     * si la dernière a été effectué il y a plus de 24H
     */
    private void launchSynchroIfNecessary() {
        SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        boolean initalLoad = sharedpreferences.getBoolean("initialLoad", false);
        String comserial = sharedpreferences.getString("comserial", null);
        SyncLauncher syncLauncher = new SyncLauncher(this);
        if(!initalLoad){
            Log.i("madera","Need synchro intital Load");
            EntityDao.setLockBatch(true);
            syncLauncher.initialLoad(comserial);
            EntityDao.setLockBatch(false);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putBoolean("initialLoad",true);
            editor.commit();
        }else{
            long dateInMillis = sharedpreferences.getLong("lastSync", -1);
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.HOUR, -24);
            if (dateInMillis == -1 || dateInMillis < cal.getTime().getTime()) {
                Log.i("madera","Need synchro : time : " + dateInMillis);
                syncLauncher.startSync(comserial);
            }else{
                Log.i("madera","No Need synchro : time : " + dateInMillis);
                displayAcceuil();
            }
        }
    }

    /**
     * Affiche l'accueil
     */
    public void displayAcceuil() {
        Intent intent = new Intent(AndroidLauncher.this, AccueilActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Affiche la fenêtre de connection
     */
    public void displayConnectionFragment() {
        FragmentManager fm = getFragmentManager();
        ConnectionDFragment connection = new ConnectionDFragment();
        connection.setDismissListener(new OndialogDismissListener() {
            @Override
            public void onDismiss() {
                final SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
                if(sharedpreferences.getString("comserial", null) != null
                        && !sharedpreferences.getString("comserial", null).equals("")){
                    launchSynchroIfNecessary();
                }
            }
        });
        connection.show(fm, "ConnexionDFragment");
    }

    /**
     * Ecrit la date de synchro ainsi que le nom du commercial dans le storage local de l'appli
     * @param com commercial dont on veut inscrire les infos
     */
    public void writeComToLocal(Commercial com) {
        Log.i("madera","Write com to local");
        if (com != null) {
            SharedPreferences sharedpreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("comserial", com.getComserial());
            editor.putLong("idCom", com.get_id());
            editor.putString("comName", StringUtils.capitalize(com.getFirstname())
                    + " " + StringUtils.capitalize(com.getLastname()));
            editor.commit();
        } else {
            // Si pas de connection affichage d'un popup
            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage("Erreur lors de la connexion");
            builder1.setCancelable(false);
            builder1.setPositiveButton(
                    "Rééssayer",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            displayConnectionFragment();
                            dialog.cancel();
                        }
                    });
            builder1.setNegativeButton(
                    "Annuler",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            AndroidLauncher.this.finish();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
    }

}
